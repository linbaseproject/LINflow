#!/usr/bin/env python3
import argparse
import json
import os
import sys
import warnings
from typing import Union

import numpy as np

warnings.simplefilter(action="ignore", category=FutureWarning)
# adds linflow to the path when run as an uninstalled module (source code)
sys.path.append(os.path.join(os.path.dirname(__file__), "src"))

PRIMITIVE = (str, int, float, bool, None)


def main(argv: list):  # pylint: disable=redefined-outer-name
    """Starting point of running the LINflow module

    Args:
            argv (list, optional): Arguments passed to the LINflow argument parser. Defaults to [].
    """
    # os.chdir(os.path.dirname(os.path.abspath(__file__)))
    import linflow.__main__ as module_main

    add_genomerxiv_arg_parser()
    run_result = module_main.main(argv, console_logging=False)
    # print(json.dumps(run_result, default=lambda o: "<not serializable>"))
    print(json.dumps(todict(run_result), default=lambda o: "null"))
    # print(run_result)


def todict(obj, classkey=None) -> Union[dict, list, int, float, str]:
    if isinstance(obj, dict):
        data = {}
        for k, v in obj.items():
            if hasattr(k, "dtype"):
                data[k.item()] = todict(v, classkey)
            else:
                data[k] = todict(v, classkey)
        return data
    if hasattr(obj, "_ast"):
        return todict(obj._ast())
    if hasattr(obj, "__iter__") and not isinstance(obj, str):
        return [todict(v, classkey) for v in obj]
    if hasattr(obj, "__dict__"):
        data = dict(
            [
                (key, todict(value, classkey))
                for key, value in obj.__dict__.items()
                if not callable(value) and not key.startswith("_")
            ]
        )
        if classkey is not None and hasattr(obj, "__class__"):
            data[classkey] = obj.__class__.__name__
        return data
    # if numpy type convert to python native type
    # if hasattr(obj, "dtype"):
    if isinstance(obj, (np.ndarray, np.generic)):
        return obj.item()
    # else
    return obj


def add_genomerxiv_arg_parser():
    from linflow.arg_parser import main_parser, router_parser, subparser

    scheme_parser = argparse.ArgumentParser(add_help=False)
    scheme_parser.add_argument(
        "-s",
        "--scheme",
        dest="SchemeID",
        type=int,
        default="1",
        help="the schemeID LINs are based. Default: 1",
    )

    lingroup_parser = argparse.ArgumentParser(add_help=False)

    lingroup_parser.add_argument(
        "--clean-lingroups",
        dest="cleanLINgroups",
        nargs="?",
        const=True,
        default=False,
        help="Removes all prefixes currently linked to the query LINgroups",
    )

    prefix_parser = argparse.ArgumentParser(add_help=False)

    prefix_parser.add_argument(
        "--clean-prefixes",
        dest="cleanPrefixes",
        nargs="?",
        const=True,
        default=False,
        help="Removes all LINgroups currently linked to the query Prefixes",
    )
    tax_type_parser = argparse.ArgumentParser(add_help=False)

    tax_type_parser.add_argument(
        "-tt",
        "--tax-type",
        dest="TaxTypeID",
        type=int,
        default=1,
        help="Type of taxonomy to add the LINgroups to e.g. {NCBI:1 GTDB:2 genomeRxiv:3 custom:4}. "
        "This overwrites taxtypeID in file (if applicable).",
    )

    add_prefix_parser = argparse.ArgumentParser(add_help=False)

    add_prefix_parser.add_argument(
        "-p",
        "--prefixes",
        dest="New_prefix",
        required=True,
        help="CSV file containing the Prefixes. required columns: [rankID,taxTypeID,title,prefix(string)] Optional [lingroupID,desc,link,taxonomy(g__name;s__name)]",
    )
    subparser.add_parser(
        "add_prefix",
        parents=[
            main_parser,
            add_prefix_parser,
            tax_type_parser,
            lingroup_parser,
            prefix_parser,
            scheme_parser,
        ],
        help="adds prefixes to existing or new LINgroups",
    )

    add_radii_parser = argparse.ArgumentParser(add_help=False)
    add_radii_parser.add_argument(
        "-r",
        "--radii",
        dest="Radii_file",
        required=True,
        help="TSV file containing the genome assemblyIDs and cluster radii. required columns: [genome,rankID,taxonomy(g__name;s__name),radius] Optional [lingroupID,desc,link,taxtypeID]",
    )

    add_radii_parser.add_argument(
        "--tax-type",
        dest="TaxTypeID",
        type=int,
        default=1,
        help="Type of taxonomy to add the LINgroups to e.g. {NCBI:1 GTDB:2 gRxiv:3 custom:4}. This overwrites taxtypeID in file.",
    )

    subparser.add_parser(
        "lingroup_radii",
        parents=[
            main_parser,
            add_radii_parser,
            lingroup_parser,
            prefix_parser,
            scheme_parser,
        ],
        help="adds LINgroups based on genome LINs and radii of influence",
    )

    prefix_members = argparse.ArgumentParser(add_help=False)
    lingroupID_or_prefix_list = prefix_members.add_mutually_exclusive_group(
        required=True
    )

    lingroupID_or_prefix_list.add_argument(
        "-ps",
        "--prefixes",
        dest="PrefixList",
        type=str,
        help="list of prefixes to find genomesIDs. e.g of two prefixes (1,2;1,3,0;)",
    )
    lingroupID_or_prefix_list.add_argument(
        "-lg",
        "--lingroup",
        dest="LINgroupID",
        type=int,
        help="the lingroupID to find member genomeIDs",
    )
    subparser.add_parser(
        "lingroup_members",
        parents=[main_parser, prefix_members, scheme_parser],
        help="Find all genomes which are members of a LINgroup or prefix",
    )

    add_attributes = argparse.ArgumentParser(add_help=False)
    add_attributes.add_argument(
        "-f",
        "--attribute-file",
        dest="File",
        required=True,
        help="CSV file containing the genomeID aws one column and other column headers as attributeIDs with data",
    )

    add_attributes.add_argument(
        "-d",
        dest="Date_cols",
        nargs="+",
        type=int,
        help="Date columns to parse as a date object (space separated)",
    )

    add_attributes.add_argument(
        "-u",
        dest="Update_cols",
        nargs="+",
        type=int,
        help="Attribute columns to force overwrite in the database if they exists (space separated, 0=all).",
    )

    subparser.add_parser(
        "add_attributes",
        parents=[main_parser, add_attributes],
        help="Add or update genome attributes",
    )

    keyword_submitter = argparse.ArgumentParser(add_help=False)

    keyword_submitter.add_argument(
        "-k",
        "--keyword",
        dest="Keyword",
        type=str,
        help="a keyword existing in any taxonomic level of the LINgroup",
    )

    keyword_submitter.add_argument(
        "-b",
        "--submitter",
        dest="Submitter",
        type=str,
        help="submitter of the Genome/LINgroup",
    )

    find_lingroup = argparse.ArgumentParser(add_help=False)

    # keyword_rank_exgroup = find_lingroup.add_mutually_exclusive_group()

    find_lingroup.add_argument(
        "-tr",
        "--tax-ranks",
        dest="TaxRanks",
        type=str,
        default="",
        help="""string of ranks to search (intersection of multiple) e.g. rankID$rankName$6$pantoea$7$pantoea rwandensis_b$""",
    )

    subparser.add_parser(
        "find_lingroup",
        parents=[
            main_parser,
            find_lingroup,
            keyword_submitter,
            tax_type_parser,
        ],
        help="finds LINgroups based on taxonomic ranks",
    )

    find_genome = argparse.ArgumentParser(add_help=False)
    find_genome.add_argument(
        "-at",
        "--attributes",
        dest="Attributes",
        type=str,
        default="",
        help="""string of attributes to search (intersection of multiple) e.g. $attID$attValue$5$USA$6$alabama$""",
    )
    subparser.add_parser(
        "find_genome",
        parents=[
            main_parser,
            find_genome,
            keyword_submitter,
        ],
        help="finds Genomes based on Attributes",
    )


if __name__ == "__main__":
    main(sys.argv)
