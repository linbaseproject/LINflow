import os.path
import re
from typing import Tuple

from . import logger as m_logger

logger = m_logger.get_logger(__name__)

ACCEPTED_EXTENSIONS = {
    "tar": {
        "extensions": [".tar.bz2", ".tar.xz", ".tar", ".tar.gz", ".tgz"],
        # "function":     co.convert2tgz,
    },
    "raw": {
        "extensions": [".fasta", ".fna", ".fa", ".tmp", ".sig"],
        # "function":     co.compress,
    },
    "gzip": {
        "extensions": [".gz"],
    },
    "sourmash": {"extensions": [".sig"]},
}


def find_extension(source: str, raise_exception=False) -> Tuple[str, str]:
    """Find file type (see ACCEPTED_EXTENSIONS)
    Args:
            source (str): input genome file path

    Returns:
            str: extension group
            str: extension string
    """
    filename = os.path.basename(source)
    for key, value in ACCEPTED_EXTENSIONS.items():
        for extension in value["extensions"]:
            pattern = re.compile(rf".+{extension}$", flags=re.IGNORECASE)
            match = pattern.fullmatch(filename)
            if match:
                return key, extension
    if raise_exception:
        raise BaseException("File Extension not supported '%s'" % source)
    return "", ""


def remove_extension(
    path: str, recursive=True, original="", raise_exception=False, _recursion_depth=0
) -> str:
    """removes the extension of a file

    Args:
            path (str): path to file
            recursive (bool): if extension should be removed recursively
            original (str): used internally should be left None

    Returns:
            str: removes the recognized extensions of the file (recursively if requested)
    """
    if _recursion_depth > 100:
        raise RecursionError(
            "The remove_extension recursion has gotten out of hand (will mess with debugging)"
        )
    no_extension = True
    try:
        if not (ext := find_extension(path, raise_exception))[0]:
            return path
        _, extension = ext
        no_extension = os.path.basename(path[: -1 * len(extension)])
        if no_extension and recursive:
            no_extension = remove_extension(
                no_extension, True, path, raise_exception, _recursion_depth + 1
            )
    except BaseException as exception:
        # raised if the file has no extensions
        if not original:
            if raise_exception:
                raise exception
        return path
    return no_extension or path
