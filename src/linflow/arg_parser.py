"""LINflow argument parser
"""

import argparse
import json
import os

from linflow.logger import get_logger

from .logger import get_logger
from .version import __version__

logger = get_logger(__name__)


version = __version__
preusage_text = f"""
*** LINflow version {__version__} ***
*** Please cite Long, et al. (2021), doi:10.7717/peerj.10906 *** """

# Main top parser (shared commands)
router_parser = argparse.ArgumentParser(
    prog="LINflow",
    description=preusage_text,
    formatter_class=argparse.RawDescriptionHelpFormatter,
)
router_parser.add_argument(
    "-c",
    "--config",
    dest="Config_File",
    help="JSON file containing run configurations (these overwrite command line arguments)",
)
router_parser.add_argument(
    "-v", "--version", action="version", version="%(prog)s " + __version__
)


# Add subparsers (functions) to the router_parser (LINflow)
subparser = router_parser.add_subparsers(dest="mode", help="sub-command help")

# Main top parser (shared commands)
main_parser = argparse.ArgumentParser(
    prog="LINflow",
    add_help=False,
    description=preusage_text,
    formatter_class=argparse.RawDescriptionHelpFormatter,
)
# could add a type like util.valid_workspace to make sure workspace is valid (exclude genome and init modes)
main_parser.add_argument(
    "workspace", type=str, help="name of the project in the workspaces directory"
)
main_parser.add_argument(
    "-q",
    nargs="?",
    const=True,
    default=False,
    dest="Quiet",
    help="only log errors on console.",
)
main_parser.add_argument("-l", "--log", dest="Log_File", help="Logfile name")
main_parser.add_argument(
    "-V",
    nargs="?",
    const=True,
    default=False,
    dest="Verbose",
    help="forces logging ON",
)


def arg_parser(
    argv=None,
):  # pylint: disable=redefined-outer-name,too-many-locals,too-many-statements
    """Argument parser for LINflow

    Returns:
        dict: parameters to assign to function kwargs

    # Made use of https://docs.python.org/3/library/argparse.html#sub-commands
    # and https://docs.python.org/3/library/argparse.html#parents
    """

    # common scheme selection ArgParser for selecting single scheme by ID (shared commands)
    scheme_parser = argparse.ArgumentParser(add_help=False)
    scheme_parser.add_argument(
        "-s",
        "--scheme",
        dest="SchemeID",
        nargs="+",
        type=int,
        default=[8],
        help="the schemeID LINs are based. Default: 1",
    )

    # abstract ArgParser for summarizing results (shared commands)
    summary_parser_base = argparse.ArgumentParser(add_help=False)
    summary_parser_base.add_argument(
        "-f",
        "--filter",
        dest="FilterID",
        type=int,
        default=-1,
        help="filter's ID whose measurements to retrieve. Default: -1",
    )
    summary_parser_base.add_argument(
        "-e",
        "--extended",
        nargs="?",
        const=True,
        default=False,
        dest="Extended",
        help="used to show top final comparisons when saving the summary",
    )

    # ArgParser for summarizing results
    subparser.add_parser(
        "summary",
        parents=[main_parser, scheme_parser, summary_parser_base],
        help="get a summary of the available genomes in the workspace",
    )

    # ArgParser for removing workspace
    subparser.add_parser(
        "remove", parents=[main_parser], help="remove workspace PERMANENTLY!"
    )

    subparser.add_parser(
        "clean", parents=[main_parser], help="cleans unwanted files in workspace"
    )

    # ArgParser for removing workspace
    subparser.add_parser("init", help="initiates workspace", parents=[main_parser])

    # abstract ArgParser for adding genomes (shared commands)
    add_genome_parser_base = argparse.ArgumentParser(add_help=False)
    add_genome_parser_base.add_argument(
        "-i",
        "--input",
        dest="Input_Dir",
        required=True,
        help="the directory containing genomes (gz,tar,fasta) to be added",
    )
    add_genome_parser_base.add_argument(
        "-d",
        "--metadata",
        dest="Metadata",
        required=True,
        help="the metadata corresponding to the genomes. "
        "sample in tests/data/metadata.csv, and save as CSV "
        "(comma separated values) format file.",
    )
    add_genome_parser_base.add_argument(
        "--no-qc",
        dest="No_qc",
        nargs="?",
        const=True,
        default=False,
        help="add genomes even after failed sequence quality control",
    )
    add_genome_parser_base.add_argument(
        "--threads",
        dest="Threads",
        type=int,
        default=3,
        help="Number of max threads used for running concurrent measurements",
    )
    add_genome_parser_base.add_argument(
        "--ani-timeout",
        dest="timeout_delay",
        type=int,
        default=1200,
        help="Max time given for pyANI alignment and analysis to complete (could infinite loop with bad quality or very long genomes)",
    )
    add_genome_parser_base.add_argument(
        "--identify",
        dest="Identify",
        nargs="?",
        const=True,
        default=False,
        help="Only identifies the genomes and does not add them to the database",
    )

    # ArgParser for adding genomes
    subparser.add_parser(
        "genome",
        parents=[main_parser, add_genome_parser_base, scheme_parser],
        help="add a genome to current or existing workspace",
    )

    add_identify_parser = argparse.ArgumentParser(add_help=False)
    add_identify_parser.add_argument(
        "-i",
        "--identify",
        dest="Identify_Genome",
        required=True,
        help="the file containing one genome (gz,tar,fasta) to be identified",
    )

    add_identify_parser.add_argument(
        "--finalize",
        dest="Finalize",
        nargs="?",
        const=True,
        default=False,
        help="Forcefully finalize the scheme measures",
    )

    # ArgParser for adding genomes
    subparser.add_parser(
        "identify",
        parents=[main_parser, add_identify_parser, scheme_parser],
        help="identify a genome with the existing workspace genomes",
    )

    signature_identify_parser = argparse.ArgumentParser(add_help=False)
    signature_identify_parser.add_argument(
        "-i",
        "--input-signature",
        dest="signature",
        required=True,
        help="File containing the signature to be identified",
    )

    signature_identify_parser.add_argument(
        "-o",
        "--output",
        dest="output_File",
        required=False,
        help="Output to save the signature identification results.",
    )

    signature_identify_parser.add_argument(
        "-d",
        "--database",
        dest="database",
        required=True,
        help="Complete path to the SBT (signature database) file.",
    )

    subparser.add_parser(
        "signature",
        # Include main_parser if it contains global arguments you want to reuse
        parents=[main_parser, signature_identify_parser],
        help="Identify a signature based on an SBT database.",
    )

    # abstract ArgParser for building trees (shared commands)
    build_tree_parser_base = argparse.ArgumentParser(add_help=False)
    build_tree_parser_base.add_argument(
        "-r",
        "--rank-depth",
        dest="Rank_depth",
        type=str,
        default="1-20",
        help="genome labels to include in the tree. Number of rank "
        "levels from the lowest rank. All=Default 1-20; genus-species-strain 6,7,20",
    )
    build_tree_parser_base.add_argument(
        "-t",
        "--tax-type",
        dest="Tax_type",
        type=str,
        default="ncbi",
        help="type of taxonomy to include in tree. Default: ncbi",
    )
    build_tree_parser_base.add_argument(
        "--matrix-only",
        nargs="?",
        const=True,
        default=False,
        dest="Matrix_only",
        help="only creates a matrix file and skips the tree building process",
    )
    build_tree_parser_base.add_argument(
        "-c",
        "--scaling-factor",
        dest="Scale",
        type=float,
        default=0.01,
        help="scales the matrix numbers by this amount. Default=0.01",
    )

    # ArgParser for building trees
    subparser.add_parser(
        "tree",
        parents=[main_parser, scheme_parser, build_tree_parser_base],
        help="build a phylogenetic tree",
    )

    # ArgParser for showing available scheme
    show_scheme_parser = subparser.add_parser(
        "show_scheme",
        parents=[main_parser, scheme_parser],
        help="show available schemes",
    )
    show_scheme_parser.add_argument(
        "-o",
        "--output",
        dest="Output",
        help="the filename to save summary of available schemes",
    )
    show_scheme_parser.add_argument(
        "-b",
        "--basic",
        nargs="?",
        const=True,
        default=False,
        dest="Show_Basic",
        help="used to show a simple summary of the basic schemes",
    )
    show_scheme_parser.add_argument(
        "-e",
        "--extended",
        nargs="?",
        const=True,
        default=False,
        dest="Extended",
        help="used to show full details when displaying the basic schemes",
    )

    # ArgParser for adding available schemes
    add_simple_scheme_parser = subparser.add_parser(
        "add_simple_scheme", parents=[main_parser], help="add new simple scheme"
    )
    add_simple_scheme_parser.add_argument(
        "-n",
        "--min",
        dest="Scheme_Min",
        type=float,
        help="minimum %% similarity the scheme measures",
    )
    add_simple_scheme_parser.add_argument(
        "-x",
        "--max",
        dest="Scheme_Max",
        type=float,
        help="maximum %% similarity the scheme measures",
    )
    add_simple_scheme_parser.add_argument(
        "-s",
        "--step",
        dest="Scheme_Step",
        type=float,
        help="the min-max range will be sliced with this distance",
    )
    add_simple_scheme_parser.add_argument(
        "-t",
        "--threshold",
        dest="Thresholds",
        nargs="+",
        help="List of thresholds, float (value) or int (top-k), to switch to next filter",
    )
    add_simple_scheme_parser.add_argument(
        "-p",
        "--positions",
        dest="Positions",
        nargs="+",
        type=float,
        help="define scheme with exact position percentages e.g. 90,95,95.1",
    )
    add_simple_scheme_parser.add_argument(
        "-i",
        "--id",
        dest="Filter_ID",
        nargs="+",
        type=int,
        help="list of filterIDs to add to simple scheme- run in order",
    )
    add_simple_scheme_parser.add_argument(
        "--desc", dest="Scheme_desc", type=str, help="description of the scheme"
    )

    # ArgParser for showing available schemes
    add_complex_scheme_parser = subparser.add_parser(
        "add_complex_scheme",
        parents=[main_parser, scheme_parser],
        help="add new complex scheme",
    )
    add_complex_scheme_parser.add_argument(
        "--descend-levels",
        dest="Descend_Levels",
        nargs="*",
        type=int,
        help="number of positions to descend for each simple scheme block",
    )
    add_complex_scheme_parser.add_argument(
        "-f",
        "--function",
        dest="Scoring_Function",
        type=str,
        help="function used to translate similarity score to LIN; Default: scores2dlin_default",
    )
    add_complex_scheme_parser.add_argument(
        "--desc", dest="Scheme_desc", type=str, help="description of the scheme"
    )

    # ArgParser for running the entire pipeline
    subparser.add_parser(
        "all",
        parents=[
            main_parser,
            add_genome_parser_base,
            summary_parser_base,
            build_tree_parser_base,
            scheme_parser,
        ],
        help="run the entire pipeline: genome->summary->tree",
    )

    # handles python and command line argument parsing
    args = router_parser.parse_args(argv) if argv else argparse.Namespace()

    # if config file exists add it's config to the command line ones (no overwriting) and re-parse the arguments
    argv = []
    if args.__dict__.get("Config_File") is None:
        return args
    config_file = args.get("Config_File", "")
    if os.path.isfile(config_file):
        with open(config_file, "rt", encoding="utf-8") as cnf_file:
            for key, val in json.load(cnf_file).items():
                if key in ["mode", "workspace"]:
                    argv.extend([val])
                    continue
                argv.extend([key])
                if isinstance(val, list):
                    argv.extend(val)
                else:
                    argv.extend([val])

        args = router_parser.parse_args(argv)
        args.Config_File = config_file
    return args


if __name__ == "__main__":
    from sys import argv

    args = arg_parser(argv[1:])
    print(args)
