import json
import logging
import os
from typing import Union, overload

import alive_progress
import numpy as np
import pandas as pd

from . import exceptions, lin, lindb
from . import logger as m_logger
from . import tax_enums, taxonomy, util

logger = m_logger.get_logger(__name__)


class LINgroup:
    id: int = 0
    title: str = ""
    note: str = ""
    schemeID: int = 0
    taxType: str = ""
    members: list[dict[int, int]] = []

    def _set_fields(self, fields: list[str], source: dict, required=False):
        for field in fields:
            if required and field not in source:
                raise AttributeError(
                    "Missing Attribute '%s' from '%s'" % (field, source)
                )
            if field in source and source[field] is not None:
                self.__setattr__(field, source.get(field))

    @overload
    def __init__(
        self,
        db: lindb.LINdb,
        *,
        title: str,
        description: str,
        members: list[lin.LIN],
        schemeID: int = 1,
        taxType="ncbi",
        register=False,
    ): ...

    @overload
    def __init__(self, db: lindb.LINdb, *, lingroupID: int): ...

    def __init__(self, db: lindb.LINdb, *, register=False, **kwargs):
        self.db = db
        if (lingroupID := kwargs.get("lingroupID")) is not None:
            df = db.get_lingroup(lingroupID)
            if df.empty:
                raise NameError("LINgroup with ID %s not registered" % self.id)
            self._set_fields(
                ["title", "note", "members"], df.iloc[0].to_dict(), required=True
            )
            self.id = lingroupID
            self.schemeID = df.iat[0, df.columns.get_loc("schemeSetID")]

        else:
            self._set_fields(
                ["lingroupID", "schemeID", "title", "note", "members"],
                kwargs,
                required=True,
            )

        self.tax_type = kwargs.get("taxType", "ncbi")

        if register:
            self.register()

    @property
    def tax_type(self):
        return self._tax_type

    @tax_type.setter
    def tax_type(self, tax_type: Union[int, str]):
        if isinstance(tax_type, int):
            self._tax_type = tax_enums.TaxType(tax_type).name
        else:
            self._tax_type = tax_enums.TaxType[tax_type].name

    def get_members(self):
        members_df = self.db.get_lingroup_prefixes(self.id)
        self.members = [json.loads(m) for m in members_df["member"]]
        return self.members

    def register(self, update=False):
        self.id = self.db.add_lingroup(
            self.schemeID,
            self.title,
            desc=self.note,
            taxTypeID=tax_enums.TaxType[self.taxType].value,
        )
        if self.members is not None:
            self.db.add_prefix_to_lingroup(
                self.id, self.schemeID, self.members, clean_prefix=(not update)
            )

    def remove(self, raise_exception=True):
        if not self.db.get_lingroup(self.id).empty:
            self.db.remove_lingroups([self.id])
        elif raise_exception:
            raise NameError("LINgroup with ID '%s' not registered" % self.id)

    def is_member(self, target_lin: lin.LIN) -> bool:
        if not target_lin.dlin:
            raise AttributeError("Can not check None LIN membership")
        if target_lin.scheme.id != self.schemeID:
            return False

        for dlin in self.members:
            if util.dlin_is_subset(dlin, target_lin.dlin):
                return True
        return False


def define_lingroups(
    db: lindb.LINdb,
    lingroup_df: pd.DataFrame,
    schemeSetID: int,
    clean_lingroup=False,
    clean_prefix=False,
    **kwargs,
):
    """Circumscribes a LINgroup and connects it to its prefixes

    Args:
        db (LINdb): database object to add the LINgroup to
        lingroup_df (pd.DataFrame): required columns ["rankID", "taxTypeID", "prefix"] Optional ["lingroupID", "taxonomy", "title", "desc", "link"]
        schemeSetID (int): ID of the scheme to define the LINgroup
        clean (bool, optional): whether to remove current prefixes form the currently defend lingroups. Defaults to False.

    """
    # give priority to parameter if not check for argparser input
    clean_lingroup = clean_lingroup or kwargs.get("cleanLINgroups", False)
    clean_prefix = clean_prefix or kwargs.get("cleanPrefixes", False)

    tax_obj = taxonomy.Taxonomy(db, taxid_dict={}, tax_type="ncbi")
    if "rankID" not in lingroup_df and "title" in lingroup_df:
        lingroup_df["title_full"] = lingroup_df["title"]
        lingroup_df["title_tax"] = lingroup_df["title"].apply(
            lambda x__tax: taxonomy.Taxonomy.parse_taxstring(x__tax, sep="$")
        )
        # convert column with {rankID:'title'} to two columns
        lingroup_df["rankID"], lingroup_df["title"] = zip(
            *lingroup_df["title_tax"].apply(
                lambda tax_dict: ([*tax_dict.items()][0] if tax_dict else (np.nan, ""))
            )
        )
        # replace nan values in title with the original title value
        lingroup_df["title"].fillna(
            tax_string_cleaning(lingroup_df, "title_full", return_only=True),
            inplace=True,
        )
    elif "title" in lingroup_df:
        tax_string_cleaning(lingroup_df, "title")
    else:
        # should be unreachable for now
        raise AttributeError("Missing 'title' column in input file")

    # TODO: Change the adds here to be done in one query (batch)
    lingroupIDs = []
    to_remove_lingroups = []
    to_clean_lingroups = []
    to_clean_prefixes = []

    # using lowercase title since that is what is saved in the DB (making it comparable)
    db_lingroup_df = db.all_lingroups(schemeSetID).rename(
        columns={"title": "lower_title"}
    )
    lingroup_df["lower_title"] = lingroup_df["title"].str.strip().str.lower()
    m_df = lingroup_df.reset_index(names="lingroup_index").merge(
        db_lingroup_df,
        on=["lower_title", "rankID", "taxTypeID"],
        how="inner",
        suffixes=("_in", ""),
    )
    dup_title_lingroup_df = (
        m_df[["lingroup_index", "lingroupID", "lower_title", "rankID", "taxTypeID"]]
        .groupby(["lower_title", "rankID", "taxTypeID"])
        .agg(set)
    )
    if not dup_title_lingroup_df.empty:
        # get min lingroupID to keep for each title

        clean_lingroups_df = (
            dup_title_lingroup_df.reset_index()
            .explode("lingroup_index")
            .set_index("lingroup_index")["lingroupID"]
            .apply(min)
        )
        # lingroup:index mapping
        clean_lingroups_dict = (
            clean_lingroups_df.reset_index()
            .set_index("lingroupID")["lingroup_index"]
            .to_dict()
        )
        # lingroups with duplicate titles to remove.
        # clean one per (title,rank,taxtypeID ) and remove the rest that are not NA
        to_remove_lingroups = [
            int(index)
            for index in dup_title_lingroup_df.reset_index(drop=True)["lingroupID"]
            .explode()
            .dropna()
            .values.astype(int)
            if index not in clean_lingroups_dict
        ]

    if clean_lingroup:
        db.remove_prefix_lingroups(
            clean_lingroups_df.dropna().values.astype(int).tolist()
        )
        db.remove_lingroups(to_remove_lingroups)
        logger.info("duplicate LINgroups removed by request %s", to_remove_lingroups)

    # IDs for the new set
    lingroup_df["lingroupID"] = pd.Series([pd.NA] * len(lingroup_df), dtype="Int64")
    """if "assembly" in lingroup_df.columns and "assembly" in m_df.columns:
        # when we use lingroup_radii and we have the representative genome assemblyID
        lingroups_no_id_df = lingroup_df[
            ~lingroup_df["assembly"].isin(m_df["assembly"].unique())
        ]
    else:
        # when we use add_prefix independently"""
    # finds descriptions that did not have a match in the database
    lingroups_no_id_df = lingroup_df[~lingroup_df.index.isin(m_df["lingroup_index"])]
    new_lingroupIDs = db.add_lingroup_batch(lingroups_no_id_df, schemeSetID=schemeSetID)
    # adds the new lingroupIDs to the original dataframe
    if new_lingroupIDs:
        # if any new lingroupIDs were generated
        lingroup_df.loc[lingroups_no_id_df.index.to_numpy(), "lingroupID"] = (
            new_lingroupIDs
        )

    # IDs from the cleaned set to the original dataframe
    if clean_lingroup:
        lingroup_df.loc[clean_lingroups_df.index.to_numpy(), "lingroupID"] = (
            clean_lingroups_df.values
        )

    if clean_prefix:
        db.remove_prefix_lingroups(
            m_df["prefixID"].dropna().astype(int).unique().tolist()
        )

    aa = 0
    """with alive_progress.alive_bar(
        len(lingroup_df),
        title="Scanning for Existing LINgroups",
        disable=not kwargs.get("Verbose", False),
    ) as progress:
        for idx, row in lingroup_df.iterrows():
            lingroupID = 0
            lingroupIDs = []
            if (
                "lingroupID" not in row
                or pd.isna(row["lingroupID"])
                or not row["lingroupID"]
            ):
                # if lingroupID is not given
                # try to find it based on the rank, taxtype, and title
                lingroupIDs = db.find_lingroup(schemeID=schemeSetID, **row.to_dict())
                logger.debug(
                    "Looking for LINgroup %s, found %s",
                    {"schemeID": schemeSetID, **row.to_dict()},
                    lingroupIDs,
                )
                if not lingroupIDs:
                    # if lingroupID is not found,
                    # add a new lingroup and use the new ID for link to prefixes
                    lingroupID = db.add_lingroup(schemeSetID, **row.to_dict())
                    logger.debug(
                        "Added LINgroup %s @ %d",
                        {"schemeID": schemeSetID, **row.to_dict()},
                        lingroupID,
                    )
                else:
                    lingroupID = lingroupIDs[0]
            else:
                # there should only be one LINgroupID here for the same
                # ["rankID", "taxTypeID", "title"] assert fails otherwise
                if isinstance(row["lingroupID"], int):
                    lingroupID = row["lingroupID"]
                elif len(row["lingroupID"]) == 1:
                    lingroupID = int(row["lingroupID"][0])
                lingroupIDs = row["lingroupID"]

            if clean_lingroup:
                if lingroupID:
                    to_clean_lingroups.append(lingroupID)
                # remove duplicate lingroups if they exist
                to_remove_lingroups += lingroupIDs[1:]
            elif not clean_lingroup and len(lingroupIDs) > 1:
                lingroupIDs = lingroupIDs or row["lingroupID"].tolist()
                exceptions.handle_exception(
                    LookupError(
                        "There should not exist two LINgroupIDs '%s' for '%s'"
                        ".skipped. Look into --clean-lingroups to resolve this"
                        % (lingroupIDs, row["title"])
                    ),
                    logger,
                    raise_exception=False,
                    log_level=logging.WARNING,
                )

            lingroupIDs.append(lingroupID)
            progress()  # pylint: disable=not-callable"""

    with alive_progress.alive_bar(
        len(lingroup_df),
        title="Adding New LINgroup Prefixes",
        disable=not kwargs.get("Verbose", False),
    ) as progress:
        for idx, row in lingroup_df.iterrows():
            dlins = [util.slin2dlin(dlin, prefix=True) for dlin in row["prefix"]]
            lingroupID = row["lingroupID"]
            new_prefixIDs = db.add_prefix_to_lingroup(
                lingroupID, schemeSetID, members=dlins, clean_prefix=clean_prefix
            )
            progress()  # pylint: disable=not-callable
            """if lingroup_count % 1000 == 0:
                logger.debug("LINgroup %d from %d", lingroup_count, len(lingroup_df))
            lingroup_count += 1"""

    # adding taxonomy
    if "taxonomy" in lingroup_df.columns:
        # parse the taxonomy string
        # change the string ranks to int ranks based on the Taxonomy class
        lingroup_df["taxonomy"] = lingroup_df["taxonomy"].astype(str)

        tax_string_df = (
            pd.DataFrame(lingroup_df["taxonomy"].apply(tax_obj.taxstring2list).tolist())
            .set_index(lingroup_df.index)
            .explode(column=[0, 1])
            .rename(columns={0: "rankID", 1: "taxon"})
        )
        if "title" in lingroup_df.columns:
            title_taxon_df = lingroup_df[["title"]].rename({"title": "taxon"}, axis=1)
            # duplicate indices between tax_string and title (will only keep title)
            dup_idx = np.unique(
                pd.merge(
                    tax_string_df.reset_index(),
                    title_taxon_df.reset_index(),
                    on=["index"],
                )["index"]
            )
            tax_string_df = pd.concat(
                [tax_string_df, title_taxon_df[title_taxon_df.index.isin(dup_idx)]],
                axis=0,
            )

        # find explicit taxonomic columns (e.g. genus, species, clade, ...)
        # and overwrite the tax_string information
        tax_ranks_names = tax_obj.RankName_ID_table()
        named_rank_df = lingroup_df[
            np.intersect1d(lingroup_df.columns, [*tax_ranks_names])
        ]

        # clean taxonomic columns with rank names where cells from s__XXXXX -> XXXXX
        for col in named_rank_df.columns:
            tax_string_cleaning(lingroup_df.astype(str), col)
            # filling explicit (given rank column) col with taxonomy_string data if explicit is null
            tax_string_rows = tax_string_df["rankID"] == tax_ranks_names[col]
            if tax_string_rows.any():
                # if the explicit column is all NA this will throw an error
                tax_string_df.loc[
                    tax_string_rows,
                    "taxon",
                ] = lingroup_df[
                    col
                ].fillna(tax_string_df.loc[tax_string_rows, "taxon"])

        result_df = lingroup_df.merge(
            tax_string_df,
            how="right",
            right_index=True,
            left_index=True,
        )
        if "rankID_x" in result_df:
            result_df.rename(columns={"rankID_x": "rankID_lingroup"}, inplace=True)
        if "rankID_y" in result_df:
            result_df.rename(columns={"rankID_y": "rankID"}, inplace=True)

        lingroup_tax_add_csv_path = os.path.join(
            util.workspace_dir(db.db_name, create=False),
            "new_lingroups.csv",
        )
        result_df.to_csv(lingroup_tax_add_csv_path)
        logger.info("LINgroup summary saved at %s", lingroup_tax_add_csv_path)
        result_df = result_df[
            result_df.columns.intersection(
                ["lingroupID", "taxTypeID", "rankID", "parent", "taxon", "taxID"]
            )
        ]
        if "taxTypeID" not in result_df:
            result_df["taxTypeID"] = int(
                kwargs.get("TaxtypeID", tax_enums.TaxType.gtdb.value)
            )  # defaults to latest GTDB version

        # overwrite with exclusive rank columns
        # and within-species columns
        # set lowest rank as title or overwrite title by column
        # initialize the taxonomy_df
    else:
        # only gets columns that are usable (could be non-existent)
        result_df = lingroup_df[
            lingroup_df.columns.intersection(
                [*("lingroupID", "taxTypeID", "rankID", "title", "parent", "taxID")]
            )
        ]
        if "title" in result_df.columns:
            result_df = result_df.rename({"title": "taxon"}, axis=1)
    db.update_lingroup_taxonomy(result_df.dropna())
    logger.info("Taxonomy added. %d entries", len(result_df))


def tax_string_cleaning(
    df: pd.DataFrame, target_col: str, return_only=False
) -> pd.Series:
    """clean taxonomic column within a dataframe s__XXXXXXXXXXX -> XXXXXXXXXXX

    Args:
        df (pd.DataFrame): object to be cleaned; Column is overwritten (aka inplace) by default
        target_col (str): columns to test and clean
        return_only (bool) decides whether the column is only returned or overwritten as well

    Returns:
        pd.Series: cleaned column
    """
    row_needs_cleaning = df[target_col].str.contains("^[a-zA-Z]__")
    if np.any(row_needs_cleaning) and not return_only:
        df.loc[row_needs_cleaning, target_col] = df.loc[
            row_needs_cleaning, target_col
        ].apply(lambda x: x[3:])
    return df.loc[row_needs_cleaning, target_col]
