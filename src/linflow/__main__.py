#!/usr/bin/env python3

import logging
import os.path
import sys
import time

from . import arg_parser
from . import logger as m_logger
from . import util, version


def main(argv: list, console_logging=True):  # pylint: disable=redefined-outer-name
    """Starting point of running the LINflow module

    Args:
            argv (list, optional): Arguments passed to the LINflow argument parser. Defaults to [].
    """

    # parse arv and get key-value arguments
    args_dict: dict = arg_parser.arg_parser(argv[1:]).__dict__
    mode = args_dict["mode"]

    work_dir = util.workspace_dir(
        os.path.basename(args_dict.get("workspace", "")), create=True
    )
    logger = m_logger.get_logger("linflow")
    logging_file = "run.log"
    # use the log file name if it is provided otherwise use the default
    if "Log_File" in args_dict and args_dict["Log_File"]:
        logging_file = args_dict["Log_File"]
    m_logger.add_log_file(logger, os.path.join(work_dir, logging_file))
    # if using genomerxiv.py does not print logs below ERROR
    if args_dict["Verbose"] or console_logging:
        m_logger.add_log_console(logger)
    else:
        m_logger.add_log_console(logger, log_level=logging.ERROR)

    # get function from linflow.py called
    imported_linflow_function = getattr(
        __import__("linflow.linflow", fromlist=[mode]), mode
    )
    logger.debug("Run started with parameters: %s", args_dict)
    logger.debug("Running LINflow version %s", version.__version__)
    start_time = time.time()
    result = imported_linflow_function(**args_dict)
    logger.info(
        "Run finished in %s",
        util.elapsed_time(start_time),
    )
    # clean_uuid_dirs()
    # clean_uuid_dirs(work_dir)
    return result


if __name__ == "__main__":
    main(sys.argv)
