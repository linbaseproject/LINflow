from enum import Enum, unique


@unique
class ExtendedEnum(Enum):
    @classmethod
    def has_value(cls, value):
        return value in set(key.value for key in cls)

    @classmethod
    def has_key(cls, key):
        return key in cls.__members__.keys()


class TaxType(ExtendedEnum):
    """Enumeration for types of supported hierarchical taxonomy"""

    ncbi = 1
    gtdb = 2


TaxLevel = ExtendedEnum(  # pylint: disable=too-many-function-args
    "TaxLevel",
    [
        ("superkingdom", 1),
        ("phylum", 2),
        ("class", 3),
        ("order", 4),
        ("family", 5),
        ("genus", 6),
        ("species", 7),
        ("subspecies", 8),
        ("pathovar", 9),
        ("race", 10),
        ("biovar", 11),
        ("clade", 12),
        ("genomospecies", 13),
        ("phylogroup", 14),
        ("phylotype", 15),
        ("serotype", 16),
        ("serovar", 17),
        ("biotype", 18),
        ("sequevar", 19),
        ("strain", 20),
    ],
)
