import json
import logging
import math
import os
import re
import shutil
import sys
import time
from datetime import timedelta
from itertools import islice
from typing import Any, Callable, Optional, Tuple, Union, overload

import Bio.SeqIO
import numpy as np
import numpy.typing as npt
import pandas as pd

from . import compression, exceptions
from . import logger as m_logger

logger = m_logger.get_logger(__name__)

WORKSPACE_FOLDER = os.path.realpath(os.path.join(os.getcwd(), "workspaces"))
SUB_DIRECTORIES = {
    "genomes": {"genomes": "fasta", "signatures": "sig"},
    "general": {"trees": "tree", "tmp": "tmp", "lingroup": "lingroup"},
}
SUBDIRECTORY_LIMIT = 10000


def read_fasta(filepath: str) -> Bio.SeqIO.SeqRecord:
    return Bio.SeqIO.parse(filepath, "fasta")  # type: ignore


def read_scaffold_info(filepath: str):
    scaffold_count = 0
    sequence_length = []
    try:
        for record in Bio.SeqIO.parse(filepath, "fasta"):
            scaffold_count += 1
            sequence_length += [len(record.seq.strip())]
    except Exception as e:
        logger.error("Parsing file failed: %s", filepath)
        raise e
    return scaffold_count, np.array(sequence_length, dtype=int)


def get_lin_cutoff_index(ani: float, cutoff_list: list):
    for index, num in enumerate(cutoff_list):
        if ani < float(num) / 100:
            return index


def lin_tail(lin: npt.NDArray[np.uintc]) -> int:
    result = 0
    for i in range(len(lin), 0):
        if lin[i] != "0":
            return result
        result += 1
    return len(lin) - 1


def __list2uintc(lin: list, allow_int=False):
    lin_lst = np.array([], dtype=np.uintc)
    if lin is None or lin == []:
        logger.debug("Converted LIN was None")
        return lin_lst
    try:
        # intc?
        lin_lst = np.array(lin, dtype=np.integer)
        if allow_int:
            return lin_lst
        if np.min(lin_lst) < 0:
            raise ValueError(lin_lst)
        lin_lst = lin_lst.astype(np.uintc)
    except ValueError:
        logger.debug("LIN contained non-whole number character %s", lin)
    return lin_lst


def lin2dlin(
    lin: list, offset=False, allow_int=False, prefix=False, trailing_zeros=False
) -> dict[int, int]:
    coded_lin = {}
    lin_lst = __list2uintc(lin, allow_int=allow_int)
    if lin_lst is None:
        return coded_lin

    # include (if prefix) or exclude zeros
    non_zero_indices: list[int] = list(np.nonzero(lin_lst)[0])
    coded_lin = {i: int(lin_lst[i]) for i in non_zero_indices}
    if prefix and trailing_zeros:
        # last non-zero index + 1 == first trailing zero index
        first_trailing_zero_index = np.max(non_zero_indices) + 1
        coded_lin = {
            **coded_lin,
            **{
                first_trailing_zero_index + i: 0
                for i in range(len(lin_lst[first_trailing_zero_index:]))
            },
        }
    elif prefix and int(lin[-1]) == 0:
        coded_lin = {**coded_lin, **{len(lin) - 1: 0}}

    # offsets all old lins (first position only) that start with a 0 instead of a 1
    if offset:
        if coded_lin.get(1):
            coded_lin[1] += 1
        else:
            coded_lin[1] = 1

    return coded_lin


def slin2dlin(slin: str, sep=",", **kwargs) -> dict:
    return lin2dlin(slin.split(sep), **kwargs)


@overload
def dlin2lin(
    dlin: dict[int, int], *, scheme_length=20, start=0, **kwargs
) -> np.ndarray: ...


@overload
def dlin2lin(
    dlin: dict[int, int], *, start=0, prefix=False, **kwargs
) -> np.ndarray: ...


def dlin2lin(
    dlin: dict[int, int], scheme_length=0, start=0, prefix=False, **kwargs
) -> np.ndarray:
    assert scheme_length or prefix
    keys = __list2uintc([*dlin])
    vals = __list2uintc([*dlin.values()])
    lin = (
        np.zeros(scheme_length, dtype=np.uintc)
        if scheme_length
        else np.zeros(np.max(keys) + 1, dtype=np.uintc)
    )
    if prefix and not scheme_length:
        scheme_length = sys.maxsize

    if not list(keys) or not list(vals):
        return np.array([])
    for key, val in zip(keys, vals):
        position = key - start
        if position < scheme_length or prefix:
            lin[position] = val
    return lin


def dlin2slin(dlin: dict[int, int], scheme_length=0, start=0, prefix=False) -> str:
    lin = dlin2lin(dlin, scheme_length=scheme_length, start=start, prefix=prefix)
    if len(lin) < 1:
        return ""
    return ",".join(lin.astype(str))


def lin2dlin_infile(
    infile,
    outfile,
    *,
    col_sep=",",
    lin_sep=",",
    lin_col=0,
    skiprows=0,
    offset=False,
    prefix=False,
):
    if infile == outfile:
        logger.error("Input and output files can not be the same")
        return
    with open(infile, "r") as i:
        with open(outfile, "w+") as o:
            o.write("[\n")
            for line in islice(i, skiprows, None):
                lin = line.split(col_sep)[lin_col]
                o.write(
                    json.dumps(slin2dlin(lin, offset=offset, prefix=prefix)) + ",\n"
                )
            o.write("]")


# Input format ->Genome_ID ParentGenome LIN ...
def lin2slin_sql(
    infile,
    outfile,
    out_sep=",",
    in_sep="\t",
    skiprows=0,
    lin_col=3,
    prefix=False,
    offset=False,
    out_header=True,
):
    if infile == outfile:
        logger.error("Input and output files can not be the same")
        return
    with open(infile, "r") as i:
        with open(outfile, "w+") as o:
            if out_header:
                o.write(f"{out_sep.join(['lid', 'parent', 'idx', 'val'])}\n")
            # o.write("{}\n".format(out_sep.join(["0", "NULL", "NULL", "NULL"])))
            for line in islice(i, skiprows, None):
                line_lst = line.split(in_sep)
                gid = line_lst[0]
                parent = line_lst[1]
                lin = line_lst[lin_col]
                dlin = slin2dlin(lin, offset=offset, prefix=prefix)
                keys = np.array([*dlin.keys()], dtype=np.uintc)
                last_idx = np.max(keys)
                if last_idx == 1:
                    parent = str(0)
                o.write(
                    f"{out_sep.join([gid, parent, str(last_idx), str(dlin[str(last_idx)])])}\n"
                )


def parse_lin2sql(
    history: dict,
    missing: list,
    line_lst: list,
    offset=False,
    lin_col=3,
    prefix=False,
    missing_parent=None,
):
    import copy

    gid = line_lst[0]
    lin = line_lst[lin_col]
    dlin = slin2dlin(lin, offset=offset, prefix=prefix)
    history[json.dumps(dlin)] = gid

    # find parent node ID
    keys = np.array([*dlin.keys()], dtype=np.uintc)
    last_idx = np.max(keys)
    parent_dlin = copy.deepcopy(dlin)
    last_node = parent_dlin.pop(str(last_idx))

    # Nodes with missing parents
    try:
        parent = history[json.dumps(parent_dlin)] if parent_dlin else "0"
    except KeyError:
        missing += [line_lst]
        if missing_parent:
            parent = missing_parent
        return None
    lin = line_lst[lin_col]
    val = dlin[str(last_idx)]
    return [gid, parent, str(last_idx), str(val)]


# Input format ->Genome_ID ParentGenome LIN ...
def lin2dlin_sql(
    infile,
    outfile,
    out_sep=",",
    in_sep="\t",
    skiprows=0,
    lin_col=3,
    prefix=False,
    offset=False,
    out_header=True,
):
    history = {}
    missing = line_lst = []
    if infile == outfile:
        logger.error("Input and output files can not be the same")
        return

    with open(infile, "r") as i:
        with open(outfile, "w+") as o:
            if out_header:
                o.write(f'{out_sep.join(["lid", "parent", "idx", "val"])}\n')
            # o.write("{}\n".format(out_sep.join(["0", "NULL", "NULL", "NULL"])))
            for line in islice(i, skiprows, None):
                line_lst = line.split(in_sep)
                parsed_data = parse_lin2sql(
                    history, missing, line_lst, offset=True, prefix=prefix
                )
                if parsed_data:
                    o.write(f"{out_sep.join(parsed_data)}\n")

            # Handling disordered lins (low lid) -> (high lid)
            missing2 = []
            for row in missing:
                parsed_data = parse_lin2sql(
                    history,
                    missing2,
                    line_lst,
                    offset=True,
                    missing_parent="-1",
                    prefix=prefix,
                )
                if parsed_data:
                    o.write(f"{out_sep.join(parsed_data)}\n")


"""
lin2dlin_sql("~/idea_projects/linbase/_neo4j2/try3/lins.tsv",
    "~/idea_projects/linbase/_neo4j2/try3/dlins2.csv",
    lin_col=3, skiprows=1, offset=True, out_sep=",")
"""


def nodes2dlin(nodes: pd.DataFrame, prefix=False) -> dict:
    """converts graph nodes to dict-formatted LINs

    Args:
            nodes (DataFrame): table in the format columns=[genomeID,parent,idx,lastValue]
            prefix (bool): if the conversion is for a prefix, the last position is included regardless of being zero

    Returns:
            Optional[dict]: LIN of non-zero positions with format {idx:val,...}
    """
    if nodes.empty:
        return {}
    dlin = {}
    last_index = max(nodes["idx"])
    for _, row in nodes.iterrows():
        if row["lastValue"] > 0 or (prefix and row["idx"] == last_index):
            dlin = {row["idx"]: row["lastValue"], **dlin}
    return dlin


def sqldf2dlin(
    sqldf: pd.DataFrame, parentID=1, parent_dlin: Optional[dict[int, int]] = None
) -> dict[int, dict[int, int]]:
    if parent_dlin is None:
        parent_dlin = parent_dlin or {}
    if sqldf.empty or sqldf is None:
        return {}
    if parentID < 2:
        current_level = sqldf[sqldf["idx"] == 0]
    else:
        current_level = sqldf[sqldf["parent"] == parentID]
    if sqldf.empty:
        return {}

    dlins = {}
    for _, lin in current_level.iterrows():
        self_dlin = {lin["idx"]: lin["lastValue"]}
        self_dlin.update(parent_dlin)
        dlins[lin["genomeID"]] = self_dlin
        dlins.update(sqldf2dlin(sqldf, lin["genomeID"], self_dlin))
    return dlins


def dlin_is_subset(parent_dlin: dict[int, int], child_dlin: dict[int, int]) -> bool:
    """check if the child dlin is part of the parent's children

    Args:
            parent_dlin (dict): the parent's LIN in dict format {idx:value, ...}
            child_dlin (dict): the supposed child's LIN in dict format {idx:value, ...}

    Returns:
            bool: if the child is part of the parent's children
    """
    if len(child_dlin) < len(parent_dlin) - 1:
        return False

    max_parent_key = max(parent_dlin.keys())
    filtered_cdlin = [e for e in child_dlin.keys() if e <= max_parent_key]
    distinct_idx = np.setdiff1d(filtered_cdlin, list(parent_dlin))
    # this takes care of the last zero in the LINgroup definition
    # {1:2, 3:2, 5:1}, {1:2, 3:2, 5:1, 7:0} -> True
    if not (len(distinct_idx) == 0 or list(distinct_idx) == [max_parent_key]):
        return False

    for idx, val in parent_dlin.items():
        # if value exists in child (match)
        if child_dlin.get(idx) == val:
            continue
        # if val not in child but parent has zero (is prefix)
        elif idx not in child_dlin and val == 0:
            continue
        else:
            return False
    return True


def timeout_function(
    target: Callable, args: tuple, name: str, timeout=1200, attempts=3, **kwargs
):
    """Runs a function and waits for a maximum timeout for it to return. If the function exceeds the time,
    it forcefully ends and retries the function for a number of attempts.

    Args:
        target (Callable): Function to run with time constraint.
        args (tuple): Arguments to pass to the function.
        name (str): Name of the function for logging purposes.
        timeout (int, optional): Maximum amount of time for the function to return. Defaults to 1200.
        attempts (int, optional): Number of times failed attempts (non-zero result or timeout) will be rerun before failing the run. Defaults to 3.

    Raises:
        TimeoutError: If the max number of attempts is exceeded.
        TypeError: If the function returns None.

    Returns:
        Any: The result of the target function."""

    import multiprocessing as mp

    attempt = 0
    cum_exit_val = None

    def wrapper(queue, event, *args, **kwargs):
        result = target(*args, **kwargs)
        queue.put(result)
        event.set()

    while attempt < attempts:
        cum_exit_val = None
        queue = mp.Queue()
        event = mp.Event()
        process = mp.Process(target=wrapper, args=(queue, event) + args, kwargs=kwargs)
        process.start()
        process.join(timeout)
        if process.is_alive():
            logger.warning("%s timeout attempt %s failed", name, attempt + 1)
            process.terminate()
            process.join()
            attempt += 1
        elif event.is_set():
            cum_exit_val = queue.get()
            break
        else:
            attempt += 1

    if attempt >= attempts:
        raise TimeoutError("%s failed after %s attempts" % (name, attempts))
    if cum_exit_val is None:
        raise TypeError("%s returned None" % name)
    return cum_exit_val


def function2str(func: Callable):
    return ".".join([func.__module__, func.__name__])


def import_function(module: str, function: str) -> Callable:
    """dynamically import a function from a module both by string name

    Args:
            module (str): module name
            function (str): function name

    Returns:
            Callable: function pointer (handler)
    """
    module = __import__(module, fromlist=[function])
    return getattr(module, function)


def get_filepath(
    workspace: str, genomeID: Union[int, str], filetype: str = "fasta", create=True
) -> str:
    """get filepath of a representation of a genome by ID

    Args:
            workspace (str): name of workspace the genome is in
            genomeID (str,int): the ID of the genome the representation belongs to
            filetype (str, optional): type of file requested. Defaults to "fasta".
            create (bool, optional): whether the directory path has to be created if missing. Defaults to True.

    Returns:
            str: path to file
    """

    basedir = os.path.join(os.getcwd(), "workspaces", workspace)
    if str(genomeID).isnumeric():
        split_subdir = f"{int(genomeID) // SUBDIRECTORY_LIMIT}"
    else:
        split_subdir = "00"

    if filetype == "fasta":
        file_extension = "gz"
    elif filetype == "sbt":
        file_extension = "sbt.zip"
    elif filetype == "sig":
        file_extension = "sig"
    else:
        file_extension = filetype

    file_dir = os.path.join(
        basedir, filetype, split_subdir, f"{genomeID}.{file_extension}"
    )
    if create:
        os.makedirs(os.path.join(basedir, filetype, split_subdir), exist_ok=True)

    return file_dir


# in formats 1-20 and 19,20
def parse_range_sequence(sequence: str) -> list[int]:
    """parses dash(-) comma(,) ranges to a list

    Args:
            sequence (str): sequence of numbers separated by dashes(range) or commas(elements)- all inclusive

    Raises:
            KeyError: if character found other than numbers, dashes and commas
            KeyError: if separators found other than dashes and commas

    Returns:
            list: unique sorted list of numbers
    """
    parsed = []
    sequence = sequence.strip(" ,-")
    if len(sequence) < 1:
        return parsed

    try:
        nums = np.array(re.split(r"[,|\-]", sequence), dtype=int)
        if len(nums) == 1:
            return nums.tolist()
    except TypeError as e:
        exceptions.handle_exception(
            e,
            logger,
            f"Unexpected character found in rank sequence: '{sequence}'. Allowed [- , numbers].",
        )
        raise e
    sep = [e for e in re.split(r"\d", sequence) if len(e) > 0]
    for idx, separator in enumerate(sep):
        if separator == "-":
            parsed += list(range(nums[idx], nums[idx + 1] + 1))
        elif separator == ",":
            parsed += [nums[idx], nums[idx + 1]]
        else:
            exceptions.handle_exception(
                KeyError(
                    "Unexpected separator detected '%s'. Allowed [-,]." % separator
                ),
                logger,
            )
    return np.unique(parsed).tolist()


def list2sql(elements: list, filler=-1) -> tuple:
    """converts list to sql usable tuple

    Args:
            elements (list): list to convert
            filler (Any): element to add when only one element

    Returns:
            tuple: sql usable tuple
    """
    if (genome_count := len(elements)) < 1:
        return tuple()
    elif genome_count == 1:
        elements = [filler, *elements]
    return tuple(elements)


def workspace_dir(
    workspace: str, subdir: Optional[str] = None, change_directory=False, create=False
) -> str:
    """builds the directory path string for the workspace

    Args:
            workspace (str): workspace name
            subdir (str, optional): path to the directory under the workspace. Defaults to None (workspace path).
            change_directory (bool, optional): whether the directory should be changed to. Defaults to False.
            create (bool, optional): whether the directory should be created if not existing. Defaults to False.

    Raises:
            FileNotFoundError: raised when the requested subdirectory is not found and not created

    Returns:
            str: path to the workspace or any of its subdirectories
    """
    if subdir is None:
        work_dir = os.path.join(WORKSPACE_FOLDER, workspace)
    elif subdir in SUB_DIRECTORIES["genomes"]:
        work_dir = os.path.join(
            WORKSPACE_FOLDER, workspace, SUB_DIRECTORIES["genomes"][subdir]
        )
    elif subdir in SUB_DIRECTORIES["general"]:
        work_dir = os.path.join(
            WORKSPACE_FOLDER, workspace, SUB_DIRECTORIES["general"][subdir]
        )
    else:
        raise FileNotFoundError(
            "subdirectory '%s' not found in allowed list '%s'. See SUB_DIRECTORIES in util.py"
            % (subdir, workspace)
        )
    if create:
        os.makedirs(work_dir, exist_ok=True)
    if change_directory:
        os.chdir(work_dir)

    return work_dir


def clean_uuid_dirs(base_dir=os.getcwd()):
    """cleans directories with uuid names (intermediate linflow temp folders)

    Args:
            base_dir (str, optional): directory to search/clean for uuid folders. Defaults to os.getcwd().
    """
    from glob import glob

    directories = glob(os.path.join(base_dir, "*-*-*-*-*/"))
    pattern = re.compile(r"[\w]{8}-[\w]{4}-[\w]{4}-[\w]{4}-[\w]{12}")
    for dir in directories:
        if (
            re.match(pattern, os.path.basename(os.path.dirname(directories[0])))
            is not None
        ):
            rmtree2(dir)


def duplicate_database(current_name: str, new_name: str, raise_exception=True) -> bool:
    from os import popen

    try:
        stream = popen(f"mysqldump {current_name} | mysql {new_name}")
        output = stream.read()
        return True
    except BaseException as e:
        exceptions.handle_exception(
            e,
            logger,
            f"DATABASE DUPLICATION ERROR {current_name} to {new_name}",
            raise_exception=raise_exception,
        )
        return False


def get_pytest_workspace(workspace="pytest_linflow", extra: Optional[list[Any]] = None):
    from inspect import stack

    extra = extra or []
    extra = [str(e) for e in extra]
    calling_function_name = stack()[1][3]
    return "_".join([workspace, *extra, *calling_function_name.split("_")[1:]])


def get_fasta(workspace: str, tmp_id: Union[str, int]) -> str:
    if not tmp_id:
        raise FileNotFoundError("No file ID was given")
    # looking for fasta in tmp by id
    tmp_file_location = get_filepath(workspace, tmp_id, "tmp")
    if not os.path.isfile(tmp_file_location):
        gz_file = get_filepath(workspace, tmp_id, "fasta")
        compression.uncompress(gz_file, tmp_file_location)
    return tmp_file_location


class Suppressor(object):
    def __enter__(self):
        self.stdout = sys.stdout
        sys.stdout = self

    def __exit__(self, type, value, traceback):
        sys.stdout = self.stdout
        if type is not None:
            logger.exception(value)

    def write(self, x):
        pass


def downgrade_info_logs(record):
    if record.levelno == logging.INFO:
        record.levelno = logging.DEBUG
        record.levelname = "DEBUG"
    return record


def elapsed_time(start_time: float) -> str:
    # return time.strftime("%d-%H:%M:%S.%f", time.gmtime(time.time() - start_time))
    return timedelta(seconds=time.time() - start_time).__str__()


def rmtree2(path, ignore_errors=False):
    """Remove directory at specified path.
    Adapted from https://github.com/boegel/easybuild-framework/blob/develop/easybuild/tools/filetools.py
    """

    if os.path.exists(path):
        ok = False
        errors = []
        # Try multiple times to cater for temporary failures on e.g. NFS mounted paths
        max_attempts = 3
        for i in range(0, max_attempts):
            try:
                shutil.rmtree(path, ignore_errors)
                ok = True
                break
            except OSError as err:
                logger.debug(
                    "Failed to remove path %s with shutil.rmtree at attempt %d: %s",
                    path,
                    i,
                    err,
                )
                errors.append(err)
                time.sleep(2)
                # make sure write permissions are enabled on entire directory
                # shutil.chown(path, os.getlogin())
        if ok:
            logger.debug("Path %s successfully removed.", path)
        else:
            logger.warning(
                "Failed to remove directory %s even after %d attempts.\nReasons: %s",
                path,
                max_attempts,
                errors,
            )


def attributes2Table(
    genome_att_table: pd.DataFrame,
    usable_attribute_table: pd.DataFrame,
    integer_row_ids=False,
    rowID_col_name="genomeID",
) -> pd.DataFrame:
    """filters and creates a row-wise attribute table with IDs from a reference secondary table

    Args:
        genome_att_table (pd.DataFrame): the primary attribute table with the data attributes
        usable_attribute_table (pd.DataFrame): the reference table where the attribute IDs will be sourced from (should only have two columns "name" and "id")

    Returns:
        pd.DataFrame: a row-wise attribute table with attributes only in the reference table with columns=["genomeID", "attID", "val"]
    """
    assert (
        len(usable_attribute_table.columns) == 2
        and "id" in usable_attribute_table.columns
        and "name" in usable_attribute_table.columns
    )
    usable_attributes = {
        e["name"].lower(): e["id"] for _, e in usable_attribute_table.iterrows()
    }
    genome_att_table.columns = genome_att_table.columns.str.strip().str.lower()

    extra_cols = [
        col
        for col in genome_att_table.columns
        if col not in usable_attributes and col != rowID_col_name
    ]
    genome_att_table.drop(columns=extra_cols, inplace=True)

    data: list[tuple] = []
    for _, genome in genome_att_table.iterrows():
        gid = genome[rowID_col_name]
        for attribute_name in genome.drop([rowID_col_name]).keys():
            data += [
                (
                    gid,
                    usable_attributes[attribute_name],
                    genome[attribute_name],
                )
            ]

    result = pd.DataFrame(data, columns=[rowID_col_name, "attID", "val"])
    if integer_row_ids:
        return result.astype({rowID_col_name: int})
    return result


def extract_assemblyID(filepath: str) -> str:
    # 'GCA_000017245.1_genomic.fna.gz' -> '000017245.1'
    match = re.search(r"GC[A|F]_(\d{5,15})\.\d", filepath)
    if not match:
        return ""
    span = match.span()
    # skipping GC?_
    return filepath[span[0] + 4 : span[1]]


def sort_lins(lins: list, return_argsort=False) -> Tuple[list[list[int]], list[int]]:
    df = pd.DataFrame(lins, dtype=pd.Int64Dtype())
    result = df.sort_values(df.columns.to_list()).reset_index()
    argsort: list[int] = result["index"].to_list()
    # deletes <NA> values where lins/prefixes are shorter than the others to keep their original length
    sorted_lins: list[list[int]] = [
        [digit for digit in lin if not pd.isna(digit)]
        for lin in result.drop(columns=["index"]).to_numpy()
    ]

    if return_argsort:
        return sorted_lins, argsort
    return sorted_lins, []


def sort_dlins(
    dlins: list, return_argsort=False, **kwargs
) -> Tuple[list[dict[int, int]], list[int]]:
    # [{0:76,1:15,2:6},{0:76,1:5,2:5},{0:76,1:5,3:4},{0:177}]
    lin_list = [dlin2lin(dlin, **kwargs).tolist() for dlin in dlins]
    sorted_lins, arg_sorted_lins = sort_lins(lin_list, return_argsort)
    return [lin2dlin(lin, **kwargs) for lin in sorted_lins], arg_sorted_lins


def rename_complete_file(source: str, destination: str, timeout=901):
    start_t = time.time()
    while (time.time() - start_t) < timeout:
        try:
            # checks if file exists and complete (can be written to)
            file = open(source, "a")
            file.close()
            os.rename(source, destination)
            return
        except Exception as e:
            time.sleep(2)
    raise FileNotFoundError("File '%s' could not be renamed" % source)


def not_none_kwargs(**kwargs) -> dict[str, Any]:
    """creates a kwargs dict with non-None from using the given kwargs

    Args:
        kwarg_list (list): non-None kwargs list for another function
    """
    return {k: v for k, v in kwargs.items() if v is not None}


def valid_file(path: str) -> str:
    for _ in range(10):
        if os.path.isfile(path):
            return path
        time.sleep(1)
    raise FileNotFoundError("Not a valid file '%s'" % path)


def valid_dir(path: str) -> str:
    if os.path.isdir(path):
        return path
    else:
        raise NotADirectoryError("Not a valid directory '%s'" % path)


def valid_workspace(path: str) -> str:
    current_workspace_dir = os.path.join(os.getcwd(), path)
    if os.path.isdir(current_workspace_dir):
        return path
    raise NotADirectoryError("Not a valid directory '%s'" % current_workspace_dir)


def dataframe_mysql_split(
    data_df: pd.DataFrame, row_count: Optional[int] = None, data_in_subquery=False
) -> Tuple[int, int]:
    """Splits a dataframe to manageable chunks for MySQL

    Args:
        data_df (pd.DataFrame): The dataframe where each row represents a row for add/update/delete
        row_count (int, optional): The number of rows to expect in case they are to be grouped. Defaults to None.
        data_in_subquery (bool, optional): Is the bulk of data in subqueries? if so the chuck file is smaller. Will be based on thread_stack instead of max_package_limit. Defaults to False.

    Returns:
        splits (int): Number of splits
        step (int): The step size to achieve the splits
    """
    row_count = row_count or len(
        [f"{row}"[1:-1] for row in data_df.itertuples(index=False, name=None)]
    )
    splits = 0
    step = 0
    max_chunk_size = 1
    if data_df.empty:
        return splits, step

    if data_in_subquery:
        # splits queries to chunks to not exceed MySQL thread_stack@266720 byte stack
        max_chunk_size = 20000
    else:
        # splits queries to <14MB to not exceed MySQL max_package_limit@16MB
        max_chunk_size = 1048576 * 14

    splits = math.ceil(sys.getsizeof(data_df) / max_chunk_size)
    step = math.ceil(row_count / splits)

    return splits, step


def clean_workspace_name(workspace: str) -> str:
    if not workspace:
        return "NO_WORKSPACE"
    workspace_name = os.path.basename(workspace.rstrip("/"))
    return workspace_name
