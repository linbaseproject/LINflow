import copy
import os
import shutil
from collections.abc import Iterable
from typing import Any, Optional, Union, overload
from uuid import uuid4

import numpy as np
import numpy.typing as npt

from . import compression, exceptions, extensions, linflow
from . import logger as m_logger
from . import util
from .lin import LIN
from .lindb import LINdb
from .scheme import SchemeSet
from .taxonomy import Taxonomy

logger = m_logger.get_logger(__name__)


class Genome:
    """Class to keep the Genome as an object"""

    _scaffold_count: int = 0
    _filepath: str = ""
    _id: int = 0
    _scaffold_sizes: npt.NDArray[np.uintc] = np.array([], dtype=np.uintc)
    _sequence_length: int = 0
    _hash: str = ""
    is_duplicate = False
    # {schemeSetID:{idx:lastValue}}
    _lins: dict[int, LIN] = {}

    @overload
    def __init__(self, db: LINdb, *, genome_id: int): ...

    @overload
    def __init__(
        self,
        db: LINdb,
        *,
        filepath: str,
        lins: Optional[dict[int, dict[int, int]]] = None,
    ): ...

    def __init__(self, db: LINdb, **kwargs):
        self.db = db
        self.lins: dict[int, dict[int, int]] = kwargs.get("lins", {}) or {}
        if (genome_id := kwargs.get("genome_id")) is not None:
            self._id = genome_id
            genome_dict = db.get_genome(genome_id)
            filepath = genome_dict.get("filepath")
            if not genome_dict.empty:
                kwargs.update(
                    {
                        str(k).lower(): v
                        for k, v in genome_dict.iloc[0].to_dict().items()
                    }
                )
            else:
                exceptions.handle_exception(
                    FileNotFoundError(
                        "The genome with ID %s was not found" % genome_id
                    ),
                    logger,
                )

        # TODO: is a valid genome file
        elif (filepath := kwargs.get("filepath")) is None or not os.path.isfile(
            filepath
        ):
            exceptions.handle_exception(
                FileNotFoundError("Genome file not found %s" % filepath), logger
            )

        # lazy loaded properties
        for field in ["filepath", "lins"]:
            if (val := kwargs.get(field)) is not None:
                self.__setattr__(f"_{field}", val)

        self.filename = os.path.basename(self._filepath)
        if (tax := kwargs.get("taxonomy")) is not None:
            self.set_taxonomy(tax, kwargs.get("tax_type", "ncbi"))

    def __eq__(self, other):
        if not isinstance(other, Genome):
            raise TypeError(
                "Comparing equality between different types '%s' and '%s'"
                % (type(self), type(other))
            )
        if self.id and self.id == other.id:
            return True
        elif self.filepath and compression.uniform_hash_file(
            self.filepath
        ) == compression.uniform_hash_file(
            util.get_filepath(self.db.db_name, other.id, create=False)
        ):
            return True
        return False

    def to_dict(self) -> dict[str, Any]:
        result = copy.copy(self.__dict__)
        if not isinstance(self.id, int):
            return result
        if "db" in result:
            result["db"] = result["db"].db_name
        available_schemesIDs = self.db.get_genome_lin_schemes(self.id, visibleOnly=True)
        missing_lin_schemes = np.setdiff1d(available_schemesIDs, [*self._lins])
        for schemeID in missing_lin_schemes:
            self.assign_lin(schemeID)
        if "_lins" not in result:
            result["_lins"] = {}
        for schemeID in set([*available_schemesIDs, *self._lins]):
            schemeID = int(schemeID)
            lins_dict = (
                self._lins[schemeID].__dict__
                if not isinstance(self._lins[schemeID], dict)
                else self._lins[schemeID]
            )
            tmp = copy.copy(lins_dict)
            result["_lins"].pop(schemeID, None)
            result["_lins"][int(schemeID)] = tmp
            schemeID = int(schemeID)
            for idx, val in result["_lins"][schemeID]["_dlin"].items():
                result["_lins"][schemeID]["_dlin"][int(idx)] = int(val)
            # could recursively build dicts for linflow module classes (not needed yet)
            # check result[genomeID]["_lins"][schemeID]["scheme"].__module__ starts with "linflow."
            if not isinstance(result["_lins"][schemeID]["scheme"], dict):
                result["_lins"][schemeID]["scheme"] = result["_lins"][schemeID][
                    "scheme"
                ].__dict__
        if result["_scaffold_count"] > 50:
            result.pop("_scaffold_sizes")
        result["type"] = "genome"
        return result

    @property
    def filepath(self) -> str:
        if self._filepath:
            return self._filepath
        elif self._id:
            path_df = self.db.get_genome_path(self._id)
            if not path_df.empty:
                self._filepath = path_df.iat[0, path_df.columns.get_loc("filepath")]
                self.filename = os.path.basename(self._filepath)
                return self._filepath
        return ""

    @filepath.setter
    def filepath(self, filepath: str):
        self._filepath = filepath

    @property
    def id(self) -> int:
        if not self._id:
            if self.filepath:
                df = self.db.get_genomeID(self.filepath)
            else:
                return self._id
            if df.empty:
                self._id = 0
            else:
                self._id = int(df.iat[0, df.columns.get_loc("genomeID")])
                self._filepath = df.iat[0, df.columns.get_loc("filePath")]
                self._scaffold_count = df.iat[0, df.columns.get_loc("scaffolds")]
                self._hash = df.iat[0, df.columns.get_loc("fileHash")]
        return self._id

    @id.setter
    def id(self, genome_id: int):
        self._id = genome_id

    def get_neighbor(self, schemeSetID=1):
        return self.db.get_neighbor(self.id, schemeSetID)

    def get_lin(
        self, target_scheme: Union[int, SchemeSet] = 0, assign=True
    ) -> dict[int, dict[int, int]]:
        # get the ID whether the SchemeSet object was given or just its ID
        target_scheme_id: int = (
            target_scheme.id if isinstance(target_scheme, SchemeSet) else target_scheme
        )
        dlin_dict: dict[int, dict[int, int]] = {}
        if not self._lins and (
            stored_lin := self.db.get_LIN(self.id, target_scheme_id)
        ):
            # if no LINs are defined but we have them in the database
            return {target_scheme_id: stored_lin}
        elif self._lins and isinstance(self._lins, Iterable):
            # if LINs are defined handle by type [dict or LIN]
            first_key, first_value = next(iter(self._lins.items()))  # type: ignore
            if isinstance(first_value, dict):
                first_value: dict[int, int]  # type: ignore
                dlin_dict = {
                    schemeID: lin_dict["_dlin"]  # type: ignore
                    for schemeID, lin_dict in self._lins.items()
                }
            elif isinstance(first_value, LIN):
                first_value: LIN
                dlin_dict = {
                    schemeID: using_lin.dlin
                    for schemeID, using_lin in self._lins.items()
                }
            if target_scheme_id and target_scheme_id in dlin_dict:
                # if a certain scheme's lin was requested return only that one
                return {target_scheme_id: dlin_dict[target_scheme_id]}
        elif target_scheme and assign:
            # if all fails and we are allowed to assign a new LIN do so
            return {target_scheme_id: self.assign_lin(target_scheme).dlin}
        if not target_scheme:
            # if no specific scheme was requested return all
            return dlin_dict
        # this only happens when we were looking for one and didn't find one and
        # were not allowed to assign one either
        return {}

    @property
    def scaffold_count(self):
        if not self._scaffold_count:
            try:
                (
                    self._scaffold_count,
                    self._scaffold_sizes,
                ) = util.read_scaffold_info(self.filepath)
            except Exception:
                self.fill_sequence_properties()
        return self._scaffold_count

    @property
    def scaffold_sizes(self):
        if not len(self._scaffold_sizes):
            if not self.scaffold_count:
                self.fill_sequence_properties()
        return self._scaffold_sizes

    @property
    def sequence_length(self):
        if not self._sequence_length:
            try:
                self._sequence_length = int(np.sum(self.scaffold_sizes))
            except Exception:
                self.fill_sequence_properties()
        return self._sequence_length

    @property
    def hash(self):
        if not self._hash:
            try:
                self._hash = compression.uniform_hash_file(self.filepath)
            except Exception:
                self.fill_sequence_properties()
        return self._hash

    def fill_sequence_properties(self):
        group, _ = extensions.find_extension(self.filepath, raise_exception=True)
        if group != "raw":
            uuid = str(uuid4())
            sequence_file = self.get_fasta(uuid)
            if sequence_file is None:
                return
            self.filepath = sequence_file
            # avoids an infinite loop if the get attribute fails at this point
            for field in ["scaffold_count", "sequence_length", "hash"]:
                try:
                    self.__getattribute__(field)
                except Exception:
                    pass
            self.filepath = sequence_file

    # dict of dict {1:{NCBI Taxonomy} ,2: {GTDB Taxonomy}}
    def get_taxonomy(self, tax_type: Union[int, str]):
        tax_type_name: str = Taxonomy.resolve_tax_type(tax_type)
        if not self._taxonomy:
            self._taxonomy = Taxonomy(self.db, genomeID=self.id, tax_type=tax_type_name)
        return self._taxonomy

    def set_taxonomy(self, tax_values, tax_type_id: Union[int, str], sep=";"):
        tax_type: str = Taxonomy.resolve_tax_type(tax_type_id)
        if isinstance(tax_values, dict):
            self._taxonomy = Taxonomy(self.db, taxid_dict=tax_values, tax_type=tax_type)
        elif isinstance(tax_values, str):
            self._taxonomy = Taxonomy(
                self.db, taxonomy_string=tax_values, tax_type=tax_type, sep=sep
            )
        else:
            logger.error(
                "Please provide taxonomy as a dictionary or formatted string: "
                "%s. Skipping assignment",
                tax_values,
            )

    def assign_lin(self, scheme: Union[SchemeSet, int] = 1, identify=False) -> LIN:
        if isinstance(scheme, SchemeSet):
            schemeID: int = scheme.id
        else:
            schemeID: int = scheme

        genome_hash = compression.uniform_hash_file(self.filepath)
        duplicate_genome = self.db.get_genome(genome_hash, field="hash")
        duplicate_gid = (
            int(duplicate_genome.iat[0, duplicate_genome.columns.get_loc("genomeID")])
            if not duplicate_genome.empty
            else 0
        )
        duplicate_lin = self.db.get_LIN(duplicate_gid, schemeID)
        # If genome already exists and has a LIN
        if len(duplicate_lin) > 0:
            logger.debug(
                "Exact genome with LIN match found with ID: %s. Genome skipped.",
                duplicate_gid,
            )
            self.id = duplicate_gid
            self._lins[schemeID] = LIN(
                self.db, genome_id=duplicate_gid, schemeSetID=schemeID
            )
            self.is_duplicate = True
            return self._lins[schemeID]
            # return self.db.get_genome(duplicate_gid).get_lin(schemeID, assign=False)
        # If genome already exists but does not have LIN
        if duplicate_gid:
            logger.debug(
                "Exact genome found with ID: %s. Recalculating LIN for scheme %d",
                duplicate_gid,
                schemeID,
            )
            self.id = duplicate_gid
        else:
            self.process_file()

        # Register a new lin (new genome OR genome with no LIN) or get the existing LIN
        new_lin = LIN(self.db, genome_id=self.id, schemeSetID=schemeID)
        new_dlin = new_lin.get_lin(identify=identify)
        self._lins[schemeID] = new_lin
        return new_lin

    def process_file(self):
        self.filename = os.path.basename(self.filepath)
        tmp_genomeID = str(uuid4())
        tmp_genome_path = util.get_filepath(
            workspace=self.db.db_name, genomeID=tmp_genomeID, create=True
        )
        tmp_fasta, extracted, process = compression.add_uniform_input(
            self.db.db_name, self.filepath, tmp_genome_path
        )
        self._hash = compression.uniform_hash_file(tmp_fasta)
        self._scaffold_count, self._scaffold_sizes = util.read_scaffold_info(tmp_fasta)
        self.id, exists = self.db.add_genome(
            self.filepath, self.scaffold_count, self.sequence_length, hash=self.hash
        )
        linbase_uncompressed_file_path = util.get_filepath(
            workspace=self.db.db_name, genomeID=self.id, filetype="tmp", create=True
        )
        linbase_compressed_file_path = util.get_filepath(
            workspace=self.db.db_name, genomeID=self.id, filetype="fasta", create=True
        )

        if (
            exists
            and compression.is_gzip_file_valid(linbase_compressed_file_path)
            and process.is_alive()
        ):
            # terminates the compression process if genome was already in the database and the compressed file is valid
            process.terminate()
        else:
            process.start()
        shutil.copyfile(tmp_fasta, linbase_uncompressed_file_path)
        if os.path.realpath(self.filepath) != os.path.realpath(tmp_fasta):
            # if tmp_fasta is not the file to be kept in the database (i.e. is_temporary)
            os.remove(tmp_fasta)
        linflow.POOL.apply_async(
            util.rename_complete_file,
            args=(tmp_genome_path, linbase_compressed_file_path),
        )
        # os.rename(tmp_genome_path, linbase_compressed_file_path)
        tmp_uuid_fasta = util.get_filepath(
            workspace=self.db.db_name, genomeID=tmp_genomeID, filetype="tmp"
        )
        if os.path.isfile(tmp_uuid_fasta):
            os.remove(tmp_uuid_fasta)
        return linbase_uncompressed_file_path, extracted

    def remove(self):
        self.db.remove_genome(self.id)

    def hide_lins(self, schemeSetID):
        self.db.hide_lin([self.id], schemeSetID)

    def get_fasta(self, genomeID: Optional[Union[int, str]] = None):
        # looking for fasta in tmp by ids
        if self._id:
            # if the genomeID is defined use it to get the file
            genomeID = self._id
        elif not genomeID:
            # if the genomeID is not defined and no custom ID is given
            return ""

        saved_file_location = util.get_filepath(self.db.db_name, genomeID, "fasta")
        tmp_file_location = util.get_filepath(self.db.db_name, genomeID, "tmp")
        if os.path.isfile(saved_file_location):
            if not os.path.isfile(tmp_file_location):
                compression.uncompress(saved_file_location, tmp_file_location)
        else:
            # if the source file does not exist to uncompress
            exceptions.handle_exception(
                FileNotFoundError(
                    "File '%s' not found as FASTA source" % saved_file_location
                ),
                logger,
            )
        return tmp_file_location

    def rename_tmp_fasta(self, uuid: int):
        if not self._id:
            logger.exception(
                "Genome %d does not yet have an ID so you "
                "cannot rename the tmp fasta file yet",
                uuid,
            )
        tmp_file_location = util.get_filepath(self.db.db_name, uuid, "tmp")
        if os.path.isfile(tmp_file_location):
            return
