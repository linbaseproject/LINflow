from collections.abc import Iterable
from typing import Any, Callable, Optional, Union, overload

import numpy as np
import numpy.typing as npt

from . import exceptions
from . import logger as m_logger
from . import scheme_reducers as reducers
from . import util
from .filter import Filter
from .lindb import LINdb

logger = m_logger.get_logger(__name__)


class BaseScheme:
    step: float = 0
    filterIDs: list[int] = []
    id: int = 0
    note: str = ""
    _filters: list[Filter] = []
    _thresholds: list = []
    _positions: npt.NDArray[np.floating] = np.array([], dtype=np.floating)

    @overload
    def __init__(
        self,
        db: LINdb,
        *,
        min: float,
        max: float,
        step: float,
        filters: "list[Filter]",
        thresholds: Optional[list],
        note: Optional[str] = None,
        register=True,
    ): ...

    @overload
    def __init__(
        self,
        db: LINdb,
        *,
        positions: npt.NDArray[np.floating],
        filters: list[Filter],
        thresholds: Optional[list],
        note: Optional[str] = None,
        register=True,
    ): ...

    @overload
    def __init__(self, db: LINdb, *, schemeID: int): ...

    def _set_fields(self, fields: list[str], source: dict[str, Any], required=False):
        for field in fields:
            if required and field not in source:
                raise AttributeError(
                    "Missing Attribute '%s' from '%s'" % (field, source)
                )
            if field in source and source[field] is not None:
                self.__setattr__(field, source.get(field))

    def __init__(self, db: LINdb, register=True, **kwargs):
        self.db = db

        if kwargs.get("min") is not None:
            self.id = 0
            fields = ["min", "max", "step", "filters", "thresholds", "note"]
            self._set_fields(fields[:3], kwargs, required=True)
            self._set_fields(fields[3:], kwargs)
            self.positions = np.arange(
                self.min,  # pylint: disable=access-member-before-definition
                self.max  # pylint: disable=access-member-before-definition
                + (self.step / 2),
                self.step,
            ).tolist()
            assert len(self.filters) == len(self.thresholds)
        elif (positions := kwargs.get("positions")) is not None:
            fields = ["positions", "filters", "thresholds", "note"]
            self._set_fields([fields[0]], kwargs, required=True)
            self._set_fields(fields, kwargs)
            self.min = np.min(positions)
            self.max = np.max(positions)
        elif (schemeID := kwargs.get("schemeID")) is not None:
            self.id = int(schemeID)
            scheme_df = self.db.get_baseScheme([self.id])
            if scheme_df.empty:
                raise ReferenceError(
                    "BaseScheme with specified ID %s was not found" % schemeID
                )
            fields = [
                "min",
                "max",
                "step",
                "filters",
                "thresholds",
                "positions",
                "length",
                "note",
            ]
            self._set_fields(fields, scheme_df.iloc[0].to_dict())
        else:
            exceptions.handle_exception(
                BaseException(
                    "Please use one of the overloaded BaseScheme constructors"
                ),
                logger,
            )

        if self.filters:
            self.filterIDs = [f.id for f in self.filters]
        self.length = len(self.positions)
        if not self.id:
            self._check_filters()
            if register:
                self.register()

    # only load filter id and load the rest later
    @property
    def filters(self):
        if not self._filters and self.filterIDs and self.id:
            self.filterIDs = self.db.get_baseScheme([self.id])["filters"].to_list()
            self._filters = [
                Filter(self.db, filterID=id, fill=False) for id in self.filterIDs
            ]
        return self._filters

    @filters.setter
    def filters(self, filter_list: Union[list[int], list[Filter]]):  # type: ignore
        if filter_list is None or (
            isinstance(filter_list, Iterable) and not len(filter_list)
        ):
            self._filters = []
            self.filterIDs = []
        if isinstance(filter_list[0], (int, np.integer)):
            filter_list: list[int] = np.array(filter_list, dtype=int).tolist()
            self._filters = [
                Filter(self.db, filterID=id, fill=False) for id in filter_list
            ]
            self.filterIDs = filter_list
        elif isinstance(filter_list[0], Filter):
            self._filters: list[Filter] = filter_list  # type: ignore
            self.filterIDs = [f.id for f in self._filters]
        else:
            exceptions.handle_exception(
                TypeError(
                    "Filter should be of type int or Filter. Actual: %s"
                    % filter_list[0]
                ),
                logger,
            )

    @property
    def thresholds(self):
        if not self._thresholds and self.filters:
            return np.negative(np.ones(len(self.filters)))
        return self._thresholds

    # filters has to be set before thresholds
    @thresholds.setter
    def thresholds(self, val: Union[list[int], list[float]]):
        if isinstance(val, Iterable) and not len(val):
            return
        elif not isinstance(val, Iterable):
            exceptions.handle_exception(
                TypeError(
                    "thresholds attribute should be set to list was %s" % type(val)
                ),
                logger,
            )
        elif not self.filters:
            exceptions.handle_exception(
                IndexError("There are no Filters defined for BaseScheme"), logger
            )
        elif len(val) != len(self.filters):
            exceptions.handle_exception(
                IndexError(
                    "Size of thresholds(%d) and filters(%d) should be the same."
                    % (len(val), len(self.filters)),
                ),
                logger,
            )
        else:
            self._thresholds = list(val)

    @property
    def positions(self):
        if not len(self._positions) and self.step:
            self._positions = np.arange(
                self.min, self.max + (self.step / 2.0), self.step
            )
            return self._positions
        return self._positions

    @positions.setter
    def positions(self, pos_list: list[float]):
        if not isinstance(pos_list, Iterable):
            exceptions.handle_exception(
                TypeError(
                    "Position in BaseScheme can only be a list or np.ndarray, got: %s"
                    % type(pos_list),
                ),
                logger,
            )
        elif not len(pos_list):
            return
        elif isinstance(pos_list, list):
            self._positions = np.array(pos_list, dtype=float)
        elif isinstance(pos_list, np.ndarray):
            self._positions = pos_list.astype(float)
        else:
            # should never get here
            raise BaseException("unreachable branch!!")

    def _check_filters(self) -> bool:
        if not len(self.thresholds) or not self.filters:
            return False
        elif len(self.thresholds) != len(self.filters):
            return False
        return True

    def register(self):
        """registers the scheme on the database

        Returns:
                int: id of the newly created scheme on the database
        """
        if self.step:
            self.id = self.db.add_base_scheme(
                min=self.min,
                max=self.max,
                step=self.step,
                thresholds=np.array(self.thresholds),
                description=self.note,
                filterIDs=np.array(self.filterIDs),
            )
        else:
            self.id = self.db.add_base_scheme(
                positions=self.positions,
                thresholds=np.array(self.thresholds),
                filterIDs=np.array(self.filterIDs),
                description=self.note,
            )
        return self.id

    def in_range(self, rankscore: float, include_none=False) -> bool:
        """checks if the rankscore is in the range of the scheme positions"""
        if rankscore is None and include_none:
            return True
        if self.min is not None and self.max is not None:
            return self.min <= rankscore <= self.max
        elif len(self.positions) and np.min(
            self.positions.flatten()
        ) <= rankscore <= np.max(self.positions.flatten()):
            return True
        return False

    def apply_filters(
        self, gid: int, ref_genome_ids: npt.NDArray[np.uintc], **kwargs
    ) -> tuple[
        npt.NDArray[np.uintc],
        Filter,
        npt.NDArray[np.floating],
        npt.NDArray[np.floating],
    ]:
        """Applies the filters to the reference genomes in order

        Args:
                gid (int): query genomeID
                ref_genome_ids (np.ndarray): array of genomeIDs to filter step-by-step

        Returns:
                np.ndarray[np.uintc]: filtered reference genomeIDs
                Filter: Last filter to perform filtering
                np.ndarray[np.floating]: 2d array of scores genome (row) score categories (column)
                np.ndarray[np.floating]: 1d array of rankscore (score to rank the genomes by)
        """
        done = False
        filter: Filter
        for filter, k in zip(self.filters, self.thresholds):
            saved_scores: np.ndarray = np.array([[]], dtype=float)
            saved_comparisons = self.db.get_comparison(
                [gid], [filter.id], ref_genome_ids.tolist(), **filter.default_args
            )
            if not saved_comparisons.empty:
                saved_scores = np.array(
                    saved_comparisons["measure"].to_list(), dtype=float
                )
            compute_ref_genome_ids = np.setdiff1d(
                ref_genome_ids, saved_comparisons["refID"], assume_unique=True
            ).astype(np.uintc)
            # runs the measure
            computed_scores = filter.compare(
                np.array([gid], dtype=np.uintc), compute_ref_genome_ids, **kwargs
            )
            ref_genome_ids = np.append(
                compute_ref_genome_ids, saved_comparisons["refID"]
            )

            # drop all-nan columns and reshaping to match computed
            saved_scores = saved_scores[:, ~np.all(np.isnan(saved_scores), axis=0)]
            # handling cases where one is empty. reshaping them to match dimension for append
            if computed_scores.shape[-1] > 0:
                saved_scores = saved_scores.reshape((-1, computed_scores.shape[-1]))
            elif saved_scores.shape[-1] > 0:
                computed_scores = computed_scores.reshape((-1, saved_scores.shape[-1]))
            else:
                saved_scores = computed_scores = saved_scores.reshape((0, 0))

            needed_scores = np.append(computed_scores, saved_scores, axis=0)
            last_filter = filter

            # scaling the result for the comparison
            scaled_scores = filter.scale_scores(needed_scores)

            # decide to stop the chain of filters (or not=> default)
            done, r_ref_genome_ids, r_scores = filter.reasoner(
                gid, ref_genome_ids, scaled_scores
            )
            rankscores = filter.compute_rankscore(r_scores)

            # sort in descending order based on the rankscore
            ranks = np.argsort(rankscores)[::-1][: len(rankscores)]
            ref_genome_ids = r_ref_genome_ids[ranks]
            # this separation is made in case the scores are changed in the reasoner
            # similar to k=51 for the linbase sourmash scheme. if not handled this will cause
            # incorrect scores (e.g. different k-mer scores) to be retrieved for a filter
            unreasoned_scores = (
                needed_scores[ranks]
                if len(ranks) == len(needed_scores)
                else np.array([[]])
            )
            scores = r_scores[ranks]
            rankscores = rankscores[ranks]

            if filter.nscores_to_save != 0 and ref_genome_ids.size > 0:
                top = filter.nscores_to_save
                self.db.add_comparison(
                    gid,
                    filter.id,
                    ref_genome_ids[:top].tolist(),
                    unreasoned_scores[:top],
                    rankscores[:top],
                )

            # using k threshold as percentage on rankscore
            if isinstance(k, float):
                filter_criteria = rankscores >= k
                ref_genome_ids = ref_genome_ids[filter_criteria]
                scores = scores[filter_criteria]
                rankscores = rankscores[filter_criteria]
            # using topk genomes
            else:
                ref_genome_ids = ref_genome_ids[:k]
                scores = scores[:k, :]
                rankscores = rankscores[:k]

            if done:
                break
        return ref_genome_ids, last_filter, scores, rankscores

    def add_query(self, queryID: int, top_rankscore: float, range_check=True):
        """adds query genome to each filter's reference set when the top_rankscore is in range"""
        if range_check and not self.in_range(top_rankscore):
            return

        filter: Filter
        for filter in self.filters:
            filter.add_query(queryID)


class SchemeSet:
    scheme_blocks: list[BaseScheme] = []
    id: int = 0
    _positions: npt.NDArray[np.floating] = np.array([], dtype=np.floating)
    _score2lin: Optional[Callable] = None
    descend_levels: npt.NDArray[np.uintc] = np.array([], dtype=np.uintc)
    note: str = ""

    @overload
    def __init__(
        self,
        db: LINdb,
        *,
        scheme_blocks: list[BaseScheme],
        descend_levels: npt.NDArray[np.integer],
        score2lin: Optional[Callable] = None,
        note=None,
        register=True,
    ): ...

    @overload
    def __init__(self, db: LINdb, *, schemeID: int): ...

    def __init__(self, db: LINdb, register=True, **kwargs):
        kwargs.setdefault("register", True)
        self.db = db
        self.scheme_length = 0
        if kwargs.get("schemeID") is not None:
            self.id = int(kwargs["schemeID"])
            scheme_df = self.db.get_scheme_set(self.id)
            if scheme_df.empty:
                raise TypeError(
                    "SchemeSet with specified ID '%s' was not found" % self.id
                )
            # TODO: This will be slow if not lazy (it's not)
            self.scheme_blocks = [
                BaseScheme(self.db, schemeID=int(base_schemeID))
                for base_schemeID in scheme_df["blocks"].iloc[0]
            ]
            self.descend_levels = np.array(
                scheme_df.iloc[0].get("levels", []), dtype=float
            )
            self.score2lin = scheme_df.iloc[0].get("score")
            self.note = scheme_df.iloc[0].get("note", "")
            register = False
        else:
            # levels to descend within the LIN graph (scheme positions)
            # ZERO denotes only the refs, (None OR -1) denotes all the way down
            for field in ["scheme_blocks", "descend_levels", "note"]:
                self.__setattr__(field, kwargs.get(field))
            # uses the scores to determine the lin (use top ref, average ref, etc.)
            if not self.score2lin:
                self.score2lin = reducers.scores2dlin_default
            if not len(self.descend_levels):
                self.descend_levels = np.array(
                    [scheme.length for scheme in self.scheme_blocks]
                ).astype(np.uintc)
                if len(self.descend_levels) > 1:
                    self.descend_levels[-1] = -1

            if register:
                self.id = self.db.add_scheme_set(
                    np.array([scheme.id for scheme in self.scheme_blocks]),
                    score=util.function2str(self.score2lin),
                    levels=self.descend_levels,
                    note=self.note,
                )

    @property
    def positions(self) -> npt.NDArray[np.floating]:
        if not len(self._positions):
            self._positions = self.compute_positions()
        return self._positions

    @property
    def length(self) -> int:
        return len(self.positions)

    @property
    def score2lin(self) -> Callable:
        if self._score2lin is None:
            return reducers.scores2dlin_default
        return self._score2lin

    @score2lin.setter
    def score2lin(self, score_function: Optional[Union[Callable, str]]):
        if score_function is None:
            self._score2lin = None
        if isinstance(score_function, str):
            callable_import = score_function.split(".")
            if len(callable_import) > 1:
                self._score2lin = getattr(
                    __import__(
                        ".".join(callable_import[:-1]), fromlist=callable_import[-1:]
                    ),
                    callable_import[-1],
                )
            else:
                self._score2lin = getattr(
                    __import__("linflow.scheme_reducers", fromlist=[score_function]),
                    score_function,
                )
        elif isinstance(score_function, Callable):
            self._score2lin = score_function
        else:
            exceptions.handle_exception(
                TypeError(
                    "score function should be of type str or Callable but was %s"
                    % type(score_function),
                ),
                logger,
            )

    def compute_positions(self) -> npt.NDArray[np.floating]:
        """computes the sorted set of spanning lin positions of all the blocks

        Returns:
                np.ndarray[np.floating]: sorted set of spanning lin positions
        """
        positions = np.array([], dtype=np.floating)
        scheme: BaseScheme
        for scheme in self.scheme_blocks:
            if len(scheme.positions):
                self.scheme_length += len(scheme.positions)
            else:
                self.scheme_length += ((scheme.max - scheme.min) / scheme.step) + 1
            if not len(scheme.positions):
                logger.warning(
                    "One of SchemeSet '%s' BaseSChemes has None positions %s",
                    self.id,
                    scheme.id,
                )
                continue
            positions = np.append(positions, scheme.positions)
        return np.unique(positions)

    def _check_blocks(self):
        if not list(self.scheme_blocks) or not list(self.descend_levels):
            return False
        elif len(self.scheme_blocks) != len(self.descend_levels):
            exceptions.handle_exception(
                IndexError(
                    "Number of scheme blocks and descend_levels do not match blocks/levels: %s/%s"
                    % (len(self.scheme_blocks), len(self.descend_levels)),
                ),
                logger,
            )
            return False
        return True

    def register(self):
        self.db.add_scheme_set(
            np.array([s.id for s in self.scheme_blocks]),
            score=util.function2str(self.score2lin),
            levels=self.descend_levels,
            note=self.note,
        )

    def tochange_position(self, score: float) -> int:
        # returns i when a[i-1] <= v < a[i]
        return int(np.searchsorted(self.positions, score, side="right"))

    def calculate_schemes(
        self, gid: int, identify=False, **kwargs
    ) -> dict[int, int]:  # -> LIN:
        ref_genome_ids = np.array([], dtype=int)
        rankscores = scores = np.array([], dtype=float)
        last_filter = None
        if not self.scheme_blocks:
            exceptions.handle_exception(
                TypeError(
                    "SchemeSet %d had no BaseScheme blocks to calculate" % self.id
                ),
                logger,
            )
            return {}

        for idx, scheme in enumerate(self.scheme_blocks):
            descend_level = self.descend_levels[idx]
            # initial LIN positions to include
            if ref_genome_ids.size < 1 and idx == 0:
                ref_genome_ids = self.db.get_lin_depth(descend_level, self.id)
            # selects unique genomesIDs from the previous set of refs' children of a specific depth
            # ZERO denotes only the refs, (None OR -1) denotes all the way down
            elif descend_level != 0:
                # TODO: Could be parallelized
                for genomeID in ref_genome_ids.ravel():
                    children_at_depth = self.db.get_lin_child(
                        genomeID, self.id, descend_level, descend_mode="absolute"
                    )
                    ref_genome_ids = np.append(
                        ref_genome_ids, children_at_depth["genomeID"]
                    )

                ref_genome_ids = np.unique(ref_genome_ids.ravel())
            # if len(ref_genome_ids) > 0:
            ref_genome_ids, last_filter, scores, rankscores = scheme.apply_filters(
                gid, ref_genome_ids, identify=identify, **kwargs
            )
            logger.debug(
                "Filter set #%d applied resulting in %d references",
                idx + 1,
                ref_genome_ids.size,
            )

        new_lin, top_rankscore = self.score2lin(
            self,
            gid,
            ref_genome_ids,
            rankscores,
            last_filter,
            scores=scores,
            add=not identify,
        )  # pylint: disable=E1102
        if identify:
            return new_lin
        # use the lin and the query genome ID to add query to
        # each filter's reference dataset as a new genome
        for idx, scheme in enumerate(self.scheme_blocks):
            if idx == 0 and top_rankscore < scheme.min:
                # add the genome to the first scheme filters if the range starts from > 0
                scheme.add_query(gid, top_rankscore, range_check=False)
            elif scheme.in_range(top_rankscore):
                scheme.add_query(gid, top_rankscore, range_check=False)

        return new_lin


def strictly_decreasing(L):
    return np.all(L[:, 1:] < L[:, :-1], axis=1)


def strictly_increasing(L):
    return np.all(L[:, 1:] > L[:, :-1], axis=1)
