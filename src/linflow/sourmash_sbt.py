import logging
import os
import time
from typing import Optional

import numpy as np
import pandas as pd
import sourmash
import sourmash.index
import sourmash.picklist
import sourmash.sbt
import sourmash.sbtmh
from sourmash.logging import set_quiet
from sourmash.search import JaccardSearch, SearchType

from . import (
    exceptions,
    extensions,
    linflow,
)
from . import logger as m_logger
from . import (
    measure_base,
    util,
)
from .sourmash_custom4 import SourmashSigFilter

# from sourmash.index import MultiIndex

DEFAULT_KMER = 21
DEFAULT_KMERS = [21, 51]
DEFAULT_K51_SBT_THRESHOLD = 0.03
DEFAULT_K21_THRESHOLD = 0.0001
SBTSEARCH_COLUMNS = ["signature", "score"]
logger = m_logger.get_logger(__name__)
set_quiet(True)

sig_dbs: dict[int, sourmash.sbt.SBT] = {}


class SourmashSBT(measure_base.Measure):
    @classmethod
    def add_sketch(
        cls,
        signatures: list[sourmash.SourmashSignature],
        workspace,
        force_save=False,
        **kwargs,
    ):
        # global keep SBT databases in memory
        global sig_dbs  # pylint: disable=global-variable-not-assigned
        # sig_dbs: dict[int, sourmash.sbt.SBT] = {}

        signature = None
        try:
            for signature in signatures:
                k_mer = signature.minhash.ksize

                try:
                    if k_mer in sig_dbs:
                        # if SBT database in memory
                        sig_db = sig_dbs[k_mer]
                    else:
                        sbt_filepath = cls.filepath(workspace, k_mer)
                        if os.path.isfile(sbt_filepath):
                            sig_db = cls.load_sbt(
                                sbt_filepath, skip_errors=True, **kwargs
                            )
                            logger.debug(
                                "SBT-%d loaded from file when adding sketches", k_mer
                            )
                        else:
                            raise FileNotFoundError(
                                "SBT file not found at %s" % sbt_filepath
                            )
                except FileNotFoundError as e:
                    # new sbt
                    sig_db = cls.create_sbt()
                """
                # signature does not exist => add to SBT (this check was too expensive)
                if cls.find_sketch_in_sbt(
                    signature, sig_db, sbt_threshold=1.0
                ).empty:"""

                sig_db.insert(signature)
                sig_dbs[k_mer] = sig_db
                logger.debug(
                    "sketch added to SBT-%d for signature '%s'",
                    k_mer,
                    signature._name,  # pylint: disable=protected-access
                )

            if force_save:
                # forcefully update SBT files
                cls.finalize(workspace=workspace, force=force_save, **kwargs)

            return signatures, sig_dbs

        except Exception as e:
            if signature:
                logger.error(  # pylint: disable=protected-access
                    "Sketch adding failed for file '%s'",
                    signature._name,
                )
            logger.exception(e)
            raise e

    @classmethod
    def save_sbt(cls, sbt_db: sourmash.sbt.SBT, sbt_filepath: str):
        # removing all filtering criteria from database
        sbt_db.picklists = []
        sbt_db.save(sbt_filepath[:-8])
        logger.debug("SBT saved at %s", sbt_filepath)

    @classmethod
    def find_sketch_in_sbt(
        cls,
        query_sig: sourmash.SourmashSignature,
        sig_db: sourmash.sbt.SBT,
        refs: Optional[list[str]] = None,
        k_mer=DEFAULT_KMER,
        **kwargs,
    ):
        refs = refs or []
        sig_start_time = time.time()
        results = []
        threshold = float(kwargs.get("sbt_threshold", DEFAULT_K21_THRESHOLD))
        sig_db.picklists = []
        if not isinstance(query_sig, sourmash.SourmashSignature):
            e = Exception(
                "The query can only be a SourmashSignature was '%s'" % query_sig
            )
            logger.exception(e)
            raise e

        searchF = JaccardSearch(SearchType.JACCARD, threshold=threshold)

        if refs is None:
            ref_dict = {}
        else:
            ref_dict = {extensions.remove_extension(ref_path): 1 for ref_path in refs}
        # filtering sbt by reference signatures
        """picklist = sourmash.picklist.SignaturePicklist("name")
        if refs:
            picklist.init(refs)
            # sig_db2 = copy.deepcopy(sig_db)
            sig_db = sig_db.select(picklist=picklist)
            logger.debug("SBT picklist has %d refs, 1st %s", len(refs), refs[0])"""

        if not sig_db:
            return pd.DataFrame(columns=SBTSEARCH_COLUMNS)
        # found: list[sourmash.index.IndexSearchResult] = list(sbt_results)
        # Handle non-scaled signatures
        if not query_sig.minhash.scaled:
            logger.warning(
                "The signature does not have a scaled MinHash. Converting to scaled."
            )
            scaled_minhash = sourmash.MinHash(
                n=0,
                ksize=query_sig.minhash.ksize,
                scaled=1000,  # Default scaled value, adjust as needed
                is_protein=query_sig.minhash.is_protein,
            )
            scaled_minhash.add_many(query_sig.minhash.get_mins())
            query_sig = sourmash.SourmashSignature(minhash=scaled_minhash)

        try:
            found = list(sig_db.find(searchF, query_sig))
        except Exception as e:
            logger.error(f"Error during SBT search: {e}")
            return pd.DataFrame(columns=SBTSEARCH_COLUMNS)

        results = [
            (result.signature.name, result.score)
            for result in found
            if not ref_dict
            or extensions.remove_extension(result.signature.name) in ref_dict
        ]
        df = pd.DataFrame(results, columns=SBTSEARCH_COLUMNS)

        logger.debug(
            "Compared '%s' to %d sbt_signatures found %d matches. threshold %s, args %s. took %s",
            query_sig.name,
            len(sig_db._leaves),
            len(df),
            threshold,
            {"k_mer": k_mer, **kwargs},
            util.elapsed_time(sig_start_time),
        )
        # removing all filtering criteria from database
        sig_db.picklists = []
        return df.fillna(-1)

    @classmethod
    def load_sbt(cls, sbt_filepath, skip_errors=False, **kwargs) -> sourmash.sbt.SBT:
        # with util.Suppressor():
        # sig_db = sourmash.sourmash_args.load_file_as_index(sbt_filepath)
        logger.debug("Loading SBT from file '%s'", sbt_filepath)
        try:
            sbt = sourmash.sbtmh.load_sbt_index(sbt_filepath)
            logger.debug("Done SBT Loading from file '%s'", sbt_filepath)
            return sbt
        except Exception as e:

            if not skip_errors:
                logger.error("Error loading SBT from %s: '%s'", sbt_filepath, e)
                raise
            else:
                logger.debug("Error loading SBT from %s: '%s'", sbt_filepath, e)
            return cls.create_sbt(**kwargs)

    @classmethod
    def create_sbt(cls, **kwargs) -> sourmash.sbt.SBT:
        # create_sbt_index(bloom_filter_size=1e5, n_children=2)
        """logger.debug(
            "creating empty SBT index with args '%s'.(args not used here!)", str(kwargs)
        )"""
        return sourmash.sbtmh.create_sbt_index()

    @classmethod
    def compare(cls, ref: list, query, workspace: str, **kwargs) -> np.ndarray:
        result = pd.DataFrame(columns=SBTSEARCH_COLUMNS)

        # genomeID
        if isinstance(query, int):
            query_sig = SourmashSigFilter.load_signatures(
                [cls.filepath(workspace, query, SourmashSigFilter.filetype())], **kwargs
            )[0]
        # full file path
        elif isinstance(query, str):
            query_sig = SourmashSigFilter.load_signatures([query], **kwargs)[0]

        # signature object
        elif isinstance(query, sourmash.SourmashSignature):
            query_sig = query
        else:
            return result.to_numpy()
        if not query_sig:
            return result.to_numpy()

        k_mer = query_sig.minhash.ksize
        if k_mer not in sig_dbs:
            # if SBT database not in memory
            logger.warning("SBT-%d not in memory. Updating", k_mer)
            cls.mem_sbt_update(workspace, k_mer, force=True, **kwargs)

        sig_sbt = sig_dbs[k_mer]
        sbt_filepath = cls.filepath(workspace, k_mer, create=False)
        # does not work without None
        if sig_sbt is not None:
            refs = [
                cls.filepath(workspace, refID, SourmashSigFilter.filetype())
                for refID in ref
            ]
            result = cls.find_sketch_in_sbt(query_sig, sig_sbt, refs, **kwargs)
            result[SBTSEARCH_COLUMNS[0]] = result[SBTSEARCH_COLUMNS[0]].apply(
                lambda path: SourmashSigFilter.path2id(path, keep_extension=True)
            )
            return result.to_numpy()
        raise FileNotFoundError("Sourmash SBT '%s' not found." % sbt_filepath)

    @classmethod
    def create(
        cls,
        workspace: str,
        genomeIDs: list[int],
        update_db=True,
        force=False,
        is_query=False,
        **kwargs,
    ) -> list:
        query_sigs = []
        kwargs.setdefault("db_k_mers", [kwargs.get("k_mer")])
        db_k_mers = kwargs["db_k_mers"]
        k_mer = kwargs.pop("k_mer", DEFAULT_KMER)
        # when just the query
        if is_query:
            genomeID = genomeIDs[0]
            sig_filepath = cls.filepath(
                workspace, genomeID, SourmashSigFilter.filetype()
            )
            if os.path.isfile(sig_filepath) and not force:
                for db_kmer in db_k_mers:
                    query_sigs += SourmashSigFilter.load_signatures(
                        [sig_filepath], k_mer=db_kmer, **kwargs
                    )
            else:
                tmp_fasta = util.get_fasta(workspace, genomeID)
                sketch = SourmashSigFilter.create_sketch(
                    tmp_fasta, sig_filepath, k_mer=k_mer, **kwargs
                )
                query_sigs += sketch

        if not is_query and not kwargs.get("identify", False):
            # on reference SBT creation only
            cls.mem_sbt_live(workspace, genomeIDs, call_update=True, **kwargs)

        if update_db and query_sigs and not is_query:
            cls.add_sketch(query_sigs, workspace, **kwargs)
            logger.debug(
                "Genomes %s were added to the SBT database.",
                [q.name for q in query_sigs],
            )
        return query_sigs

    @classmethod
    def mem_sbt_live(
        cls,
        workspace: str,
        genomeIDs: list[int],
        db_k_mers: list[int],
        call_update=False,
        **kwargs,
    ) -> list[int]:
        """Checks whether the SBT database on memory is up to date

        Args:
            workspace (str): name of the workspace
            genomeIDs (list[int]): list of genomeIDs that need to be in of the SBT database
            db_k_mers (list[int]): list of k_mers that need SBT databases on memory
            call_update (bool, optional): whether to call update the SBT database in at the end of the function. Defaults to False.

        Returns:
            list[int]: database k_mers that need updating
        """
        dbs_need_update = []
        logger.debug(
            "checking if SBT-%s not up-to-date (live) in memory. Updating", db_k_mers
        )
        for k_mer in db_k_mers:
            sig_db = cls.create_sbt(**kwargs)

            if k_mer not in sig_dbs:
                sbt_filepath = cls.filepath(workspace, k_mer)
                sbt_exists = os.path.isfile(sbt_filepath)
                try:
                    if sbt_exists:
                        sig_db = cls.load_sbt(sbt_filepath, **kwargs)
                except (FileNotFoundError, ValueError) as ex:
                    # if the SBT file is corrupted or empty create an empty SBT
                    # and say it does not exist
                    exceptions.handle_exception(
                        ex, logger, raise_exception=False, log_level=logging.WARNING
                    )
                except BaseException as e:
                    exceptions.handle_exception(e, logger)
            else:
                sig_db = sig_dbs[k_mer]
            database_size = len(sig_db._leaves)  # pylint: disable=protected-access
            # resorted to count since checking IDs was very expensive (SBT gid retrieval)
            if len(genomeIDs) <= database_size:
                sig_dbs[k_mer] = sig_db
                logger.debug("SBT-%s live in memory.", k_mer)
                continue
            elif len(genomeIDs) == database_size + 1:
                # if only one (last) signature is missing -> add it to the SBT
                last_ref_signature = SourmashSigFilter.load_signatures(
                    [
                        cls.filepath(
                            workspace, genomeIDs[-1], SourmashSigFilter.filetype()
                        )
                    ],
                    k_mer=k_mer,
                    skip_errors=True,
                    **kwargs,
                )
                if last_ref_signature:
                    sig_db.insert(last_ref_signature[0])
                    sig_dbs[k_mer] = sig_db
                continue
            else:
                logger.debug(
                    "SBT-%s not up-to-date (live) in memory ref(%d) > sbt(%d). queuing update",
                    k_mer,
                    len(genomeIDs),
                    database_size,
                )
                dbs_need_update.append(k_mer)
            if call_update:
                cls.mem_sbt_update(workspace, k_mer, genomeIDs, **kwargs)
        return dbs_need_update

    @classmethod
    def mem_sbt_update(
        cls, workspace: str, k_mer: int, genomeIDs: Optional[list[int]] = None, **kwargs
    ):
        """updates the sig_sbt on mem since it is not up to date

        Args:
            workspace (str): name of the workspace
            k_mer (int): k_mer of the SBT database that is to be updated
            genomeIDs (list[int]): list of genomeIDs that need to be in of the SBT
        """
        global sig_dbs  # pylint: disable=global-variable-not-assigned
        genomeIDs = genomeIDs or []
        if identify := kwargs.get("identify", False):
            # if identify is set to True, we do not need the latest SBT
            # just what is available on disk (even if it is not up to date)
            sbt_filepath = cls.filepath(workspace, k_mer)
            sbt_exists = os.path.isfile(sbt_filepath)
            if sbt_exists:
                sig_dbs[k_mer] = cls.load_sbt(sbt_filepath, **kwargs)
                logger.debug(
                    "SBT-%d loaded from file instead of updating since identify is %s",
                    k_mer,
                    identify,
                )
                return
        if not genomeIDs:
            sig_dbs[k_mer] = cls.create_sbt()
        sig_paths = [
            cls.filepath(workspace, genomeID, SourmashSigFilter.filetype())
            for genomeID in genomeIDs
        ]
        query_sigs = SourmashSigFilter.load_signatures(
            sig_paths, k_mer=k_mer, skip_errors=True, **kwargs
        )
        cls.add_sketch(query_sigs, workspace, **kwargs)
        logger.debug(
            "built SBT-%s in memory from scratch. %d references.",
            k_mer,
            len(query_sigs),
        )

    @classmethod
    def file_sbt_live(cls, workspace: str, call_update=False, **kwargs):
        """checks if the file SBT is up to date with the mem SBT

        Args:
            workspace (str): name of the workspace to check

        Returns:
            list[int]: list of k_mers whose SBT files need updating
        """
        dbs_need_update = []
        for k_mer, sig_db in sig_dbs.items():
            update = False
            if (
                not isinstance(k_mer, int)
                and len(sig_db._leaves) < 1  # pylint: disable=protected-access
            ):
                # if not part of the databases to update skip
                continue
            sbt_filepath = cls.filepath(workspace, k_mer)
            sbt_exists = os.path.isfile(sbt_filepath)
            if not sbt_exists:
                logger.debug(
                    "SBT live check: SBT-%d file not found at '%s'", k_mer, sbt_filepath
                )
                dbs_need_update.append(k_mer)
                update = True
            else:
                # if sbt file exists check members to match with memory SBT
                file_sig_db = cls.load_sbt(sbt_filepath, **kwargs)
                sig_dbIDs = cls.get_members(sig_db)
                file_sig_dbIDs = cls.get_members(file_sig_db)
                diff = np.setdiff1d(sig_dbIDs, file_sig_dbIDs)
                if len(diff):
                    # if more signatures in memory SBT than in file SBT
                    # save SBT in background
                    logger.debug(
                        "SBT live check: SBT-%d file not up-to-date. missing signatures %s",
                        k_mer,
                        diff,
                    )
                    dbs_need_update.append(k_mer)
                    update = True
            if update and call_update:
                linflow.POOL.apply_async(
                    cls.sbt_file_update, args=(sig_db, sbt_filepath, k_mer)
                )
                logger.debug("Saving updated SBT-%d", k_mer)
        return dbs_need_update

    """@classmethod
    def verify_sbt(
        cls,
        workspace: str,
        genomeIDs: list[int],
        db_k_mers: list[int],
        **kwargs,
    ):
        # global keep SBT databases in memory
        global sig_dbs  # (see sig_dbs[k_mer]) # pylint: disable=global-variable-not-assigned
        query_sigs = []
        dbs_need_update = []

        for k_mer in db_k_mers:
            if k_mer in sig_dbs:
                continue
            sbt_filepath = cls.filepath(workspace, k_mer)
            sbt_exists = os.path.isfile(sbt_filepath)

            try:
                sig_db = cls.load_sbt(sbt_filepath, **kwargs)
            except (FileNotFoundError, ValueError):
                # if the SBT file is corrupted or empty create an empty SBT and say it does not exist
                sig_db = cls.create_sbt()
                sbt_exists = False
            except BaseException as e:
                exceptions.handle_exception(e, logger)
            sig_dbs[k_mer] = sig_db
            # resorted to count since checking IDs was very expensive (SBT gid retrieval)
            if len(genomeIDs) <= len(
                sig_db._leaves  # pylint: disable=protected-access
            ):
                return
            db_signatureIDs = cls.get_members(sig_db)
            if (
                not sbt_exists or np.setdiff1d(genomeIDs, db_signatureIDs).any()
            ) and not kwargs.get("identify", False):
                # needed signature list not matching what is on disk (because of system crashes) => update SBT on disk
                for genomeID in genomeIDs:
                    sig_filepath = cls.filepath(
                        workspace, genomeID, SourmashSigFilter.filetype()
                    )
                    query_sigs += SourmashSigFilter.load_signatures(
                        [sig_filepath], k_mer=k_mer, **kwargs
                    )

                # TODO: could reduce the creation time of SBT by loading existing SBT and
                # adding missing genomes instead of creating one from scratch
                sig_db = cls.create_sbt()
                for signature in query_sigs:
                    sig_db.insert(signature)
                logger.debug(
                    "Recreated '%s' SBT from scratch since reference count did not match %d vs %d",
                    sbt_filepath,
                    len(genomeIDs),
                    len(db_signatureIDs),
                )
                sig_dbs[k_mer] = sig_db
                dbs_need_update += [k_mer]
        kwargs["db_k_mers"] = dbs_need_update
        cls.update_sbt(workspace=workspace, force=True, **kwargs)"""

    @classmethod
    def update_file_sbt(cls, workspace: str, force=False, **kwargs):
        # global keep SBT databases in memory
        global sig_dbs  # pylint: disable=global-variable-not-assigned
        save = False
        db_k_mers = kwargs.setdefault("db_k_mers", [*sig_dbs])
        for k_mer, sig_db in sig_dbs.items():
            if k_mer not in db_k_mers or not isinstance(k_mer, int):
                # if not part of the databases to update skip
                continue
            file_sig_db: sourmash.sbt.SBT = None  # type: ignore
            sbt_filepath = cls.filepath(workspace, k_mer)
            sbt_exists = os.path.isfile(sbt_filepath)
            if not sbt_exists or force:
                logger.debug("Force saving updated SBT-%s", k_mer)
                save = True
            else:
                # if the SBT exists check members to match with memory SBT and save if missing signatures
                file_sig_db = cls.load_sbt(sbt_filepath, **kwargs)
                sig_dbIDs = cls.get_members(sig_db)
                file_sig_dbIDs = cls.get_members(file_sig_db)
                if np.setdiff1d(sig_dbIDs, file_sig_dbIDs).any():
                    # if more signatures in memory SBT than in file SBT -> save
                    logger.debug("Saving updated SBT-%d", k_mer)
                    save = True
            if save:
                # save SBT in background
                linflow.POOL.apply_async(
                    cls.sbt_file_update, args=(sig_db, sbt_filepath, k_mer)
                )
                logger.info("updating SBT-%d in background", k_mer)

    @classmethod
    def finalize(cls, **kwargs):
        cls.update_file_sbt(force=True, **kwargs)
        logger.debug("finalized %s", cls.__name__)

    @classmethod
    def sbt_file_update(cls, sig_db: sourmash.sbt.SBT, sbt_filepath, k_mer):
        if os.path.isfile(sbt_filepath):
            os.remove(sbt_filepath)
            logger.debug("Removed old SBT-%d file", k_mer)
        cls.save_sbt(sig_db, sbt_filepath)
        # logger.debug("Saved updated SBT-%d to %s", k_mer)

    @classmethod
    def filetype(cls):
        return "sbt"

    @classmethod
    def get_members(cls, sig_db: sourmash.sbt.SBT):
        """this gets all the signature members of the SBT database (very expensive for large SBTs 3min for 50k leaves)

        Args:
            sig_db (sourmash.sbt.SBT): the SBT to check

        Returns:
            list: genomeID based on signature filenames in the SBT
        """
        if not sig_db:
            return []
        return [  # pylint: disable=protected-access
            int(extensions.remove_extension(sig.data.name))
            for sig in sig_db._leaves.values()
        ]

    """@classmethod
    def remove(cls, genomeIDs:list[int], workspace:str, **kwargs):
        gid_pos = {}
        sig_dbs: dict[int, sourmash.sbt.SBT] = {}
        for k_mer in DEFAULT_KMERS:
            sig_filepaths = [cls.filepath(workspace, genomeID, filetype="sig") for genomeID in genomeIDs]
            sigs = SourmashSigFilter.load_signatures(sig_filepaths, k_mer=k_mer, **kwargs)
            sbt_filepath = cls.filepath(workspace, k_mer)
            sig_db = cls.load_sbt(sbt_filepath, **kwargs)
            for target_sig in sigs:
                for pos,s in sig_db._leaves.items():
                    if target_sig.md5sum() == s.metadata:
                        gid_pos[target_sig] = pos
                        break

            for gid, delpos in gid_pos.items():
                parent = sig_db._leaves[delpos].pare
                del sig_db._leaves[delpos]"""
