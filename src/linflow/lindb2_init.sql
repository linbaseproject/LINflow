# Possible attributes for a genome
REPLACE INTO Attribute VALUES 
	(1,'Type strain'),
	(2,'NCBI Taxonomy ID'),
	(3,'NCBI Accession Number'),
	(4,'Date of isolation'),
	(5,'Country'),
	(6,'Region'),
	(7,'GPS Coordinates'),
	(8,'Link to peer-reviewed paper'),
	(9,'Host of isolation'),
	(10,'Secondary host'),
	(11,'Disease'),
	(12,'Symptom'),
	(13,'Phenotype'),
	(14,'Fluorescence'),
	(15,'Environmental source'),
	(16,'Source of isolation'),
	(17,'Outbreak'),
    (18,'Person/Institution who made the isolation'),
    (20,'R3B2 630/631 PCR test'),
    (21,'Sequencing Technology'),
    (22,'Sequencing Technology Version'),
    (23,'Assembly program'),
    (24,'Number of contigs'),
    (25,'Genome length after assembly, bp'),
    (26,'Coverage'),
    (27,'Alternate strain name'),
    (28,'Additional strain names'),
    (29,'NCBI organism name'),
	(30,'Strain'),
	(31,'BioProject'),
	(32,'BioSample'),
	(33,'NCBI AssemblyID'),
	(34,'NCBI ftp link'),
	(35,'Completeness %'),
	(36,'Contamination %'),
	(37,'GPS Category'),
	(38,'Isolation Source Category')
;$$

# attributes with categoric types
REPLACE INTO Compound_Attribute VALUES
(38,1,'Animal associated'),
(38,2,'Arthropod associated'),
(38,3,'Atmosphere associated'),
(38,4,'Built environment associated'),
(38,5,'Extreme environment associated'),
(38,6,'Food and feed-associated'),
(38,7,'Fresh water including precipitation'),
(38,8,'Human associated'),
(38,0,'Other'),
(38,9,'Plant associated'),
(38,10,'Salt water associated'),
(38,11,'Soil associated excluding rhizosphere'),
(38,12,'Wastewater associated'),
(37,1,'Exact location'),
(37,2,'Country center'),
(37,3,'Region/Place')
;$$

# types of supported taxonomy
REPLACE INTO TaxType (taxTypeID, typeName)
VALUES (1,"ncbi"), (2, "gtdb"),(3, "genomerxiv"),(4, "custom")
;$$

# taxonomic ranks by taxonomic type
REPLACE INTO TaxRank (rankID, rankName, taxTypeID, compulsory) 
VALUES (0,"root", 1, 0),
	(1,"superkingdom", 1, 1),
	(2,"phylum", 1, 1),
	(3,"class", 1, 1),
	(4,"order", 1, 1),
	(5,"family", 1, 1),
	(6,"genus", 1, 1),
	(7,"species", 1, 1),
	(8,"subspecies", 1, 0),
	(9,"pathovar", 1, 0),
	(10,"race", 1, 0),
	(11,"biovar", 1, 0),
	(12,"clade", 1, 0),
	(13,"genomospecies", 1, 0),
	(14,"phylogroup", 1, 0),
	(15,"phylotype", 1, 0),
	(16,"serotype", 1, 0),
	(17,"serovar", 1, 0),
	(18,"biotype", 1, 0),
	(19,"sequevar", 1, 0),
	(20,"strain", 1, 1),
	(0,"root", 2, 0),
    (1,"superkingdom", 2, 1),
	(2,"phylum", 2, 1),
	(3,"class", 2, 1),
	(4,"order", 2, 1),
	(5,"family", 2, 1),
	(6,"genus", 2, 1),
	(7,"species", 2, 1),
	(8,"subspecies", 2, 0),
	(9,"pathovar", 2, 0),
	(10,"race", 2, 0),
	(11,"biovar", 2, 0),
	(12,"clade", 2, 0),
	(13,"genomospecies", 2, 0),
	(14,"phylogroup", 2, 0),
	(15,"phylotype", 2, 0),
	(16,"serotype", 2, 0),
	(17,"serovar", 2, 0),
	(18,"biotype", 2, 0),
	(19,"sequevar", 2, 0),
	(20,"strain", 2, 1),
	(1,"superkingdom", 3, 1),
	(2,"phylum", 3, 1),
	(3,"class", 3, 1),
	(4,"order", 3, 1),
	(5,"family", 3, 1),
	(6,"genus", 3, 1),
	(7,"species", 3, 1),
	(8,"subspecies", 3, 0),
	(9,"pathovar", 3, 0),
	(10,"race", 3, 0),
	(11,"biovar", 3, 0),
	(12,"clade", 3, 0),
	(13,"genomospecies", 3, 0),
	(14,"phylogroup", 3, 0),
	(15,"phylotype", 3, 0),
	(16,"serotype", 3, 0),
	(17,"serovar", 3, 0),
	(18,"biotype", 3, 0),
	(19,"sequevar", 3, 0),
	(20,"strain", 3, 1)
;$$
REPLACE INTO User (User_ID, FirstName, LastName, Institute, RegistrationDate, 
Username, `Password`, Email, EmailValidateSecret, EmailValidated, Permission)
VALUES (1, "guest", NULL, "Welcome to Virginia Tech", "2019/11/24", "guest",
    "$2y$10$1nafrGHmksTW3wyXxI5zAuk4sFknMJboCMgSGE.g/1V09R4owUl5S",
    "Help @ linbase@vt.edu", "18997733ec248a9fcaf239cc44d43363", 1, 31),
    (5,'dummy','user','N/A','2020/02/10','hcj1009',
    '$2y$10$dJnwhOkEWKI2BD6KhRk.xe.YN6CBSfmeENk7L2fdx/HapBsfh47ci',
    'abc@c.e','170c944978496731ba71f34c25826a34',1,255)
;$$


REPLACE INTO JobInfo(JobInfo_ID, `Name`, `Type`, Content, MCW, BufferSize) VALUES  
# MCW is the number of parallel workers (threads) Go makes to add do the tasks 0 means it is done in the main scheduler process
# (5,'ident_gene','cmd','node jobs/blast.js --uuid={$uuid}',4,4),
	(6,'search_genome','cmd','./jobs/linflow/genomerxiv.py find_genome {$workspace} -k {$keyword} -at {$att} -b {$submitter}',8,20),
	(7,'search_lingroup','cmd','./jobs/linflow/genomerxiv.py find_lingroup {$workspace} -k {$keyword} -tr {$taxon} -b {$describer} -tt {$taxTypeID}',8,20),
	(8,'upload','cmd','python3 jobs/upload.py {$username} {$uuid} {$interest} {$taxonomy} {$attributes} {$priority}',1,1),
	(9,'ident_genome','cmd','./jobs/linflow/genomerxiv.py identify {$workspace} -i {$destination} -s 8',8,8),
	(10,'search_ncbi','cmd','bash jobs/get_ncbi_genome.sh {$query} {$destination}',8,8),
	(11,'lingroupID_genome_members','cmd','./jobs/linflow/genomerxiv.py lingroup_members {$workspace} -s 8 -lg {$lingroupID}', 8, 20),
	(12,'prefix_genome_members','cmd','./jobs/linflow/genomerxiv.py lingroup_members {$workspace} -s 8 -ps {$slins}', 8, 20),
	(13,'ident_signature','cmd',
	  CONCAT('./jobs/linflow/genomerxiv.py signature {$workspace} -i {$destination} -s 8 ',
	  '-d /home/shared/genomerxiv/scheduler/workspaces/genomerxiv_main/sbt/0/51.sbt.zip')
	  ,8,8),
	(15,'create_phylogeny', 'cmd', 'python jobs/linflow/pylotree.py {$workspace} -g {$genomeIDs_str} -p {$parentJobID} -j {$job_uuid}', 8, 8)
;$$


DROP PROCEDURE IF EXISTS prefix_init;$$
CREATE PROCEDURE prefix_init(IN db_name VARCHAR(500))
  BEGIN
    IF (SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = db_name AND table_name ='Prefix') > 0 THEN 
      INSERT INTO Prefix (schemeSetID, idx, lastValue, parent) 
        SELECT schemeSetID,NULL,0,NULL FROM SchemeSet; 
    END IF;
END;$$

