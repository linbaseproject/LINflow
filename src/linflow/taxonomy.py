import re
from functools import lru_cache
from typing import Optional, Union, overload

import numpy as np

from . import logger as m_logger
from . import tax_enums
from .lindb import LINdb

# {"d": 1, "p": 2, "c": 3, "o": 4, "f": 5, "g": 6, "s": 7}
TAX_STRING_KEYS = ["d", "p", "c", "o", "f", "g", "s"]
TAX_STRING_KEYS = dict(zip(TAX_STRING_KEYS, range(1, len(TAX_STRING_KEYS) + 1)))
logger = m_logger.get_logger(__name__)


class RankTypeError(Exception):
    """Exception raised for errors on the input rank type.

    Attributes:
            rank ([str,int], optional): input rank which caused the error
            message (str, optional): explanation of the error
    """

    def __init__(self, rank, message=""):
        self.rank = rank
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return (
            f"Rank should be of type int or str got: {type(self.rank)}. {self.message}"
        )


class Taxonomy:
    # rankID:taxID
    taxid_dict: dict[int, int] = {}
    # rankID:taxName
    taxa_dict: dict[int, str] = {}

    @lru_cache(maxsize=20)
    def RankID_name_table(self, tax_type="") -> dict[int, str]:
        tax_type = tax_type or self.tax_type
        return self.db.taxonomy_rank_table(tax_type=tax_type)

    @lru_cache(maxsize=20)
    def RankName_ID_table(self, tax_type="") -> dict[str, int]:
        tax_type = tax_type or self.tax_type
        return self.db.taxonomy_rank_table_name_key(tax_type=tax_type)

    @classmethod
    def resolve_tax_type(cls, val: Union[int, str]) -> str:
        try:
            if isinstance(val, str):
                return str(tax_enums.TaxType[val].value)
            elif isinstance(val, int):
                return tax_enums.TaxType(val).name
            else:
                logger.error(
                    "Please specify taxonomy by name or number: %s. Using default", val
                )
        except KeyError:
            logger.error(
                "The specified taxonomy type does not exist: %s. Using default", val
            )
        return "ncbi"

    def set_rank(
        self,
        rank: Union[int, str],
        taxid: Union[int, str],
        tax_type: Optional[str] = None,
    ):
        tax_type = self.tax_type if tax_type is None else tax_type
        if isinstance(rank, str):
            rankID = self.RankName_ID_table(tax_type=tax_type).get(rank.lower())
        elif isinstance(rank, int):
            rankID = rank
        else:
            raise RankTypeError(rank)
        if rankID is None or taxid is None:
            return

        if isinstance(taxid, int):
            self.taxid_dict[rankID] = taxid
        elif isinstance(taxid, str):
            try:
                lookupID = self.db.taxid_conversion(
                    taxid.lower(), rankID=rankID, tax_type=tax_type
                )
            except Exception:
                self.taxa_dict[rankID] = taxid
                return

            if not lookupID.empty:
                self.taxid_dict[rankID] = lookupID.iat[
                    0, lookupID.columns.get_loc("taxID")
                ]
            else:
                self.taxa_dict[rankID] = taxid

    def set_ranks(self, ranks_dict: dict, tax_type: Optional[str] = None):
        for rank, tax in ranks_dict.items():
            self.set_rank(rank, tax, tax_type=tax_type)

    @classmethod
    def parse_taxstring(cls, taxonomy_string: str, sep=";") -> dict[int, str]:
        """converts taxonomy_string x__name to dic[rank,name]

        Args:
            taxonomy_string (str): Default taxonomy string superKingdom-to-species
            sep (str, optional): the separator between ranks. Defaults to ";".

        Returns:
            dict: {rank_name:rank_value, ...}
        """
        tax_list = taxonomy_string.split(sep)
        result = {}

        for idx, section in enumerate(tax_list):
            if match := re.match(r"^([a-z])__", section):
                if match.group(1) in TAX_STRING_KEYS:
                    result[TAX_STRING_KEYS[match.group(1)]] = section[3:]
        return result

    @classmethod
    def taxstring2list(cls, taxonomy_string: str, sep=";"):
        result = cls.parse_taxstring(taxonomy_string, sep)
        return list(result), list(result.values())

    @overload
    def __init__(
        self, db: LINdb, *, taxid_dict: dict, tax_type: Optional[str] = "ncbi"
    ):
        """create a Taxonomy object

        Args:
                db (LINdb): database object
                taxid_dict (dict): the genome taxonomy in the format tax_lvl: taxid. e.g. {1:Bacteria, 2:Firmicutes, ... }
                tax_type (str, optional): type of taxonomy. Defaults to 'ncbi'.
        """

    # e.g. d__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;
    # f__Clostridiaceae;g__Clostridium;s__Clostritax_colsdium perfringens
    @overload
    def __init__(
        self,
        db: LINdb,
        *,
        taxonomy_string: str,
        sep=";",
        tax_type: Optional[str] = "ncbi",
    ):
        """taxonomy string to taxonomy object

        Args:
                db (LINdb): database object
                taxonomy_string (str): e.g. d__Bacteria;p__Firmicutes;c__Clostridia;o__Clostridiales;
                         f__Clostridiaceae;g__Clostridium;s__Clostridium perfringens
                sep (str, optional): key taxonomy level separator. Defaults to ";".
                tax_type (str, optional): type of taxonomy. Defaults to 'ncbi'.
        """

    @overload
    def __init__(
        self,
        db: LINdb,
        *,
        genomeID: int,
        tax_type: Optional[str] = tax_enums.TaxType(1).name,
    ):
        """loads taxonomy from database based on type and genomeID

        Args:
                db (LINdb): database object
                genomeID (int): ID of genome to pull taxonomy from
                tax_type (str, optional): type of taxonomy. Defaults to TaxType(1).
        """

    @overload
    def __init__(
        self,
        db: LINdb,
        *,
        genomeID: int,
        tax_type: Optional[str] = tax_enums.TaxType(1).name,
    ):
        """create an empty taxonomy connected to the workspace database

        Args:
                db (LINdb): database object
                tax_type (str, optional): type of taxonomy. Defaults to TaxType(1).
        """

    def __init__(
        self,
        db: LINdb,
        tax_type: Optional[str] = tax_enums.TaxType(1).name,
        sep=";",
        **kwargs,
    ):
        self.db = db
        self.tax_type = tax_enums.TaxType(1).name if tax_type is None else tax_type
        if kwargs.get("taxonomy_string") is not None:
            self.taxa_dict = self.parse_taxstring(
                kwargs.get("taxonomy_string", ""), sep=sep
            )
        elif kwargs.get("taxid_dict") is not None:
            # has format tax_lvl: taxid. e.g. {1:Bacteria, 2:Firmicutes, ... }
            self.set_ranks(kwargs.get("taxid_dict", {}) or {})
        elif kwargs.get("genomeID") is not None:
            taxon_df = db.get_taxonomy(
                kwargs.get("genomeID", 0), tax_type=self.tax_type
            )
            tax_table = self.RankID_name_table(tax_type=self.tax_type)
            for idx, row in taxon_df.iterrows():
                rankID = row["rankID"]
                if row["taxID"] is np.nan:
                    # taxon without a fixed ID in the DB use the string directly
                    self.set_rank(tax_table[rankID], row["taxon"])
                else:
                    self.taxid_dict[rankID] = row["taxID"]

    # if requested property part of levels then if empty lookup tax_type
    def get_rank(self, rank: Union[int, str], tax_type: Optional[str] = None) -> str:
        taxa = None
        if isinstance(rank, int):
            if (taxa := self.taxid_dict.get(rank)) is None:
                taxa = self.taxa_dict.get(rank)
        elif isinstance(rank, str):
            rankID = self.RankName_ID_table(tax_type=tax_type).get(rank)
            if rankID is None:
                pass
            elif (taxa := self.taxid_dict.get(rankID)) is None:
                taxa = self.taxa_dict.get(rankID)
        if taxa is None:
            raise RankTypeError(rank)
        if isinstance(taxa, int):
            return self.RankID_name_table()[taxa]
        return taxa

    def trace(self):
        """traverses and fills all of the taxonomy ranks based on the highest available rank level"""
        tax_lvl = max([*self.taxid_dict])
        if tax_lvl is None:
            return
        # ["taxID", "rankID", "taxon", "parent"]
        tax_df = self.db.traverse_taxonomy(
            self.taxid_dict[tax_lvl], tax_type=self.tax_type
        )
        rankID_taxon_dict = dict(zip(tax_df.rankID, tax_df.taxon))
        self.set_ranks(rankID_taxon_dict)

    def update_taxonomy(self, genomeID: int, clean=False):
        """updates the taxonomy for the given genome based on the taxonomy object being called on

        Args:
                genomeID (int): ID of the genome to add/update their taxonomy
                clean (bool, optional): all old taxonomies of the type will be removed
                before adding. Default False.
        """
        self.db.add_taxonomy(
            genomeID,
            tax_dict={**self.taxa_dict, **self.taxid_dict},
            tax_type=self.tax_type,
            clean=clean,
        )
