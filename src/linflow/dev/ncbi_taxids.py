#!/usr/bin/env python3
# With LOAD DATA (Recommended)
# sqlite3 ~/.etetoolkit/taxa.sqlite ".dump species" > taxa.sql
# tail -n +3 taxa.sql > tmp.txt && mv tmp.txt taxa.sql
# sqlite3 -header -csv taxa.sqlite "select taxid,parent,spname,common,rank from species;" > taxa.csv
# sed -i -e 's/""/NULL/g' -e 's/"no rank"/NULL/g' -e 's/,rank$/,rankName/g' taxa.csv
# mysql -e "DROP DATABASE IF EXISTS ncbi_taxa; CREATE DATABASE ncbi_taxa;SET GLOBAL local_infile=1;"
# mysql ncbi_taxa -e "$(head -1 ~/.etetoolkit/taxa.sql |sed -e 's/COLLATE NOCASE//g' -e 's/rank/rankName/g' -e 's/VARCHAR(50)/VARCHAR(100)/g' -e 's/, track TEXT//g')"
# bash data/db/load_file.sh ~/.etetoolkit/taxa.csv ncbi_taxa.species data/db/warn.log "taxid,parent,spname,common,rankName" "," ""

# Without LOAD DATA
# sqlite3 ~/.etetoolkit/taxa.sqlite ".dump species" > taxa.sql
# tail -n +3 taxa.sql > tmp.txt && mv tmp.txt taxa.sql
# sed -i -e "s/''/NULL/g" -e "s/'no rank'/NULL/g" -e "s/COLLATE NOCASE//g" -e "s/rank/rankName/g" -e "s/VARCHAR(50)/VARCHAR(100)/g" taxa.sql
# mysql -e "DROP DATABASE ncbi_taxa; CREATE DATABASE ncbi_taxa;"
# mysql ncbi_taxa < taxa.sql
# mysql ncbi_taxa -e "DROP COLUMN track FROM species;"

# Cleaning and indexing

# mysql ncbi_taxa -e "DROP COLUMN common FROM TABLE species;"
# mysql ncbi_taxa -e "ALTER TABLE species ADD INDEX (parent), ADD INDEX (rankName), ADD INDEX (spname), ADD INDEX (rankName, spname);"
# TODO: create keys and primary keys

import os
import subprocess
import sys

from ete3 import NCBITaxa
from ete3.ncbi_taxonomy.ncbiquery import update_db

from linflow.logger import get_logger

# Add the module's directory to sys.path
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "../..")))

DATABASE_NAME = "ncbi_taxa"
COMMANDS = f"""
#!/bin/bash
sqlite3 ~/.etetoolkit/taxa.sqlite ".dump species" > taxa.sql
tail -n +3 taxa.sql > tmp.txt && mv tmp.txt taxa.sql
sed -i -e "s/''/NULL/g" -e "s/'no rank'/NULL/g" -e "s/COLLATE NOCASE//g" -e "s/rank/rankName/g" -e "s/VARCHAR(50)/VARCHAR(100)/g" taxa.sql
mysql -e "DROP DATABASE {DATABASE_NAME}; CREATE DATABASE {DATABASE_NAME};"
mysql {DATABASE_NAME} < taxa.sql
mysql {DATABASE_NAME} -e "DROP COLUMN track FROM species;"
mysql {DATABASE_NAME} -e "DROP COLUMN common FROM TABLE species;"
mysql {DATABASE_NAME} -e "ALTER TABLE species ADD INDEX (parent), ADD INDEX (rankName), ADD INDEX (spname), ADD INDEX (rankName, spname);"
"""
COMMAND_FILE = "./ncbi_taxids.sh"

logger = get_logger(__name__)
NCBI_TAXID_DB_FILE = "data/db/"


def main():

    ncbi = NCBITaxa()
    update_db(ncbi.dbfile)

    with open(COMMAND_FILE, "w+") as file:
        file.write(COMMANDS)
    os.chmod(COMMAND_FILE, 0o775)
    process = subprocess.Popen(COMMAND_FILE, stdout=subprocess.PIPE, shell=True)
    output, error = process.communicate()
    os.remove(COMMAND_FILE)
    if error is not None and len(error) > 0:
        logger.error(error)
        return -1
    else:
        logger.debug(output)


if __name__ == "__main__":
    main()
