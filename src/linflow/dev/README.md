# 1.Testing

## 1.1 Installing the packages

You need to install the following packages:

```shell script
pip install pytest pytest-xdist black pylint
```

## 1.2 Testing the code

Use pytest to run the tests. Please keep in mind that running the test in parallel may cause some of them to fail however, the run itself will be much faster.

```shell script
#parallel run (8 threads)
pytest -n8 > pytest0.log 
#sequential run
pytest > pytest1.log
#running only failed tests
pytest --lf -n8
```
