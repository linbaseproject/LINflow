import os
import re

import ete3
import PyQt5

from . import logger as m_logger

# ete3 fix headless tree rendering
os.environ["QT_QPA_PLATFORM"] = "offscreen"


logger = m_logger.get_logger(__name__)


def rename_clades(clade):
    if not clade.clades:
        return
    else:
        clade.name = None
        for c in clade.clades:
            rename_clades(c)


# This method should be run in the "trees" directory under the workspace
# otherwise you need to provide the full path for each parameter (including annotation_file)
def draw_tree(newick: str, output_file: str, annotation: dict, directory: str = "."):
    t = ete3.Tree(os.path.join(newick), quoted_node_names=True)
    for leaf in t.iter_leaves():
        strain = re.sub("['\"]", "", leaf.name)
        leaf_annotation = ete3.TextFace(annotation[strain])
        leaf.add_face(leaf_annotation, 0, "aligned")

    t.render(os.path.join(directory, output_file))
