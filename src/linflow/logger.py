"""
Adapted from
https://www.toptal.com/python/in-depth-python-logging
"""

import logging
import sys

log_file_handlers: dict[str, logging.FileHandler] = {}
FORMATTER = logging.Formatter(
    "%(asctime)s — %(threadName)s:%(name)s — %(levelname)s — %(funcName)s:%(lineno)d — %(message)s"
)
LOG_FILE = "run.log"
console_logger = None
logger = None


def add_log_file(
    current_logger: logging.Logger, file_path: str, log_level=logging.DEBUG
):
    global log_file_handlers  # pylint: disable=global-variable-not-assigned
    new_file_logger = logging.FileHandler(file_path)
    log_file_handlers[file_path] = new_file_logger
    new_file_logger.setLevel(log_level)
    new_file_logger.setFormatter(FORMATTER)
    current_logger.addHandler(new_file_logger)


def add_log_console(current_logger: logging.Logger, log_level=logging.INFO):
    global console_logger
    main_console_logger = logging.StreamHandler()
    main_console_logger.setLevel(log_level)
    main_console_logger.setFormatter(FORMATTER)
    current_logger.addHandler(main_console_logger)
    console_logger = main_console_logger


def get_console_handler():
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(FORMATTER)
    return console_handler


def get_file_handler(file_path: str):
    file_handler = log_file_handlers[file_path]
    file_handler.setFormatter(FORMATTER)
    return file_handler


def get_logger(logger_name: str, log_level=logging.DEBUG):
    specific_logger = logging.getLogger(logger_name)
    specific_logger.setLevel(log_level)
    return specific_logger


def get_root_logger():
    global logger
    logger = logging.getLogger()
    logger.setLevel(logging.ERROR)
    return logger
