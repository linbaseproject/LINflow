"""Manages compression related processes for fasta files"""

import gzip
import hashlib
import os
import shutil
import tarfile
import time
from multiprocessing import Process
from multiprocessing.pool import AsyncResult
from typing import Tuple

from . import extensions, linflow
from . import logger as m_logger
from . import util

BLOCKSIZE = 65536
READING_MODES = {"gzip": "rb", "tar": "r:*", "raw": "rb", "targz": "r:gz"}
logger = m_logger.get_logger(__name__)


def uncompress(source: str, dest: str, ext_group=None):
    """Uncompresses given file and saves it at destination

    Args:
            source (str): path to compressed file
            dest (str): path to save the uncompressed file
            ext_group (str, optional): extension group identifying the compression software.
            Set to None to automatically detect. Defaults to None.

    Raises:
            FileNotFoundError: source is not a valid file
            tarfile.TarError: tar file is corrupted
    """
    if not os.path.isfile(source):
        raise FileNotFoundError("File '%s' not found for conversion" % source)
    if not ext_group:
        ext_group, _ = extensions.find_extension(source)
    start_time = time.time()
    if ext_group == "gzip":
        with open(dest, "wb+") as uncompressed:
            with gzip.open(source, "rb") as compressed:
                # when source file is not finished compressing (in pool)
                # a bit and retry for 6 times 60 seconds
                for _ in range(6):
                    try:
                        shutil.copyfileobj(compressed, uncompressed)
                        break
                    except EOFError:
                        time.sleep(10)
    elif ext_group == "tar":
        tar = tarfile.open(source, "r:*")
        file = tar.getmembers()[0]
        if file.isreg():
            tar.extract(file)
            os.rename(file.name, dest)
    else:
        raise tarfile.TarError("Failed to uncompress '%s" % source)
    logger.debug(
        "uncompressed '%s' to '%s'. took %s ",
        source,
        dest,
        util.elapsed_time(start_time),
    )


def is_gzip_file_valid(file_path):
    """Checks if a gzip file is valid"""
    if not os.path.isfile(file_path) or os.path.getsize(file_path) == 0:
        return False
    try:
        with gzip.open(file_path, "rb") as f:
            f.read(1)  # Read a small portion to trigger the integrity check
        return True  # File is valid
    except (OSError, gzip.BadGzipFile):
        return False  # File is corrupted


def compress(source: str, dest: str):
    """Compresses a file and saves it at the destination

    Args:
            source (str): path of the file to compress
            dest (str): path of the location to save the compressed file

    Raises:
            FileNotFoundError: file to compress is not found
    """
    if not os.path.isfile(source):
        raise FileNotFoundError("File '%s' not found for compression" % source)
    if os.path.isfile(dest) and is_gzip_file_valid(dest):
        # logger.debug("File '%s' already compressed and valid at '%s'", source, dest)
        return
    start_time = time.time()
    with open(source, "rb") as uncompressed:
        with gzip.open(dest, "wb+") as compressed:
            shutil.copyfileobj(uncompressed, compressed)  # type: ignore
    logger.debug(
        "compressed '%s' to '%s'. took %s ",
        source,
        dest,
        util.elapsed_time(start_time),
    )


def hash_file(filepath: str, hash_function=hashlib.sha256) -> str:
    """Hashed a raw file

    Args:
            filepath (str): path of file to hash
            hasher (class): class used to hash the file. Default: hashlib.sha256()

    Returns:
            str: hash of the file
    """

    hasher = hash_function
    with open(filepath, "rb") as file:
        buf = file.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = file.read(BLOCKSIZE)
    return hasher.hexdigest()


# pylint: disable=bad-open-mode
def uniform_hash_file(filepath: str, hash_function=hashlib.sha256) -> str:
    """Hashes a raw or compressed genome file

    Args:
            filepath (str): path of file to hash

    Raises:
            Exception: File extension not supported

    Returns:
            str: SHA256 hash of the file
    """
    hasher = hash_function()
    ext_group, _ = extensions.find_extension(filepath)
    if ext_group == "gzip":
        file = gzip.open(filepath, READING_MODES[ext_group])
    elif ext_group == "tar":
        tar = tarfile.open(name=filepath, mode=READING_MODES[ext_group])
        internal_file = tar.next()
        if internal_file is not None:
            file = tar.extractfile(internal_file)
        else:
            raise BaseException("Tar file '%s' did not contain any files" % filepath)
    elif ext_group == "raw":
        file = open(  # pylint: disable=unspecified-encoding
            filepath, READING_MODES[ext_group]
        )
    else:
        raise BaseException(
            "Extension of the file '%s' is not supported for hashing" % filepath
        )
    if file is None:
        raise BaseException("file was None")
    buf = file.read(BLOCKSIZE)
    while len(buf) > 0:
        hasher.update(buf)
        buf = file.read(BLOCKSIZE)
    file.close()
    return hasher.hexdigest()


def add_uniform_input(
    workspace: str, source: str, dest: str
) -> Tuple[str, bool, Process]:
    """Add genome file to Genomes file regardless of file type (see ACCEPTED_EXTENSIONS)

    Args:
            workspace (str): workspace the file has to be added to (used to checked already uncompressed file)
            source (str): input file path
            dest (str): uniformly compressed destination path

    Raises:
            Exception: Extension not supported

    Returns:
            str: path to a temporary extracted version of the file
            bool: did the source file need extraction
            Process: the process reference compressing the file
    """
    # from . import linflow as lf
    # used for waiting for asynchronous uncompressing step when input file is compressed
    group, _ = extensions.find_extension(source)

    source = os.path.realpath(source)
    dest = os.path.realpath(dest)
    tmp_fasta_id = extensions.remove_extension(dest, raise_exception=False)
    tmp_fasta = util.get_filepath(workspace, tmp_fasta_id, filetype="tmp")
    process = Process()

    # might cause issues if genomes are deleted and files are not properly cleaned up
    if os.path.isfile(dest) and os.path.isfile(tmp_fasta):
        return tmp_fasta, True, process

    # compression will be done in the background
    if group == "raw":
        logger.debug("Compressing file '%s'", source)
        shutil.copyfile(source, tmp_fasta)
        if not is_gzip_file_valid(dest):
            process = Process(target=compress, args=(source, dest))
        return source, False, process
    # wait for uncompress to finish and compress in the background
    elif group == "tar":
        uncompress(source, tmp_fasta, ext_group=group)
        logger.debug(
            "Uncompressed re-compressing file to '%s'",
            dest,
        )
        if not is_gzip_file_valid(dest):
            process = Process(target=compress, args=(tmp_fasta, dest))
        return tmp_fasta, True, process
    elif group == "gzip":
        uncompress(source, tmp_fasta, ext_group=group)
        logger.debug(
            "Uncompressed and re-compressing file to '%s'",
            dest,
        )
        shutil.copyfile(source, dest)
        return tmp_fasta, True, process
    else:
        raise BaseException("Extension not supported '%s'" % source)


def test_gzip(filepath: str) -> bool:
    """tests if a gzip file is not corrupted

    Args:
        filepath (str): full path of a gzip file to test

    Returns:
        bool: if the file integrity is valid
    """
    ok = True
    if not os.path.isfile(filepath):
        return False
    with gzip.open(filepath, "rb") as f:
        try:
            while f.read(BLOCKSIZE) != b"":
                pass
        except gzip.BadGzipFile:
            ok = False
    return ok
