CREATE TABLE IF NOT EXISTS Genome (
    genomeID            INT UNSIGNED AUTO_INCREMENT,
    filePath            VARCHAR(2047) NOT NULL,
    scaffolds           INT UNSIGNED NOT NULL,
    seqLength           INT UNSIGNED NOT NULL,
    fileHash            VARCHAR(65),
    addedAt           TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (fileHash),
    UNIQUE KEY (genomeID)
);$$

CREATE TABLE IF NOT EXISTS Filters (
    filterID            INT UNSIGNED AUTO_INCREMENT,
    filterName          VARCHAR(127) NOT NULL,
    minEffect           float,
    maxEffect           float,
    saveScores          INT UNSIGNED,
    compareType         VARCHAR(7) COMMENT 'in the format of 1-1 1-m m-1 m-m' ,
    measure             VARCHAR(255) DEFAULT NULL,
    reasonerF           VARCHAR(255) DEFAULT NULL,
    rankscoreF          VARCHAR(255) DEFAULT NULL,
    dargs               JSON DEFAULT NULL,
    PRIMARY KEY (filterName),
    UNIQUE KEY (filterID)
);$$

CREATE TABLE IF NOT EXISTS BaseScheme (
    baseSchemeID        INT UNSIGNED AUTO_INCREMENT,
    minEffect           float,
    maxEffect           float,
    step                float DEFAULT NULL,
    thresholds          JSON DEFAULT NULL,
    positions           Text DEFAULT NULL,
    positionCount       INT UNSIGNED,
    note                VARCHAR(127) DEFAULT NULL,
    PRIMARY KEY (baseSchemeID)
);$$

CREATE TABLE IF NOT EXISTS BaseScheme_Filters (
    baseSchemeID        INT UNSIGNED,
    filterID            INT UNSIGNED,
    idx                 INT UNSIGNED,
    FOREIGN KEY (baseSchemeID)  REFERENCES BaseScheme (baseSchemeID)    ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (filterID)      REFERENCES Filters (filterID)           ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (baseSchemeID, idx)
);$$


CREATE TABLE IF NOT EXISTS SchemeSet (
    schemeSetID         INT UNSIGNED AUTO_INCREMENT,
    blockCount          INT UNSIGNED NOT NULL,
    scoreF              VARCHAR(127) DEFAULT NULL,
    note                VARCHAR(511) DEFAULT NULL,
    PRIMARY KEY (schemeSetID)
);$$

CREATE TABLE IF NOT EXISTS SchemeSet_Base(
    schemeSetID         INT UNSIGNED,
    baseSchemeID        INT UNSIGNED,
    idx                 INT UNSIGNED,
    positionDepth       INT,
    FOREIGN KEY (baseSchemeID)  REFERENCES BaseScheme (baseSchemeID)    ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (schemeSetID)   REFERENCES SchemeSet (schemeSetID)      ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (schemeSetID, idx),
    KEY (baseSchemeID)
);$$


CREATE TABLE IF NOT EXISTS Comparison (
    queryID             INT UNSIGNED NOT NULL,
    refID               INT UNSIGNED NOT NULL,
    filterID            INT UNSIGNED NOT NULL,             
--  compareRank         INT UNSIGNED NOT NULL, -- could be computed by rankscore
    rankScore           float NOT NULL,
    measure1            float NOT NULL,
    measure2            float DEFAULT NULL,
    measure3            float DEFAULT NULL,
    measure4            float DEFAULT NULL,
    measureN            JSON DEFAULT NULL,
    FOREIGN KEY (queryID)       REFERENCES Genome (genomeID)            ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (refID)         REFERENCES Genome (genomeID)            ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (filterID)      REFERENCES Filters (filterID)           ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (queryID, refID, filterID),
    INDEX (queryID, filterID)
);$$

CREATE TABLE IF NOT EXISTS LINTree_back (
    genomeID            INT UNSIGNED NOT NULL,
    schemeSetID         INT UNSIGNED NOT NULL DEFAULT 1,
    idx                 INT UNSIGNED NOT NULL,
    lastValue           INT UNSIGNED NOT NULL,
    parent              INT UNSIGNED NOT NULL DEFAULT 1,
    neighbor            INT UNSIGNED NOT NULL DEFAULT 1,
    visible             TINYINT UNSIGNED NOT NULL DEFAULT 1,
    computedAt          TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (genomeID)      REFERENCES Genome (genomeID)            ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (parent)        REFERENCES Genome (genomeID)            ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (neighbor)      REFERENCES Genome (genomeID)            ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (schemeSetID)   REFERENCES SchemeSet (schemeSetID)      ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (genomeID, schemeSetID),
    INDEX (idx, lastValue, schemeSetID),
    INDEX (genomeID, parent, schemeSetID)
);$$

CREATE TABLE IF NOT EXISTS TaxType (
    taxTypeID              INT UNSIGNED AUTO_INCREMENT,
    typeName               VARCHAR(31) NOT NULL,
    PRIMARY KEY (taxTypeID),
    UNIQUE KEY (typeName)
);$$

CREATE TABLE IF NOT EXISTS TaxRank (
    rankID                INT UNSIGNED NOT NULL,
    rankName              VARCHAR(255) NOT NULL,
    taxTypeID             INT UNSIGNED NOT NULL,
    FOREIGN KEY (taxTypeID)     REFERENCES TaxType (taxTypeID)          ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (rankID, taxTypeID),
    UNIQUE KEY (rankName, taxTypeID)
);$$

CREATE TABLE IF NOT EXISTS TaxID (
    rankID              INT UNSIGNED NOT NULL,
    taxTypeID           INT UNSIGNED NOT NULL,
    taxID               INT UNSIGNED NOT NULL,
    parent              INT UNSIGNED DEFAULT NULL,
    taxon               VARCHAR(255) NOT NULL,
    INDEX (taxID),
    FOREIGN KEY (rankID)            REFERENCES TaxRank (rankID)         ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (taxTypeID)         REFERENCES TaxType (taxTypeID)      ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (parent)            REFERENCES TaxID (taxID)            ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (rankID, taxTypeID, taxID),
    INDEX (taxTypeID, taxID),
    INDEX (rankID, taxID),
    INDEX (rankID, taxon),
    INDEX (taxon)
);$$


CREATE OR REPLACE VIEW LINTree AS SELECT * FROM LINTree_back WHERE Visible > 0;$$

