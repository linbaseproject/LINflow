import copy
from typing import Optional, Tuple

import numpy as np
import numpy.typing as npt

from . import logger as m_logger
from . import sourmash_custom4, sourmash_sbt
from .filter import Filter

logger = m_logger.get_logger(__name__)
# >95%, >80% >70% ANI for k=21
SBT_SEARCH_THRESHOLDS = [0.15, 0.048, 0.0001]


def default_rankscore(
    using_filter: Filter, scores: np.ndarray, column: Optional[int] = None
):
    """computes rank scores, but does not sort, based on a fixed result column (e.g. percentage_identity)
    Args:
            using_filter (Filter): defined ani filter with specific args
            scores (np.ndarray): 2d list of genomes(row) and scaled scores(columns)
            column (int): column to return as rankscore

    Returns:
            list[list]: 2d list of ranked genomes(row) and scores(columns)
    """
    if not column:
        column = using_filter.get_arg("rankscore_column", 0)
    if scores.size < 1:
        return np.array([[]], dtype=float)
    return scores[:, column]


def sourmash_ani_rankscore(using_filter: Filter, scores: np.ndarray):
    rankscore_column = using_filter.get_arg("rankscore_column", 0)
    include_false_positives = using_filter.get_arg("include_false_positives", True)
    if scores.size < 1:
        return np.array([[]], dtype=float)
    if include_false_positives:
        return scores[:, rankscore_column] * np.logical_not(scores[:, 2])
    return scores[:, rankscore_column]


def pyani_rankscore(using_filter: Filter, scores: np.ndarray, default=0.0):
    if scores.size < 1:
        return scores
    cov = using_filter.get_arg("coverage_threshold", 20)
    rankscore = copy.deepcopy(scores[:, 0])
    if cov:
        for i, score in enumerate(scores):
            if score[1] < cov:
                rankscore[i] = default
    return rankscore


def sourmash_reasoner_representatives(
    using_filter: Filter,
    gid: int,  # pylint: disable=unused-argument
    ref_genome_ids: npt.NDArray[np.uintc],
    scores: npt.NDArray[np.floating],
) -> Tuple[bool, npt.NDArray[np.uintc], npt.NDArray[np.floating]]:
    """Reasons when to greedily stop the sourmash process on the representative level when genomes are very distant

    Args:
            using_filter (Filter): the filter object that is used to make reasoning (has alterable args)
            gid (int): ID of the query genome
            ref_genome_ids (np.ndarray[np.uintc]): IDs of the reference genomes
            scores (np.ndarray): 2d array of computed scores genomes(row) and scores(columns)

    Returns:
            np.ndarray: ranked 2d array of computed scores genomes(row) and scores(columns)
            np.ndarray: ranked IDs of the reference genomes based on scores
    """
    greedy_stop = False
    if len(ref_genome_ids) < 1:
        return True, ref_genome_ids, scores
    # 0.2475 >= jaccard_similarity > 0.0025
    threshold = using_filter.get_arg("60%_threshold", 0.00015)
    if threshold is None:
        return True, ref_genome_ids, scores
    main_score = scores[:, 0]
    # too distant to closest genome (stop computation and assign lin)
    if np.count_nonzero(main_score < threshold):
        greedy_stop = True
    return greedy_stop, ref_genome_ids, scores


def sourmash_reasoner_lingroups(
    using_filter: Filter,
    gid: int,
    ref_genome_ids: npt.NDArray[np.uintc],
    scores: npt.NDArray[np.floating],
):
    """Reasons when to re-run the sourmash process on the lingroups level when genomes are very similar (high Jaccard)

    Args:
            using_filter (Filter): the filter object that is used to make reasoning
            (has alterable args)
            gid (int): ID of the query genome
            ref_genome_ids (np.ndarray[np.uintc]): IDs of the reference genomes
            scores (np.ndarray): 2d array of scaled scores genomes(row) and scores(columns)

    Returns:
            np.ndarray: ranked 2d array of reasoned scaled scores genomes(row) and scores(columns)
            np.ndarray: ranked IDs of the reference genomes based on scores
    """
    greedy_stop = False
    if ref_genome_ids.size < 1:
        return True, ref_genome_ids, scores
    # 0.2475 >= jaccard_similarity > 0.0025
    threshold = using_filter.get_arg(
        "k51_threshold", sourmash_custom4.DEFAULT_K51_THRESHOLD
    )
    if threshold is None:
        return False, ref_genome_ids, scores

    main_score = scores[:, 0]
    # too close to closest genomes (increase precision)
    new_kwargs = copy.deepcopy(using_filter.default_args)
    new_kwargs.update({"k": 51})  # type: ignore
    if np.count_nonzero(main_score > threshold) >= 2:
        new_scores = using_filter.compare(np.array([gid]), ref_genome_ids, **new_kwargs)
        scores = using_filter.scale_scores(new_scores)
    return greedy_stop, ref_genome_ids, scores


def sourmash_sbt_reasoner_lingroups(
    using_filter: Filter,
    gid: int,
    ref_genome_ids: npt.NDArray[np.uintc],
    scores: npt.NDArray[np.floating],
):
    """Reasons when to re-run the sbt search process on the lingroups level when genomes are very similar (high Jaccard)

    Args:
            using_filter (Filter): the filter object that is used to make reasoning
            (has alterable args)
            gid (int): ID of the query genome
            ref_genome_ids (np.ndarray[np.uintc]): IDs of the reference genomes
            scores (np.ndarray): 2d array of scaled scores genomes(row) and scores(columns)

    Returns:
            np.ndarray: ranked 2d array of reasoned scaled scores genomes(row) and scores(columns)
            np.ndarray: ranked IDs of the reference genomes based on scores
    """
    greedy_stop = False
    usable_scores = np.array([np.any(e >= 0) for e in scores], dtype=np.bool_)
    if np.any(usable_scores) and using_filter.get_arg("sbt_threshold", 0) > 0.1:
        # too close to closest genomes (increase precision)
        new_kwargs = copy.deepcopy(using_filter.default_args)
        new_threshold = (
            using_filter.get_arg(
                "sbt_threshold_k51", sourmash_sbt.DEFAULT_K51_SBT_THRESHOLD
            )
            or sourmash_sbt.DEFAULT_K51_SBT_THRESHOLD
        )
        new_kwargs.update({"k_mer": 51, "sbt_threshold": new_threshold})  # type: ignore

        new_scores = using_filter.compare(np.array([gid]), ref_genome_ids, **new_kwargs)
        usable_scores = np.array([np.any(e > 0) for e in new_scores], dtype=np.bool_)
        if np.any(usable_scores):
            scores = using_filter.scale_scores(new_scores)
        return greedy_stop, ref_genome_ids, scores

    # 0.2475 >= jaccard_similarity > 0.0025
    # 0.15 >= jaccard_similarity > 0.00015
    progressive_thresholds = (
        using_filter.get_arg("progressive_thresholds", SBT_SEARCH_THRESHOLDS)
        or SBT_SEARCH_THRESHOLDS
    )
    for threshold in progressive_thresholds[1:]:
        # too far from genomes (decrease precision)
        new_kwargs = copy.deepcopy(using_filter.default_args)
        new_kwargs.update(
            {"k_mer": using_filter.get_arg("k_mer", 21), "sbt_threshold": threshold}
        )
        new_scores = using_filter.compare(np.array([gid]), ref_genome_ids, **new_kwargs)

        usable_scores = np.array([np.any(e > 0) for e in new_scores], dtype=np.bool_)
        if not np.any(usable_scores):
            continue

        scores = using_filter.scale_scores(new_scores)
        return greedy_stop, ref_genome_ids, scores

    return greedy_stop, ref_genome_ids, scores
