import numpy as np

from . import lindb
from .ani_custom import PyANI
from .filter import Filter
from .filter_helpers import (  # pylint: disable=unused-import
    SBT_SEARCH_THRESHOLDS,
    default_rankscore,
    pyani_rankscore,
    sourmash_ani_rankscore,
    sourmash_reasoner_lingroups,
    sourmash_reasoner_representatives,
    sourmash_sbt_reasoner_lingroups,
)
from .scheme import BaseScheme, SchemeSet
from .sourmash_custom4 import SourmashSigFilter
from .sourmash_sbt import SourmashSBT


def initialize_schemes(db: lindb.LINdb, **kwargs):

    # Defining the usable measures in LINflow
    # Filter 1
    ANI_FILTER = Filter(
        db,
        name="ani",
        min=70.0,
        max=99.9999,
        type="1-1",
        measure=PyANI,
        rankscore_function=pyani_rankscore,
        default_args={
            "use_anib": True,
            "scale": [100, 100],
            "coverage_threshold": 20,
            "timeout_delay": kwargs.get("timeout_delay"),
        },
        nscores_to_save=3,
    )
    # Filter 2
    SCALED_SOURMASH_REP_FILTER = Filter(
        db,
        name="scale_sourmash_rep",
        min=70.0,
        max=99.0,
        type="1-m",
        measure=SourmashSigFilter,
        reasoner_function=sourmash_reasoner_representatives,
        default_args={
            "k_mer": 21,
            "60%_threshold": 0.00015,
            "shared_filter": [3, 4],
            "scaled_sketch": 1000,
        },
        nscores_to_save=3,
    )
    # Filter 3
    SCALED_SOURMASH_SPECIES_FILTER = Filter(
        db,
        name="scale_sourmash_lingroup",
        min=70.0,
        max=99.0,
        type="1-m",
        measure=SourmashSigFilter,
        reasoner_function=sourmash_reasoner_lingroups,
        default_args={
            "k_mer": 21,
            "k51_threshold": 0.15,
            "shared_filter": [2, 4],
            "scaled_sketch": 1000,
        },
        nscores_to_save=3,
    )
    # Filter 4
    SCALED_SOURMASH2LIN_FILTER = Filter(
        db,
        name="scale_sourmash2lin",
        min=70.0,
        max=99.0,
        type="1-m",
        measure=SourmashSigFilter,
        rankscore_function=sourmash_ani_rankscore,
        reasoner_function=sourmash_reasoner_lingroups,
        default_args={
            "k_mer": 21,
            "k51_threshold": 0.15,
            "shared_filter": [2, 3],
            "scaled_sketch": 1000,
            "scale": [1, 100, 1, 1, 1],
            "rankscore_column": 1,
        },
        nscores_to_save=3,
    )
    # Filter 5
    NUM_SOURMASH_REP_FILTER = Filter(
        db,
        name="num_sourmash_rep",
        min=70.0,
        max=99.0,
        type="1-m",
        measure=SourmashSigFilter,
        reasoner_function=sourmash_reasoner_representatives,
        default_args={
            "k_mer": 21,
            "60%_threshold": 0.00015,
            "shared_filter": [6],
            "num_sketch": 2000,
        },
        nscores_to_save=3,
    )
    # Filter 6
    NUM_SOURMASH_SPECIES_FILTER = Filter(
        db,
        name="num_sourmash_lingroup",
        min=70.0,
        max=99.0,
        type="1-m",
        measure=SourmashSigFilter,
        reasoner_function=sourmash_reasoner_lingroups,
        default_args={
            "k_mer": 21,
            "k51_threshold": 0.15,
            "shared_filter": [5],
            "num_sketch": 2000,
        },
        nscores_to_save=3,
    )
    # Filter 7
    SCALED_SOURMASH_SBT_REP_FILTER = Filter(
        db,
        name="scale_sourmash_sbt_jaccard_rep",
        min=70.0,
        max=99.0,
        type="1-m",
        measure=SourmashSBT,
        reasoner_function=sourmash_sbt_reasoner_lingroups,
        default_args={
            "k_mer": 21,
            "db_k_mers": [21, 51],
            "sbt_threshold": 0.15,
            "progressive_thresholds": [0.15, 0.048, 0.0001],
            "scaled_sketch": 1000,
            "include_false_positives": False,
        },
        nscores_to_save=3,
    )
    # Filter 8
    SCALED_SOURMASH_SBT_LINGROUP_FILTER = Filter(
        db,
        name="scale_sourmash_sbt_jaccard_lingroup",
        min=70.0,
        max=99.0,
        type="1-m",
        measure=SourmashSBT,
        reasoner_function=sourmash_sbt_reasoner_lingroups,
        default_args={
            "k_mer": 21,
            "db_k_mers": [21, 51],
            "sbt_threshold": 0.15,
            "progressive_thresholds": [0.15, 0.048, 0.0001],
            "scaled_sketch": 1000,
            "include_false_positives": False,
        },
        nscores_to_save=3,
    )

    # default LINbase scheme
    s1b1 = BaseScheme(
        db,
        min=70,
        max=95,
        step=5,
        thresholds=[3],
        filters=[SCALED_SOURMASH_REP_FILTER],
    )
    s1b2 = BaseScheme(
        db,
        positions=np.array(
            [
                70,
                75,
                80,
                85,
                90,
                95,
                96,
                97,
                98,
                98.5,
                99,
                99.25,
                99.5,
                99.75,
                99.9,
                99.925,
                99.95,
                99.975,
                99.99,
                99.999,
            ],
            dtype=float,
        ),
        filters=[SCALED_SOURMASH_SPECIES_FILTER, ANI_FILTER],
        thresholds=[3, 1],
    )
    # SchemeSet1: LINbase scheme with 3 lingroup searches and 3 ANI computations
    ss1 = SchemeSet(db, scheme_blocks=[s1b1, s1b2], descend_levels=np.array([5, -1]))

    # 300, 3k and 300k length schemes
    s2b1 = BaseScheme(
        db,
        min=70,
        max=95,
        step=0.1,
        thresholds=[1],
        filters=[SCALED_SOURMASH_REP_FILTER],
    )
    s2b2 = BaseScheme(
        db,
        min=70,
        max=99.9,
        step=0.1,
        filters=[SCALED_SOURMASH_SPECIES_FILTER, ANI_FILTER],
        thresholds=[3, 1],
    )
    # SchemeSet2: 3k long scheme from 70 to 99.9
    SchemeSet(
        db,
        scheme_blocks=[s2b1, s2b2],
        descend_levels=np.array([s2b1.length, -1]),
    )

    s3b1 = BaseScheme(
        db,
        min=70,
        max=95,
        step=0.01,
        thresholds=[1],
        filters=[SCALED_SOURMASH_REP_FILTER],
    )
    s3b2 = BaseScheme(
        db,
        min=70,
        max=99.99,
        step=0.01,
        filters=[SCALED_SOURMASH_SPECIES_FILTER, ANI_FILTER],
        thresholds=[3, 1],
    )
    # SchemeSet3: 30k long scheme from 70 to 99.99
    SchemeSet(
        db,
        scheme_blocks=[s3b1, s3b2],
        descend_levels=np.array([s3b1.length, -1]),
    )

    s4b1 = BaseScheme(
        db,
        min=70,
        max=95,
        step=0.001,
        thresholds=[1],
        filters=[SCALED_SOURMASH_REP_FILTER],
    )
    s4b2 = BaseScheme(
        db,
        min=70,
        max=99.999,
        step=0.001,
        filters=[SCALED_SOURMASH_SPECIES_FILTER, ANI_FILTER],
        thresholds=[3, 1],
    )
    # SchemeSet4: 300k long scheme from 70 to 99.999
    SchemeSet(
        db,
        scheme_blocks=[s4b1, s4b2],
        descend_levels=np.array([s4b1.length, -1]),
    )

    s5b2 = BaseScheme(
        db,
        positions=np.array(
            [
                70,
                75,
                80,
                85,
                90,
                95,
                96,
                97,
                98,
                98.5,
                99,
                99.25,
                99.5,
                99.75,
                99.9,
                99.925,
                99.95,
                99.975,
                99.99,
                99.999,
            ],
            dtype=float,
        ),
        filters=[SCALED_SOURMASH2LIN_FILTER],
        thresholds=[1],
    )
    # SchemeSet5: 2 layer scaled sourmash k21 and k21 OR 51 based on how close the genomes are
    ss5 = SchemeSet(db, scheme_blocks=[s1b1, s5b2], descend_levels=np.array([5, -1]))
    # SchemeSet6: 1 layer scaled sourmash k21 OR 51 based on how close the genomes are
    ss6 = SchemeSet(db, scheme_blocks=[s5b2], descend_levels=np.array([-1]))

    # default LINbase scheme
    s7b1 = BaseScheme(
        db,
        min=70,
        max=95,
        step=5,
        thresholds=[3],
        filters=[NUM_SOURMASH_REP_FILTER],
    )
    s7b2 = BaseScheme(
        db,
        positions=np.array(
            [
                70,
                75,
                80,
                85,
                90,
                95,
                96,
                97,
                98,
                98.5,
                99,
                99.25,
                99.5,
                99.75,
                99.9,
                99.925,
                99.95,
                99.975,
                99.99,
                99.999,
            ],
            dtype=float,
        ),
        filters=[NUM_SOURMASH_SPECIES_FILTER, ANI_FILTER],
        thresholds=[3, 1],
    )
    # SchemeSet7: Exact LINbase.org scheme
    ss7 = SchemeSet(db, scheme_blocks=[s7b1, s7b2], descend_levels=np.array([5, -1]))

    s8b1 = BaseScheme(
        db,
        min=70,
        max=95,
        step=5,
        thresholds=[3],
        filters=[SCALED_SOURMASH_SBT_REP_FILTER],
    )
    s8b2 = BaseScheme(
        db,
        positions=np.array(
            [
                70,
                75,
                80,
                85,
                90,
                95,
                96,
                97,
                98,
                98.5,
                99,
                99.25,
                99.5,
                99.75,
                99.9,
                99.925,
                99.95,
                99.975,
                99.99,
                99.999,
            ],
            dtype=float,
        ),
        filters=[SCALED_SOURMASH_SPECIES_FILTER, ANI_FILTER],
        thresholds=[3, 1],
    )
    # SchemeSet8: LINbase scheme with 3 lingroup SBT searches and 3 ANI computations
    ss8 = SchemeSet(db, scheme_blocks=[s8b1, s8b2], descend_levels=np.array([5, -1]))
