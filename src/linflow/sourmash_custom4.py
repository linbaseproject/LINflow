import copy
import os
import time
from typing import Optional, Union

import numpy as np
import pandas as pd
import screed
import sourmash
import sourmash.sketchcomparison as sour_comp
from sourmash.logging import set_quiet

from . import exceptions
from . import logger as m_logger
from . import measure_base, util

# from sourmash.index import MultiIndex

DEFAULT_KMER = 21
DEFAULT_KMERS = [21, 31, 51]
DEFAULT_K51_THRESHOLD = 0.15
DEFAULT_K21_THRESHOLD = 0.00015
logger = m_logger.get_logger(__name__)
set_quiet(True)


class SourmashSigFilter(measure_base.Measure):
    @classmethod
    def create_sketch(
        cls,
        filepath,
        dest,
        num_sketch: int = 0,
        scaled_sketch=1000,
        **kwargs,
    ) -> list[sourmash.SourmashSignature]:
        signatures = []
        start_time = time.time()
        try:
            kmer_size = np.unique(
                copy.deepcopy(DEFAULT_KMERS) + [kwargs.get("k_mer", DEFAULT_KMER)]
            )
            for k in kmer_size:
                if num_sketch > 0:
                    mh = sourmash.MinHash(n=num_sketch, ksize=k)
                else:
                    mh = sourmash.MinHash(n=0, ksize=k, scaled=scaled_sketch)

                record = screed.read_fasta_sequences(filepath)
                with screed.open(filepath) as seqfile:
                    for _, record in enumerate(seqfile):
                        mh.add_sequence(record.sequence, True)

                signatures += [sourmash.SourmashSignature(mh, name=dest)]

            with open(dest, "w+") as fp:
                sourmash.save_signatures(signatures, fp)
            logger.debug(
                "sketch created for file '%s'. took %s",
                filepath,
                util.elapsed_time(start_time),
            )
            return signatures

        except Exception as e:
            logger.error("Sketch creation failed for file '%s'", filepath)
            logger.exception(e)
            raise e

    @classmethod
    def compare_sketch(
        cls,
        query: Union[str, sourmash.SourmashSignature],
        refs: list[str],
        k_mer=DEFAULT_KMER,
        **kwargs,
    ):
        sig_start_time = time.time()
        results = []

        if isinstance(query, str):
            query_sig = cls.load_signatures([query], k_mer=k_mer, **kwargs)[0]

        elif not isinstance(query, sourmash.SourmashSignature):
            e = Exception(
                "The query can be a signature path or a SourmashSignature was %s"
                % query
            )
            logger.exception(e)
            raise e
        else:
            query_sig = query
        mh1 = query_sig.minhash

        ref_sigs = cls.load_signatures(refs, query_sig, **kwargs)
        if mh1.num > 0:
            result_cols = [
                "signature",
                "jaccard",
                "ignore_abundance",
            ]
        else:
            result_cols = [
                "signature",
                "jaccard",
                "avg_containment_ani",
                "potential_false_negative",
                "avg_containment",
                "max_containment",
            ]

        for ref_sig in ref_sigs:
            mh2 = ref_sig.minhash
            if mh1.num > 0:
                cmpr = sour_comp.NumMinHashComparison(mh1, mh2)
                results += [(ref_sig.name, cmpr.jaccard, cmpr.ignore_abundance)]
                continue
            cmpr = sour_comp.FracMinHashComparison(mh1, mh2)
            results += [
                (
                    ref_sig.name,
                    cmpr.jaccard,
                    cmpr.avg_containment_ani,
                    cmpr.potential_false_negative,
                    cmpr.avg_containment,
                    cmpr.max_containment,
                )
            ]
        df = pd.DataFrame(results, columns=result_cols)
        logger.debug(
            "Compared '%s' to %d signatures. args %s. took %s",
            query_sig.name,
            len(refs),
            {"k": k_mer, **kwargs},
            util.elapsed_time(sig_start_time),
        )
        return df.fillna(-1)

    @classmethod
    def load_signatures(
        cls,
        sig_paths: list[str],
        query: Optional[sourmash.SourmashSignature] = None,
        k_mer=DEFAULT_KMER,
        skip_errors=False,
        **kwargs,
    ) -> list[sourmash.SourmashSignature]:
        sigs = []
        if not sig_paths:
            return sigs

        if query:
            with util.Suppressor():
                ref_sigs = sourmash.sourmash_args.load_dbs_and_sigs(
                    sig_paths, query, True
                )
            for ref_man in ref_sigs:
                if not ref_man:
                    exceptions.handle_exception(
                        BaseException(
                            "The ref signature path was not a file %s"
                            % ref_man.__dict__
                        ),
                        logger,
                        raise_exception=(not skip_errors),
                    )
                sigs.append(next(ref_man.signatures()))
        else:
            for path in sig_paths:
                try:
                    sig_file = sourmash.load_file_as_index(path)
                    if sig_file:
                        db = sig_file.select(ksize=k_mer).signatures()
                    else:
                        exceptions.handle_exception(
                            BaseException(
                                "signature file '%s' could not be loaded" % sig_file
                            ),
                            logger,
                            raise_exception=False,
                        )
                        return sigs
                    sigs.append(next(db))
                except StopIteration as e:
                    exceptions.handle_exception(
                        e,
                        logger,
                        f"signature of the appropriate size {k_mer} was not found in {path}",
                        raise_exception=(not skip_errors),
                    )
        return sigs

    @classmethod
    def compare(
        cls, ref: list[int], query: int, workspace: str, **kwargs
    ) -> np.ndarray:
        query_path = cls.filepath(workspace, query)
        ref_paths = [cls.filepath(workspace, refID) for refID in ref]
        return cls.compare_sketch(query_path, ref_paths, **kwargs).to_numpy()

    @classmethod
    def create(
        cls, workspace: str, genomeIDs: list[int], force=False, **kwargs
    ) -> list:
        result = []
        for genomeID in genomeIDs:
            filepath = cls.filepath(workspace, genomeID, create=True)
            # this avoids creating a new signature if one already exists, but if used in
            # an unclear workspace could use the wrong signature.
            if not os.path.isfile(filepath) or force:
                tmp_fasta = util.get_fasta(workspace, genomeID)
                result += [cls.create_sketch(tmp_fasta, filepath, **kwargs)[0]]
            else:
                result += [cls.load_signatures([filepath], **kwargs)[0]]
        return result

    @classmethod
    def filetype(cls):
        return "sig"
