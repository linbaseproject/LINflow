DROP FUNCTION IF EXISTS casefix_taxon;$$
CREATE FUNCTION casefix_taxon (to_upper BOOLEAN, taxon1 VARCHAR(255), taxon2 VARCHAR(255))
    RETURNS VARCHAR(255) DETERMINISTIC
    RETURN IF(to_upper, 
    CONCAT(UCASE(LEFT(COALESCE(taxon1,taxon2), 1)),SUBSTRING(COALESCE(taxon1,taxon2), 2)),
    COALESCE(taxon1,taxon2)
    );$$

-- Triggers to move genome taxonomy (strain only) to genomerxiv genome attributes

DROP PROCEDURE IF EXISTS add_attribute_strain;$$
CREATE PROCEDURE add_attribute_strain (rankID INT UNSIGNED, genomeID INT UNSIGNED, taxon VARCHAR(255))
    BEGIN
        DECLARE strain_attID INT UNSIGNED DEFAULT 30;
        SET strain_attID = CASE
            WHEN "strain" IN (SELECT rankName FROM TaxRank WHERE rankID=rankID) THEN NULL
            WHEN (SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = DATABASE() AND table_name ='Genome_Attribute') < 1 THEN NULL
            ELSE (SELECT MAX(attID) FROM Attribute WHERE attName="strain")
            END;
        IF strain_attID IS NOT NULL        
        THEN
            INSERT INTO Genome_Attribute (attID, genomeID, attValue) 
            VALUES (strain_attID, genomeID, taxon);
        END IF;
    END;$$

DROP TRIGGER IF EXISTS strain_taxon_attribute_ncbi;$$
CREATE TRIGGER strain_taxon_attribute_ncbi AFTER INSERT ON NCBI_taxon
    FOR EACH ROW 
    BEGIN
        CALL add_attribute_strain(NEW.rankID, NEW.genomeID, NEW.taxon);
    END;$$

DROP TRIGGER IF EXISTS strain_taxon_attribute_gtdb;$$
CREATE TRIGGER strain_taxon_attribute_gtdb AFTER INSERT ON GTDB_taxon
    FOR EACH ROW 
    BEGIN
        CALL add_attribute_strain(NEW.rankID, NEW.genomeID, NEW.taxon);
    END;$$

-- adds prefix root nodes for each defined scheme
DROP PROCEDURE IF EXISTS prefix_init;$$
CREATE PROCEDURE prefix_init(IN db_name VARCHAR(500))
 BEGIN
   IF (SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = db_name AND table_name ='Prefix') > 0 
   THEN INSERT INTO Prefix (schemeSetID, idx, lastValue, parent) SELECT schemeSetID,NULL,0,NULL FROM SchemeSet; 
   END IF;
END;$$

-- LINGROUP DLIN PROCESSES
-- procedure and triggers to create a DLIN table for LINgroups 
DROP PROCEDURE IF EXISTS lingroup_dlin_proc;$$
CREATE PROCEDURE lingroup_dlin_proc (prefixID_val INT UNSIGNED, schemeSetID_val INT UNSIGNED)
    BEGIN
        DECLARE dlin_val JSON;
        DECLARE parents_val JSON;
        WITH RECURSIVE get_parent3 (originID, nodeID, parent, i, val, schemeSetID) AS (
                SELECT prefixID as originID, prefixID, parent, idx, lastValue, schemeSetID
                FROM Prefix WHERE prefixID = prefixID_val AND schemeSetID=schemeSetID_val
                UNION ALL
                SELECT get_parent3.originID, c.prefixID, c.parent, c.idx, c.lastValue, c.schemeSetID
                FROM Prefix c 
                INNER JOIN get_parent3 ON c.prefixID = get_parent3.parent AND c.schemeSetID=get_parent3.schemeSetID AND idx>-1)
        SELECT originID AS prefixID, schemeSetID, JSON_OBJECTAGG(i, val), JSON_OBJECTAGG(i, parent) 
            INTO prefixID_val, schemeSetID_val, dlin_val, parents_val
        FROM get_parent3
        WHERE i IS NOT NULL AND val IS NOT NULL 
        GROUP BY originID, schemeSetID;

        REPLACE INTO LINgroup_DLIN (lingroupID, prefixID, dlin, parents) 
        SELECT lingroupID, prefixID_val, dlin_val, parents_val
        FROM LINgroup_Prefix WHERE prefixID=prefixID_val;
    END;$$

-- on each new lingroup=prefix link finds the assigned dlin
DROP TRIGGER IF EXISTS lingroup_dlin_tracker;$$
CREATE TRIGGER lingroup_dlin_tracker AFTER INSERT ON LINgroup_Prefix
    FOR EACH ROW 
    BEGIN
        CALL lingroup_dlin_proc(NEW.prefixID, NEW.schemeSetID);
    END;$$


-- inserts new dlins if a prefix's dlin is changes (triggers an automatic update). This updates affected children too
DROP PROCEDURE IF EXISTS update_prefix_children_dlin;$$
CREATE PROCEDURE update_prefix_children_dlin (prefixID_val INT UNSIGNED, schemeSetID_val INT UNSIGNED)
    BEGIN
            REPLACE INTO LINgroup_Prefix(lingroupID, prefixID, schemeSetID)
            WITH RECURSIVE get_child (prefixID, parent, idx, lastValue, lvl, schemeSetID) AS (
                SELECT prefixID, parent, idx, lastValue, 0 as lvl, schemeSetID
                FROM Prefix 
                WHERE prefixID = prefixID_val AND schemeSetID=schemeSetID_val
                UNION ALL
                SELECT c.prefixID, c.parent, c.idx, c.lastValue, get_child.lvl + 1, c.schemeSetID 
                FROM Prefix c
                INNER JOIN get_child ON c.parent = get_child.prefixID AND c.schemeSetID=get_child.schemeSetID
            )
            SELECT lingroupID, c.prefixID, c.schemeSetID FROM get_child c
                JOIN LINgroup_Prefix USING (prefixID, schemeSetID);
    END;$$

-- updates all dlins if a prefix's dlin changes (usually happens manually). This updates affected children too
DROP TRIGGER IF EXISTS lingroup_dlin_tracker_up;$$
CREATE TRIGGER lingroup_dlin_tracker_up AFTER UPDATE ON Prefix
    FOR EACH ROW 
    BEGIN
        CALL update_prefix_children_dlin(NEW.prefixID, NEW.schemeSetID);
    END;$$

-- removes dlins of a lingroup-prefix link is removed
DROP TRIGGER IF EXISTS lingroup_dlin_tracker_del;$$
CREATE TRIGGER lingroup_dlin_tracker_del AFTER DELETE ON LINgroup_Prefix
    FOR EACH ROW 
    BEGIN
        DELETE FROM LINgroup_DLIN 
        WHERE lingroupID=OLD.lingroupID AND prefixID=OLD.prefixID;
    END;$$

-- LINGROUP LINEAGE PROCESSES
-- procedure and prefixes to create a JSON lineage for each LINgroup
DROP PROCEDURE IF EXISTS lingroup_lineage_proc;$$
CREATE PROCEDURE lingroup_lineage_proc (lingroupID_val INT UNSIGNED, taxTypeID_val INT UNSIGNED)
    BEGIN
        DECLARE maxRank_val INT UNSIGNED;
        DECLARE lineage_val JSON;
        DECLARE lineage2_val JSON;
        WITH RECURSIVE get_parent2 (taxID, parent, rankID, taxTypeID, customTaxon, taxon) AS (
            SELECT tt.taxID, COALESCE(lt.parent, tt.parent), lt.rankID, lt.taxTypeID, lt.customTaxon, tt.taxon
            FROM LINgroup_Taxon lt 
            LEFT JOIN TaxID tt ON lt.rankID = tt.rankID AND tt.taxTypeID=lt.taxTypeID AND tt.taxID = lt.taxID  
            WHERE lt.lingroupID = lingroupID_val AND lt.taxTypeID = taxTypeID_val
            UNION ALL
            SELECT tt.taxID, tt.parent, 
                    tt.rankID, tt.taxTypeID, gp2.customTaxon, tt.taxon
            FROM TaxID tt
            INNER JOIN get_parent2 gp2 ON tt.taxID = gp2.parent AND tt.taxTypeID = gp2.taxTypeID AND tt.rankID>0)
        SELECT lingroupID_val AS lingroupID, gp.taxTypeID, MAX(gp.rankID) AS maxRank, 
            JSON_OBJECTAGG(gp.rankID, casefix_taxon(tr.upper, gp.taxon, gp.customTaxon)) AS lineage, 
            JSON_OBJECTAGG(casefix_taxon(tr.upper,tr.rankName,NULL), casefix_taxon(tr.upper, gp.taxon, gp.customTaxon)) AS lineage2
            INTO lingroupID_val, taxTypeID_val, maxRank_val, lineage_val, lineage2_val
        FROM get_parent2 gp
        LEFT JOIN TaxRank tr ON gp.rankID=tr.rankID AND gp.taxTypeID=tr.taxTypeID
        GROUP BY lingroupID, gp.taxTypeID
        ORDER BY lingroupID ASC, gp.taxTypeID ASC;

        -- if statement necessary to avoid null lineages being inserted (they are not allowed)
        IF lineage_val IS NOT NULL THEN
            REPLACE INTO LINgroup_Lineage (lingroupID, taxTypeID, maxRank, lineage, lineage2) 
                VALUES (lingroupID_val, taxTypeID_val, maxRank_val, lineage_val, lineage2_val)
                ;
        END IF;
    END;$$

-- adds lineages per lingroup-taxonomy link
DROP TRIGGER IF EXISTS lingroup_lineage_tracker;$$
CREATE TRIGGER lingroup_lineage_tracker AFTER INSERT ON LINgroup_Taxon
    FOR EACH ROW 
    BEGIN
        CALL lingroup_lineage_proc(NEW.lingroupID, NEW.taxTypeID);
    END;$$

-- updates lineages when a lingroup-taxonomy link is updated
DROP TRIGGER IF EXISTS lingroup_lineage_tracker_up;$$
CREATE TRIGGER lingroup_lineage_tracker_up AFTER UPDATE ON LINgroup_Taxon
    FOR EACH ROW 
    BEGIN
        DELETE FROM LINgroup_Lineage WHERE lingroupID=OLD.lingroupID AND taxTypeID=OLD.taxTypeID;
        CALL lingroup_lineage_proc(NEW.lingroupID, NEW.taxTypeID);
    END;$$

-- removes lineages when a lingroup-taxonomy link is deleted
DROP TRIGGER IF EXISTS lingroup_lineage_tracker_del;$$
CREATE TRIGGER lingroup_lineage_tracker_del AFTER DELETE ON LINgroup_Taxon
    FOR EACH ROW 
    BEGIN
        DELETE FROM LINgroup_Lineage WHERE lingroupID=OLD.lingroupID AND taxTypeID=OLD.taxTypeID;
        CALL lingroup_lineage_proc(OLD.lingroupID, OLD.taxTypeID);
    END;$$

-- GENOME DLIN PROCESSES
-- process to generate a DLIN for one genome based on its parents
DROP PROCEDURE IF EXISTS genome_dlin_proc;$$
CREATE PROCEDURE genome_dlin_proc (genomeID_val INT UNSIGNED, schemeSetID_val INT UNSIGNED)
    BEGIN
        REPLACE INTO DLIN (genomeID, schemeSetID, dlin, parents)
            WITH RECURSIVE get_parent3 (originID, nodeID, parent, i, val, schemeSetID) AS (
                    SELECT genomeID as originID, genomeID, parent, idx, lastValue, schemeSetID
                    FROM LINTree
                    WHERE genomeID=genomeID_val AND schemeSetID=schemeSetID_val
                    UNION ALL
                    SELECT get_parent3.originID, c.genomeID, c.parent, c.idx, c.lastValue, c.schemeSetID
                    FROM LINTree c
                    INNER JOIN get_parent3 ON c.genomeID = get_parent3.parent AND c.schemeSetID=get_parent3.schemeSetID)
            SELECT originID AS genomeID, schemeSetID, JSON_OBJECTAGG(i, val) AS dlin, JSON_OBJECTAGG(i, parent) AS parents
            FROM get_parent3
            GROUP BY originID, schemeSetID
            ORDER BY originID ASC;
    END;$$

-- procedure that updates all children genome DLINs of a given genomeID
DROP PROCEDURE IF EXISTS update_genome_children_dlin;$$
CREATE PROCEDURE update_genome_children_dlin (genomeID_val INT UNSIGNED, schemeSetID_val INT UNSIGNED)
    BEGIN
        DECLARE dlin_val JSON;
        DECLARE parents_val JSON;
        SELECT dlin, parents INTO dlin_val, parents_val
        FROM DLIN
        WHERE genomeID = genomeID_val AND schemeSetID=schemeSetID_val;

        REPLACE INTO DLIN(genomeID, schemeSetID, dlin, parents)
            WITH RECURSIVE get_child (genomeID, parent, idx, lastValue, lvl, schemeSetID, dlin, parents) AS (
                SELECT genomeID, parent, idx, lastValue, 0 as lvl, schemeSetID, dlin_val, parents_val
                FROM LINTree 
                WHERE genomeID = genomeID_val AND schemeSetID=schemeSetID_val
                UNION ALL
                SELECT c.genomeID, c.parent, c.idx, c.lastValue, get_child.lvl + 1, c.schemeSetID, 
                JSON_MERGE_PATCH(get_child.dlin, JSON_OBJECT(c.idx, c.lastValue)) AS dlin,
                JSON_MERGE_PATCH(get_child.parents, JSON_OBJECT(c.idx, c.parent)) AS parents
                FROM LINTree c
                INNER JOIN get_child ON c.parent = get_child.genomeID AND c.schemeSetID=get_child.schemeSetID
            )
            SELECT genomeID, schemeSetID, dlin, parents
            FROM get_child;
    END;$$


-- on a new insert to LINTree generate its dlin 
DROP TRIGGER IF EXISTS genome_dlin_in;$$
CREATE TRIGGER genome_dlin_in AFTER INSERT ON LINTree_back
    FOR EACH ROW 
    BEGIN
        CALL genome_dlin_proc(NEW.genomeID, NEW.schemeSetID);
    END;$$

-- on a new insert to LINTree generate its dlin again (update NEW.genomeID's dlin)
-- and update its children dlins based on the new parent dlin (NEW.genomeID)
DROP TRIGGER IF EXISTS genome_dlin_up;$$
CREATE TRIGGER genome_dlin_up AFTER UPDATE ON LINTree_back
    FOR EACH ROW 
    BEGIN
        CALL genome_dlin_proc(NEW.genomeID, NEW.schemeSetID);
        CALL update_genome_children_dlin(NEW.genomeID, NEW.schemeSetID);
    END;$$

-- PREFIX_DLIN PROCESSES
-- process to generate a DLIN for one prefix based on its parents
DROP PROCEDURE IF EXISTS prefix_dlin_proc;$$
CREATE PROCEDURE prefix_dlin_proc (prefixID_val INT UNSIGNED, schemeSetID_val INT UNSIGNED)
    BEGIN
        REPLACE INTO Prefix_DLIN (prefixID, schemeSetID, dlin, parents)
            WITH RECURSIVE get_parent3 (originID, nodeID, parent, i, val, schemeSetID) AS (
                    SELECT prefixID as originID, prefixID, parent, idx, lastValue, schemeSetID
                    FROM Prefix
                    WHERE prefixID=prefixID_val AND schemeSetID=schemeSetID_val
                    UNION ALL
                    SELECT get_parent3.originID, c.prefixID, c.parent, c.idx, c.lastValue, c.schemeSetID
                    FROM Prefix c
                    INNER JOIN get_parent3 ON c.prefixID = get_parent3.parent AND c.schemeSetID=get_parent3.schemeSetID
                    AND c.parent IS NOT NULL)
            SELECT originID AS prefixID, schemeSetID, JSON_OBJECTAGG(i, val) AS dlin, JSON_OBJECTAGG(i, parent) AS parents
            FROM get_parent3
            GROUP BY originID, schemeSetID
            ORDER BY originID ASC;
    END;$$

-- procedure that updates all children prefix DLINs of a given prefixID
DROP PROCEDURE IF EXISTS update_prefix_children_dlin;$$
CREATE PROCEDURE update_prefix_children_dlin (prefixID_val INT UNSIGNED, schemeSetID_val INT UNSIGNED)
    BEGIN
        DECLARE dlin_val JSON;
        DECLARE parents_val JSON;
        SELECT dlin, parents INTO dlin_val, parents_val
        FROM Prefix_DLIN
        WHERE prefixID = prefixID_val AND schemeSetID=schemeSetID_val;

        REPLACE INTO Prefix_DLIN(prefixID, schemeSetID, dlin, parents)
            WITH RECURSIVE get_child (prefixID, parent, idx, lastValue, lvl, schemeSetID, dlin, parents) AS (
                SELECT prefixID, parent, idx, lastValue, 0 as lvl, schemeSetID, dlin_val, parents_val
                FROM Prefix 
                WHERE prefixID = prefixID_val AND schemeSetID=schemeSetID_val
                UNION ALL
                SELECT c.prefixID, c.parent, c.idx, c.lastValue, get_child.lvl + 1, c.schemeSetID, 
                JSON_MERGE_PATCH(get_child.dlin, JSON_OBJECT(c.idx, c.lastValue)) AS dlin,
                JSON_MERGE_PATCH(get_child.parents, JSON_OBJECT(c.idx, c.parent)) AS parents
                FROM Prefix c
                INNER JOIN get_child ON c.parent = get_child.prefixID AND c.schemeSetID=get_child.schemeSetID
            )
            SELECT prefixID, schemeSetID, dlin, parents
            FROM get_child;
    END;$$

-- on a new insert to Prefix generate its dlin 
DROP TRIGGER IF EXISTS prefix_dlin_in;$$
CREATE TRIGGER prefix_dlin_in AFTER INSERT ON Prefix
    FOR EACH ROW 
    BEGIN
        IF NEW.parent IS NOT NULL THEN
            CALL prefix_dlin_proc(NEW.prefixID, NEW.schemeSetID);
        END IF;
    END;$$

-- on a new insert to LINTree generate its dlin again (update NEW.prefixID's dlin)
-- and update its children dlins based on the new parent dlin (NEW.prefixID)
DROP TRIGGER IF EXISTS prefix_dlin_up;$$
CREATE TRIGGER prefix_dlin_up AFTER UPDATE ON Prefix
    FOR EACH ROW 
    BEGIN
        CALL prefix_dlin_proc(NEW.prefixID, NEW.schemeSetID);
        CALL update_prefix_children_dlin(NEW.prefixID, NEW.schemeSetID);
    END;$$


-- SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Custom error';
-- VIEWS

CREATE OR REPLACE VIEW LINTree AS 
SELECT * FROM LINTree_back WHERE Visible > 0;$$


CREATE OR REPLACE VIEW LINgroup_Lineage2 AS
WITH RECURSIVE get_parent2 (lingroupID, taxID, parent, rankID, taxTypeID, customTaxon, taxon) AS (
            SELECT lingroupID, taxID, COALESCE(lt.parent, tt.parent), rankID, taxTypeID, customTaxon, taxon
            FROM LINgroup_Taxon lt LEFT JOIN TaxID tt USING (taxID, rankID, taxTypeID)
            UNION ALL
            SELECT get_parent2.lingroupID, tt.taxID, COALESCE(lt.parent, tt.parent), 
                    tt.rankID, tt.taxTypeID, lt.customTaxon, tt.taxon
            FROM TaxID tt
            INNER JOIN get_parent2 ON tt.taxID = get_parent2.parent AND tt.taxTypeID = get_parent2.taxTypeID AND tt.rankID>0
            LEFT JOIN LINgroup_Taxon lt ON tt.rankID=lt.rankID AND tt.taxTypeID=lt.taxTypeID AND tt.taxID = lt.taxID)
SELECT lingroupID, gp.taxTypeID, MAX(gp.rankID) AS maxRank, 
    JSON_OBJECTAGG(gp.rankID, casefix_taxon(tr.upper, taxon, customTaxon)) AS lineage, 
    JSON_OBJECTAGG(casefix_taxon(tr.upper,rankName,NULL), casefix_taxon(tr.upper, taxon, customTaxon)) as lineage2
FROM get_parent2 gp
LEFT JOIN TaxRank tr ON gp.rankID=tr.rankID AND gp.taxTypeID=tr.taxTypeID
GROUP BY lingroupID, taxTypeID
ORDER BY lingroupID ASC, taxTypeID ASC;$$


CREATE OR REPLACE VIEW LINgroup_DLIN2 AS
    WITH RECURSIVE get_parent3 (originID, nodeID, parent, i, val, schemeSetID) AS (
                SELECT prefixID as originID, prefixID, parent, idx, lastValue, schemeSetID
                FROM Prefix
                UNION ALL
                SELECT get_parent3.originID, c.prefixID, c.parent, c.idx, c.lastValue, c.schemeSetID
                FROM Prefix c 
                INNER JOIN get_parent3 ON c.prefixID = get_parent3.parent AND c.schemeSetID=get_parent3.schemeSetID AND idx>-1)
    SELECT lingroupID, prefixID, dlin, parents FROM
        (SELECT originID AS prefixID, schemeSetID, JSON_OBJECTAGG(i, val) AS dlin, JSON_OBJECTAGG(i, parent) AS parents
        FROM get_parent3
        WHERE i IS NOT NULL AND val IS NOT NULL 
        GROUP BY originID, schemeSetID
        ORDER BY originID ASC) lingroup_dlin
    JOIN LINgroup_Prefix lp USING (prefixID);$$

CREATE OR REPLACE VIEW DLIN2 AS
    WITH RECURSIVE get_parent3 (originID, nodeID, parent, i, val, schemeSetID) AS (
                SELECT genomeID as originID, genomeID, parent, idx, lastValue, schemeSetID
                FROM LINTree
                UNION ALL
                SELECT get_parent3.originID, c.genomeID, c.parent, c.idx, c.lastValue, c.schemeSetID
                FROM LINTree c
                INNER JOIN get_parent3 ON c.genomeID = get_parent3.parent AND c.schemeSetID=get_parent3.schemeSetID)
    SELECT originID AS genomeID, schemeSetID, JSON_OBJECTAGG(i, val) AS dlin, JSON_OBJECTAGG(i, parent) AS parents
    FROM get_parent3
    GROUP BY originID, schemeSetID
    ORDER BY originID ASC;$$



CREATE OR REPLACE VIEW Genome_Attribute_List AS
    SELECT genomeID, COALESCE(JSON_OBJECTAGG(IFNULL(attName, 'N/A'), IFNULL(attValue, 'N/A'))) AS Attributes_JSON
    FROM 
        (SELECT Genome.genomeID AS genomeID, att.attID AS attID, att.attName AS attName, ga.attValue AS attValue
        FROM Genome 
        LEFT JOIN Genome_Attribute ga ON Genome.genomeID=ga.genomeID
        LEFT JOIN Attribute att ON ga.attID=att.attID) AS JoinedTable 
    WHERE attName IS NOT NULL AND attValue IS NOT NULL
    GROUP BY genomeID;$$


CREATE OR REPLACE VIEW Prefix_DLIN2 AS
    WITH RECURSIVE get_parent3 (originID, nodeID, parent, i, val, schemeSetID) AS (
                SELECT prefixID as originID, prefixID, parent, idx, lastValue, schemeSetID
                FROM Prefix WHERE Prefix.parent IS NOT NULL
                UNION ALL
                SELECT get_parent3.originID, c.prefixID, c.parent, c.idx, c.lastValue, c.schemeSetID
                FROM Prefix c
                INNER JOIN get_parent3 ON c.prefixID = get_parent3.parent AND c.schemeSetID=get_parent3.schemeSetID AND c.parent IS NOT NULL)
    SELECT originID AS prefixID, schemeSetID, JSON_OBJECTAGG(i, val) AS dlin, JSON_OBJECTAGG(i, parent) AS parents
    FROM get_parent3
    GROUP BY originID, schemeSetID
    ORDER BY originID ASC;$$
