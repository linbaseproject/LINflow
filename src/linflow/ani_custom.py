import os
import shutil
import time
import uuid

import numpy as np
import pyani.anib
import pyani.anim
import pyani.pyani_config
import pyani.pyani_files
import pyani.run_multiprocessing as run_mp

from . import exceptions, extensions
from . import logger as m_logger
from . import measure_base, util

logger = m_logger.get_logger(__name__)
logger.addFilter(util.downgrade_info_logs)

NUCMER_EXE_PATH = shutil.which("nucmer")
DELTAFILTER_EXE_PATH = shutil.which("delta-filter")
if NUCMER_EXE_PATH is None or DELTAFILTER_EXE_PATH is None:
    exceptions.handle_exception(
        FileNotFoundError(
            "The ANI executables were not found. "
            "Please check your PyANI installation."
            "NUCMER_EXE_PATH = '%s'\t"
            "DELTAFILTER_EXE_PATH = '%s'" % (NUCMER_EXE_PATH, DELTAFILTER_EXE_PATH)
        ),
        logger,
        raise_exception=False,
    )


class PyANI(measure_base.Measure):
    """ANI implementation of the Measure class for use as a filter in LINflow

    Args:
            Measure (Class): Inherits the abstract class Measure

    """

    @classmethod
    def anim_timeout(cls, queue, joblist, custom_logger=logger):
        """tries ANIm calculation multiple times before failing
            (avoids infinite loops during multiprocessing)

        Args:
                queue (_type_): _description_
                joblist (_type_): _description_
                custom_logger (_type_, optional): _description_. Defaults to logger.
        """
        ret_val = run_mp.run_dependency_graph(joblist, logger=custom_logger)
        queue.put(ret_val)

    @classmethod
    def anib_timeout(cls, queue, jobgraph, custom_logger=logger):
        """_summary_

        Args:
                queue (_type_): _description_
                jobgraph (_type_): _description_
                custom_logger (_type_, optional): _description_. Defaults to logger.
        """
        # run BLAST jobs in multiprocessing mode and return the cumulative exit status
        cumval = run_mp.run_dependency_graph(jobgraph, logger=custom_logger)
        queue.put(cumval)

    @classmethod
    def pyani_pairwise_adapter(
        cls, ref_file_path, query_file_path, use_anib=True, **kwargs
    ):
        """_summary_

        Args:
                ref_file_path (_type_): _description_
                query_file_path (_type_): _description_
                use_anib (bool, optional): _description_. Defaults to True.

        Raises:
                e: _description_
                Exception: _description_
                Exception: _description_

        Returns:
                _type_: _description_
        """
        ani_start_time = time.time()
        results = [0, 0]
        logger.debug("Comparing %s to %s", ref_file_path, query_file_path)
        workspace = kwargs.get("workspace", "")
        timeout_delay = kwargs.get("timeout_delay", 1200)

        infiles = []
        task_working_dir = os.path.join(
            util.workspace_dir(workspace), str(uuid.uuid4())
        )
        outdir = (
            os.path.join(task_working_dir, pyani.pyani_config.ALIGNDIR["ANIb"])
            if use_anib
            else os.path.join(task_working_dir, pyani.pyani_config.ALIGNDIR["ANIm"])
        )
        os.makedirs(task_working_dir, exist_ok=True)
        os.makedirs(outdir, exist_ok=True)

        # get decompressed files for comparison
        for path in [ref_file_path, query_file_path]:
            group, _ = extensions.find_extension(path)
            fastaID = extensions.remove_extension(path)
            if group == "raw":
                infiles.append(path)
            elif group == "tar" or group == "gzip":
                for attempt in range(6):
                    if os.path.getsize(path) == 0:
                        time.sleep(30)
                        logger.debug("Waiting for compression to complete %s", attempt)
                        if attempt >= 5:
                            logger.error(
                                "Decompression failed on empty file '%s'", path
                            )
                        continue
                    try:
                        fasta_path = util.get_fasta(workspace, fastaID)
                        infiles.append(fasta_path)
                        break
                    except FileNotFoundError as ex:
                        logger.debug("Decompression failed %s", attempt)
                        time.time.sleep(15)
                        if attempt >= 2:
                            logger.error("Decompression failed")
                            raise ex
                    raise FileNotFoundError(
                        "Decompression failed critically %s" % attempt
                    )
        infile_lengths = pyani.pyani_files.get_sequence_lengths(infiles)

        if len(infiles) < 2:
            logger.error("Not enough files to compare")
            return np.array([ref_file_path, *results], dtype=object)

        if use_anib:
            logger.debug("Using ANIb for comparison")
            # create fragmented files
            fragfiles, _ = pyani.anib.fragment_fasta_files(
                infiles, outdir, pyani.pyani_config.FRAGSIZE
            )
            # create BLAST jobs
            jobgraph = pyani.anib.make_job_graph(
                infiles, fragfiles, pyani.anib.make_blastcmd_builder("ANIb", outdir)
            )
            # run BLAST jobs in multiprocessing mode and return the cumulative exit status
            # cumval = run_mp.run_dependency_graph(jobgraph, logger=logger)
            cumval = util.timeout_function(
                run_mp.run_dependency_graph,
                (jobgraph,),
                "ANIb",
                timeout=timeout_delay,
                logger=logger,
            )
            if cumval is None or cumval > 1:
                logger.error("At least one BLAST run failed.")
                raise BaseException("At least one BLAST run failed.")

            # process BLAST data
            try:
                data = pyani.anib.process_blast(
                    outdir,
                    infile_lengths,
                    fraglengths=pyani.pyani_config.FRAGSIZE,
                    mode="ANIb",
                )
                results = [0, 0]
                for dfr, filestem in data.data:
                    if "ANIb_percentage_identity" in filestem:
                        results[0] = (dfr.iat[0, 1] + dfr.iat[1, 0]) / 2
                    elif "ANIb_alignment_coverage" in filestem:
                        results[1] = (dfr.iat[0, 1] + dfr.iat[1, 0]) / 2
                results = tuple(results)
            except (ZeroDivisionError, KeyError) as exception:
                exceptions.handle_exception(
                    exception,
                    logger,
                    f"ANIb calculation failed between reference: '{ref_file_path}' "
                    "and query '{query_file_path}'",
                )
        else:
            logger.debug("Using ANIm for comparison")
            # create list of jobs with dependencies
            joblist = pyani.anim.generate_nucmer_jobs(
                infiles,
                task_working_dir,
                nucmer_exe=NUCMER_EXE_PATH,  # type: ignore
                filter_exe=DELTAFILTER_EXE_PATH,  # type: ignore
                maxmatch=False,
                jobprefix="ANI",
            )
            # compute cumulative exit state
            logger.debug("Running ANIm jobs")
            try:
                cum_exit_val = util.timeout_function(
                    run_mp.run_dependency_graph,
                    (joblist,),
                    "ANIm",
                    timeout=timeout_delay,
                    logger=logger,
                )

                if cum_exit_val:
                    logger.error(
                        "%s vs %s alignment failed.\n "
                        "This is alternatively due to NUCmer run failure,\n"
                        "analysis will continue, but please investigate.",
                        ref_file_path,
                        query_file_path,
                    )
                logger.debug("ANIm alignment Done")
                stats = pyani.anim.process_deltadir(
                    outdir, infile_lengths, logger=logger
                )
                logger.debug("ANIm sequence filtering Done")
                if stats.zero_error:
                    # ref and query cannot be aligned (very low coverage)
                    pass
                elif len(stats.percentage_identity) < 2:
                    # happens when both files have the same source/name => results = 1.0
                    results = (
                        stats.percentage_identity.iat[0, 0],
                        stats.alignment_coverage.iat[0, 0],
                    )
                else:
                    results = (
                        stats.percentage_identity.iat[1, 0],
                        stats.alignment_coverage.iat[1, 0],
                    )
            except BaseException as exception:
                logger.error(exception)
                logger.error(
                    "ANIm calculation failed between reference: '%s' and query '%s'",
                    ref_file_path,
                    query_file_path,
                )

        if os.path.isdir(task_working_dir):
            util.rmtree2(task_working_dir, ignore_errors=True)
        logger.debug("Finished ANI comparison in %s", util.elapsed_time(ani_start_time))
        return np.array([ref_file_path, *results], dtype=object)

    @classmethod
    def compare(cls, ref: int, query: int, workspace: str, **kwargs) -> np.ndarray:
        kwargs = {"workspace": workspace, **kwargs}
        ref_path = cls.filepath(workspace, ref)
        query_path = cls.filepath(workspace, query)
        return np.array(cls.pyani_pairwise_adapter(ref_path, query_path, **kwargs))

    @classmethod
    def filetype(cls):
        return "fasta"

    @classmethod
    def create(cls, workspace: str, genomeIDs: list[int], **kwargs) -> list:
        return [
            cls.filepath(workspace, genomeID, create=True) for genomeID in genomeIDs
        ]
