import copy
import os
import shutil
from collections.abc import Iterable
from typing import Any, Optional, Union, overload
from uuid import uuid4

import numpy as np
import numpy.typing as npt

import linflow.compression
import linflow.exceptions
import linflow.extensions
import linflow.linflow
import linflow.logger
import linflow.util
from linflow.lin import LIN
from linflow.lindb import LINdb
from linflow.scheme import SchemeSet
from linflow.taxonomy import Taxonomy

logger = linflow.logger.get_logger(__name__)


class Signature:
    """Class to keep the Signature as an object"""

    _filepath: str = ""
    _id: int = 0
    _hash: str = ""
    is_duplicate = False
    _lins: dict[int, LIN] = {}
    type = "signature"
    rankScore = 0.0
    neighbor = 0

    @overload
    def __init__(self, db: LINdb, *, signature_id: int): ...

    @overload
    def __init__(
        self,
        db: LINdb,
        *,
        filepath: str,
        lins: Optional[dict[int, dict[int, int]]] = None,
    ): ...

    def __init__(self, db: LINdb, **kwargs):
        self.db = db
        # self.lins: dict[int, dict[int, int]] = kwargs.get("lins", {})
        self._filepath = kwargs.get("filepath", "")
        if (signature_id := kwargs.get("signature_id")) is not None:
            self._id = signature_id
            signature_dict = db.get_signature(signature_id)
            if not signature_dict.empty:
                kwargs.update(
                    {
                        str(k).lower(): v
                        for k, v in signature_dict.iloc[0].to_dict().items()
                    }
                )
        elif (filepath := kwargs.get("filepath")) is None or not os.path.isfile(
            filepath
        ):
            linflow.exceptions.handle_exception(
                FileNotFoundError("Signature file not found %s" % filepath),
                logger,
            )
        for field in ["filepath", "lins"]:
            if (val := kwargs.get(field)) is not None:
                self.__setattr__(f"_{field}", val)

        self.filename = os.path.basename(self._filepath)
        self._hash = linflow.compression.uniform_hash_file(self._filepath)

    def __eq__(self, other):
        if not isinstance(other, Signature):
            raise TypeError(
                "Comparing equality between different types '%s' and '%s'"
                % (type(self), type(other))
            )
        if self.id and self.id == other.id:
            return True
        elif self.filepath and linflow.compression.uniform_hash_file(
            self.filepath
        ) == linflow.compression.uniform_hash_file(
            linflow.util.get_filepath(self.db.db_name, other.id, create=False)
        ):
            return True
        return False

    def to_dict(self) -> dict[str, Any]:
        result = copy.copy(self.__dict__)
        if not isinstance(self.id, int):
            return result
        if "db" in result:
            result["db"] = result["db"].db_name
        # Add other signature-specific logic here
        result["type"] = "signature"
        return result

    @property
    def filepath(self) -> str:
        if self._filepath:
            return self._filepath
        elif self._id:
            path_df = self.db.get_signature_path(self._id)
            if not path_df.empty:
                self._filepath = path_df.iat[0, path_df.columns.get_loc("filepath")]
                self.filename = os.path.basename(self._filepath)
            return self._filepath
        return ""

    @filepath.setter
    def filepath(self, filepath: str):
        self._filepath = filepath

    @property
    def id(self) -> int:
        if not self._id:
            if self.filepath:
                df = self.db.get_signatureID(self.filepath)
                if not df.empty:
                    self._id = int(df.iat[0, df.columns.get_loc("signatureID")])
                    self._filepath = df.iat[0, df.columns.get_loc("filePath")]
                    self._signature_count = df.iat[0, df.columns.get_loc("signatures")]
                    self._hash = df.iat[0, df.columns.get_loc("fileHash")]
        return self._id

    @id.setter
    def id(self, signature_id: int):
        self._id = signature_id

    @property
    def lins(self) -> dict[int, dict]:
        if not self._lins:
            return {}
        return self._lins

    @lins.setter
    def lins(self, value: dict[int, LIN]):
        if not isinstance(value, dict):
            raise TypeError("lins must be a dictionary")
        for key, val in value.items():
            if not isinstance(key, int) or not isinstance(val, LIN):
                raise TypeError("lins must be a dict[int, LIN]")
            value[key] = value[key].__dict__
        self._lins = value

    @property
    def hash(self):
        if not self._hash:
            try:
                self._hash = linflow.compression.uniform_hash_file(self.filepath)
            except Exception:
                self.fill_signature_properties()
        return self._hash

    def process_file(self):
        self.filename = os.path.basename(self.filepath)
        tmp_signatureID = str(uuid4())
        tmp_signature_path = linflow.util.get_filepath(
            self.db.db_name, tmp_signatureID, "sig", create=True
        )
        tmp_signature, extracted = linflow.compression.add_uniform_input(
            self.db.db_name, self.filepath, tmp_signature_path
        )
        self._hash = linflow.compression.uniform_hash_file(tmp_signature)
        # self._signature_count, self._signature_sizes = linflow.util.read_signature_info(
        #     tmp_signature
        # )
        # self.id = self.db.add_signature(
        #     self.filepath, self.signature_count, self.sequence_length, hash=self.hash
        # )
        # linbase_uncompressed_file_path = linflow.util.get_filepath(
        #     workspace=self.db.db_name, signatureID=self.id, filetype="tmp", create=True
        # )
        # linbase_compressed_file_path = linflow.util.get_filepath(
        #     workspace=self.db.db_name, signatureID=self.id, filetype="sig", create=True
        # )

        linbase_uncompressed_file_path = linflow.util.get_filepath(
            workspace=self.db.db_name, genomeID=7, filetype="tmp", create=True
        )
        linbase_compressed_file_path = linflow.util.get_filepath(
            workspace=self.db.db_name, genomeID=7, filetype="sig", create=True
        )

        shutil.copyfile(tmp_signature, linbase_uncompressed_file_path)
        if os.path.realpath(self.filepath) != os.path.realpath(tmp_signature):
            os.remove(tmp_signature)
        linflow.linflow.POOL.apply_async(
            linflow.util.rename_complete_file,
            args=(tmp_signature_path, linbase_compressed_file_path),
        )
        tmp_uuid_signature = linflow.util.get_filepath(
            workspace=self.db.db_name, genomeID=tmp_signatureID, filetype="tmp"
        )
        if os.path.isfile(tmp_uuid_signature):
            os.remove(tmp_uuid_signature)
        return linbase_uncompressed_file_path, extracted

    def remove(self):
        self.db.remove_signature(self.id)

    def get_signature(self, signatureID: Optional[Union[int, str]] = None):
        if self._id:
            signatureID = self._id
        elif not signatureID:
            return ""

        saved_file_location = linflow.util.get_filepath(
            self.db.db_name, signatureID, "sig"
        )
        tmp_file_location = linflow.util.get_filepath(
            self.db.db_name, signatureID, "tmp"
        )

        if os.path.isfile(saved_file_location):
            if not os.path.isfile(tmp_file_location):
                linflow.compression.uncompress(saved_file_location, tmp_file_location)
        else:
            linflow.exceptions.handle_exception(
                FileNotFoundError(
                    "File '%s' not found as signature source" % saved_file_location
                ),
                logger,
            )
        return tmp_file_location

    def fill_signature_properties(self):
        # Implement this method to fill signature properties
        pass

    # Add other signature-specific methods here
