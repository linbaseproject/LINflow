from abc import ABC, abstractmethod

import numpy as np

from . import logger as m_logger
from . import util

logger = m_logger.get_logger(__name__)


class Measure(ABC):
    """Abstract class for measure definitions. All public functions should be @classmethod"""

    @classmethod
    @abstractmethod
    def filetype(cls) -> str:
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def create(cls, workspace: str, genomeIDs: list[int], **kwargs) -> list:
        raise NotImplementedError

    @classmethod
    @abstractmethod
    def compare(cls, ref: list, query, workspace: str, **kwargs) -> np.ndarray:
        raise NotImplementedError

    @classmethod
    def filepath(
        cls, workspace: str, genomeID: int, filetype=None, create=True, **kwargs
    ) -> str:  # pylint: disable=unused-argument
        filetype = cls.filetype() if filetype is None else filetype
        return util.get_filepath(workspace, genomeID, filetype, create=create)

    @classmethod
    def logger(cls):
        return logger

    @classmethod
    def path2id(cls, path: str, keep_extension=False):
        if keep_extension:
            return path.split("/")[-1]
        return path.split("/")[-1].replace(f".{cls.filetype()}", "")

    @classmethod
    def finalize(cls, **kwargs):
        """function to run before before program ends. E.g. cleanup or database commit, etc."""
