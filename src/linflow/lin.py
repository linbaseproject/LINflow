from typing import Optional, Union, overload

import numpy as np
import numpy.typing as npt
from pandas import DataFrame

from . import logger as m_logger
from . import util
from .lindb import LINdb
from .scheme import SchemeSet

logger = m_logger.get_logger(__name__)


class LIN:
    scheme: SchemeSet
    _lin: Union[npt.NDArray[np.uintc], list[int]] = []
    _dlin: dict[int, int] = {}
    visible = True
    genome_id: int = 0

    @overload
    def __init__(
        self,
        db: LINdb,
        *,
        scheme: SchemeSet,
        lin: Union[npt.NDArray[np.uintc], list[int]],
        dlin: Optional[dict[int, int]] = None,
        genome_id: int = 0,
        idx: int = -1,
        visible=True,
        del_reason="",
    ): ...

    @overload
    def __init__(
        self, db: LINdb, *, genome_id: int, schemeSetID=1, visibleOnly=True, **kwargs
    ): ...

    @overload
    def __init__(
        self,
        db: LINdb,
        *,
        lin: Union[npt.NDArray[np.intc], list[int]],
        dlin: Optional[dict[int, int]] = None,
        schemeSetID: int = 1,
        scheme: Optional[SchemeSet] = None,
        visible=True,
    ): ...

    def __init__(self, db: LINdb, *, schemeSetID=1, visible=True, **kwargs):
        self.db = db
        if kwargs.get("scheme") is not None:
            for field in ["lin", "idx", "visible", "del_reason", "genome_id", "scheme"]:
                self.__setattr__(field, kwargs.get(field))
            self._set_lin_init(**kwargs)

        if (genomeID := kwargs.get("genome_id")) is not None:
            self.genome_id = genomeID
            self.scheme = SchemeSet(db, schemeID=schemeSetID, **kwargs)
            lin_df = self.db.get_lin_parents(self.genome_id, schemeID=schemeSetID)
            self._set_attributes(lin_df)
        elif kwargs.get("lin") is not None:
            self._set_lin_init(**kwargs)
        else:
            self.scheme = SchemeSet(self.db, schemeID=int(schemeSetID), **kwargs)
            self.__setattr__("visible", visible)

    def _set_lin_init(self, **kwargs):
        self.scheme = kwargs.get("scheme", None)
        if not self.scheme:
            schemeSetID = kwargs.get("schemeSetID", 1)
            self.scheme = SchemeSet(self.db, schemeID=schemeSetID)

        if isinstance(lin := kwargs.get("lin"), np.ndarray):
            self.lin = lin.tolist()
        elif isinstance(lin := kwargs.get("lin"), list):
            self.lin = lin
        elif isinstance(dlin := kwargs.get("dlin"), dict):
            self.dlin = dlin

    def _set_attributes(self, lin_df: DataFrame):
        if lin_df.empty:
            return
        self.dlin = util.nodes2dlin(lin_df)
        self.idx = lin_df.iloc[-1]["idx"]
        self.genome_id = lin_df.iloc[-1]["genomeID"]

    def __str__(self):
        return (
            f"{self.genome_id:<8}\t"
            f"{[str(e) for e in self.lin]:<{self.scheme.length}}\t"
            f"{self.visible:<8}\t"
            f"{self.scheme.id:<8}\n"
        )

    def pprint(self):
        return (
            f"{'ID':<8}\t"
            f"{'LIN':<{self.scheme.length}}\t"
            f"{'Visible':<8}\t"
            f"{'Scheme_ID':<8}\n"
            f"{self.genome_id:<8}\t"
            f"{[str(e) for e in self.lin]:<{self.scheme.length}}\t"
            f"{self.visible:<8}\t"
            f"{self.scheme.id:<8}\n"
        )

    @property
    def lin(self):
        if not self._lin and not self._dlin:
            self._dlin = self.get_lin(existing_only=True)
        if self._dlin:
            self._lin = util.dlin2lin(
                self._dlin, scheme_length=self.scheme.length
            ).tolist()
        return self._lin

    @lin.setter
    def lin(self, lin: list[int]):
        self._lin = lin
        if not self._dlin:
            self._dlin = util.lin2dlin(lin)

    @property
    def dlin(self):
        if not self._lin and not self._dlin:
            self._dlin = self.get_lin(existing_only=True)
        if self._lin:
            self._dlin = util.lin2dlin(list(self._lin))
        return self._dlin

    @dlin.setter
    def dlin(self, dlin: dict[int, int]):
        self._dlin = dlin
        self._lin = util.dlin2lin(dlin, scheme_length=self.scheme.length).tolist()

    # TODO: Get all LINgroups is is a member in
    def get_lingroup(self):
        # run with max_dlin key
        # get all parents of leaves to construct entire prefix
        # match prefixes to dlin and take the one useful + it's parents +
        # All its immediate zero value child nodes

        # otherwise
        # if none are found move to the second highest position
        # match prefixes to dlin and take the one useful + it's parents +
        # all its immediate zero value child nodes where
        # 'current matching key' < key < 'next non-zero key'
        pass

    def is_child(self, lin: "LIN") -> bool:
        lin.is_empty()
        if len(lin.lin) <= len(self.lin) and self.is_related(lin):
            return True
        return False

    def is_parent(self, lin: "LIN") -> bool:
        lin.is_empty()
        if len(lin.lin) > len(self.lin) and self.is_related(lin):
            return True
        return False

    def is_related(self, lin: "LIN") -> bool:
        lin.is_empty()
        min_size = min(len(lin.lin), len(self.lin))
        if lin.lin[:min_size] == self.lin[:min_size]:
            return True
        return False

    def remove_lin(self, reason=None):
        self.del_reason = reason
        self.db.hide_lin([self.genome_id], self.scheme.id)

    def is_empty(self, raise_e=False) -> bool:
        if self.lin is None:
            if raise_e:
                raise AttributeError("Can not check None LIN membership")
            return True
        return False

    def get_parent(self) -> Optional[DataFrame]:
        """get parent of LIN

        Returns:
                Optional[DataFrame]: Table with parent information columns=[genomeID,parent,idx,lastValue]
        """
        self.is_empty()
        if self.genome_id is not None and self.scheme is not None:
            return self.db.get_lin_parents(self.genome_id, self.scheme.id)
        for i in range(len(self.lin)):
            if self.lin[-i] > 0:
                parent_lin = np.append(
                    self.lin[:-i], np.zeros(i, dtype=np.uintc)
                ).tolist()
                return self.db.search_lin(
                    util.lin2dlin(parent_lin), self.scheme.id
                )  # , exact=True)
        return None

    # Gets the LIN if already registered otherwise registers a new one
    def get_lin(self, existing_only=False, identify=False) -> dict[int, int]:
        registered_lin = self.db.get_LIN(self.genome_id, self.scheme.id)
        if registered_lin == {} and not existing_only:
            return self.register(identify=identify)
        return registered_lin

    def register(self, identify) -> dict[int, int]:
        self.dlin = self.scheme.calculate_schemes(self.genome_id, identify=identify)
        return self.dlin

    def get_idx(self) -> int:
        """finds the highest non-zero LIN position

        Returns:
                int: [highest non-zero LIN position]
        """
        if self.idx is not None:
            return self.idx
        if self.dlin is not None:
            return np.array(*self.dlin, dtype=np.intc).max()
        lin_array = np.array(self.lin)
        return int(np.argmax((lin_array[lin_array > 0])))
