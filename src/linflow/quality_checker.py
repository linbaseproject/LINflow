import numpy as np
import numpy.typing as npt

from . import logger as m_logger
from . import util

logger = m_logger.get_logger(__name__)

MAX_SCAFFOLD_COUNT = 500
SHORTEST_CONTIG_LENGTH = 500
MIN_N50 = 50000
# fewer than 500 contigs
# shortest contig > 500bp
# n50 >50,000bp


def n50(sequence_lengths: npt.NDArray[np.integer]) -> int:
    desc_sorted_lengths = np.flip(np.sort(sequence_lengths, axis=None))
    cum_sum = np.cumsum(desc_sorted_lengths)
    idx = np.searchsorted(cum_sum, cum_sum[-1] // 2, side="left")
    return int(cum_sum[idx])


def passes_qc(
    filepath: str,
    max_scaffold_count=MAX_SCAFFOLD_COUNT,
    shortest_contig_length=SHORTEST_CONTIG_LENGTH,
    min_n50=MIN_N50,
    **kwargs,
) -> bool:
    try:
        scaffold_count, sequence_lengths = util.read_scaffold_info(filepath)
        if scaffold_count > max_scaffold_count:
            logger.warning(
                "scaffold count of the genome was higher. expected <%d but got %d",
                max_scaffold_count,
                scaffold_count,
            )
            return False
        if (sorted_lengths := np.sort(sequence_lengths))[0] < shortest_contig_length:
            logger.warning(
                "the length of the shortest scaffold was too short. "
                "expected >%d but got %d",
                shortest_contig_length,
                sorted_lengths[0],
            )
            return False
        if (current_n50 := n50(sorted_lengths)) < min_n50:
            logger.warning(
                "the N50 of the genome was too small. expected >%d but got %d",
                min_n50,
                sorted_lengths,
            )
            return False
        logger.debug(
            "genome qc was successful. %d scaffolds, shortest_scaffold is %d bp, and N50 = %d bp.",
            scaffold_count,
            sorted_lengths[0],
            current_n50,
        )
    except Exception:
        return False
    return True
