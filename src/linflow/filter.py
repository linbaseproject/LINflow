import copy
from abc import ABCMeta
from typing import Any, Callable, Optional, Tuple, Type, TypeVar, Union, overload

import numpy as np
import numpy.typing as npt

from . import exceptions, extensions, lindb, linflow
from . import logger as m_logger
from . import util
from .measure_base import Measure

logger = m_logger.get_logger(__name__)
T = TypeVar("T")
FILTER_FUNCTIONS = ["reasoner_function", "rankscore_function"]


class Filter:
    """Instantiates a Measure with specific criteria (default_args)"""

    @staticmethod
    def reasoner_identity(
        filter,
        gid: int,
        ref_genome_ids: npt.NDArray[np.uintc],
        scores: npt.NDArray[np.floating],
    ) -> Tuple[bool, npt.NDArray[np.uintc], npt.NDArray[np.floating]]:
        """returns the ref genomeIDs and scores as they are"""
        return False, ref_genome_ids, scores

    @staticmethod
    def rankscore_identity(
        filter, scores: npt.NDArray[np.floating], target_col=0
    ) -> npt.NDArray[np.floating]:
        """returns the first score as the ranking score"""
        # no_scores
        if scores.shape[1] < 1:
            return scores.flatten()
        return scores[:, target_col]

    reasoner_function: Callable = reasoner_identity
    rankscore_function: Callable = rankscore_identity
    default_args: dict[str, Any] = {}
    measure: Type[Measure]

    @overload
    def __init__(
        self,
        db: lindb.LINdb,
        *,
        name: str,
        min: float,
        max: float,
        type: str,
        measure: Type[Measure],
        reasoner_function: Optional[Union[Callable, str]] = "",
        rankscore_function: Optional[Union[Callable, str]] = "",
        default_args: Optional[dict[str, Any]] = None,
        nscores_to_save=1,
        register=True,
    ): ...

    @overload
    def __init__(self, db: lindb.LINdb, *, filterID: int, fill=False): ...

    @overload
    def __init__(self, db: lindb.LINdb, *, name: str, fill=False): ...

    def __init__(self, db: lindb.LINdb, fill=False, register=True, **kwargs):
        self.db = db
        self.default_args = {"workspace": self.db.db_name}
        if kwargs.get("min") is not None:
            self.name = kwargs.get("name")
            self.nscores_to_save = 1
            # type in the format of 1-1 1-m m-m
            self.type = "1-1"
            for field in ["min", "max", "type", "nscores_to_save"]:
                if kwargs.get(field) is not None:
                    self.__setattr__(field, kwargs.get(field))
            self.default_args.update(kwargs.get("default_args", {}) or {})
            for key, default in zip(
                ["reasoner_function", "rankscore_function", "measure"],
                [self.reasoner_identity, self.rankscore_identity, None],
            ):
                if function := kwargs.get(key):
                    self.__setattr__(key, self._eval_function(function))
                else:
                    self.__setattr__(key, default)

            if register:
                new_id = self.register()
                self.id = new_id
        elif kwargs.get("filterID") is not None:
            self.id = int(kwargs.get("filterID", 1))
            self.fill_attributes()
        elif kwargs.get("name") is not None:
            self.name = kwargs.get("name")
            self.fill_attributes()
        else:
            exceptions.handle_exception(
                AttributeError(
                    "Please use a correct version of the Filter constructor."
                ),
                logger,
            )

            if fill:
                self.fill_attributes()

    def __getattr__(self, __name: str) -> Any:
        self.fill_attributes()
        return self.__getattribute__(__name)

    def fill_attributes(self):
        """fills all the attributes of the filter from the database if a filter with the self.id exists

        Raises:
                AttributeError: raised when Filter not found in database ID with self.id
        """
        filter_id = filter_df = None
        if self.id is not None:
            filter_df = self.db.get_filter(filterID=self.id)
            filter_id = self.id
        elif self.name is not None:
            filter_df = self.db.get_filter(filterName=self.name)
            filter_id = self.name
        if filter_df is None or filter_df.empty:
            raise AttributeError("Filter not found in database ID: %s" % filter_id)
        first_match = filter_df.iloc[0]
        new_filter = Filter(
            self.db,
            name=first_match["filterName"],
            min=first_match["minEffect"],
            max=first_match["maxEffect"],
            type=first_match["compareType"],
            measure=first_match["measure"],
            default_args=first_match["dargs"],
            nscores_to_save=first_match["saveScores"],
            reasoner_function=self._eval_function(
                first_match["reasonerF"], self.reasoner_identity
            ),
            rankscore_function=self._eval_function(
                first_match["rankscoreF"], self.rankscore_identity
            ),
            register=False,
            fill=False,
        )  # type: ignore
        self.__dict__.update(new_filter.__dict__)
        self.id = first_match["filterID"]

    def _eval_function(
        self, obj: Union[Callable, str], default: Callable = lambda x: x
    ) -> Callable:
        if obj is None:
            return default
        if obj in [
            util.function2str(self.rankscore_identity),
            util.function2str(self.reasoner_identity),
        ]:
            return default
        if isinstance(obj, Callable):
            return obj
        callable_import = obj.split(".")
        if isinstance(obj, ABCMeta):
            return __import__(
                ".".join(callable_import[:-1]), fromlist=callable_import[-1:]
            )
        if len(callable_import) > 1:
            return getattr(
                __import__(
                    ".".join(callable_import[:-1]), fromlist=callable_import[-1:]
                ),
                callable_import[-1],
            )
        return getattr(__import__("linflow.filter_helpers", fromlist=[obj]), obj)

    # TODO: Handle multi query measures
    # TODO:Overload init to make copy of a filter and change some fields
    # would get list of ref as str (full path) or int (ids)
    def compare(
        self, query: np.ndarray, ref: npt.NDArray[np.uintc], **kwargs
    ) -> npt.NDArray[np.floating]:
        """Compares genomes using built-in Measure

        Args:
                query (np.ndarray): list of query genomeIDs
                ref ('np.ndarray[np.uintc]'): list of reference genomeIDs

        Raises:
                TypeError: raised when query is None
                Exception: raised when single-query Filter was given multiple query genomes

        Returns:
                'np.ndarray[np.floating]': 2d array of scores in order of ref genomeIDs
        """

        # NoneType checks
        if query is None or len(query) < 1:
            raise TypeError("The filter can't compare None query")
        if self.measure is None:
            self.fill_attributes()

        # Checking number of query-ref sequences matches method type
        if self.type[0] == "1" and not len(query) == 1:
            raise BaseException("The filter does not accept more than one query input")

        # Overwriting the default args with the given ones
        dargs = copy.deepcopy(self.default_args)
        dargs.update(kwargs)
        # removing workspace to avoid parameter duplication in ones that already have workspace
        dargs_nowork = copy.deepcopy(dargs)
        dargs_nowork.pop("workspace")

        mappings = {}
        ref_filepaths = []
        # measure_pool = None
        # Default result is -1 meaning N/A
        results = (
            [[-1] for _ in list(ref)] if len(ref) > 0 else np.array([[]], dtype=float)
        )

        for idx, refID in enumerate(ref):
            mappings[f"{refID}"] = idx
        # TODO: make to use multiple queries
        # self.measure.create(self.db.db_name, ref.tolist() + [queryID], **dargs_nowork)
        self.measure.create(
            self.db.db_name,
            ref.tolist(),
            update_db=False,
            **dargs_nowork,
        )
        self.measure.create(
            self.db.db_name,
            query.tolist(),
            update_db=False,
            is_query=True,
            **dargs_nowork,
        )

        # if no references add the query for later (e.g. 1st genome)
        if len(ref) < 1:
            return np.array([[]], dtype=float)

        if self.type[0] == "m":
            raise NotImplementedError(
                "This type of filter is not yet supported %s" % self.type
            )
        # running comparisons
        queryID = int(query[0])
        if self.type[-1] == "1":
            outputs = []
            for refID in ref:
                outputs += [
                    linflow.POOL.apply_async(
                        self.measure.compare,
                        args=(refID, queryID, self.db.db_name),
                        kwds=dargs_nowork,
                    )
                ]

        else:
            outputs = self.measure.compare(
                ref.tolist(), queryID, self.db.db_name, **dargs_nowork
            )

        for process in outputs:
            """array([['/home/.../LINflow/workspaces/test0/sig/0/1.sig',
            0.235],
            ['/home/.../LINflow/workspaces/test0/sig/0/1.sig',
            0.235]], dtype=object)"""
            # if process is a thread output that has to be waited for wait to get the result
            if not isinstance(process, np.ndarray):
                process = process.get()
            refID = extensions.remove_extension(process[0])
            results[mappings[refID]] = process[1:]
        logger.debug("Stopped and consolidated filtering threads")

        return np.array(results, dtype=float)

    def reasoner(
        self,
        gid: int,
        ref_genome_ids: npt.NDArray[np.uintc],
        scores: npt.NDArray[np.floating],
    ) -> tuple[
        bool,
        npt.NDArray[np.uintc],
        npt.NDArray[np.floating],
    ]:
        """decides when to stop the filter stack or when to re-run it using more stringent parameters (scores is a matrix)

        Args:
                gid (int): query genomeID
                ref_genome_ids ('np.ndarray[np.uintc]'): reference genomeIDs
                scores ('np.ndarray[np.floating]'): 2d score array where rows are genomes and columns are scores

        Returns:
                bool: set as "True" should the filter application finish greedily (e.g. have reached enough precision?)
                np.ndarray[np.uintc]: new reference genomeIDs array corresponding to new scores
                np.ndarray[np.floating]: new 2d score array where rows are genomes and columns are scores (filtered or re-run)
        """
        if self.reasoner_function is None:
            self.fill_attributes()
        return self.reasoner_function(self, gid, ref_genome_ids, scores=scores)

    # computes the rankscore based on all the scores (combination or individual),
    # thresholds, etc. (scores is a matrix)
    def compute_rankscore(
        self, scores: npt.NDArray[np.floating]
    ) -> npt.NDArray[np.floating]:
        """computes a single score to rank the scores by

        Args:
                scores (np.ndarray[np.float]): scores of comparison

        Returns:
                np.ndarray[np.float]: single score to rank the scores by
        """
        if self.rankscore_function is None:
            self.fill_attributes()
        return self.rankscore_function(self, scores)

    def get_args(self) -> dict[str, Any]:
        return self.default_args

    def get_arg(self, name: str, default: T) -> T:
        """Returns the default argument (by name) of the filter used in many stages of the filter to customize workflow (e.g. rankscore, reasoner, Measure, etc.)

        Args:
                name (str): name of the argument to get
                default(Any): what to return by default

        Returns:
                Optional[Any]: default argument of exists else None
        """
        arg = None
        if name is None:
            raise IndexError("Filter argument '%s' cannot be None" % name)
        try:
            arg = self.default_args.get(name)
        except (TypeError, KeyError) as e:
            exceptions.handle_exception(
                e, logger, "default_args key was not found", raise_exception=False
            )
        return arg or default

    def register(self) -> int:
        """registers the filter on the workspace database

        Returns:
                int: ID of the newly created filter
        """
        if self.name is not None and self.measure is not None:
            untethered_args = copy.deepcopy(self.default_args)
            untethered_args.pop("workspace", None)
            return self.db.add_filter(
                self.name,
                util.function2str(self.measure),
                self.min,
                self.max,
                self.nscores_to_save,
                self.type,
                util.function2str(self.reasoner_function),
                util.function2str(self.rankscore_function),
                untethered_args,
            )
        else:
            raise BaseException(
                "No connecting name or measure to the Filter Name: '%s' Measure: '%s'"
                % (self.name, self.measure)
            )

    def scale_scores(self, scores: npt.NDArray) -> npt.NDArray:
        """scales the 2-D score matrix of a filter using the scale default argument

        Args:
            scores (npt.NDArray): scores to be scaled

        Returns:
            npt.NDArray: scaled scores with the same dimension as the input scores
        """
        scalar = self.get_arg("scale", []) or []
        if scores is None or not scalar:
            return scores
        if np.array(scores).size < 1 or np.array(scalar).size < 1:
            return scores
        if isinstance(scalar, (list, np.ndarray)):
            try:
                return np.multiply(scores, scalar)
            except Exception as e:
                exceptions.handle_exception(
                    e,
                    logger,
                    "Scaling scores failed",
                )
        elif isinstance(scalar, Callable):
            scores = np.apply_along_axis(scalar, 1, scores)
        else:
            exceptions.handle_exception(
                BaseException(
                    "Type of scaler must be list, ndarray, or function was '%s'"
                    % type(scalar),
                ),
                logger,
            )
        return scores

    def add_query(self, queryID: int, **kwargs):
        """adds the query genome as a new reference ID in the database

        Args:
            queryID (int): genomeID of the query

        """
        # Overwriting the default args with the given ones
        dargs = copy.deepcopy(self.default_args)
        dargs.update(kwargs)
        # removing workspace to avoid parameter duplication in ones that already have workspace
        dargs_nowork = copy.deepcopy(dargs)
        dargs_nowork.pop("workspace")
        self.measure.create(self.db.db_name, [queryID], is_query=False, **dargs_nowork)


# TODO: Filter given references based on queries?
