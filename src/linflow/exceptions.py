import logging
from typing import Optional

from . import logger as m_logger

exception_logger = m_logger.get_logger(__name__)


class UncleanWorkspaceError(BaseException):
    """Exception raised when workspace folder or database already exists.

    Attributes:
            workspace (str): workspace name
            message (str, optional): explanation of the error
    """

    def __init__(self, workspace, message=""):
        self.workspace = workspace
        self.message = f" {message}"
        super().__init__(self.message)

    def __str__(self):
        return (
            f"Workspace folder or database with name '{self.workspace}' already exists "
            f"Please choose another name or use the 'remove' command to clean delete the workspace."
        )


def handle_exception(
    exception: BaseException,
    logger: Optional[logging.Logger] = None,
    message="",
    raise_exception=True,
    log_level=logging.ERROR,
):
    """logs exceptions and raises/skips them when prompted

    Args:
        exception (BaseException): the exception to log/skip
        message (str, optional): a message to log after the exception's existing message. Defaults to "".
        logger (logging.Logger, optional): logger object to log the exception. Defaults to None.
        raise_exception (bool, optional): whether or not to raise the exception or skip it, after logging. Defaults to True.

    Raises:
        ex (Exception): raises te exception if raise_exception is not False (disabled)
    """
    if logger is not None:
        logger.log(log_level, exception, exc_info=True)
        # logger.log(log_level, exception.__traceback__)
        if message:
            logger.log(log_level, message)
    if raise_exception:
        raise exception
