
CREATE TABLE IF NOT EXISTS User (
  User_ID               INT UNSIGNED NOT NULL AUTO_INCREMENT,
  FirstName             VARCHAR(255) NOT NULL,
  LastName              VARCHAR(255) DEFAULT NULL,
  Institute             VARCHAR(255) NOT NULL,
  Username              VARCHAR(255) NOT NULL,
  Email                 VARCHAR(512) NOT NULL,
  EmailValidateSecret   VARCHAR(32) NOT NULL,
  `Password`            TEXT NOT NULL,
  EmailValidated        INT(1) UNSIGNED NOT NULL DEFAULT 0,
  Permission            INT(8) UNSIGNED NOT NULL DEFAULT 0,
  RegistrationDate      TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (User_ID),
  KEY (Username),
  KEY (Email)
);$$

CREATE TABLE IF NOT EXISTS LINgroup (
   lingroupID          INT UNSIGNED NOT NULL AUTO_INCREMENT,
   schemeSetID         INT UNSIGNED NOT NULL,
   taxTypeID           INT UNSIGNED NOT NULL,
   title               VARCHAR(1023) NOT NULL,
   note                VARCHAR(3061) DEFAULT NULL,
   sourceLink          VARCHAR(2047) DEFAULT NULL,
   submitter           INT UNSIGNED DEFAULT 1,
   FOREIGN KEY (schemeSetID)   REFERENCES SchemeSet (schemeSetID)       ON DELETE CASCADE ON UPDATE CASCADE,
   FOREIGN KEY (taxTypeID)     REFERENCES TaxType (taxTypeID)           ON DELETE CASCADE ON UPDATE CASCADE,
   FOREIGN KEY (submitter)     REFERENCES User (User_ID)                ON UPDATE CASCADE,
   PRIMARY KEY (lingroupID),
   INDEX (lingroupID, schemeSetID)
);$$

CREATE TABLE IF NOT EXISTS Prefix (
    prefixID            INT UNSIGNED NOT NULL AUTO_INCREMENT,
    schemeSetID         INT UNSIGNED NOT NULL DEFAULT 1,
    idx                 INT UNSIGNED DEFAULT NULL,
    lastValue           INT UNSIGNED NOT NULL,
    parent              INT UNSIGNED DEFAULT 1,
    FOREIGN KEY (parent)            REFERENCES Prefix (prefixID)        ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (schemeSetID)       REFERENCES SchemeSet (schemeSetID)  ON DELETE CASCADE ON UPDATE CASCADE,
    INDEX (idx, lastValue, schemeSetID),
    UNIQUE KEY (schemeSetID,idx,lastValue,parent),
    PRIMARY KEY (prefixID, schemeSetID)
);$$


CREATE TABLE IF NOT EXISTS LINgroup_Taxon (
    rankID              INT UNSIGNED NOT NULL,
    taxTypeID           INT UNSIGNED NOT NULL,
    lingroupID          INT UNSIGNED NOT NULL,
    taxID               INT UNSIGNED DEFAULT NULL,
    parent              INT UNSIGNED DEFAULT NULL,
    customTaxon         VARCHAR(255) DEFAULT NULL,
    -- forces one of taxID or taxon to be NOT NULL
    common_tax  VARCHAR(225) GENERATED ALWAYS AS (coalesce(taxID, customTaxon)) VIRTUAL NOT NULL,
    FOREIGN KEY (rankID)            REFERENCES TaxRank (rankID)         ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (taxTypeID)         REFERENCES TaxType (taxTypeID)      ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (lingroupID)        REFERENCES LINgroup (lingroupID)    ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (taxID)             REFERENCES TaxID (taxID)            ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (rankID, taxTypeID, lingroupID),
    INDEX (common_tax),
    INDEX (taxID, parent, taxTypeID, rankID)
);$$

CREATE TABLE IF NOT EXISTS LINgroup_Prefix (
    lingroupID          INT UNSIGNED NOT NULL,
    prefixID            INT UNSIGNED NOT NULL,
    schemeSetID         INT UNSIGNED NOT NULL DEFAULT 1,
    FOREIGN KEY (schemeSetID)       REFERENCES SchemeSet (schemeSetID)      ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (schemeSetID)       REFERENCES Prefix (schemeSetID)         ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (lingroupID)        REFERENCES LINgroup (lingroupID)        ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (prefixID)          REFERENCES Prefix (prefixID)            ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (lingroupID, prefixID, schemeSetID)
);$$




CREATE TABLE IF NOT EXISTS Attribute (
  attID                 INT UNSIGNED NOT NULL AUTO_INCREMENT,
  attName               VARCHAR(511) NOT NULL,
  PRIMARY KEY (attID),
  KEY (attName)
);$$

CREATE TABLE IF NOT EXISTS Compound_Attribute (
  attID                 INT UNSIGNED NOT NULL,
  attCategory           INT NOT NULL,
  attValue              TEXT,
  PRIMARY KEY (attID, attCategory),
  FOREIGN KEY (attID)             REFERENCES Attribute (attID)
);$$
 
CREATE TABLE IF NOT EXISTS Genome_Attribute (
  attID                 INT UNSIGNED NOT NULL,
  genomeID              INT UNSIGNED NOT NULL,
  userID                INT UNSIGNED NOT NULL DEFAULT 1,
  attValue              TEXT NOT NULL,
  PRIMARY KEY (genomeID, attID),
  FOREIGN KEY (genomeID)            REFERENCES Genome (genomeID)        ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (attID)               REFERENCES Attribute (attID)        ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (userID)              REFERENCES User (User_ID)           ON DELETE CASCADE ON UPDATE CASCADE
);$$

CREATE TABLE IF NOT EXISTS Compound_Attribute (
  attID                 INT UNSIGNED NOT NULL,
  attCategory           INT NOT NULL,
  attValue              TEXT,
  PRIMARY KEY (attID, attCategory),
  FOREIGN KEY (attID)             REFERENCES Attribute (attID)
);$$

CREATE TABLE ci_sessions (
	id                  VARCHAR(128) NOT NULL,
	ip_address          VARCHAR(45) NOT NULL,
	`timestamp`         INT(10) UNSIGNED NOT NULL DEFAULT 0,
	`data`              blob NOT NULL,
	KEY ci_sessions_timestamp (`timestamp`)
);$$


CREATE TABLE JobInfo
(
	JobInfo_ID          INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`Name`              VARCHAR(64) DEFAULT NULL,
	`Type`              VARCHAR(64) DEFAULT NULL,
	Content             TEXT,
-- number of parallel workers the scheduler forks 0 means the main scheculer process 
	MCW                 INT(11) UNSIGNED DEFAULT 0,
	BufferSize          INT(11) UNSIGNED DEFAULT 0,
	PRIMARY KEY (JobInfo_ID)
);$$



CREATE TABLE Job (
       Job_ID           INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
       User_ID          INT(11) UNSIGNED NOT NULL,
       JobInfo_ID       INT(11) UNSIGNED DEFAULT NULL,
       Job_uuid         VARCHAR(512) DEFAULT NULL,
       Title            VARCHAR(64) DEFAULT NULL,
       `Status`         VARCHAR(64) DEFAULT NULL,
       Arguments        TEXT,
       Result           TEXT,
       SubmitTime       DATETIME(3) DEFAULT NULL,
       StartTime        DATETIME(3) DEFAULT NULL,
       TerminateTime    DATETIME(3) DEFAULT NULL,
       LIN_ID           INT(11) DEFAULT NULL,
       Conserved_LIN    TEXT,
       `Priority`       TINYINT NOT NULL DEFAULT 5,
       PRIMARY KEY (Job_ID),
       FOREIGN KEY (User_ID)        REFERENCES User (User_ID)           ON DELETE CASCADE ON UPDATE CASCADE,
       FOREIGN KEY (JobInfo_ID)     REFERENCES JobInfo (JobInfo_ID)     ON DELETE CASCADE ON UPDATE CASCADE,
       INDEX (Job_uuid)
);$$

CREATE TABLE LINgroup_DLIN(
    lingroupID          INT UNSIGNED NOT NULL,
    prefixID            INT UNSIGNED NOT NULL,
    dlin                JSON NOT NULL,
    parents             JSON NOT NULL,
    FOREIGN KEY (lingroupID)        REFERENCES LINgroup (lingroupID)        ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (prefixID)          REFERENCES Prefix (prefixID)            ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (lingroupID, prefixID));$$

CREATE TABLE LINgroup_Lineage(
    lingroupID          INT UNSIGNED NOT NULL,
    taxTypeID           INT UNSIGNED NOT NULL,
    maxRank             INT UNSIGNED DEFAULT 0,
    lineage             JSON NOT NULL,
    lineage2            JSON NOT NULL,
    FOREIGN KEY (lingroupID)        REFERENCES LINgroup (lingroupID)        ON DELETE CASCADE,
    FOREIGN KEY (taxTypeID)         REFERENCES TaxType (taxTypeID)          ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (lingroupID, taxTypeID));$$

CREATE TABLE DLIN(
genomeID                INT UNSIGNED NOT NULL,
schemeSetID             INT UNSIGNED NOT NULL,
dlin                    JSON NOT NULL,
parents                 JSON NOT NULL,
FOREIGN KEY (genomeID)              REFERENCES Genome (genomeID)            ON DELETE CASCADE,
FOREIGN KEY (schemeSetID)           REFERENCES SchemeSet (schemeSetID)      ON DELETE CASCADE ON UPDATE CASCADE,
PRIMARY KEY (genomeID, schemeSetID));$$


CREATE TABLE Prefix_DLIN(
prefixID                INT UNSIGNED NOT NULL,
schemeSetID             INT UNSIGNED NOT NULL,
dlin                    JSON NOT NULL,
parents                 JSON NOT NULL,
FOREIGN KEY (prefixID)              REFERENCES Prefix (prefixID)            ON DELETE CASCADE,
FOREIGN KEY (schemeSetID)           REFERENCES SchemeSet (schemeSetID)      ON DELETE CASCADE ON UPDATE CASCADE,
PRIMARY KEY (prefixID, schemeSetID));$$

ALTER TABLE Genome ADD userID INT UNSIGNED NOT NULL DEFAULT 0 
    REFERENCES User (User_ID) ON DELETE SET DEFAULT ON UPDATE CASCADE;$$

ALTER TABLE TaxRank ADD COLUMN compulsory      BOOLEAN NOT NULL DEFAULT 0;$$


-- set species level data to be uppercase
ALTER TABLE TaxRank ADD COLUMN upper           BOOLEAN DEFAULT FALSE;$$
UPDATE TaxRank SET upper=TRUE WHERE taxTypeID IN (1,2,3) AND rankID < 8 AND rankID > 0;$$


-- for prefix table parent cannot have a zero lastValue
