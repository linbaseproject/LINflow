"""
The functions in this module are designed to convert (reduce) a list of rankscores 
into lins using different methodologies.
score2lin_default is the default lin reduction function
"""

from typing import TYPE_CHECKING, Optional, Tuple

import numpy as np
import numpy.typing as npt

from . import logger as m_logger

if TYPE_CHECKING:
    from .filter import Filter
    from .scheme import SchemeSet

logger = m_logger.get_logger(__name__)


def use_top_ref(
    scheme: "SchemeSet",
    gid: int,
    ref_genome_ids: npt.NDArray[np.integer],
    rankscore: npt.NDArray[np.floating],
    last_filter: int = 0,
    scores: Optional[npt.NDArray[np.floating]] = None,
    add=True,
) -> Tuple[dict[int, int], float]:
    scores = scores or np.array([], dtype=float)
    """Only use the first ref_genome rankscore to create a lin"""
    index = int(np.argmax(scheme.positions > rankscore[0]))
    # gid = scheme.db.add_genome(genome_file)
    lin_df = scheme.db.new_lin(gid, ref_genome_ids[0], index, scheme.id, add=add)
    #  columns=["genomeID", "schemeSetID", "idx", "lastValue", "parent", "neighbor"]
    if add:
        return scheme.db.get_LIN(gid, scheme.id), rankscore[0]
    assert len(lin_df) == 1
    last_idx: dict[int, int] = {
        lin_df.iat[0, lin_df.columns.get_loc("idx")]: lin_df.iat[
            0, lin_df.columns.get_loc("lastValue")
        ]
    }
    parent_dlin = scheme.db.get_LIN(
        lin_df.iat[0, lin_df.columns.get_loc("parent")], scheme.id
    )
    last_idx.update(parent_dlin)
    return last_idx, rankscore[0]


def scores2dlin_default(
    scheme: "SchemeSet",
    gid: int,
    ref_genome_ids: npt.NDArray[np.integer],
    rankscores: list[float],
    last_filter: Optional["Filter"] = None,
    scores: Optional[npt.NDArray[np.floating]] = None,
    add=True,
) -> Tuple[dict[int, int], float]:
    """assigns a LIN based on the indexing methodology of the scheme"""
    scores = scores if scores is not None and len(list(scores)) > 0 else np.array([], dtype=float)  # type: ignore
    if ref_genome_ids is None or ref_genome_ids.size < 1:
        closest_genome = None
        top_rankscore = 0.0
    else:
        closest_genome = int(ref_genome_ids[0])
        top_rankscore = rankscores[0]
    tochange_index = scheme.tochange_position(top_rankscore)
    lin_df = scheme.db.new_lin(gid, closest_genome, tochange_index, scheme.id, add=add)
    # if the genome is to be added to the database (not identify)
    if add:
        return scheme.db.get_LIN(gid, scheme.id), top_rankscore
    assert len(lin_df) == 1
    last_idx: dict[int, int] = {
        lin_df.iat[0, lin_df.columns.get_loc("idx")]: lin_df.iat[
            0, lin_df.columns.get_loc("lastValue")
        ]
    }
    parent_dlin = scheme.db.get_LIN(
        lin_df.iat[0, lin_df.columns.get_loc("parent")], scheme.id
    )
    last_idx.update(parent_dlin)
    return last_idx, top_rankscore


# TODO: Test 2 ANI filters in one BaseScheme -> scale cannot be loaded correctly
