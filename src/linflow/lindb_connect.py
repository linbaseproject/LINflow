"""
    Login file should be in the following format (don't forget to make it readable only by you)
    You can find your socket file at /etc/mysql/mysql.conf.d/mysqld.cnf or /etv/httpd/mysql.conf.d/mysqld.cnf
    [client]
    user=yourusername
    password=yourpassowrd
    socket=/var/run/mysqld/mysqld.sock
"""

import os
from pathlib import Path
from typing import Tuple

import mysql.connector
import mysql.connector.cursor
from mysql.connector.errors import DatabaseError

from . import logger as m_logger

MODULE_DIR = Path(__file__).parents[2]
LOGIN_FILE = os.path.join(MODULE_DIR, ".my.cnf")
HOST = os.getenv("LINFLOW_DB_HOSTNAME")
HOST = HOST if HOST else "localhost"
logger = m_logger.get_logger(__name__)


def connect_to_db(
    database=None, raise_exception=True, log_error=True
) -> Tuple[mysql.connector.MySQLConnection, mysql.connector.cursor.MySQLCursor]:
    if not log_error:
        raise_exception = log_error
    try:
        if database is not None:
            conn = mysql.connector.connect(
                host=HOST,
                read_default_file=LOGIN_FILE,
                autocommit=True,
                database=database,
            )
        else:
            conn = mysql.connector.connect(
                host=HOST, read_default_file=LOGIN_FILE, autocommit=True
            )
        return conn, conn.cursor()  # type: ignore
    except DatabaseError as e:
        if log_error:
            logger.error(
                "Error connecting to database %s:%s with credentials at %s",
                HOST,
                database,
                LOGIN_FILE,
            )
            logger.exception(e)
        if raise_exception:
            raise e
    return None, None  # type: ignore


def close(conn):
    conn.close()


def database_exists(db_name) -> bool:
    cursor: mysql.connector.cursor.MySQLCursor
    # conn: mysql.connector.MySQLConnection
    _, cursor = connect_to_db()
    cursor.execute(
        "SELECT SCHEMA_NAME FROM information_schema.schemata "
        f"WHERE SCHEMA_NAME = '{db_name}'"
    )
    db_exists = cursor.fetchall()
    if db_exists or len(db_exists) > 0:
        return True
    return False


def drop_database(db_name):
    try:
        _, cursor = connect_to_db()
        cursor.execute(f"DROP DATABASE IF EXISTS `{db_name}`")
    except Exception as e:
        logger.exception(e)
        raise e
