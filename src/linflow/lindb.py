import copy
import json
import logging
import os
from collections.abc import Iterable
from enum import Enum
from typing import Any, Optional, Tuple, Union, overload

import alive_progress
import mysql.connector
import mysql.connector.cursor
import numpy as np
import numpy.typing as npt
import pandas as pd

from . import compression, exceptions, lindb_connect
from . import logger as m_logger
from . import tax_enums, util

logger = m_logger.get_logger(__name__)
TABLE_FILES = ["lindb.sql", "lindb2.sql"]
EXTRA_FILES = ["lindb2_extra.sql", "lindb2_init.sql"]

MEASURE_COLS = ["measure1", "measure2", "measure3", "measure4", "measureN"]

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

# for when foreign-key cascading fails
# SET IT BACK TO 1 AFTER DONE DELETING
# SET foreign_key_checks = 0;


class RecursiveTable(Enum):
    Genome = 1
    Prefix = 2


class LINgroupIDTypeError(Exception):
    """Exception raised for errors on the input rank type.

    Attributes:
            lingroupID (int): unique identifier for a lingroup description
            message (str, optional): explanation of the error
    """

    def __init__(self, lingroupID, message=""):
        self.lingroupID = lingroupID
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return (
            f"LINgroupID provided was not an int got: {self.lingroupID} "
            f"of type {type(self.lingroupID)}. {self.message}"
        )


class LINdb(object):
    """
    Object to interact with the MySQL database
    """

    cursor: mysql.connector.cursor.MySQLCursor
    conn: mysql.connector.MySQLConnection

    @staticmethod
    def _jnull(obj):
        if isinstance(obj, (int, float)):
            return obj
        if not isinstance(obj, Iterable) and (not obj or pd.isna(obj)):
            return "NULL"
        if (
            isinstance(obj, (list, np.ndarray, pd.Series, tuple, set, dict))
            and len(obj) < 1
        ):
            return "NULL"
        if isinstance(obj, np.ndarray):
            obj = obj.tolist()
        if isinstance(obj, str):
            return f"'{obj.lower()}'"
        return f"'{json.dumps(obj)}'"

    def __init__(self, database_name="LINflow", create=False):
        self.conn, self.cursor = lindb_connect.connect_to_db()
        if database_name == "":
            exceptions.handle_exception(
                FileNotFoundError(
                    "Empty string Database does not exist '%s'" % database_name
                ),
                logger,
            )
        self.db_name = database_name
        if lindb_connect.database_exists(database_name):
            self.execute(f"USE `{self.db_name}`")
        elif create:
            self.execute(f"CREATE DATABASE {database_name}")

    def lastRowID(self) -> int:
        if id := self.cursor.lastrowid:
            return int(id)
        return 0

    def execute(self, query: str, multi=False, raise_exception=True) -> list[tuple[Any, ...]]:  # type: ignore
        """Wraps the SQL execution cursor to log exceptions

        Args:
                query: (str): SQL query to run

        Returns:
                list(tuple): list of tuples as rows in the SQL table matching the query
        """
        # logger.debug(f"Running query {re.sub('[\n]+', ' ', query[:100])}")
        try:
            # query = sub("[\n|\t]+", " ", query)
            self.cursor.execute(query, multi=multi)
            return self.cursor.fetchall()
        except BaseException as e:
            # re.sub(r"[\n|\t]+", "", query)
            logger.error("SQL ERROR: %s", query)
            exceptions.handle_exception(e, logger)
            if raise_exception:
                raise e

    def multi_execute(
        self, query: str, sep: str = "$$", raise_exception=True
    ) -> list[list[tuple[Any, ...]]]:
        """Executes multiple SQL statements separated by something other than ';'

        Args:
            query (str): query string
            sep (str, optional): string that separates the different query statements. Defaults to "$$".
            raise_exception (bool, optional): Whether to raise an exception if one of the queries fails or to just skip it. Defaults to True.

        Returns:
            list: a list of results from each query
        """
        results = []
        query.rstrip("\n")
        for statement in query.split(sep):
            # removes empty queries
            if statement.strip():
                result = self.execute(
                    statement, multi=True, raise_exception=raise_exception
                )
                results += result if isinstance(result, Iterable) else [result]
        return results

    def remove(self):
        if lindb_connect.database_exists(self.db_name):
            self.execute(f"DROP DATABASE {self.db_name};")
            logger.info("Removed database %s", self.db_name)
            self.commit()

    def verify(self):
        return lindb_connect.database_exists(self.db_name)

    def get_cursor(self):
        return self.cursor

    def commit(self):
        self.conn.commit()

    def run_sql_file(self, filename: str):
        with open(os.path.join(SCRIPT_DIR, filename), "r") as file:
            lines = file.readlines()
        self.multi_execute("".join(lines))
        logger.debug("Adding defaults from %s", SCRIPT_DIR)

    def initiate(self, add_taxa=True):
        logger.info("Initializing DB %s", self.db_name)
        if lindb_connect.database_exists(self.db_name):
            e = exceptions.UncleanWorkspaceError(
                self.db_name, message=f"Database with the name {self.db_name} exists"
            )
            logger.exception(e)
            raise e

        logger.debug("Creating database %s", self.db_name)
        self.execute(f"CREATE DATABASE `{self.db_name}`")
        # self.query(
        #    "CREATE TABLESPACE `{0}` ADD DATAFILE './{0}.ibd' Engine='InnoDB';".format(self.db_name))
        self.commit()
        self.execute(f"USE `{self.db_name}`")
        logger.debug("Creating all tables")
        for init_file in TABLE_FILES:
            self.run_sql_file(init_file)
        query = f"""REPLACE INTO TaxType (typeName, taxTypeID) VALUES
            {','.join([f'("{key}",{val.value})' for key, val in tax_enums.TaxType.__members__.items()])}"""
        self.execute(query)
        if add_taxa:
            GENOME_TAX_TABLE = """
                CREATE TABLE IF NOT EXISTS {0} (
                    genomeID              INT UNSIGNED NOT NULL,
                    rankID                INT UNSIGNED NOT NULL,
                    taxID                 INT UNSIGNED DEFAULT NULL,
                    taxon                 VARCHAR(255) DEFAULT NULL,
                    FOREIGN KEY (genomeID)      REFERENCES Genome (genomeID) ON DELETE CASCADE,
                    FOREIGN KEY (rankID)        REFERENCES TaxRank (rankID) ON DELETE CASCADE,
                    PRIMARY KEY (genomeID, rankID)
                );"""
            TAXID_TABLE = """
                CREATE TABLE IF NOT EXISTS {0} (
                    taxID                   INT NOT NULL,
                    rankID                  INT UNSIGNED NOT NULL,
                    taxon                   VARCHAR(255) NOT NULL,
                    parent                  INT NOT NULL,
                    PRIMARY KEY (taxID),
                    KEY (taxon),
                    INDEX (parent) -- do we need this?
                );"""
            for taxonomy in tax_enums.TaxType:
                self.execute(
                    GENOME_TAX_TABLE.format(self._taxon_type2table(taxonomy.name))
                )
                self.execute(TAXID_TABLE.format(self._taxid_type2table(taxonomy.name)))

        for init2_file in EXTRA_FILES:
            self.run_sql_file(init2_file)
        rows = []
        for tt in tax_enums.TaxType:
            for key, obj in tax_enums.TaxLevel.__members__.items():
                value = obj.value
                rows += [f"('{key}', {value}, {1 if value < 8 else 0}, {tt.value})"]
        query = f"""INSERT IGNORE INTO TaxRank (rankName, RankID, compulsory, taxTypeID) VALUES {
            ",".join(rows)}"""
        self.execute(query)

        query = (
            "INSERT INTO Genome (genomeID, filepath, scaffolds, seqLength, fileHash, addedAt) "
            'VALUES (1,"",0,0,"",NULL)'
        )
        self.execute(query)

        self.conn.commit()
        logger.debug("Database initialization done %s", self.db_name)

    def reset_counter(self, tables: Optional[list[str]] = None):  # type: ignore
        tables: list[str] = ["Genome"] if tables is None else tables
        for table in tables:
            self.execute(f"ALTER TABLE {table} AUTO_INCREMENT=1;")

    def get_visible(
        self,
        genome_id_list: npt.NDArray[np.uintc] = np.array([], dtype=np.uintc),
        schemeSetID: Optional[int] = 1,
    ) -> pd.DataFrame:
        """filters hidden genomes

        Args:
                genome_id_list (np.ndarray[np.uintc], optional): list of genomeIDs to find visible LINs for. Defaults to None.
                schemeSetID (int, optional): schemeSet's LINs to affect, None effects all. Defaults to 1.

        Returns:
                DataFrame: Dataframe with default LIN format columns=["genomeID", "parent", "idx", "lastValue"]
        """
        condition = f" AND schemeSetID={schemeSetID} "
        if genome_id_list is None:
            return pd.DataFrame()
        if schemeSetID is None:
            condition = ""

        query = f"""SELECT genomeID, parent, idx, lastValue, schemeSetID FROM LINTree
                        WHERE genomeID IN {util.list2sql(genome_id_list.tolist())} {condition}"""
        result = self.execute(query)
        return pd.DataFrame(
            result, columns=["genomeID", "parent", "idx", "lastValue", ""]
        )

    # hides lin from LIN view but keeps record in LIN_back
    def hide_lin(self, genome_id_list: list[int], schemeSetID: Optional[int] = 1):
        """hides LINs from view (delete with history)

        Args:
                genome_id_list (list[np.uintc], optional): list of genomes to hide/delete. Defaults to None.
                schemeSetID (int, optional): schemeSet's LINs to affect, None effects all. Defaults to 1.
        """
        condition = f" AND schemeSetID={schemeSetID} "
        if genome_id_list is None or len(genome_id_list) < 1:
            return
        if schemeSetID is None:
            condition = ""

        logger.debug("Hiding genomes: %s", genome_id_list)
        query = f"""UPDATE LINTree_back SET Visible=0 WHERE genomeID IN
                {util.list2sql(list(genome_id_list))} {condition};"""
        self.execute(query)
        self.commit()

    # removes genome from database entirely (using cascading effect)
    def remove_genome(self, genomeID: int):
        if genomeID is None:
            return
        query = f"DELETE FROM Genome WHERE genomeID={genomeID}"
        self.execute(query, raise_exception=False)
        logger.debug("Removed genome with ID: %s", genomeID)

    def new_lin(
        self,
        genomeID: int,
        neighborID: Optional[int],
        change_index: int = 0,
        schemeID=1,
        add=True,
    ):
        """Adds a new LIN to a genome

        Args:
                genomeID (int): ID of the genome to add a LIN for
                neighborID (int): ID of the closest genome
                change_index (int): index of the LIN to change
                schemeID (int, optional): ID of the schemeSet to use. Defaults to 1.
        """

        if neighborID is not None and change_index > 0:
            # normal case with inherited lin prefix
            parent_df = self.get_lin_parents(neighborID, schemeID)

            # remove keys that are >= change_index
            parent_df = parent_df[parent_df["idx"] < change_index]
            if parent_df.empty:
                parentID = 1
                change_index = 0
            else:
                parentID = int(
                    parent_df[parent_df["idx"] == parent_df["idx"].max()][
                        "genomeID"
                    ].iloc[0]
                )
        elif neighborID is None:
            # first genome or non-related genome
            neighborID = parentID = 1
            change_index = 0
        else:
            # lin with no inherited prefix
            parentID = 1

        query = ""

        if add:
            query += "INSERT INTO LINTree (genomeID, schemeSetID, idx, lastValue, parent, neighbor) "
        query += f"""SELECT {genomeID}, {schemeID},
                {change_index}, COALESCE(MAX(lastValue),0) + 1, {parentID}, {neighborID} FROM
                    LINTree WHERE parent = {parentID} AND idx = {change_index} AND schemeSetID = {schemeID};
        """
        result = self.execute(query)
        if add:
            logger.debug("Added LIN %s", query[:100])
            return pd.DataFrame()

        result_df = pd.DataFrame(
            result,
            columns=[
                "genomeID",
                "schemeSetID",
                "idx",
                "lastValue",
                "parent",
                "neighbor",
            ],
        )
        return result_df

    def add_genome(
        self,
        filepath: str,
        scaffolds: int = 0,
        genome_length: int = 0,
        hash: Optional[str] = None,
    ) -> Tuple[int, bool]:
        hash = compression.uniform_hash_file(filepath) if hash is None else hash
        query = f'SELECT genomeID FROM Genome WHERE fileHash="{hash}";'
        result = self.execute(query)
        if len(result) > 0:
            id = int(result[0][0])
            logger.warning(
                "The genome file %s already existed within the database with ID: %s",
                os.path.basename(filepath),
                id,
            )
            return id, True

        filepath = os.path.basename(filepath)
        query = f"""
            INSERT INTO Genome (filePath, scaffolds, seqLength, fileHash) VALUES
            ("{filepath}", {self._jnull(scaffolds)}, {self._jnull(genome_length)}, "{hash}")"""
        logger.debug("Add genome %s", query)
        self.execute(query)
        return self.lastRowID(), False

    def update_genome_scaffolds(
        self, genomeID: int, scaffolds: int = 0, genome_length: int = 0
    ):
        if scaffolds < 1 and genome_length < 1:
            logger.warning("Nothing to update on genome %d", genomeID)
        query = f"""
            UPDATE Genome set scaffolds={scaffolds}, seqLength={genome_length} WHERE
            genomeID={genomeID}"""
        logger.debug("Updated genome %d with %s", genomeID, query)
        self.execute(query)

    def update_scheme_filter_link(
        self, schemeID: int, filterIDs: npt.NDArray[np.uintc]
    ):
        """adds/updates filters to baseScheme

        Args:
                schemeID (int): parent baseScheme to add filters to.
                filterIDs (np.ndarray[np.unitc]): filters to add to the baseScheme (ordered).
        """
        if filterIDs is None or len(filterIDs) < 1:
            return
        PATTERN = "({},{},{})"
        values = ",".join(
            [
                PATTERN.format(schemeID, bscheme, idx + 1)
                for idx, bscheme in enumerate(filterIDs)
            ]
        )

        query = f"""
            INSERT INTO BaseScheme_Filters (baseSchemeID, filterID, idx) VALUES {values}
            ON DUPLICATE KEY UPDATE filterID = VALUES(filterID);
        """
        # TODO: This could fail if any of the filterIDs are not defined
        self.execute(query)
        logger.debug(
            "Filters %s were added to BaseScheme %s", list(filterIDs), schemeID
        )

    # def add_base_scheme(self, LIN_percents, number_of_labels, description) -> int:
    @overload
    def add_base_scheme(
        self,
        *,
        positions: "np.ndarray",
        description: Optional[str] = None,
        thresholds: Optional[npt.NDArray] = None,
        filterIDs: Optional[npt.NDArray[np.uintc]] = None,
    ) -> int:
        """Adds a baseScheme to the database by custom positions

        Args:
                positions (np.ndarray): list of LIN positions
                description (str, optional): description of the BaseScheme. Defaults to None.
                thresholds (np.ndarray, optional): list of thresholds, float (value) or int (topk),
                        to switch to next filter. Defaults to None.

        Returns:
                int: ID of the newly created BaseScheme
        """
        '''thresholds = self._jnull(thresholds)
        description = self._jnull(description)
        query = f"""
            INSERT INTO BaseScheme (minEffect, maxEffect, positionCount, thresholds, description)
            VALUES ({np.min(positions)},
                    {np.max(positions)},
                    {len(positions)},
                    {thresholds},
                    {description});
        """
        self.execute(query)
        returned_id = self.lastRowID()
        logger.debug("New BaseScheme added with ID {}".format(returned_id))
        return returned_id'''

    @overload
    def add_base_scheme(
        self,
        *,
        min: float,
        max: float,
        step: float,
        description: Optional[str] = None,
        thresholds: Optional[npt.NDArray] = None,
        filterIDs: Optional[npt.NDArray[np.uintc]] = None,
    ) -> int:
        """Adds a baseScheme to the database with uniform steps

        Args:
                min (float): min range of BaseScheme (inclusive)
                max (float): max range of BaseScheme (inclusive)
                step (float): steps to take between min-max of BaseScheme
                description (str, optional): description of the BaseScheme. Defaults to None.
                thresholds (np.ndarray, optional): list of thresholds, float (value) or int (topk),
                        to switch to next filter. Defaults to None.
                filterIDs (np.ndarray[np.uintc], optional): list of filterIDs, to link scheme with. Defaults to None.

        Returns:
                int: ID of the newly created BaseScheme
        """
        '''thresholds = self._jnull(thresholds)
        description = self._jnull(description)

        query = f"""
            INSERT INTO BaseScheme (minEffect, maxEffect, step, positionCount, thresholds, description)
            VALUES ({min},
                    {max},
                    {step},
                    {len(np.arange(min,max,step))},
                    {thresholds},
                    {description});
        """
        self.execute(query)
        returned_id = self.lastRowID()
        logger.debug("New BaseScheme added with ID {}".format(returned_id))
        return returned_id'''

    def add_base_scheme(self, **kwargs) -> int:
        thresholds = self._jnull(kwargs.get("thresholds"))
        description = self._jnull(kwargs.get("description"))
        columns = ["minEffect", "maxEffect", "positionCount", "thresholds", "note"]
        if kwargs.get("min") is not None:
            min: float = kwargs.get("min", 70)
            max: float = kwargs.get("max", 99)
            step: float = kwargs.get("step", 1)
            count = len(np.arange(min, max + (step / 2), step))
            columns += ["step"]
            extra_values = f"{step}"
        elif kwargs.get("positions") is not None:
            positions = kwargs.get("positions", np.array([], dtype=np.uintc))
            min = np.min(positions)
            max = np.max(positions)
            count = len(positions)
            columns += ["positions"]
            extra_values = f"{self._jnull(positions)}"
        else:
            raise AttributeError("Please check your parameters")
        values = f"{min}, {max}, {count}, {thresholds}, {description}, {extra_values}"
        query = f"""
            INSERT INTO BaseScheme ({",".join(columns)})
            VALUES ({values});
        """
        self.execute(query)
        returned_id = self.lastRowID()
        logger.debug("New BaseScheme added with ID %s", returned_id)

        filterIDs = kwargs.get("filterIDs")
        if filterIDs is not None:
            self.update_scheme_filter_link(returned_id, kwargs.get("filterIDs", None))
            logger.debug("New BaseScheme filters connected")
        return returned_id

    def update_scheme_link(
        self,
        schemeSetID: int,
        baseSchemeIDs: npt.NDArray[np.uintc],
        levels: Optional[npt.NDArray[np.uintc]] = None,
    ):
        """add/update links between schemes

        Args:
                schemeSetID (int): ID of the SchemeSet to link baseSchemes to.
                baseSchemeIDs (np.ndarray[np.uintc]): ordered list of baseSchemes to add to schemeSet.
                levels (np.ndarray[np.intc], optional): depth of LIN positions to select genomes to apply filters to . Defaults to None.
        """

        # adding links to the base schemes
        PATTERN = "({},{},{},{})"
        if levels is None:
            levels = np.ones(len(baseSchemeIDs)).astype(np.uintc)
        values = ",".join(
            [
                PATTERN.format(schemeSetID, bscheme, idx + 1, levels[idx])
                for idx, bscheme in enumerate(baseSchemeIDs)
            ]
        )

        query = f"""
            INSERT INTO SchemeSet_Base (schemeSetID, baseSchemeID, idx, positionDepth) VALUES {values};
        """
        # TODO: This could fail if any of the baseSchemeIDs are not defined
        self.execute(query)
        logger.debug(
            "baseSchemes %s were added to schemeSet %s",
            baseSchemeIDs.tolist(),
            schemeSetID,
        )

    def add_scheme_set(
        self,
        base_schemeIDs: npt.NDArray[np.uintc],
        score: Optional[str] = None,
        levels: Optional[npt.NDArray[np.uintc]] = None,
        note: Optional[str] = None,
    ) -> int:
        """Adds a schemeSet to the database

        Args:
                base_schemeIDs (np.ndarray[np.intc]): ID of basic schemes to add to add as blocks to schemeSet
                score (str, optional): name of the scoring function. should be defined in scheme_reducers.py. Defaults to None.
                levels (np.ndarray[np.intc], optional): depth of LIN positions to select genomes to apply filters to . Defaults to None.
                note (str, optional): description of schemeSet. Defaults to None.

        Returns:
                int: ID of the newly created schemeSet
        """
        # TODO: Is is not better to use the function itself and get the string from there
        query = f"""INSERT INTO SchemeSet (blockCount, scoreF, note) VALUES
                    ({len(base_schemeIDs)},
                    {self._jnull(score)},
                    {self._jnull(note)});"""

        self.execute(query)
        schemeSetID = self.lastRowID()
        logger.debug("New SchemeSet added with ID %s", schemeSetID)

        self.update_scheme_link(schemeSetID, base_schemeIDs, levels=levels)
        # add lingroup root
        self.add_prefix_roots(schemeSetID)

        return schemeSetID

    def add_lin_pos1(self, genomeID: int, schemeSetID: int, val: int, neighbor=None):
        # CAUTION: This function is unsafe and will throw an error if a LIN with the same first
        # position exists for the schemeSet
        query = f"""INSERT INTO LIN (genomeID,schemeSetID,idx,lastValue,parent,neighbor) VALUES
                ({genomeID},{schemeSetID},1,{val},0,{neighbor})"""
        logger.debug("Add fixed LIN %s", query)
        self.execute(query)

    @classmethod
    def _taxid_type2table(cls, tax_type: str):
        return f"{tax_type.upper()}_IDs"

    @classmethod
    def _taxon_type2table(cls, tax_type: str):
        return f"{tax_type.upper()}_taxon"

    def taxonomy_rank_table(self, tax_type="ncbi") -> dict[int, str]:
        """returns entire taxonomy rank table for a database

        Args:
                tax_type (str, optional): taxonomy hierarchical database. Defaults to "ncbi".
                key  (bool, optional): whether to take rankID as the dict key or rankName. Defaults to True.

        Returns:
                dict: with {rankID:rankName, ...} format
        """
        query = f"""
            SELECT rankID, rankName FROM TaxRank JOIN TaxType USING (taxTypeID)
            WHERE typeName = "{tax_type}";
        """
        results = self.execute(query)
        rank_table = {int(e[0]): e[1] for e in results}
        logger.debug("Rank table retrieved %s", rank_table)
        return rank_table

    def taxonomy_rank_table_name_key(self, tax_type="ncbi") -> dict[str, int]:
        """returns entire taxonomy rank table for a database

        Args:
                tax_type (str, optional): taxonomy hierarchical database. Defaults to "ncbi".
                key  (bool, optional): whether to take rankID as the dict key or rankName. Defaults to True.

        Returns:
                dict: with {rankName:rankID, ...} format
        """
        query = f"""
            SELECT rankID, rankName FROM TaxRank JOIN TaxType USING (taxTypeID)
            WHERE typeName = "{tax_type}";
        """
        results = self.execute(query)
        rank_table = {e[1]: int(e[0]) for e in results}
        logger.debug("Rank table retrieved %s", rank_table)
        return rank_table

    @overload
    def rank_conversion(self, rank: str, tax_type="ncbi") -> dict:
        """rank-rankName conversion

        Args:
                rank (int): rankID to convert to name
                tax_type (str, optional): taxonomy hierarchical database. Defaults to "ncbi".

        Raises:
                TypeError: TypeError if rank is not int

        Returns:
                dict: with {rankID:rankName, ...} format
        """

    @overload
    def rank_conversion(self, rank: int, tax_type="ncbi") -> dict:
        """rankName-rankID conversion

        Args:
                rank (str): rank name to convert to rankID
                tax_type (str, optional): taxonomy hierarchical database. Defaults to "ncbi".

        Raises:
                TypeError: TypeError if rank is not int

        Returns:
                dict: with {rankID:rankName, ...} format
        """

    def rank_conversion(self, rank: Union[int, str], tax_type="ncbi") -> dict:
        """rank-rankName bi-directional conversion

        Args:
                rank (Union[int,str]): rank OR rank name to convert
                tax_type (str, optional): taxonomy hierarchical database. Defaults to "ncbi".

        Raises:
                TypeError: TypeError if rank is not int or str

        Returns:
                dict: with {rankID:rankName, ...} format
        """
        if isinstance(rank, str):
            condition = f" AND rankName = '{rank}' "
        elif isinstance(rank, int):
            condition = f" AND rankID = {rank} "
        else:
            raise TypeError(
                "Rank conversion only supports str and int got '%s'" % type(rank)
            )

        query = f"""
            SELECT rankID, rankName FROM TaxRank JOIN TaxType USING (taxTypeID)
            WHERE typeName = "{tax_type}" {condition};
        """
        results = self.execute(query)
        rank_dict = {e[0]: e[1] for e in results}
        logger.debug("Rank dictionary retrieved %s", rank_dict)
        return rank_dict

    # TODO: implement contains start, finish, middle version
    def taxid_conversion(
        self, taxon: Union[int, str], rankID: Optional[int] = None, tax_type="ncbi"
    ) -> pd.DataFrame:
        """taxid-taxon bi-directional conversion

        Args:
                taxon (Union[int,str]): taxid OR taxon to convert
                rankID (int, optional): rank to find the taxon at. default searches all ranks. Defaults to None.
                tax_type (str, optional): taxonomy hierarchical database. Defaults to "ncbi".

        Raises:
                TypeError: TypeError if taxon is not int or str

        Returns:
                DataFrame: table format of the taxon with columns=["taxID", "rankID", "taxon", "parent"]
        """
        table = self._taxid_type2table(tax_type)
        if isinstance(taxon, str):
            condition = f" taxon = '{taxon}' "
        elif isinstance(taxon, int):
            condition = f" taxID = {taxon} "
        else:
            raise TypeError(
                "Taxon conversion only supports str and int got '%s'" % type(taxon)
            )

        if rankID is not None:
            condition = f"{condition} AND rankID = {rankID} "

        query = f"""
            SELECT taxID, rankID, taxon, parent FROM {table}
            WHERE {condition};
        """
        results = self.execute(query)
        conversion_df = pd.DataFrame(
            results, columns=["taxID", "rankID", "taxon", "parent"]
        )
        conversion_df = conversion_df.astype(
            dtype={"taxID": int, "rankID": int, "taxon": str, "parent": int}
        )
        logger.debug("Converted taxIDs %s", conversion_df.values.tolist())
        return conversion_df

    def add_taxonomy(self, genomeID: int, tax_dict: dict, tax_type="ncbi", clean=False):
        """Adds or updates taxonomy for a genome

        Args:
                genomeID (int): ID of the genome to add taxonomy for
                tax_dict (dict): taxonomy in dict format with format {rankID:taxonID/taxon (ID if available),...}
                tax_type (str, optional): type of hierarchical taxonomy. Defaults to 'ncbi'.
                clean (bool, optional): all old taxonomies of the type will be removed before adding. Default False.

        Raises:
                e: non-integer rank
        """
        if clean:
            self.remove_taxonomy(genomeID, tax_type=tax_type)
        table = self._taxon_type2table(tax_type)
        try:
            np.array(list(tax_dict.keys()), dtype=np.intc)
        except TypeError as e:
            logger.error("tax_dict has non integer ranks (keys) %s", tax_dict)
            exceptions.handle_exception(e, logger)

        values = []
        for k, v in tax_dict.items():
            if isinstance(v, str):
                values += [f"""({genomeID},{k},NULL,"{v.replace('"', '').lower()}")"""]
            elif isinstance(v, int):
                values += [f"""({genomeID},{k},{v},NULL)"""]

        if not values:
            logger.debug("Adding taxonomy skipped, taxonomy empty")
            return
        query = f"""INSERT INTO {table} (genomeID,rankID,taxID,taxon) VALUES {",".join(values)}
                    ON DUPLICATE KEY UPDATE taxID=VALUES(taxID), taxon=VALUES(taxon);
        """
        logger.debug("Adding taxonomy %s", query)
        self.execute(query, raise_exception=False)

    def remove_taxonomy(
        self, genomeID: int, tax_levels: Optional[list[int]] = None, tax_type="ncbi"
    ):
        """Adds or updates taxonomy for a genome

        Args:
                genomeID (int): ID of the genome to add taxonomy for
                tax_lvls (list, optional): rank levels to remove. Defaults to None (removes all).
                tax_type (str, optional): type of hierarchical taxonomy. Defaults to 'ncbi'.

        Raises:
                e: non-integer rank
        """
        tax_levels = tax_levels or []
        table_name = self._taxon_type2table(tax_type)
        try:
            np.array(tax_levels, dtype=int)
        except TypeError as e:
            logger.error("tax_lvls has non integer ranks %s", tax_levels)
            exceptions.handle_exception(e, logger)
        if len(tax_levels) < 1:
            condition = ""
        else:
            condition = f"AND rankID IN {util.list2sql(tax_levels)}"

        query = f"""DELETE FROM {table_name} WHERE genomeID = {genomeID} {condition};
        """
        logger.debug("removing taxonomy %s", query)
        self.execute(query)

    def traverse_taxonomy(self, taxid: int, tax_type: str = "ncbi") -> pd.DataFrame:
        table = self._taxid_type2table(tax_type)
        query2 = f"""
            WITH RECURSIVE get_parent (nodeID, par, i, val) AS (
                SELECT taxID, parent, rankID, taxon
                FROM {table} WHERE taxID = {taxid}
                UNION
                SELECT c.taxID, c.parent, c.idx, c.lastValue
                FROM {table} c
                INNER JOIN get_parent ON c.taxID = get_parent.par)
            SELECT * FROM get_parent ORDER BY i;
        """

        result = self.execute(query2)
        taxon_tree = pd.DataFrame(
            result, columns=["taxID", "rankID", "taxon", "parent"]
        )
        taxon_tree = taxon_tree.astype(
            dtype={"taxID": int, "rankID": int, "taxon": str, "parent": int}
        )
        logger.debug("Traversed taxonomy %s", taxon_tree.values.tolist())
        return taxon_tree

    def get_taxonomy(
        self, genomeID: list[int], tax_type: str = "ncbi", ranks: Optional[list[int]] = None  # type: ignore
    ) -> pd.DataFrame:
        ranks = ranks or []
        tax_type = tax_type or "ncbi"
        df_cols = ["genomeID", "rankID", "taxID", "taxon"]

        condition = f" WHERE genomeID={genomeID} "
        if isinstance(genomeID, list):
            if (genome_count := len(genomeID)) < 1:
                return pd.DataFrame(columns=df_cols)
            condition = f" WHERE genomeID IN {util.list2sql(genomeID)} "

        parsed_ranks = util.list2sql(ranks)
        if len(parsed_ranks) > 0:
            condition += f" AND rankID IN {parsed_ranks}"

        taxon_table = self._taxon_type2table(tax_type)
        taxid_table = self._taxid_type2table(tax_type)
        query = f"""
            SELECT genomeID, rankID, taxID,
            COALESCE({taxid_table}.taxon, {taxon_table}.taxon) as taxon
            FROM {taxon_table} LEFT JOIN
                {taxid_table} USING (rankID, taxID) {condition}
                ORDER BY genomeID, rankID;
        """
        result = self.execute(query)
        taxon_df = pd.DataFrame(result, columns=df_cols)
        logger.debug("Retrieved taxonomy %s", taxon_df.values.tolist())
        return taxon_df
        # if asdict:
        # 	return {e["rankID"]:e["taxID"] for e in taxon_df.iterrows() if e["taxID"] != np.nan}
        # return taxon_df

    def update_taxids(self, taxid_table: pd.DataFrame, type: str = "ncbi"):
        table = self._taxid_type2table(type)
        query = f"""
            SELECT COLUMN_NAME, IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS WHERE
            table_schema = '{self.db_name}' AND table_name = '{table}'
            """
        results = self.execute(query)
        db_cols = {e[0]: e[1] for e in results}
        # the input dataframe has to match the columns of the database
        db_mandatory_cols = [k for k, v in db_cols.items() if v == "NO"]
        input_cols = list(taxid_table.keys())
        intersect = set(db_cols) & set(input_cols)
        if set(db_mandatory_cols).issubset(input_cols):
            logger.error(
                "TaxIDs updated failed DB: %s; Input: %s", db_mandatory_cols, input_cols
            )
            raise TypeError(
                "Input table columns do not match db table columns\n"
                "DB: '%s'; Input: '%s'" % (db_mandatory_cols, input_cols)
            )

        flat_intersect_list: list = [tuple(e) for e in taxid_table[intersect].values.tolist()]  # type: ignore
        query = (
            f"""INSERT INTO {table} {tuple(intersect)} VALUES"""
            f'{",".join(flat_intersect_list)} AS new'  # type: ignore
            """ON DUPLICATE KEY UPDATE
                    {",".join([f" {e} = new.{e}" for e in intersect])};
        """
        )
        self.execute(query)
        logger.info("TaxIDs updated")

    @overload
    def get_genome_path(self, genomeID: int) -> pd.DataFrame:
        """retrieves path to the genome with the specified genomeID

        Args:
                genomeID (int): genomeID to search by

        Returns:
                DataFrame: DataFrame with columns=["genomeIDs", "filePath"]
        """

    @overload
    def get_genome_path(self, genomeID: npt.NDArray[np.uintc]) -> pd.DataFrame:
        """retrieves path to the genomes with the specified genomeIDs

        Args:
                genomeIDs (np.ndarray[np.uintc]): list of genomeIDs to search by. Ignores the ones not found.

        Returns:
                DataFrame: DataFrame with columns=["genomeIDs", "filePath"]
        """

    def get_genome_path(
        self, genomeID: Union[npt.NDArray[np.uintc], int]
    ) -> pd.DataFrame:
        if isinstance(genomeID, int):
            genomeID = np.array([genomeID], dtype=np.uintc)
        query = f"SELECT genomeID, filePath FROM Genome WHERE genomeID in {util.list2sql(genomeID.tolist())}"
        genome_paths = self.execute(query)
        logger.debug("Get genomePath %s", query)
        result_df = pd.DataFrame(genome_paths, columns=["genomeID", "filePath"])
        result_df = result_df.astype(dtype={"genomeID": int, "filePath": str})
        return result_df

    def get_genomeID(self, filename: str):
        return self.get_genome(genomeID=filename, field="path")

    @overload
    def get_genome(self, genomeID: int) -> pd.DataFrame:
        """finds genome by genomeID

        Args:
                genomeID (int): ID of the genome to search by.

        Returns:
                DataFrame: DataFrame with columns=["genomeID", "filePath", "scaffolds", "fileHash"]. Empty if no matches are found
        """

    @overload
    def get_genome(self, genomeID: str, field="path") -> pd.DataFrame:
        """finds genome by path or hash

        Args:
                genome_identifier (str): identifier string, fullpath or file hash
                field (str, optional): field to search by. Supports one of ["path", "hash"]. Defaults to "path".

        Returns:
                DataFrame: DataFrame with columns=["genomeID", "filePath", "scaffolds", "seqLength", "fileHash"]. Empty if no matches are found
        """

    def get_genome(self, genomeID: Union[int, str], field="path") -> pd.DataFrame:
        query = "SELECT genomeID, filePath, scaffolds, seqLength, fileHash FROM Genome WHERE "
        if isinstance(genomeID, int):
            query += f""" genomeID = {genomeID} """
        elif field == "hash":
            query += f""" fileHash = "{genomeID}" """
        elif field == "path":
            query += f""" filePath like "{os.path.realpath(genomeID)}" """
        else:
            raise TypeError("Can not retrieve genome by '%s' '%s'" % (field, genomeID))
        logger.debug("Get genome %s", query)

        results = self.execute(query)
        result_df = pd.DataFrame(
            results,
            columns=["genomeID", "filePath", "scaffolds", "seqLength", "fileHash"],
        )
        result_df = result_df.astype(
            dtype={
                "genomeID": int,
                "filePath": str,
                "scaffolds": int,
                "seqLength": int,
                "fileHash": str,
            }
        )
        return result_df

    def get_lin_depth(
        self, depth: int = 0, schemeSetID: int = 1
    ) -> npt.NDArray[np.uintc]:
        """Retrieves genomeIDs for the filter to compare the query by

        Args:
                depth (int, optional): number of positions to consider (starts from zero), negative depth signifies full depth. Defaults to 0.
                schemeSetID (int, optional): ID if the schemeSet to search lins in. Defaults to 1.

        Returns:
                np.ndarray[np.uintc]: unsigned int ndarray of genomeIDs
        """
        comparator = "<="
        if depth < 0:
            comparator = ">="
            depth = -1
        elif depth == 0:
            comparator = "="
        query = f"SELECT genomeID FROM LINTree WHERE idx {comparator} {depth} AND SchemeSetID = {schemeSetID}"
        results = [s[0] for s in self.execute(query)]
        return np.array(results, dtype=np.uintc)

    def get_LIN(self, genomeID: int, schemeID=1, visibleOnly=True) -> dict[int, int]:
        if genomeID is None:
            return {}
        nodes = self.get_lin_parents(
            genomeID, schemeID=schemeID, visibleOnly=visibleOnly
        )
        return (
            {row["idx"]: row["lastValue"] for _, row in nodes.iterrows()}
            if not nodes.empty
            else {}
        )

    def get_genome_lin_schemes(self, genomeID: int, visibleOnly=True) -> list[int]:
        """get the last lin position for every scheme given a genome

        Args:
            genomeID (int): ID of the target genome
            visibleOnly (bool, optional): include only visible LINs (not historically deleted ones). Defaults to True.

        Returns:
            list: rows fom LINTree showing the genome specific last position and information to build a LIN
        """
        columns = ["genomeID", "parent", "idx", "lastValue", "schemeSetID"]
        if genomeID is None:
            return []
        table_name = "LINTree_back" if visibleOnly else "LINTree"

        query = f"""SELECT genomeID, parent, idx, lastValue, schemeSetID FROM {table_name} WHERE genomeID={genomeID};"""
        result = self.execute(query)
        lin_df = pd.DataFrame(result, columns=columns)
        return lin_df["schemeSetID"].to_list() if not lin_df.empty else []

    def get_all_LINs(self, schemeSetID: int, filterID=1):
        condition = f" WHERE schemeSetID={schemeSetID} "
        columns = ["genomeID", "parent", "idx", "lastValue"]
        query = f"SELECT {','.join(columns)} from LINTree {condition};"
        logger.debug("Get all LINs of schemeSet %s", schemeSetID)
        results = self.execute(query)
        logger.debug("Retrieved %s LINs ", len(results))
        return pd.DataFrame(results, columns=columns)

    def close(self):
        logger.debug("Closing DB connection %s ", self.db_name)
        self.conn.close()

    # TODO: fix
    """def verify_lin(self, lin_id: int):
        columns = ["LIN_ID", "Genome_ID", "Scheme_ID", "tail", "SubjectGenome", \
                   "ANI", "LabelNum", "Cutoff", "LIN", "SubjectLIN"]
        query = "SELECT {0} FROM " \
                "(SELECT lin_ani.LIN_ID, lin_ani.Genome_ID, lin_ani.Scheme_ID, lin_ani.tail, lin_ani.SubjectGenome, " \
                "lin_ani.ANI, lin_ani.LIN, LIN.LIN as SubjectLIN " \
                "FROM (SELECT * FROM LIN LEFT JOIN ANI USING (Genome_ID) WHERE LIN_ID = {1}) lin_ani " \
                "LEFT JOIN LIN ON (lin_ani.SubjectGenome = LIN.Genome_ID) WHERE lin_ani.Scheme_ID = LIN.Scheme_ID) lins " \
                "LEFT JOIN Scheme USING (Scheme_ID)".format(", ".join(columns), lin_id)

        result = self.execute(query)
        changing_index = get_lin_cutoff_index(result[columns.index("ANI")], result[columns.index("Cutoff")].split(","))
        lin = result[columns.index("LIN")].split(",")
        subj_lin = result[columns.index("SubjectLIN")].split(",")
        prefix_status = lin[:changing_index] == subj_lin[:changing_index]
        index_status =  lin[changing_index] != subj_lin[changing_index]
        postfix_status = np.sum(np.array(lin[changing_index+1:], dtype=np.integer)) == 0
        if prefix_status and index_status and postfix_status:
            return True
        return False"""

    def _parse_json_measure(self, json_str: str):
        if json_str == "NULL" or json_str is None:
            return []
        else:
            return list(json.loads(json_str))

    def get_comparison(
        self,
        queryIDs: list[int],
        filterIDs: list[int],
        referenceID: Optional[list[int]] = None,
        shared_filter: Optional[list[int]] = None,
        **kwargs,
    ) -> pd.DataFrame:

        # queryIDs = [int(id) for id in queryIDs]
        # filterIDs = [int(id) for id in filterIDs]
        # if referenceID:
        #     referenceID = [int(id) for id in referenceID]
        # if shared_filter:
        #     shared_filter = [int(id) for id in shared_filter]

        ref_condition = ""
        if not queryIDs or not filterIDs:
            return pd.DataFrame(
                columns=["queryID", "refID", "filterID", "rankScore", *MEASURE_COLS]
            )
        if referenceID:
            referenceID_str = util.list2sql(referenceID)
            ref_condition = f" AND refID IN {referenceID_str} "

        if not shared_filter:
            shared_filter = []
        using_filters = util.list2sql(list(set().union([*filterIDs, *shared_filter])))

        query = f"""
            SELECT queryID, refID, filterID, rankScore,
                {",".join(MEASURE_COLS)}
            FROM Comparison WHERE
                queryID IN {util.list2sql(queryIDs)} AND filterID IN {using_filters} {ref_condition}
            ORDER BY queryID, refID, filterID, rankScore DESC;
        """
        results = self.execute(query)

        result_df = pd.DataFrame(
            results,
            columns=["queryID", "refID", "filterID", "rankScore", *MEASURE_COLS],
        )
        # converters={"measureN": self._parse_json_measure})
        # removing duplicate results for the same reference
        result_df = result_df.groupby("refID", as_index=False).first()

        # combining all measures together and removing empty string (NULL)
        result_df["measure"] = self.combining_measures(result_df)

        result_df = result_df.drop(MEASURE_COLS, axis=1)
        result_df = result_df.astype(
            dtype={"queryID": int, "refID": int, "filterID": int, "rankScore": float}
        )
        logger.debug("Retrieved JSON measures %s", result_df.values.tolist())
        return result_df

    def add_comparison(
        self,
        queryID: int,
        filterID: int,
        referenceIDs: list,
        comparisons: npt.NDArray[np.floating],
        rankscore: npt.NDArray[np.floating],
    ):
        comparisons = comparisons.tolist()
        if (
            np.array(referenceIDs).size < 1
            or comparisons is None
            or np.array(comparisons).size < 1
            or rankscore.size < 1
        ):
            logger.debug(
                "No measurements provided for query %s, reference %s for filter %s",
                queryID,
                referenceIDs,
                filterID,
            )
            return

        for i, comparison in enumerate(comparisons):
            comparisons[i] = comparison + ["NULL"] * (4 - len(comparison))

        comparisons = np.array(comparisons, dtype=object)
        values = []
        for idx in range(len(comparisons)):
            values += [
                f"""({queryID},{referenceIDs[idx]},{filterID},{rankscore[idx]},
                {comparisons[idx,0]},{comparisons[idx,1]},{comparisons[idx,2]},
                {comparisons[idx,3]},{self._jnull(comparisons[idx,4:].tolist())})"""
            ]

        query = f"""
            INSERT IGNORE INTO Comparison (queryID, refID, filterID, rankScore,
            measure1, measure2, measure3, measure4, measureN) VALUES {",".join(values)}
        """
        logger.debug("Add Measurements: %s", query)
        _ = self.execute(query)

    '''# TODO: Check syncing issue with multiple statements in one;
    def get_parent(self, genomeID: int, schemeID=1, neighbor=False) -> pd.DataFrame:
        """gets the parent genome based on an already defined and calculated schemeSet LIN tree.
        The neighbor argument extends its usage refer to get_neighbor.

        Args:
                genomeID (int): ID of genome to get its parent
                schemeID (optional): ID of schemeSet(complex scheme) to use calculated the LIN tree]. Defaults to 1.
                neighbor (bool, optional): gets neighbor (closest element) instead of parent (completely different tree)]. Defaults to False.

        Returns:
                DataFrame: Result in Dataframe format with columns [genomeID,parent,idx,lastValue]
        """
        parent = "neighbor" if neighbor else "parent"
        query = f"""
            SELECT {parent} INTO @parent FROM LINTree WHERE genomeID={genomeID} AND schemeSetID={schemeID};
            SELECT genomeID,{parent},idx,lastValue
                FROM LINTree WHERE genomeID=@parent AND schemeSetID={schemeID};
            """
        result = self.execute(query)
        parent_genome = pd.DataFrame(
            result, columns=["genomeID", parent, "idx", "lastValue"]
        )
        parent_genome = parent_genome.astype(dtype=int)
        logger.debug(
            "%s genome retrieved %s", parent.capitalize(), parent_genome.values.tolist()
        )
        return parent_genome
    '''

    def get_neighbor(self, genomeID: int, schemeID=1) -> pd.DataFrame:
        """gets the closest genome based on an already defined and calculated schemeSet

        Args:
                genomeID (int): [ID of genome to get its neighbor]
                schemeID (optional): [ID of schemeSet(complex scheme) to use calculated the neighbor tree]. Defaults to 1.

        Returns:
                DataFrame: [Result in Dataframe format with columns [genomeID,neighbor,idx,lastValue]]
        """
        return self.get_lin_parents(genomeID, schemeID=schemeID, neighbor=True)

    def get_tree_parent(
        self,
        nodeID: int,
        id_column_name: str,
        table_name: str,
        schemeID=1,
        ascending=True,
        neighbor=False,
        visibleOnly=True,
    ) -> pd.DataFrame:
        """gets the entire parental lineage of genomes based on an already defined and calculated schemeSet LIN tree.

        Args:
                start_nodeID (int): ID of node to get tree for
                id_column_name (int): name of the identifier column. "genomeID"->lins, "prefixID"->prefixes
                table_name (int): name of table containing the tree. "LINTree"->lins, "Prefix"->prefixes
                schemeID (optional): ID of schemeSet(complex scheme) to use calculated the tree. Defaults to 1.
                ascending (bool, optional): Index oder of returned tree elements. Defaults to True.
                neighbor (bool, optional): gets neighbor (closest element) instead of parent (completely different tree). Defaults to False.
                visibleOnly (bool, optional): searches only in visible nodes. Defaults to True.

        Returns:
                DataFrame: LINs of the genome and its parents
        """
        parent = "parent"

        if neighbor:
            parent = "neighbor"
        if not visibleOnly and table_name == "LINTree":
            table_name = "LINTree_back"
        if schemeID is None:
            schemeID = 1

        order_str = "ASC" if ascending else "DESC"
        query = f"""
            WITH RECURSIVE get_parent (nodeID, par, i, val, lvl) AS (
                SELECT {id_column_name}, {parent}, idx, lastValue, 0 as lvl
                FROM {table_name} WHERE {id_column_name} = {nodeID} AND schemeSetID={schemeID}
                UNION
                SELECT c.{id_column_name}, c.{parent}, c.idx, c.lastValue, get_parent.lvl + 1
                FROM {table_name} c
                INNER JOIN get_parent ON c.{id_column_name} = get_parent.par AND c.schemeSetID={schemeID})
            SELECT * FROM get_parent ORDER BY i {order_str};
        """
        results = self.execute(query)
        logger.debug("LIN tree found %s", results)
        parent_tree = pd.DataFrame(
            results, columns=[f"{id_column_name}", parent, "idx", "lastValue", "lvl"]
        )
        parent_tree = parent_tree.dropna(
            subset=["parent", "lastValue", "idx", "lvl"]
        ).astype(dtype=int)
        logger.debug(
            "%s tree retrieved %s", parent.capitalize(), parent_tree.values.tolist()
        )
        return parent_tree

    def get_tree_branch(
        self,
        nodeIDs: list,
        id_column_name: str,
        table_name: str,
        schemeID=1,
        ascending=True,
        neighbor=False,
        visibleOnly=True,
    ) -> pd.DataFrame:
        """gets the entire parental lineage of genomes based on an already defined and calculated schemeSet LIN tree.

        Args:
                start_nodeID (list): list of IDs of nodes to get branches for
                id_column_name (int): name of the identifier column. "genomeID"->lins, "prefixID"->prefixes
                table_name (int): name of table containing the tree. "LINTree"->lins, "Prefix"->prefixes
                schemeID (optional): ID of schemeSet(complex scheme) to use calculated the tree. Defaults to 1.
                ascending (bool, optional): Index oder of returned tree elements. Defaults to True.
                neighbor (bool, optional): gets neighbor (closest element) instead of parent (completely different tree). Defaults to False.
                visibleOnly (bool, optional): searches only in visible nodes. Defaults to True.

        Returns:
                DataFrame: LINs of the genome and its parents
        """
        parent = "parent"

        if neighbor:
            parent = "neighbor"
        if not visibleOnly and table_name == "LINTree":
            table_name = "LINTree_back"
        if schemeID is None:
            schemeID = 1

        parent_tree = pd.DataFrame(columns=[f"{id_column_name}", "branch"])

        if not isinstance(nodeIDs, list):
            logger.error(
                "was expecting list but got %s from '%s'", type(nodeIDs), nodeIDs
            )
            return parent_tree

        order_str = "ASC" if ascending else "DESC"
        query = f"""
            WITH RECURSIVE get_parent2 (originID, nodeID, par, i, val, lvl) AS (
                SELECT {id_column_name} as originID, {id_column_name}, {parent}, idx, lastValue, 0 as lvl
                FROM {table_name} WHERE {id_column_name} in {self._jnull(nodeIDs)} AND schemeSetID={schemeID}
                UNION
                SELECT get_parent2.originID, c.{id_column_name}, c.{parent}, c.idx, c.lastValue, get_parent2.lvl + 1
                FROM {table_name} c
                INNER JOIN get_parent2 ON c.{id_column_name} = get_parent2.par AND c.schemeSetID={schemeID})
            SELECT originID AS {id_column_name}, JSON_OBJECTAGG(i, val) AS branch FROM get_parent2 group by (originID) ORDER BY originID {order_str};
        """
        results = self.execute(query)
        logger.debug("LIN tree found %s", results)
        parent_tree = pd.DataFrame(
            results, columns=[f"{id_column_name}", "branch"]
        ).dropna()

        logger.debug(
            "%s branch  tree retrieved %s",
            parent.capitalize(),
            parent_tree.values.tolist(),
        )
        return parent_tree

    def get_lin_parents(
        self,
        prefixID: int,
        schemeID=1,
        ascending=True,
        neighbor=False,
        visibleOnly=True,
    ):
        return self.get_tree_parent(
            prefixID,
            "genomeID",
            "LINTree",
            schemeID,
            ascending,
            neighbor,
            visibleOnly,
        )

    def get_prefix_parents(self, prefixID: int, schemeID=1, ascending=True):
        return self.get_tree_parent(
            prefixID,
            "prefixID",
            "Prefix",
            schemeID,
            ascending,
            neighbor=False,
            visibleOnly=True,
        )

    def get_tree_child(
        self,
        start_nodeID: int,
        id_column_name: str,
        table_name: str,
        schemeID=1,
        descend_idx: Optional[int] = None,
        descend_mode="relative",
        min_idx=-1,
        exclude: Optional[list[int]] = None,
    ) -> pd.DataFrame:
        """gets the entire children nodes based on an already defined and calculated schemeSet.

        Args:
                start_nodeID (int): ID of node to get tree for
                id_column_name (int): name of the identifier column. "genomeID"->lins, "prefixID"->prefixes
                table_name (int): name of table containing the tree. "LINTree"->lins, "Prefix"->prefixes
                schemeID (optional): ID of schemeSet(complex scheme) to use calculated the LIN tree. Defaults to 1.
                descend_idx (int, optional): filter children by max idx (inclusive). Defaults to None (all children).
                relative (str, optional): depth calculation e.g. depth 5 'relative' to target node with idx 6 is 5+6=11=>(max idx value retrieved),
                    'absolute' retrieves elements with idx <= 5, 'branch' allows 5 branch traversals (tree depth). Defaults to relative.
                exclude (Optional[list[int]]): [list if IDs to skip]

        Returns:
                DataFrame: [LINs of the genome and its children]
        """

        if descend_idx is None or descend_idx < 0:
            condition = ""
        elif descend_mode == "relative":
            condition = f" HAVING idx <= (SELECT idx + {descend_idx} FROM {table_name} WHERE {id_column_name} = {start_nodeID} LIMIT 1) "
        elif descend_mode == "absolute":
            condition = f" HAVING idx <= {descend_idx} "
        elif descend_mode == "branch":
            condition = f" HAVING lvl <= {descend_idx} "
        else:
            e = BaseException(
                "descend_mode only accepts [absolute,relative,branch] as acceptable modes. was '%s'"
                % descend_mode
            )
            exceptions.handle_exception(e, logger, raise_exception=True)
            raise e

        minIdx_condition = f" AND c.idx >= {min_idx} " if min_idx >= 0 else ""
        exclude_condition = ""
        if exclude:
            exclude_condition = (
                f" AND c.{id_column_name} NOT IN {util.list2sql(exclude)} "
            )

        query = f"""
            WITH RECURSIVE get_child AS (
                SELECT {id_column_name}, parent, idx, lastValue, 0 as lvl
                FROM {table_name} WHERE {id_column_name} = {start_nodeID} AND schemeSetID={schemeID}
                UNION
                SELECT c.{id_column_name}, c.parent, c.idx, c.lastValue, get_child.lvl + 1
                FROM {table_name} c
                INNER JOIN get_child ON c.parent = get_child.{id_column_name} AND schemeSetID={schemeID} 
                {minIdx_condition} {exclude_condition} 
            )
            SELECT * FROM get_child {condition} ORDER BY idx ;
        """
        results = self.execute(query)
        logger.debug("LIN tree found %s", results)
        children_tree = pd.DataFrame(
            results, columns=[f"{id_column_name}", "parent", "idx", "lastValue", "lvl"]
        )
        children_tree = children_tree.astype(dtype=int)
        logger.debug("Children of tree retrieved %s", children_tree.values.tolist())

        return children_tree

    def get_lin_child(
        self,
        genomeID: int,
        schemeID=1,
        descend_idx: Optional[int] = None,
        descend_mode="relative",
        min_idx=-1,
        exclude: Optional[list[int]] = None,
    ) -> pd.DataFrame:
        return self.get_tree_child(
            genomeID,
            "genomeID",
            "LINTree",
            schemeID,
            descend_idx,
            descend_mode,
            min_idx,
            exclude,
        )

    def get_prefix_child(
        self,
        prefixID: int,
        schemeID=1,
        descend_idx: Optional[int] = None,
        descend_mode="relative",
        exclude: Optional[list[int]] = None,
    ) -> pd.DataFrame:
        if descend_idx == 0:
            return self.get_prefix(
                prefixID,
                schemeID,
            )
        return self.get_tree_child(
            prefixID,
            "prefixID",
            "Prefix",
            schemeID,
            descend_idx,
            descend_mode,
            exclude=exclude,
        )

    def get_prefix(
        self,
        prefixID: int,
        schemeSetID=1,
        exclude: Optional[list[int]] = None,
    ):
        if prefixID < 1:
            return pd.DataFrame(columns=["prefixID", "idx", "lastValue", "parent"])

        exclude_condition = ""
        if exclude:
            exclude_condition = f" AND prefixID NOT IN {util.list2sql(exclude)} "
        query = f"""
            SELECT prefixID, idx, lastValue, parent 
            FROM Prefix WHERE prefixID = {prefixID} AND SchemeSetID = {schemeSetID} {exclude_condition}"""
        results = self.execute(query)
        prefix_df = pd.DataFrame(
            results, columns=["prefixID", "idx", "lastValue", "parent"]
        )
        prefix_df["lvl"] = np.array([0], dtype=int)
        return prefix_df

    def search_lin(self, dlin: dict[int, int], schemeID=1) -> pd.DataFrame:
        return self.search_graph(dlin, schemeID, RecursiveTable.Genome).rename(
            columns={"id": "genomeID"}
        )

    def search_graph(
        self,
        dlin: dict[int, int],
        schemeID=1,
        ref_table: RecursiveTable = RecursiveTable.Genome,
        middle_zeros=False,
        exclude: Optional[list[int]] = None,
    ) -> pd.DataFrame:
        """search for genome by LIN based on an already computed schemeSet
                table must have columns (XX_ID, idx, lastValue, parent)

        Args:
                dlin (dict): [LINs in dict format {idx:val,...} (0's are skipped)]
                schemeID (int): [ID of schemeSet(complex scheme) to use calculated the LIN tree]
                ref_table (RecursiveTable): [searches the prefix table]
                middle_zeros (bool): allows for middle matches that end with zeros (does nothing for genome since no genome LINs end with zero)
                exclude (Optional[list[int]]): [list if IDs to skip]

        Returns:
                DataFrame: [LINs of the genome and its parents]
        """
        # Type checking
        if not isinstance(ref_table, RecursiveTable):
            raise TypeError("direction must be an instance of Direction Enum")
        prefix_middle_group = ""
        exclude = exclude or []
        # for each idx (next non-zero index -1) end=end. for partial matches
        max_idx_for_middles = copy.deepcopy(dlin)
        if ref_table == RecursiveTable.Prefix:
            key_column = "prefixID"
            table = "Prefix"
        elif ref_table == RecursiveTable.Genome:
            key_column = "genomeID"
            table = "LINTree"
        else:
            raise NotImplementedError

        if middle_zeros:
            sorted_dlin_keys = np.sort([*dlin])
            idx_max = -1
            # set last position to unlimited max trailing zeros if last idx is zero anyway
            for idx in reversed(sorted_dlin_keys):
                max_idx_for_middles[idx] = idx_max
                # change max only if idx is non-zero otherwise use last non-zero (if not it is automatically -1)
                if dlin[idx]:
                    idx_max = idx

            prefix_middle_group = (
                " OR (c.lastValue = 0 AND"
                # + " AND c.idx NOT IN (SELECT idx FROM reference)"
                + " c.idx > reference.idx AND get_parent.lvl = reference.lvl"
                + " AND (c.idx < reference.mzmax OR reference.mzmax < 0)"  # up to next non-zero idx (excluding) or all(mzmax=-1) if zero val or end idx
                + " )"
            )
        if dlin == {}:
            return pd.DataFrame(
                columns=["id", "parent", "idx", "lastValue", "lvl", "mzmax"]
            )

        exclude_condition = ""
        if exclude:
            exclude_condition = f" AND c.{key_column} NOT IN {util.list2sql(exclude)} "
        PATTERN = "SELECT {},{},{},{}"
        dlin_table = []
        for i, idx in enumerate(sorted(dlin)):
            dlin_table += [PATTERN.format(idx, dlin[idx], i, max_idx_for_middles[idx])]
        # print(" ".join(dlin_table))
        query = f"""
                        
            WITH RECURSIVE get_parent AS (
                WITH reference (idx,lastValue,lvl,mzmax) AS (
                    {" UNION ALL ".join(dlin_table)}
                )
                SELECT {key_column}, parent, idx, lastValue, 0 as lvl, {max_idx_for_middles[0]} AS mzmax
                FROM {table} WHERE idx=0 AND lastValue={dlin[0]} AND schemeSetID={schemeID}
                UNION
                SELECT c.{key_column}, c.parent, c.idx, c.lastValue, get_parent.lvl + 1 AS lvl, reference.mzmax FROM {table} c
                INNER JOIN get_parent ON c.parent = get_parent.{key_column} AND schemeSetID={schemeID} {exclude_condition}
                INNER JOIN reference ON (c.idx = reference.idx AND c.lastValue = reference.lastValue AND get_parent.lvl + 1 = reference.lvl) {prefix_middle_group})
            SELECT * FROM get_parent ORDER BY idx ;
        """
        results = self.execute(query)
        result_genomes = pd.DataFrame(
            results, columns=["id", "parent", "idx", "lastValue", "lvl", "mzmax"]
        )
        result_genomes = result_genomes.astype(dtype=int)
        logger.debug("LIN found with tree %s", result_genomes.values.tolist())
        return result_genomes

    def search_prefix(
        self,
        dlin: dict[int, int],
        schemeID=1,
        exact=False,
        all_members=False,
        zeros=False,
    ) -> pd.DataFrame:
        """search for LINgroup by prefix based on an already computed schemeSet

        Args:
                dlin (dict): [LINs in dict format {idx:val,...} (0's are skipped except the last ones)]
                schemeID (int): [ID of schemeSet(complex scheme) to use calculated the LIN tree]
                exact (Optional[bool]): [only matches exactly to the dlin. Not usable with all_members]
                all_members (Optional[bool]): [get all members (children) of the prefix]
                zeros (Optional[bool]): [get prefixes with zeros in between or trailing]
        Returns:
                [DataFrame]: [LINs of the genome and its parents columns=["prefixID", "parent", "idx", "lastValue"]]
        """
        if exact and all_members:
            exceptions.handle_exception(
                NotImplementedError(
                    "The exact parameter can only be used disjoint from all_members"
                ),
                logger,
            )
        if dlin == {}:
            return pd.DataFrame(columns=["prefixID", "parent", "idx", "lastValue"])
        nonzero_dlin = {k: v for k, v in dlin.items() if v > 0}
        lingroup_df = (
            self.search_graph(
                dlin, schemeID, ref_table=RecursiveTable.Prefix, middle_zeros=zeros
            )
            .rename(columns={"id": "prefixID"})
            .astype(int)
        )

        if lingroup_df.empty or not all_members:
            # if there are not prefixes matching any parts of the lin
            return lingroup_df

        # This happens when some positions are found and the last position ID is used
        # to look for exact, trailing zeros, or all_members
        leaf_node = lingroup_df[lingroup_df["idx"] == max([*dlin])]
        # the last position was not found
        if leaf_node.empty:
            return lingroup_df
        leaf_nodeID = lingroup_df[lingroup_df["idx"] == max([*nonzero_dlin])].iloc[0][
            "prefixID"
        ]
        # last_idx = max([*dlin])
        # partial matching nonzero leaf node
        children_df = self.get_prefix_child(leaf_nodeID, schemeID)

        # iloc skips the leaf node itself
        lingroup_df = pd.concat(
            [lingroup_df, children_df], ignore_index=True
        ).drop_duplicates(["prefixID"])

        if "mzmax" in lingroup_df.keys():
            lingroup_df.drop(columns=["mzmax"], inplace=True)

        logger.debug("Prefix found with tree %s", lingroup_df["prefixID"].tolist())
        return lingroup_df.astype(int)

    def connect_scheme_filter(
        self, base_schemeID: int, filterIDs: npt.NDArray[np.intc]
    ):
        """add filter blocks to a baseScheme

        Args:
                base_schemeID (int): [baseScheme to add the filters to]
                filterIDs (np.ndarray[np.intc]): [list of filterIDs to add to scheme (in order of use)]
        """
        PATTERN = """({},"{}",{})"""
        values = ",".join(
            [
                PATTERN.format(base_schemeID, filter, idx + 1)
                for idx, filter in enumerate(filterIDs)
            ]
        )

        query = f"""
            INSERT INTO BaseScheme_Filters (baseSchemeID, filterID, idx) VALUES {values};
        """
        # TODO: This could fail if any of the IDs are not defined
        self.execute(query)
        logger.debug(
            "Filters %s were added to BaseScheme %s", filterIDs.tolist(), base_schemeID
        )

    def get_baseScheme(self, ids: Optional[list[int]] = None) -> pd.DataFrame:
        """retrieve a baseScheme from the database

        Args:
                id (list, optional): [ID of the BaseScheme to retrieve. None retrieves all]. Defaults to None.

        Returns:
                DataFrame: results in table format
        """
        df_columns = [
            "id",
            "min",
            "max",
            "step",
            "positions",
            "thresholds",
            "filters",
            "size",
            "note",
        ]
        condition_string = ""
        if ids is not None:
            condition_string = f"WHERE baseSchemeID IN {util.list2sql(ids)}"

        query = f"""
            SELECT baseSchemeID, minEffect, maxEffect, step, positions, thresholds, filters,
                positionCount, note FROM (
            SELECT * FROM BaseScheme {condition_string}) z1 LEFT JOIN(
                SELECT baseSchemeID, GROUP_CONCAT(filterID ORDER BY idx ASC SEPARATOR ',') as filters FROM BaseScheme_Filters
                GROUP BY (baseSchemeID)) z
            USING (baseSchemeID) ORDER BY baseSchemeID;
        """
        result = self.execute(query)
        base_scheme = pd.DataFrame(result, columns=df_columns)
        base_scheme["positions"] = base_scheme["positions"].apply(
            lambda x: json.loads(x) if x is not None else None
        )
        base_scheme["thresholds"] = base_scheme["thresholds"].apply(json.loads)
        base_scheme["filters"] = base_scheme["filters"].apply(
            lambda x: np.array(x.split(","), dtype=int).tolist()
        )  # type: ignore
        base_scheme = base_scheme.astype(
            dtype={
                "id": int,
                "min": float,
                "max": float,
                "step": float,
                "size": int,
                "note": str,
            }
        )
        logger.debug("Base scheme retrieved %s", base_scheme.values.tolist())
        return base_scheme

    def get_scheme_set(self, schemeSetID: Optional[int] = None) -> pd.DataFrame:
        return (
            self.get_scheme_sets([schemeSetID])
            if schemeSetID is not None
            else self.get_scheme_sets(schemeSetID)
        )

    def get_scheme_sets(self, ids: Optional[list[int]] = None) -> pd.DataFrame:
        """retrieve a schemeSet from the database

        Args:
                ids (list, optional): ID of the SchemeSet to retrieve. None retrieves all. Defaults to None.

        Returns:
                DataFrame: results in table format
        """
        condition_string = ""

        if ids is not None:
            condition_string = f" WHERE schemeSetID IN {util.list2sql(ids)}"

        query = f"""
            SELECT schemeSetID, blockCount, scoreF, blocks, levels, note FROM
                (SELECT * FROM SchemeSet {condition_string}) z LEFT JOIN(
                SELECT schemeSetID, GROUP_CONCAT(baseSchemeID ORDER BY idx ASC SEPARATOR ',') AS blocks,
                    GROUP_CONCAT(positionDepth ORDER BY idx ASC SEPARATOR ',') as levels
                    FROM SchemeSet_Base GROUP BY (schemeSetID)
                ) lists USING (schemeSetID) ORDER BY schemeSetID;
        """
        result = self.execute(query)
        scheme_set = pd.DataFrame(
            result, columns=["id", "size", "score", "blocks", "levels", "note"]
        )

        scheme_set["blocks"] = scheme_set["blocks"].apply(
            lambda x: np.array(x.split(","), dtype=int).tolist()
        )  # type: ignore
        scheme_set["levels"] = scheme_set["levels"].apply(
            lambda x: np.array(x.split(","), dtype=int).tolist()
        )  # type: ignore
        scheme_set = scheme_set.astype(
            dtype={"id": int, "size": int, "score": str, "note": str}
        )
        logger.debug("Base scheme retrieved %s", scheme_set.values.tolist())
        return scheme_set

    def get_filter(
        self, filterID: Optional[int] = None, filterName: Optional[str] = None
    ) -> pd.DataFrame:
        """retrieves a filter by name or ID

        Args:
                filterID (int, optional): ID to fetch the filter by. Either this or filterName is required. Defaults to None.
                filterName (str, optional): name to fetch the ID by. Either this or filterID is required. Defaults to None.

        Raises:
                TypeError: When both ID and name are not specified for search

        Returns:
                DataFrame: columns=["filterID", "filterName", "minEffect", "maxEffect", "saveScores", "compareType", "measure", "reasonerF", "rankscoreF", "dargs"]
        """
        if filterID is None and filterName is None:
            raise TypeError(
                "Either an ID or name should be specified for search, both cannot be None"
            )
        if filterID is not None:
            condition = f" filterID = {filterID} "
        else:
            condition = f' filterName = "{filterName}" '

        query = f"""
            SELECT filterID, filterName, minEffect, maxEffect, saveScores, compareType,
            measure, reasonerF, rankscoreF, dargs FROM Filters WHERE {condition}
            """
        results = self.execute(query)
        filter_df = pd.DataFrame(
            results,
            columns=[
                "filterID",
                "filterName",
                "minEffect",
                "maxEffect",
                "saveScores",
                "compareType",
                "measure",
                "reasonerF",
                "rankscoreF",
                "dargs",
            ],
        )
        filter_df = filter_df.astype(
            dtype={
                "filterID": int,
                "filterName": str,
                "minEffect": float,
                "maxEffect": float,
                "saveScores": int,
                "compareType": str,
                "measure": str,
                "reasonerF": str,
                "rankscoreF": str,
            }
        )
        filter_df["dargs"] = filter_df["dargs"].apply(json.loads)
        logger.debug("Retrieved filter %s", filter_df.values.tolist())
        return filter_df

    def add_filter(
        self,
        name: str,
        measure: str,
        minEffect: float,
        maxEffect: float,
        saveScores=1,
        compareType="1-1",
        reasonerF="reasoner_identity",
        rankscoreF="rankscore_identity",
        dargs: Optional[dict[str, Any]] = None,
        raise_exception=True,
    ) -> int:
        """register a filter on the database

        Args:
                name (str): name of the filter function. MUST be unique and in the filter helpers folder (except defaults)
                measure (str): measure class path from module to use in filtering
                minEffect (float): minimum effective result
                maxEffect (float): maximum effective result
                saveScores (int, optional): How many scores to register in the database. Defaults to 1.
                compareType (str, optional): how many comparisons can be performed in one comparison. 'in the format of 1-1 1-m m-1 m-m'. Defaults to "1-1".
                reasonerF (str, optional): name of the reasoner function which decides greedy termination or reruns. Defaults to 'reasoner_identity'.
                rankscoreF (str, optional): name of the ranking function that combined and ranks the scores. Defaults to 'rankscore_identity'.
                dargs (dict, optional): extra argument specific to the filter i.e thresholds. Defaults to None.
                raise_exception (bool, optional): raise exception when filter with same name if exists or return its ID instead. Defaults to True.

        Raises:
                Exception: raised when flag provided and name is not unique

        Returns:
                int: ID of an existing filter (raise=False) or the newly added filter
        """
        verify_exists = self.get_filter(filterName=name)
        if not verify_exists.empty:
            if raise_exception:
                raise BaseException(
                    "Filter with the name: '%s' and ID: '%s'"
                    "exists. Please use a unique name.\n %s"
                    % (
                        name,
                        verify_exists.iat[0, verify_exists.columns.get_loc("filterID")],
                        verify_exists,
                    )
                )
            return verify_exists.iat[0, verify_exists.columns.get_loc("filterID")]
        # removing workspace to make filter workspace independent
        if dargs:
            _ = dargs.pop("workspace", None)
        query = f"""
            INSERT INTO Filters (filterName, minEffect, maxEffect, saveScores, compareType,
            measure, reasonerF, rankscoreF, dargs) VALUES
            ("{name}", {self._jnull(minEffect)}, {self._jnull(maxEffect)},
            {self._jnull(saveScores)}, "{compareType}", "{measure}", "{reasonerF}", "{rankscoreF}",
             {self._jnull(dargs)});
        """
        self.execute(query)
        logger.debug("Added Filter %s", query)
        filterID = self.lastRowID()
        return filterID

    def add_lingroup(
        self,
        schemeID: int,
        title: str,
        lingroupID=None,
        desc: Optional[str] = None,
        taxTypeID: Optional[int] = 1,
        link: Optional[str] = None,
        **kwargs,
    ):
        """Add a LINgroup to the database"""
        if not lingroupID:
            lingroupID = "lingroupID"
        # PRIMARY KEY (lingroupID)
        query = f"""
            INSERT INTO LINgroup (lingroupID, schemeSetID, title, sourceLink, note, taxTypeID) VALUES 
            ({lingroupID}, {','.join([str(self._jnull(x)) for x in [schemeID,title.lower(), link, desc, taxTypeID]])})
            ON DUPLICATE KEY UPDATE 
                title=VALUES(title), sourceLink=VALUES(sourceLink),
                note=VALUES(note), taxTypeID=VALUES(taxTypeID)
            ;"""

        self.execute(query)
        logger.debug("Added LINgroup: %s", query)
        lingroupID = self.lastRowID()
        return lingroupID

    def add_lingroup_batch(
        self, lingroup_df: pd.DataFrame, schemeSetID=1, taxTypeID=0
    ) -> list[int]:
        """Add multiple lingroups to the database

        Args:
            lingroup_df (pd.DataFrame): Required columns ['schemeSetID', 'title', 'taxTypeID', 'link', 'desc']

        Returns:
            list[int]: list of lingroupIDs added
        """
        if lingroup_df.empty:
            return []
        lingroup_values = []
        for _, row in lingroup_df.iterrows():
            fields = [
                row.get("schemeSetID", schemeSetID),
                row["title"],
                row.get("taxTypeID", taxTypeID),
                row.get("link"),
                row.get("desc"),
            ]
            lingroup_values += [f"({','.join([str(self._jnull(x)) for x in fields])})"]
        query = f"""
            INSERT INTO LINgroup (schemeSetID, title, taxTypeID, sourceLink, note) VALUES 
            {','.join(lingroup_values)}
            ;"""

        self.execute(query)
        logger.debug("Added LINgroup: %s", query)
        lastID = self.lastRowID()
        # inferring the all the new IDs based on the last ID
        lingroupIDs = list(range(lastID - len(lingroup_df) + 1, lastID + 1))
        return lingroupIDs

    def all_lingroups(self, schemeSetID=1) -> pd.DataFrame:
        query = f"""
            SELECT lg.lingroupID, lg.title, lg.taxTypeID, lt.rankID, lp.prefixID FROM LINgroup lg
            LEFT JOIN LINgroup_Taxon lt USING (taxTypeID, lingroupID)
            LEFT JOIN LINgroup_Prefix lp USING (lingroupID, schemeSetID)
            WHERE lg.schemeSetID = {schemeSetID};
        """
        results = self.execute(query)
        if not results:
            return pd.DataFrame()
        lingroups = pd.DataFrame(
            results, columns=["lingroupID", "title", "taxTypeID", "rankID", "prefixID"]
        )
        return lingroups

    def find_lingroup(
        self, title="", taxTypeID=1, rankID=0, schemeID=1, **kwargs
    ) -> list[int]:
        rankID_condition = f" AND lt.rankID = {rankID} " if rankID else ""
        query = f"""
            SELECT DISTINCT lg.lingroupID FROM LINgroup lg
            JOIN LINgroup_Taxon lt ON lg.lingroupID = lt.lingroupID {rankID_condition}
                AND title = {self._jnull(title)} AND lg.taxTypeID = {taxTypeID} AND lg.schemeSetID = {schemeID}
            ;
        """
        result = self.execute(query)
        if result:
            return [int(row[0]) for row in result]
        return []

    def update_lingroup_taxonomy(self, taxonomy_df: pd.DataFrame):
        """Updates the taxonomy information of lingroups in the dataframe

        Args:
            taxonomy_df (pd.DataFrame): Required columns ['rankID', 'taxTypeID', 'lingroupID', 'parent'] and one of ['taxID', 'taxon'] are mutually exclusive
        """
        assert "rankID" in taxonomy_df.columns and "taxTypeID" in taxonomy_df.columns
        # rankID:int, taxTypeID:Optional[int]=1, taxID:Optional[int]=None, parent:Optional[int]=None, taxon:Optional[str]=None):
        taxonomy_values = []
        for _, row in taxonomy_df.iterrows():
            # only one should exist XOR
            assert ("taxID" in row) ^ ("taxon" in row)
            fields = [
                row["rankID"],
                row["taxTypeID"],
                row["lingroupID"],
                row.get("taxID"),
                row.get("parent"),
                row.get("taxon"),
            ]
            taxonomy_values += [f"({','.join([str(self._jnull(x)) for x in fields])})"]
        query = f"""
            INSERT INTO LINgroup_Taxon (rankID, taxTypeID, lingroupID, taxID, parent, customTaxon) VALUES 
            {','.join(taxonomy_values)}
            ON DUPLICATE KEY UPDATE 
                taxID=VALUES(taxID), parent=VALUES(parent),
                customTaxon=VALUES(customTaxon)
            ;"""

        self.execute(query)
        logger.debug("Added LINgroup taxonomy: %s", query)

    def remove_lingroups(self, lingroupIDs: list[int]):
        if not lingroupIDs:
            return
        if not isinstance(lingroupIDs, list) or not isinstance(lingroupIDs[0], int):
            raise LINgroupIDTypeError(lingroupIDs)
        query = (
            f"DELETE FROM LINgroup WHERE lingroupID IN {util.list2sql(lingroupIDs)};"
        )
        self.execute(query)
        logger.debug("Removed LINgroup(s) %s: %s", lingroupIDs, query)

    def remove_prefix(self, prefixID: list[int], membership_only=False):
        if not isinstance(prefixID, list):
            raise LINgroupIDTypeError(prefixID)
        link_only = ""
        if membership_only:
            table = "LINgroup_Prefix;"
            link_only = "(link only)"
        else:
            table = "Prefix;"

        query = f"DELETE FROM {table} WHERE prefixID in {util.list2sql(prefixID)};"
        self.execute(query)
        logger.debug("Removed LINgroup members %s: %s", link_only, prefixID)

    def remove_lingroup_prefixes(
        self,
        lingroupIDs: list[int],
    ):
        if not isinstance(lingroupIDs, list):
            raise LINgroupIDTypeError(lingroupIDs)
        query = f"DELETE FROM LINgroup_Prefix WHERE lingroupID in {util.list2sql(lingroupIDs)};"
        result = self.execute(query)
        if result:
            logger.debug("Removed prefixes of LINgroups '%s'", lingroupIDs)

    def remove_prefix_lingroups(self, prefixIDs: list[int]):
        if not isinstance(prefixIDs, list):
            raise LINgroupIDTypeError(prefixIDs)
        if not prefixIDs:
            return
        query = (
            f"DELETE FROM LINgroup_Prefix WHERE prefixID in {util.list2sql(prefixIDs)};"
        )
        self.execute(query)
        logger.debug("Removed prefix links of %s", prefixIDs)

    def update_prefix(self, idx: int, val: int, parent=1, schemeSetID=1) -> int:
        query = (
            "INSERT INTO Prefix (schemeSetID, idx, lastValue, parent) "
            f"VALUES({schemeSetID}, {idx}, {val}, {self._jnull(parent)})"
            "ON DUPLICATE KEY UPDATE parent=VALUES(parent), idx=VALUES(idx), lastValue=VALUES(lastValue);"
        )
        self.execute(query)
        prefixID = self.lastRowID()
        logger.debug("Added new prefix: %s", prefixID)
        return prefixID

    def add_prefix(self, dlin: dict[int, int], schemeSetID=1):
        parent_df = self.search_prefix(dlin, schemeSetID, exact=True)
        # if the prefix already exists
        if len(parent_df) >= len(dlin):
            return parent_df.at[parent_df["idx"].idxmax(), "prefixID"]
        if parent_df.empty:
            query = f"SELECT prefixID FROM Prefix WHERE idx IS NULL AND schemeSetID = {schemeSetID}"
            result = self.execute(query)
            if not result or not result[0]:
                exceptions.handle_exception(
                    ValueError("No prefix root found for schemeSet %s" % schemeSetID),
                    logger,
                    raise_exception=False,
                )
                # add the root(s) and redoing the query
                self.add_prefix_roots(schemeSetID)
                result = self.execute(query)

            last_prefixID = int(result[0][0])
            last_idx_value = -1
        else:
            max_idx_dfindex = parent_df["idx"].idxmax()
            last_prefixID = parent_df.at[max_idx_dfindex, "prefixID"]
            last_idx_value = parent_df.at[max_idx_dfindex, "idx"]

        # now last_prefixID is the parent no matter what
        for idx, val in dlin.items():
            if idx <= last_idx_value:
                continue
            else:
                add_query = f"""
                    INSERT INTO Prefix (schemeSetID, idx, lastValue, parent) 
                    VALUES ({schemeSetID},{idx},{val}, {last_prefixID})"""
                self.execute(add_query)
                last_prefixID = self.lastRowID()

        return last_prefixID

    def add_prefix_to_lingroup(
        self,
        lingroupID: int,
        schemeSetID=1,
        members: Optional[list[dict[int, int]]] = None,
        clean_prefix=False,
    ):
        members = members or []
        prefixIDs = []

        for prefix_dlin in members:
            prefix_df = self.search_prefix(prefix_dlin, schemeSetID, exact=True)
            if len(prefix_df) < len(prefix_dlin):
                # if the prefix found is a parent i.e. your prefix does not exist
                prefixIDs += [self.add_prefix(prefix_dlin, schemeSetID)]
            elif len(prefix_df) == len(prefix_dlin):
                # if your prefix found
                prefixID = prefix_df.at[prefix_df["idx"].idxmax(), "prefixID"]
                prefixIDs += [prefixID]
            else:
                # if the prefix was not found e.g. the prefix does not match any known LIN
                # query [1234,0,0,1] where the LIN [1234,0,0,...] does not exist
                exceptions.handle_exception(
                    ModuleNotFoundError(
                        "The prefix '%s' was not found to be added to LINgroup %s"
                        % (prefix_dlin, lingroupID)
                    ),
                    logger,
                    raise_exception=False,
                    log_level=logging.WARNING,
                )
                continue
        if clean_prefix:
            self.remove_prefix_lingroups(prefixIDs)
        ignore_condition = "" if clean_prefix else "IGNORE"

        values = []
        for prefix in prefixIDs:
            values += [f"({lingroupID}, {prefix}, {schemeSetID})"]
        query = f"""INSERT {ignore_condition} INTO LINgroup_Prefix (lingroupID, prefixID, schemeSetID)
                    VALUES {','.join(values)};"""
        self.execute(query)
        logger.debug(
            "Added prefixes %s to LINgroup: %s in scheme %s",
            lingroupID,
            prefixIDs,
            schemeSetID,
        )
        return prefixIDs

    def get_lingroup(self, lingroupID: int) -> pd.DataFrame:
        if not isinstance(lingroupID, int):
            raise LINgroupIDTypeError(lingroupID)
        query = (
            f"SELECT lingroupID, schemeSetID, title, sourceLink, note, taxTypeID FROM LINgroup "
            f"WHERE lingroupID = {lingroupID}"
        )
        result = self.execute(query)
        logger.debug("Retrieved LINgroup: %s", query)
        return pd.DataFrame(
            result,
            columns=[
                "lingroupID",
                "schemeSetID",
                "title",
                "sourceLink",
                "note",
                "taxTypeID",
            ],
        )

    def get_lingroup_prefixes(self, lingroupID: int, schemeSetID=1) -> pd.DataFrame:
        if not isinstance(lingroupID, int):
            raise LINgroupIDTypeError(lingroupID)
        query = f"""
            SELECT lp.prefixID, lp.lingroupID, lp.schemeSetID, idx, lastValue, parent
            FROM LINgroup_Prefix lp
            INNER JOIN Prefix p ON lp.lingroupID = {lingroupID} AND lp.schemeSetID = {schemeSetID} AND 
            lp.prefixID = p.prefixID AND lp.schemeSetID = p.schemeSetID
        ;"""
        result = self.execute(query)
        logger.debug("Retrieved LINgroup: %s", query)
        return pd.DataFrame(
            result,
            columns=[
                "prefixID",
                "lingroupID",
                "schemeSetID",
                "idx",
                "lastValue",
                "parent",
            ],
        )

    # TODO: REDO
    def search_lingroup_description(
        self,
        keyword: str,
        schemeID: Optional[int] = None,
        tax_type: Optional[str] = None,
    ):
        condition = ""
        if schemeID is not None:
            condition += f"OR schemeSetID = {schemeID}"
        if tax_type is not None:
            int_tax_type = tax_enums.TaxType[tax_type].value
            condition += f"OR taxTypeID = {int_tax_type}"

        query = f"""
        SELECT lingroupID, schemeSetID, title, note, taxTypeID FROM LINgroup WHERE
            title LIKE "%{keyword.lower()}%" OR note LIKE "%{keyword.lower()}%" {condition};
        """
        results = self.execute(query)
        lingroup_df = pd.DataFrame(
            results, columns=["lingroupID", "schemeSetID", "title", "note", "taxTypeID"]
        )
        logger.debug("Retrieved LINgroups: %s", lingroup_df["lingroupID"].to_list())
        return lingroup_df

    def get_top_comparisons(
        self, schemeSetID: int, filterID: int, gids: Optional[list[int]] = None
    ):
        """Finds the neighbors for each genome that has a LIN for that scheme + measurements

        Args:
            schemeSetID (int): the ID of the schemeSet that generated the measurements (LIN)
            filterID (int): The ID of the filter used for the measurement
            gids (Optional[list[int]], optional): only selects some genomes and not all. Defaults to None.

        Returns:
            DataFrame: a table with columns ["genomeID", "refID","schemeSetID","filterID","rankScore",*MEASURE_COLS]
        """
        condition = ""
        if gids:
            condition = f" queryID IN {util.list2sql(gids)} AND "

        """query = f
            SELECT queryID, refID, filterID, rankScore, measure1, measure2, measure3, measure4, measureN
            FROM Comparison
            INNER JOIN (
                SELECT queryID, filterID, MAX(rankScore) rankScore
                FROM Comparison WHERE filterID={filterID} {condition}
                GROUP BY queryID, filterID
            ) AS max USING (queryID, filterID, rankScore)
            ORDER BY queryID ASC
        """
        query2 = f"""
            SELECT DISTINCT genomeID, neighbor, schemeSetID, filterID, rankScore, measure1, measure2, measure3, measure4, measureN
            FROM LINTree
            INNER JOIN Comparison 
            ON {condition} schemeSetID={schemeSetID} AND filterID={filterID} AND genomeID=queryID AND neighbor=refID ;
        """
        results = self.execute(query2)
        result_df = pd.DataFrame(
            results,
            columns=[
                "genomeID",
                "refID",
                "schemeSetID",
                "filterID",
                "rankScore",
                *MEASURE_COLS,
            ],
        )
        return result_df.dropna(axis=1)

    def combining_measures(self, comparison_df: pd.DataFrame) -> pd.Series:
        measures = comparison_df[MEASURE_COLS[:-1]].agg(list, axis=1)
        return measures + comparison_df["measureN"].apply(self._parse_json_measure)

    def add_prefix_roots(self, schemeSetID=None):
        return self.execute(f'CALL prefix_init("{self.db_name}")')

    def update_attributes(
        self, attribute_df: pd.DataFrame, overwrite=False, userID=5, logging=False
    ):
        mode_str = "Updating" if overwrite else "Inserting"
        if attribute_df.empty:
            return
        for req_col in ["genomeID", "attID"]:
            if req_col not in attribute_df.columns:
                exceptions.handle_exception(
                    KeyError("no link to %s in DataFrame" % req_col), logger
                )
        if len(attribute_df.columns) < 3:
            logger.error(
                "The DataFrame has less than the required columns %d",
                len(attribute_df.columns),
            )
            return

        # reorder columns and drop rows with empty values
        attribute_df = attribute_df[["genomeID", "attID", "attValue"]].dropna(
            subset=["attValue"]
        )
        attribute_df["userID"] = userID
        # DataFrame to list of tuples. passing attValue as a string to keep int type
        # (used to change to float by default)
        values = [
            f"""({genomeID},{attID},"{attValue}",{userID})"""
            for [genomeID, attID, attValue, userID] in attribute_df.itertuples(
                index=False, name=None
            )
        ]
        query_prefix = ""
        query_postfix = ""
        if overwrite:
            query_postfix = (
                " ON DUPLICATE KEY UPDATE attValue=tmp_attribute_update.attValue"
            )
        else:
            query_prefix = " IGNORE "

        values_size = len(values)

        splits, step = util.dataframe_mysql_split(
            attribute_df, row_count=values_size, data_in_subquery=True
        )
        values_query = """
                    INSERT INTO tmp_attribute_update (genomeID, attID, attValue, userID)
                    VALUES {}
        """
        inter_table_query = """
                    INSERT {} INTO Genome_Attribute (genomeID, attID, attValue, userID)
                    SELECT genomeID, attID, attValue, userID 
                    FROM tmp_attribute_update 
                    WHERE genomeID IN (SELECT genomeID FROM Genome)
                    {}
                ;"""

        with alive_progress.alive_bar(
            len(range(0, values_size, step)),
            title=f'{mode_str} {values_size:,} entries for column {attribute_df["attID"].unique()}',
            disable=not logging,
        ) as progress:
            self.execute(
                """CREATE TEMPORARY TABLE IF NOT EXISTS tmp_attribute_update (
                genomeID INT UNSIGNED,
                attID INT UNSIGNED,
                attValue TEXT,
                userID INT UNSIGNED)
                ;"""
            )
            self.execute("DELETE FROM tmp_attribute_update;")

            for i in range(0, values_size, step):
                # Adding attributes to temporary table in acceptable chunks by MySQL
                self.execute(values_query.format(",".join(values[i : i + step])))
                logger.debug(  # pylint: disable=logging-fstring-interpolation
                    f"Added attributes {i + step:,}/{values_size:,}"
                )
                progress()  # pylint: disable=not-callable
            self.execute(inter_table_query.format(query_prefix, query_postfix))
            self.conn.commit()
            logger.info(
                "Committed %s %s entries for column %s",
                mode_str,
                f"{values_size:,}",
                attribute_df["attID"].unique(),
            )

    def remove_attributes(self, remove_df: pd.DataFrame, logging=False) -> int:
        import math

        if remove_df.empty:
            return 0
        for req_col in ["genomeID", "attID"]:
            if req_col not in remove_df.columns:
                exceptions.handle_exception(
                    KeyError("no link to %s in DataFrame" % req_col), logger
                )
        if len(remove_df.columns) < 3:
            logger.error(
                "The DataFrame has less than the required columns %d",
                len(remove_df.columns),
            )
            return 0
        # genomeID, attID, attValue
        remove_df = remove_df[remove_df["attValue"] == "NULL"]
        remove_df.drop(columns=["attValue"], inplace=True)
        # genomeID, attID
        remove_sql_df = remove_df.groupby(["attID"]).agg(list)

        values_size = len(remove_df)
        # CHANGE data_in_subquery=False
        splits, step = util.dataframe_mysql_split(
            remove_df, row_count=values_size, data_in_subquery=True
        )

        query = "INSERT INTO tmp_attribute_remove VALUES {};"

        for attID in remove_sql_df.index:
            genomeIDs: list[int] = remove_sql_df.loc[attID, "genomeID"]  # type: ignore
            att_size = len(genomeIDs)
            with alive_progress.alive_bar(
                math.ceil(att_size / step) + 1,
                title=f"removing {values_size:,} entries for column [{attID}]",
                disable=not logging,
            ) as progress:

                self.execute(
                    "CREATE TEMPORARY TABLE IF NOT EXISTS tmp_attribute_remove (id INT NOT NULL PRIMARY KEY);"
                )
                self.execute("DELETE FROM tmp_attribute_remove;")

                for i in range(0, att_size, step):
                    values = ",".join([f"({gid})" for gid in genomeIDs[i : i + step]])
                    self.execute(query.format(values))
                    progress()  # pylint: disable=not-callable

                self.execute(
                    f"DELETE FROM Genome_Attribute WHERE attID = {attID} AND genomeID IN (SELECT id FROM tmp_attribute_remove);"
                )
                self.conn.commit()
                progress()  # pylint: disable=not-callable
            logger.debug(  # pylint: disable=logging-fstring-interpolation
                f"Removed {att_size:,} from attributes with ID {attID}",
            )

            self.conn.reset_session()
        return values_size

    def search_lingroup(
        self,
        rank_dict: Optional[dict[int, str]] = None,
        keyword: str = "",
        tax_type: int = 0,
        submitter: int = 0,
    ) -> pd.DataFrame:

        rank_dict = rank_dict or {}
        if not (keyword or rank_dict):
            return pd.DataFrame(columns=["prefixID", "lingroupID"])
        if keyword and rank_dict:
            raise NotImplementedError(
                "rank_dict and keyword cannot be searched at the same time"
            )

        rank_condition_list = []
        tax_type_condition = "" if not tax_type else f" AND lg.taxTypeID={tax_type} "
        submitter_condition = "" if not submitter else f" AND lg.submitter={submitter} "

        # if keyword search then skip rank_dict
        if keyword:
            rank_condition_list = [f" (common_tax like '%{keyword}%') "]
        else:
            for rankID, rank_name in rank_dict.items():
                rank_condition_list += [
                    f" (rankID = {rankID} AND common_tax like '%{rank_name}%') "
                ]
        rank_conditions = (
            "" if not rank_condition_list else f"({' OR '.join(rank_condition_list)})"
        )

        query = f"""
            SELECT lgp.prefixID, lg2.lingroupID
            FROM
                (SELECT lgt.lingroupID, COUNT(lgt.lingroupID) AS count
                FROM LINgroup_Taxon lgt JOIN LINgroup lg 
                    ON lgt.lingroupID=lg.lingroupID {tax_type_condition} {submitter_condition} 
                WHERE {rank_conditions} 
                GROUP BY (lgt.lingroupID)
                HAVING count >= {len(rank_condition_list)}) lg2
            JOIN LINgroup_Prefix lgp USING (lingroupID);
        """
        results = self.execute(query)
        return pd.DataFrame(results, columns=["prefixID", "lingroupID"]).astype(int)

    def search_genome(
        self,
        att_dict: Optional[dict[int, str]] = None,
        keyword: str = "",
        submitter: int = 0,
    ) -> pd.DataFrame:

        att_dict = att_dict or {}
        if not (keyword or att_dict):
            return pd.DataFrame(columns=["genomeID", "hit_count"])
        if keyword and att_dict:
            raise NotImplementedError(
                "att_dict and keyword cannot be searched at the same time"
            )

        att_condition_list = []
        submitter_condition = "" if not submitter else f" AND lg.submitter={submitter} "

        # if keyword search then skip rank_dict
        if keyword:
            att_condition_list = [f" (attValue like '%{keyword}%') "]
        else:
            for attID, att_val in att_dict.items():
                att_condition_list += [
                    f" (attID = {attID} AND attValue like '%{att_val}%') "
                ]
        att_conditions = (
            "" if not att_condition_list else f"({' OR '.join(att_condition_list)})"
        )

        query = f"""
            SELECT genomeID, COUNT(genomeID) AS count
            FROM Genome_Attribute
            WHERE {att_conditions} {submitter_condition} 
            GROUP BY (genomeID)
            HAVING count >= {len(att_condition_list)}
        """
        results = self.execute(query)
        return pd.DataFrame(results, columns=["genomeID", "hit_count"]).astype(int)
