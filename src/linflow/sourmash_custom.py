"""
Extension for sourmash.sourmash_args
"""

import os

import numpy as np
import sourmash.logging
from pandas import DataFrame

from . import logger as m_logger
from . import util
from .measure_base import Measure

logger = m_logger.get_logger(__name__)

sourmash.logging.set_quiet(True)
DEFAULT_LOAD_K = 31


class Sourmash(Measure):
    """Sourmash implementation of the Measure class for use as a filter in LINflow"""

    @classmethod
    def load_dbs_and_sigs(
        cls, filenames, query, is_similarity_query, logger, traverse=False
    ):
        """
        Load one or more SBTs, LCAs, and/or signatures.

        Check for compatibility with query.
        """
        import sys

        from sourmash import signature as sig
        from sourmash.index import LinearIndex
        from sourmash.lca import LCA_Database
        from sourmash.sbt import SBT
        from sourmash.sbtmh import SigLeaf
        from sourmash.sourmash_args import (
            check_tree_is_compatible,
            filter_compatible_signatures,
            get_moltype,
            traverse_find_sigs,
        )

        # used to create and manage fasta signatures (hashes)

        query_ksize = query.minhash.ksize  # 21
        query_moltype = get_moltype(query)  # DNA

        n_signatures = 0
        n_databases = 0
        databases = []
        for sbt_or_sigfile in filenames:
            logger.debug("loading from %s...", sbt_or_sigfile)
            # are we collecting signatures from a directory/path?
            if traverse and os.path.isdir(sbt_or_sigfile):
                for sigfile in traverse_find_sigs([sbt_or_sigfile]):
                    try:
                        siglist = sig.load_signatures(
                            sigfile, ksize=query_ksize, select_moltype=query_moltype
                        )
                        siglist = filter_compatible_signatures(query, siglist, True)
                        linear = LinearIndex(siglist, filename=sigfile)
                        databases.append((linear, sbt_or_sigfile, False))
                        logger.debug(
                            "loaded '%s' signatures from '%s'", len(linear), sigfile
                        )
                        n_signatures += len(linear)
                    except Exception:  # ignore errors with traverse
                        pass

                # done! jump to beginning of main 'for' loop
                continue

            # no traverse? try loading as an SBT.
            try:
                tree = SBT.load(sbt_or_sigfile, leaf_loader=SigLeaf.load)

                if not check_tree_is_compatible(
                    sbt_or_sigfile, tree, query, is_similarity_query
                ):
                    sys.exit(-1)

                databases.append((tree, sbt_or_sigfile, "SBT"))
                logger.debug("loaded SBT %s", sbt_or_sigfile)
                n_databases += 1

                # done! jump to beginning of main 'for' loop
                continue
            except (ValueError, EnvironmentError):
                # not an SBT - try as an LCA
                pass

            # ok. try loading as an LCA.
            try:
                lca_db = LCA_Database.load(sbt_or_sigfile)

                assert query_ksize == lca_db.ksize
                query_scaled = query.minhash.scaled

                logger.debug("loaded LCA %s", sbt_or_sigfile)
                n_databases += 1

                databases.append((lca_db, sbt_or_sigfile, "LCA"))

                continue
            except (ValueError, TypeError, EnvironmentError):
                # not an LCA database - try as a .sig
                pass

            # not a tree? try loading as a signature.
            try:
                siglist = sig.load_signatures(
                    sbt_or_sigfile, ksize=query_ksize, select_moltype=query_moltype
                )
                siglist = list(siglist)
                if len(siglist) == 0:  # file not found, or parse error?
                    raise ValueError(siglist)

                siglist = filter_compatible_signatures(query, siglist, False)
                linear = LinearIndex(siglist, filename=sbt_or_sigfile)
                databases.append((linear, sbt_or_sigfile, "signature"))

                logger.debug(
                    "loaded %d signatures from %s", len(linear), sbt_or_sigfile
                )
                n_signatures += len(linear)
            except (EnvironmentError, ValueError) as e:
                raise e

        if n_signatures and n_databases:
            logger.debug(
                "loaded %d signatures and %d databases total.",
                n_signatures,
                n_databases,
            )
        elif n_signatures:
            logger.debug("loaded %d signatures.", n_signatures)
        elif n_databases:
            logger.debug("loaded %d databases.", n_databases)
        else:
            return []

        return databases

    # TODO: add support scale sketch
    @classmethod
    def create_sketch(
        cls,
        filepath,
        dest,
        sketch_sizes=[21, 51],
        num_sketch=2000,
        scale_sketch=1000,
        **kwargs,
    ):
        """
        Creates a sourmash signature

        Parameters
        ----------
        filepath : str
                path to the FASTA file
        dest : str
                path to save sketch file
        """
        from os.path import realpath

        # used to read fasta files
        import screed
        from sourmash import MinHash, SourmashSignature, save_signatures

        logger.debug("Creating signature from %s to %s", filepath, dest)
        signatures = []
        try:
            for k in sketch_sizes:
                minhash = MinHash(n=num_sketch, ksize=k)
                for read in screed.open(filepath):
                    minhash.add_sequence(read.sequence, True)
                signatures.append(
                    SourmashSignature(minhash, filename=realpath(filepath))
                )
            logger.debug("Saving signature from '%s' to '%s'", filepath, dest)
            with open(dest, "w+") as signature_file:
                save_signatures(signatures, signature_file)
        except Exception as e:
            logger.debug("Create sketch for file '%s' failed", filepath)
            logger.debug(e)
            raise e
        # cmd = "sourmash compute -o {0} {1} -k 21,51 -n 2000 -q".format(dest, filepath)
        logger.info("Sketch created for file '%s'", filepath)
        return signatures

    @classmethod
    def compare_sketch(
        cls, refpath: list, query_sigs, k=21, threshold=0.0001, **kwargs
    ) -> DataFrame:
        """compare_sketch
        Compare signatures using sourmash

        Parameters
        ----------
        query_sigs : SourmashSignature
                one sourmash signature
        dest : list
                list of signature paths to compare to
        k : int
                window size for comparing signatures
        threshold: float
                search threshold for sourmash

        Returns
        -------
                df_list  : pandas DataFrame
                Sourmash result format
                File header [similarity,name,filename,md5] e.g. 0.xxx,/path/to/query/zzz.fasta,
                path/to/workspace/Signatures/rep_back/1.sig,md5 of query file
        """
        import re
        from time import time

        from sourmash.search import search_databases

        sig_start_time = time()
        paths = [os.path.realpath(path) for path in refpath if os.path.isfile(path)]
        folder_size = len(paths)

        logger.info("Comparing signature to %d other signatures", folder_size)
        # get the appropriate k-mer signature
        query = None
        for sig in query_sigs:
            if sig.minhash.ksize == int(k):
                logger.debug("Found appropriate signature K=%d", k)
                query = sig
        logger.debug("Loaded query signatures, now loading reference signatures")

        registered_sigs = [file for file in paths if re.match(r".*\/[0-9]+.sig$", file)]
        new_sigs = np.setdiff1d(np.array(paths), np.array(registered_sigs)).tolist()
        try:
            ref_signatures = cls.load_dbs_and_sigs(
                registered_sigs + new_sigs, query, False, logger, traverse=False
            )
        # this is thrown when a file in the list is removed from the directory during loading
        except (EnvironmentError, ValueError) as e:
            logger.warning("Sourmash was unable to load signature")
            logger.exception(e)

        logger.info(
            "Loading %d signatures took %d",
            len(registered_sigs + new_sigs),
            time() - sig_start_time,
        )

        results = search_databases(
            query,
            ref_signatures,
            threshold=threshold,
            do_containment=False,
            best_only=False,
            ignore_abundance=False,
        )
        # cmd = "sourmash search {0} {1} -n {2} -k {4} -q --threshold 0.0001 -o {3}"
        # cmd = cmd.format(query, join(dest, '*.sig'), folder_size, join(sourmash_result, "tmp_result.txt"), k)

        # Organize results into a dataframe
        df_list = []
        index = []
        logger.info("Found %d matches to signature", len(results))
        for result in results:
            record = {}

            record["similarity"] = result.similarity
            record["name"] = result.name
            record["filename"] = result.filename
            record["md5"] = result.md5
            # should an int be appended?
            index.append(os.path.splitext(os.path.basename(result.filename))[0])
            df_list.append(record)
        logger.info("Done comparing signatures took %d", time() - sig_start_time)
        return DataFrame(
            df_list, index, columns=["similarity", "name", "filename", "md5"]
        )

    # TODO: Make this to work for m-vs-m sigs
    @classmethod
    def compare(cls, ref: list, query, workspace: str, **kwargs) -> np.ndarray:
        return cls.compare_sketch(ref, query, **kwargs)[
            ["filename", "similarity"]
        ].to_numpy()

    @classmethod
    def create(cls, workspace: str, genomeID: int, force=False, **kwargs):
        filepath = cls.filepath(workspace, genomeID)
        if not os.path.isfile(filepath) or force:
            tmp_fasta = util.get_fasta(workspace, genomeID)
            sketch = cls.create_sketch(tmp_fasta, filepath, **kwargs)
            return sketch
        else:
            return cls.load_signature(
                [filepath], ksize=kwargs.get("k", 31), select_moltype="DNA"
            )[0]

    @classmethod
    def filetype(cls):
        return "sig"

    @classmethod
    def load_signature(cls, filepath: list, ksize=21, mol_type="DNA", **kwargs):
        from sourmash import signature as sig

        sketches = []
        for file in filepath:
            try:
                sketches += [
                    list(
                        sig.load_signatures(file, ksize=ksize, select_moltype=mol_type)
                    )
                ]
            except Exception:
                logger.warning("Sourmash was unable to load signature: %s", file)
        return sketches
