# [LINflow](#linflow)

LINflow is a standalone Life Identification Number (LIN) assignment
workflow used to discover the genomic relatedness of bacteria.  

# [For the Impatient](#for-the-impatient)

```shell
# start conda mysql (users with conda mysql)
mysqld --datadir=$HOME/mysql/data &
# add genomes
python -m linflow genome test_workspace -i tests/data/fasta -d tests/data/metadata.csv -s 8
# create LIN summary
python -m linflow summary test_workspace -s 8 -e
```

# [Table of Contents](#table-of-contents)

[TOC]

# [2 Native Installation](#2-native-installation)

Here you can install LINflow and all its dependencies on your system.

## [2.1 Dependencies](#21-dependencies)

* Bash Shell
* Git
* MySQL 8+
* BLAST<sup>1</sup>
* MUMmer<sup>1</sup>
* Conda with Python 3.9
* PyANI
* Sourmash
* MySQL python connector
* ete3 and PyQt5 for drawing phylogenetic trees

**Important**: <sup>1</sup>PyANI requires BLAST and MUMmer executables on PATH

## [2.2 Conda with Python 3.9+](#22-conda-with-python-39)

To get access to the latest Sourmash release you need to install Conda.
A minimalist approach is to install Miniconda for python3 available
[here](https://docs.conda.io/en/latest/miniconda.html#linux-installers).
This will also install MUMmer and BLAST on the environment as well.
You can then create and activate a virtual environment and install the required modules:

```shell
conda create -n linflow python=3.9
conda activate linflow
```

```shell
conda install -c conda-forge numpy=1 pandas -y; \
conda install -c bioconda sourmash -y; \
conda install -c bioconda blast pyani mummer -y; \
conda install -c conda-forge ete3 alive-progress -y; \
conda install -c conda-forge pyqt -y; \
conda install -c conda-forge mysql-connector-python -y;
```

To ensure your MUMmer and BLAST setup was successful, the two commands `blastn -h` and
`mummer -h` should show you the usage options of BLAST and MUMmer without any
error messages.

Note: For MacOS BLAST might require firewall access upon execution.\
Note2: For RHEL you might need to install a glibc library using `sudo dnf install libnsl`
if you get the error below:

>blastn: error while loading shared libraries: libnsl.so.1 : cannot open shared object file

### [2.2.1 Adding src to PYTHONPATH](#221-adding-src-to-pythonpath)

You need Python to consider the contents of the "src" folder in your PYTHONPATH when executing modules.
You can do this by setting your python path as follows (Linux/Mac version).
**If you want this change to be permanent**, please consider adding this to your
`~/.bashrc` (Linux) or `~/.bash_profile` (Mac) file:

```shell
export PYTHONPATH="$PYTHONPATH:./src"
```

## [2.3 MySQL](#23-mysql)

This section explains how to install MySQL with two methods.
[With conda](#231-conda-mysql) in 2.3.1 which is easier to set up (recommended) and a
[native MySQL server](#232-local-mysql-alternative-to-conda) in 2.3.2
which is more suited towards always available services like websites or APIs.

### [2.3.1 Conda MySQL](#231-conda-mysql)

#### [2.3.1.1 Conda MySQL Installation](#2311-conda-mysql-installation)

```shell
conda activate linflow; \
conda install -c conda-forge mysql
```

#### [2.3.1.2 Initialize MySQL](#2312-initialize-mysql)

Run this command to initialize MySQL

```shell
# initialize MySQL with data directory and create temporary password
mysqld --initialize --datadir=$HOME/mysql/data
```

This command starts the MySQL server.

**IMPORTANT** the MySQL server has to be running when you are running LINflow commands.

```shell
# start MySQL server
mysqld --datadir=$HOME/mysql/data &
```

#### [2.3.1.3 Set MySQL Password](#2313-set-mysql-password)

```shell
# This will prompt you to enter a password. Enter the temporary password here!
mysql -u root -p
```

You will now be in the MySQL console where the terminal starts with `>mysql`.
Then use the following command to change the `root` password:

```sql
-- changing temporary root password
ALTER USER root IDENTIFIED BY 'securePassword';
```

### [2.3.2 Local MySQL (alternative to conda)](#232-local-mysql-alternative-to-conda)

Install MySQL on your system if you don't already have one.
Version 8+ is recommended since it is very stable and is widely
used in the community. However, we also support newer versions with
some tweaking. Please keep in mind you can only have one version installed
natively on your operating system at one time. Here are some detailed tutorials
on how to install MySQL on different machines:

* [Ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-20-04)
* [CentOS](https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-centos-7)
* [MacOS](https://dev.mysql.com/doc/refman/8.0/en/macos-installation-notes.html)
* [Windows](https://dev.mysql.com/doc/refman/8.0/en/windows-installation.html)

## [2.4 Clone the LINflow Repository](#24-clone-the-linflow-repository)

Use the command below to clone the LINflow repository if you haven't already.

```shell
git clone https://code.vt.edu/linbaseproject/LINflow.git linflow; \
cd linflow
```

## [2.5 MySQL Connection](#25-mysql-connection)

After installing and starting MySQL follow the steps below to configure the server
to work with LINflow.

### [2.5.1 Setting up a User](#251-setting-up-a-user)

Login to mysql and create a high privileged account using the instructions below.

```shell
mysql -u username -p 
```

Within the MySQL console where the terminal starts with `>mysql` create a new user
use your own username here instead of `linflow` and a secure password instead of `uniquePassword`.

```sql
-- Creates a user
CREATE USER 'linflow'@'localhost' IDENTIFIED BY 'uniquePassword';
-- Provides the user full access to all databases and tables
GRANT ALL PRIVILEGES ON *.* TO 'linflow'@'localhost';
FLUSH PRIVILEGES;
```

Note: In case you do not like the security implications of the "ALL PRIVILEGES" access
LINflow only requires database creation access and full access within its created databases.

### [2.5.2 Edit Credential File](#252-edit-credential-file)

Create a file in the LINflow directory `.my.cnf` with the syntax below.
You should be able to log into MySQL by typing `mysql --defaults-file=.my.cnf` without a username or
password after creating the file. Make sure you have permission to create
databases and tables when logging in using this method. Use the LINflow account if you made it
in the previous step otherwise (if you skipped the step) use your own account.

```apacheconf
[client]
user=yourusername
password=yourpassowrd
# only edit below if you know what you are doing
# socket=/var/run/mysqld/mysqld.sock
# host=host.name.com
# port=3306
```

By default, you can usually find your socket file at the locations below:

* `/var/run/mysqld/mysqld.sock` (Ubuntu, Fedora, Debian)
* `/var/lib/mysql/mysql.sock` (Rocky, CentOS, RHEL)
* `/tmp/mysql.sock` (MacOS)

**If you run into any trouble see the [Troubleshooting section](#4-troubleshooting)
 for fixes to the common errors.**

# [3 Start Using LINflow](#3-start-using-linflow)

If you run into trouble running the software, you can consult [Section 4](#4-troubleshooting),
Troubleshooting, for frequent questions and solutions or email us at <linbase@vt.edu>
You can use `--help` to get information about the main and all of the sub-commands.

```shell
python -m linflow --help
# sub-command help example
python -m linflow genome --help
```

## [3.1 Functions](#31-functions)

### [3.1.1 Add genomes](#311-add-genomes)

Add genomes to the database and get LINs assigned.
Please keep in mind that **workspace names must be unique** otherwise the genomes will be added
to an existing workspace.

```shell
python -m linflow genome test_workspace  `# workspace name` \
-i tests/data/fasta/  `# path to FASTA file directory` \
-d tests/data/metadata.csv  `# metadata file listing filenames` \
-s 8
```

* `-i` for the directory with the genome files going to be added to the database
* `-d` for the taxonomic information of the genomes
going to be added, that can including genus, species, and strain names, use [EXAMPLE METADATA FILE](tests/data/metadata.csv)
as the template and save it as a CSV file (only "genome file" and "strain" columns are required and both could be genome the filename)
* `-s` for indicating the SchemeID
by entering the integer corresponding to the scheme, see result from the `show_schemes` function.

**Note:** You can add files to the container using the Mountpoint of the `linflow_docker_data`
volume found using the command `docker inspect linflow_docker_data` on the host.

### [3.1.2 Initiate the database](#312-initiate-the-database)

You can an empty workspace for LINflow with a database and file structure.  

```shell
python -m linflow init test_workspace
```

This will create a MySQL database and folders to store genome files and signatures.

### [3.1.3 Workspace Summary](#313-workspace-summary)

Exports a summary of the computed LINs and their corresponding genomes.

```shell
python -m linflow summary test_workspace `# -s <schemeID> -e for extra information`
```

### [3.1.4 Build trees](#314-build-trees)

Creates two NJ and UPGMA trees each, one ladderized and one not. The trees will be saved
in the `workspace/trees` directory.

```shell
python -m linflow tree test_workspace \
-s 8  `# schemeID` \
-r 6,7,20 `# genome labels to include in the tree` 
```

### [3.1.5 Export similarity matrix](#315-export-similarity-matrix)

Exports a similarity matrix (tab separated) based on the selected SchemeID (denoted by `-s`).
The Genome labels (denoted by `-r`) that will be used in the matrix based on the numbers
1(Genome) 2(Species) 4(Strain). Two or more labels can be used together by adding their numbers.  
e.g. 6 (=2+4) will include the *species* and *strain* name but not the *genome* name.
 7 (=1+2+4) includes Taxon data for each label.

```shell
python -m linflow tree test_workspace \
-s 8 `# schemeID` \
-r 6,7,20 `# genome labels to include in the tree` \
--matrix-only
```

### [3.1.6 Clean Workspace](#316-clean-workspace)

Cleans files and folders in workspace that are not needed (but usually optimize the runtime).

```shell
python -m linflow clean test_workspace
```

### [3.1.7 Remove Workspace](#317-remove-workspace)

Deletes all files and folders in workspace and its corresponding MySQL database.
(**ALL DATA WILL BE ERASED**)

```shell
python -m linflow remove test_workspace2
```

### [3.1.8 Show all schemes](#318-show-all-schemes)

Show current schemes in the database, based on which LINs are assigned.

```shell
python -m linflow show_scheme test_workspace
```

You can use `--basic` or `--extend` to see more information about the schemes.
See `--help` for description of details.

### [3.1.9 Add a new scheme](#319-add-a-new-scheme)

Add a new scheme to the database.

```shell
python -m linflow add_simple_scheme test_workspace
python -m linflow add_complex_scheme test_workspace
```

This will ask users to add a string of numbers delimited by a comma(,)
and a descriptive sentence of this new scheme or if evenly spaced,
a MIN, MAX range with a step size could be provided as well.  
When adding manually, the added scheme should be a list of percentages
without the percent mark (%), for example, if you wish to add a scheme
of 70%, 80%, 90%, 95%, 99%, 99.5%, 99.9%, enter "70,80,90,95,99,99.5,99.9".
Use the `show_scheme` function to see examples.

# [4 Troubleshooting](#4-troubleshooting)

Here are some fixes for the most common errors that you might encounter

## [4.1 General fixes](#41-general-fixes)

This section provides steps to validate the correct installation of LINflow's dependencies.
It also tries to address the most common issues with the general solutions.

First navigate to the LINflow directory cloned from the repository and activate the
linflow environment in conda using `conda activate linflow`.

### [4.1.1 MySQL server and configuration](#411-mysql-server-and-configuration)

First make sure MySQL is installed and running. To access MySQL run the command below:

```properties
mysql --defaults-file=.my.cnf
```

In the file `.my.cnf` you can see a number of configurations defined for connecting to MySQL.
Lines starting with sharp "#" are commented out. Parameters such as
`socket, port, host` are optional if you are running the MySQL server locally.
However, if you are connecting to a remote MySQL server, or to a centralized cluster instance of MySQL
you might need all three. Please refer to your cluster support page or your server administrator
for the exact details.

You might come across errors similar to

```console
ERROR 2002 (HY000): Can't connect to local MySQL server through socket '/path/to/mysql.sock' (2)
ERROR 2005 (HY000): Unknown MySQL server host 'host.name.com' (1)
```

The first error refers to the socket file not being found. You can ensure the file exists
manually using `ls -l /path/to/mysqld.sock`. This file is created by the MySQL server when it
starts running, which includes machine information needed to connect to the server.
The second refers to the host not having a running MySQL server. This could also indicate the
server is running on a different port as well.

### [4.1.2 MUMmer and BLAST](#412-mummer-and-blast)

Next, ensure BLAST and MUMmer are correctly installed and accessible through command line.
Running the commands below will show the usage and help for using the two commands.
Each will indicate the correct installation and accessibility of their respective dependencies.
`blastn -h`

```stdout
USAGE
  blastn [-h] [-help] [-import_search_strategy filename]
    [-export_search_strategy filename] [-task task_name] [-db database_name]
    [-dbsize num_letters] [-gilist filename] [-seqidlist filename]
    [-negative_gilist filename] [-negative_seqidlist filename]
    [-taxids taxids] [-negative_taxids taxids] [-taxidlist filename]
    [-negative_taxidlist filename] [-entrez_query entrez_query]
    ...

DESCRIPTION
   Nucleotide-Nucleotide BLAST 2.9.0+

Use '-help' to print detailed descriptions of command line arguments
```

`mummer -h`

```stdout
Usage: mummer [options] <reference-file> <query-files>

Find and output (to stdout) the positions and length of all
sufficiently long maximal matches of a substring in
<query-file> and <reference-file>

Options:
-mum           compute maximal matches that are unique in both sequences
-mumcand       same as -mumreference
...
```

If you run into errors you can reinstall BLAST and MUMmer using conda. Note PyANI includes
BLAST and manages the removal and installation. Installing BLAST directly using
conda `conda install -c bioconda blast` could cause conflicts with PyANI. Hence, it is advised
to rely on the `pyani` package to manage BLAST.

```shell
conda remove pyani mummer
conda install -c bioconda pyani mummer
```

You can also install BLAST and/or MuMmer manually if you are running into issue with conda. The Sections
[4.1.2.1](#4121-blast-manual-installation)
through
[4.1.2.3](#4123-configuring-path-for-aligners)
have instructions on how to achieve this.

#### [4.1.2.1 BLAST manual installation](#4121-blast-manual-installation)

Download the latest version of BLAST. It can be found
at ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/.
Get the version compatible with your operating system.

* `ncbi-blast-{version}-x64-linux.tar.gz`       (Linux)
* `ncbi-blast-{version}-x64-macosx.tar.gz`      (MacOS)
* `ncbi-blast-{version}-win64.exe`              (Windows)

Afterwards extract (tar.gz) or install (exe, rpm) the file you just downloaded
and add the bin directory location to your operating system PATH. Follow the instructions
in [Section 4.1.2.3](#4123-configuring-path-for-aligners) for details on this process.

If you have problems with the prebuilt binaries you can also install BLAST
using your package manager (apt, yum, brew) or directly build BLAST from
the source code available in the same FTP link provided above (not recommended).

#### [4.1.2.2 MUMmer manual installation](#4122-mummer-manual-installation)

The latest version of MUMmer can be found at
<https://sourceforge.net/projects/mummer/files/>
and installed using the following commands:

```shell
tar xzf MUMmer{version}.tar.gz
cd MuMer{version}
./configure --prefix=./mummer/
make install
```

If you are installing at a system location (optional), you need to use ```sudo make install``` .
You can find MUMmer's full installation instructions at <https://github.com/mummer4/mummer/blob/master/INSTALL.md>.
Now you need to add MuMmmer to your operating system PATH. Follow the instructions
in [Section 4.1.2.3](#4123-configuring-path-for-aligners) for this process.

#### [4.1.2.3 Configuring PATH for aligners](#4123-configuring-path-for-aligners)

You need to add the BLAST and MUMmer binaries to your PATH using the steps below.
On Linux you can add the following line to your `~/.bashrc`
(`~/.bash_profile` in OSX) to make this change permanent,
or run it to make the two available within the current terminal (temporary and will not work on other terminals).
The bin directory can be found within the installation directories

```shell
export PATH=$PATH:/path/to/installation/BLAST/bin:/path/to/installation/MUMmer/bin
```

You can verify your PATH setup was successful by following the instructions
at the beginning of [Section 4.1.2](#412-mummer-and-blast).

### [4.1.3 PyANI and Sourmash](#413-pyani-and-sourmash)

Next, ensure that PyANI and Sourmash are installed correctly and are available on command line.
You can check this by running the commands below which will show you their usage information.

`average_nucleotide_identity.py --help`

```stdout
usage: average_nucleotide_identity.py [-h] [--version] -o OUTDIRNAME -i
                                      INDIRNAME [-v] [-f] [-s FRAGSIZE]
                                      [-l LOGFILE] [--skip_nucmer]
                                      ...
optional arguments:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
...

```

`sourmash --help`

```stdout
== This is sourmash version 3.5.0. ==
== Please cite Brown and Irber (2016), doi:10.21105/joss.00027. ==
Compute, compare, manipulate, and analyze MinHash sketches of DNA sequences.
Usage instructions:
    Basic operations
        sourmash compare --help   compare sequence signatures made by compute
        sourmash compute --help   compute sequence signatures for inputs
        ...
```

If you run into errors you can reinstall PyANI and sourmash using conda. Note PyANI includes
depends on BLAST and manages the removal and installation. Installing BLAST directly using
conda `conda install -c bioconda blast` could cause conflicts with PyANI. Hence, it is advised
to rely on the PyANI package to manage BLAST.

```shell
conda remove pyani mummer
conda install -c bioconda pyani mummer
```

<!--## [4.2 MySQL 2059 authentication plugin error](#42-mysql-authentication-plugin-error)
If you encounter an error similar to the error below:

```shell
MySQLdb._exceptions.OperationalError: (2059, "Authentication plugin 
'caching_sha2_password' cannot be loaded: 
dlopen(/.../.../lib/plugin/caching_sha2_password.so, 2): image not found")
```
This is created by the incompatibility between the new default MySQL password hashing 
plugin, and the Python MySQL connector which can be solved by following the section
"MySQL Connection -> Setting up a User" and creating a new privileged user with the old MySQL hashing plugin
for the authenticator. Remember after doing so, you also have to change the `.my.cnf` file as well
to use your new user.-->

## [4.2 BLAST run failed exception](#42-blast-run-failed-exception)

if you encounter an error similar to the error below:

```stdout
Traceback (most recent call last):
  File "LINflow.py", line 979, in <module>
    add_genome(filename, taxonomy, target_filename, scheme_id, db)
  File "LINflow.py", line 707, in add_genome
    this_ANIb_result = pyani_pairwise_adapter(subject_genome_file, filename)
  File "LINflow.py", line 843, in pyani_pairwise_adapter
    raise Exception("At least one BLAST run failed.")
Exception: At least one BLAST run failed.
```

First make sure that blast is installed properly by following the instructions in
[Section 4.1.2](#412-mummer-and-blast).
