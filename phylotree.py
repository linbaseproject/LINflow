#!/usr/bin/env python

import argparse
import json
import os
import sys

import numpy as np
from Bio.Phylo.TreeConstruction import DistanceMatrix, DistanceTreeConstructor
from sourmash import load_file_as_signatures

from linflow import lindb_connect, util

K_SIZE = 21
UPPER_LIMIT = 200
WORKSPACE = "genomerxiv_dev"  # Actual workspace directory
filetype = "sig"  # Actual file type if different


class TreeGenomeCountError(Exception):
    """Exception raised for errors when too many or too few genomes were provided to make a tree
    Attributes:
            genome_count (int): number of genomes provided
            message (str, optional): explanation of the error
    """

    def __init__(self, genome_count, message=""):
        self.genome_count = genome_count
        self.message = message
        super().__init__(self.message)

    def __str__(self):
        return f"Number of genomes provided were not between 2 and {UPPER_LIMIT} got: {self.genome_count} "


def load_signatures(filepath, k_mer=K_SIZE):
    return [
        sig for sig in load_file_as_signatures(filepath) if sig.minhash.ksize == k_mer
    ]


def compute_jaccard_matrix(signatures: list) -> np.ndarray:
    """
    Computes a Jaccard similarity matrix for a list of signatures

    Args:
        signatures (list): List of signatures

    Returns:
        np.ndarray: Symmetric matrix of Jaccard similarities
    """
    # Extract MinHash objects
    minhashes = [sig.minhash for sig in signatures]
    n = len(minhashes)
    matrix = np.zeros((n, n))

    # Compute pairwise Jaccard similarities
    for i in range(n):
        for j in range(i, n):
            similarity = minhashes[i].jaccard(minhashes[j])
            matrix[i, j] = similarity
            matrix[j, i] = similarity  # Symmetric matrix
    return matrix


def link_job_to_parent(job_id, parent_job_id, workspace=WORKSPACE):
    """
    Links a job to its parent job.

    Args:

        job_id (str): Job ID
        parent_job_id (str): Parent job ID
        workspace (str): Workspace name
    """
    if parent_job_id and job_id:
        try:
            conn, c = lindb_connect.connect_to_db(workspace, log_error=False)

            # Use parameterized query to fetch the Result
            select_query = "SELECT Result FROM Job WHERE Job_uuid=%s"
            c.execute(select_query, (parent_job_id,))
            result = c.fetchall()

            # Check if the job exists
            if not result:
                # No job found with Job_uuid
                # TODO: at least log this as a warning
                # the log would be captured by the job manager as a result of the tree creation process which I think is not what we want.
                return

            # Parse the parent job JSON string
            json_result = json.loads(result[0][0])

            # Add a tree job ID to the parent job JSON
            json_result["phylotree_job_id"] = job_id

            # Serialize back to JSON string and append newline if necessary
            updated_json_str = json.dumps(json_result) + "\n"

            # Use parameterized query to update the Result field
            update_query = "UPDATE Job SET Result=%s WHERE Job_uuid=%s"
            c.execute(update_query, (updated_json_str, parent_job_id))

            # Commit the transaction to save changes
            conn.commit()
            # Close the database connection
            lindb_connect.close(conn)
        except Exception as e:
            # TODO: log this exception as an exception and throw the exception -> save it as the result of the tree creation process
            # the log would be captured by the job manager as a result of the tree creation process which I think is not what we want.
            return
    # TODO: this should also raise an error saying jobID or parentJobID is missing
    # I dont have error handling here cause I think it is not necessary to proivde a parent job id if we use the tree creation process as a standalone process
    return


def build_tree(
    genomeIDs: list[int], job_id: str, parent_job_id: str, workspace=WORKSPACE
) -> str:
    """
    Builds a phylogenetic tree using the Neighbor-Joining algorithm based on similarity data

    Args:
        genomeIDs (list[int]): A list of genome IDs
        job_id (str): Job ID
        parent_job_id (str): Parent job ID
        workspace (str): Workspace name
    Returns:
        newick_str (str): Newick-formatted string of the phylogenetic tree
    """
    filepaths = []

    try:
        genome_count = len(genomeIDs)
        if genome_count > UPPER_LIMIT or genome_count < 2:
            raise TreeGenomeCountError(genome_count)
        for genomeID in genomeIDs:
            # Construct the file path for each genome ID
            filepath = util.get_filepath(
                workspace=workspace, genomeID=genomeID, filetype=filetype
            )
            if not os.path.isfile(filepath):
                raise FileNotFoundError("Similarity file not found: '%s'" % filepath)
            filepaths.append(filepath)

        signatures = []
        for filepath in filepaths:
            signatures.extend(load_signatures(filepath, K_SIZE))

        # Ensure each signature has a unique name
        sig_dict = {str(genomeID): sig for genomeID, sig in zip(genomeIDs, signatures)}

        similarity_matrix = compute_jaccard_matrix(sig_dict.values())

        # Get list of sample names
        samples = [*sig_dict]

        # Convert similarity to distance
        distance_matrix = 1 - similarity_matrix

        # Extract lower triangular matrix including an empty list for the first sample
        lower_tri = [distance_matrix[i, : i + 1].tolist() for i in range(len(samples))]

        # Verify that the number of lists in lower_tri matches the number of samples
        if len(lower_tri) != len(samples):
            raise ValueError(
                "Expected %d rows in lower_tri, but got %d."
                % (len(samples), len(lower_tri))
            )

        # Create Biopython DistanceMatrix
        biopython_dm = DistanceMatrix(names=samples, matrix=lower_tri)

        # Initialize the DistanceTreeConstructor
        constructor = DistanceTreeConstructor()

        # Construct the tree using Neighbor-Joining
        nj_tree = constructor.nj(biopython_dm)

        # Link the job to its parent job
        link_job_to_parent(job_id, parent_job_id, workspace)
        # Convert Tree to Newick String
        newick_str = nj_tree.format("newick").strip() or ""
        return json.dumps({"success": True, "data": newick_str})
    except TreeGenomeCountError as e:
        # when the signature count is not between 2 and 200
        link_job_to_parent(job_id, parent_job_id, workspace)
        return json.dumps({"success": False, "error": str(e)})
    except Exception as e:
        return json.dumps({"success": False, "error": str(e)})


def validate_genome_ids(value: str) -> int:
    try:
        return int(value)
    except ValueError:
        print(
            json.dumps(
                {
                    "success": False,
                    "error": f"Invalid genome ID: '{value}'. Must be an integer.",
                }
            )
        )
        sys.exit(1)


def parse_arguments(argv) -> dict:
    parser = argparse.ArgumentParser(
        description="Build a phylogenetic tree from genome IDs."
    )
    parser.add_argument("workspace", type=str, help="the name of the workspace")

    parser.add_argument(
        "-g",
        "--genome-ids",
        dest="genomeIDs",
        type=lambda x: [validate_genome_ids(i) for i in x.split()],
        help="List of genome IDs to include in the tree (space separated string of numbers)",
        required=True,
    )

    parser.add_argument(
        "-p",
        "-parent-job-id",
        dest="parentJobID",
        type=str,
        help="Parent job uuid",
        default=None,  # Optional argument with a default value
    )

    parser.add_argument(
        "-j",
        "--job-id",
        dest="uuid",
        type=str,
        help="the phylogenetic tree building request Job uuid",
        default=None,  # Optional argument with a default value
    )
    return parser.parse_args(argv).__dict__


if __name__ == "__main__":
    try:
        args = parse_arguments(sys.argv[1:])

        # Ensure genomeIDs is a list of integers
        genomeIDs = args.get("genomeIDs", [])

        # Ensure workspace is provided and valid
        workspace = args.get("workspace")
        if not workspace:
            raise ValueError("Workspace is required")

        parentJobID = args.get("parentJobID")
        thisJobID = args.get("uuid")

        # Build the tree
        newick_tree = build_tree(genomeIDs, thisJobID, parentJobID, workspace)
        print(newick_tree)
    except argparse.ArgumentTypeError as e:
        print(json.dumps({"success": False, "error": str(e)}))
        sys.exit(1)
    except ValueError as e:
        print(json.dumps({"success": False, "error": str(e)}))
        sys.exit(1)
    except Exception as e:
        print(json.dumps({"success": False, "error": str(e)}))
        sys.exit(1)
