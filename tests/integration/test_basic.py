import os
from logging import ERROR
from pathlib import Path

import numpy as np
import pandas
import pytest

import linflow.compression
import linflow.lindb
import linflow.lindb_connect
import linflow.linflow as lf
import linflow.logger
import linflow.util
from linflow import __main__ as lf_main

main_test = "pytest1"
DATA_PATH = os.path.join(Path(__file__).parents[1], "data")

"""
def setup_module(module):
	logger = linflow.logger.get_logger("__main__")
	logger.setLevel(ERROR)
"""


@pytest.yield_fixture
def db(scope="module"):
    database = linflow.lindb_connect.connect_to_db()
    # conn, c
    yield database[1]
    # lindb.close(db[0])


def create_workspace(name: str = main_test, init=True) -> linflow.lindb.LINdb:
    """creates the LINdb object, and also initializes the workspace if requested

    Args:
            name (str): name of the workspace. Defaults to None aka no database selected.
            create (bool, optional): whether to initialize the database if not existing. Defaults to True.

    Returns:
            LINdb: object for accessing the databse with the correct name
    """
    if not linflow.lindb_connect.database_exists(name) and init:
        lf.init(workspace=name)
    test1 = linflow.lindb.LINdb(database_name=name)
    return test1


def run_main(argv: list):
    logger = linflow.logger.get_logger(__file__)
    logger.setLevel(ERROR)
    lf_main.main([linflow] + argv)


def test_remove_empty(workspace="pytest_empty"):
    run_main(["remove", workspace])
    assert not os.path.isdir(linflow.util.workspace_dir(workspace))
    assert not linflow.lindb_connect.database_exists(workspace)


def test_init(workspace="pytest_init"):
    from linflow.util import SUB_DIRECTORIES

    if linflow.lindb_connect.database_exists(workspace):
        run_main(["remove", workspace])
    run_main(["init", workspace])
    assert os.path.isdir(linflow.util.workspace_dir(workspace))
    assert linflow.lindb_connect.database_exists(workspace)
    for groups in SUB_DIRECTORIES.values():
        for name in groups.keys():
            assert os.path.isdir(linflow.util.workspace_dir(workspace, name))
    run_main(["remove", workspace])


def test_remove_existing(workspace="pytest_remove"):
    test1 = create_workspace(workspace)
    run_main(["remove", workspace])
    assert not os.path.isdir(linflow.util.workspace_dir(workspace))
    assert not linflow.lindb_connect.database_exists(workspace)


@pytest.mark.parametrize("with_init", [True, False])
@pytest.mark.parametrize("schemeID", [1, 4])
def test_add_fasta(with_init: bool, schemeID: int, remove=True):
    workspace = f"pytest_genome_init{int(with_init)}_scheme{schemeID}"
    if linflow.lindb_connect.database_exists(workspace) and remove:
        run_main(["remove", workspace])
    create_workspace(workspace, init=with_init)
    input_filepath = os.path.join(DATA_PATH, "fasta")
    metadata_filepath = os.path.join(DATA_PATH, "metadata.csv")
    command_string = (
        f"genome {workspace} -i {input_filepath} "
        f"-d {metadata_filepath} -s {schemeID}"
    )
    run_main(command_string.split())
    assert os.path.isdir(linflow.util.workspace_dir(workspace))
    results_file = os.path.join(
        os.path.dirname(__file__), "data", f"lin_summary_scheme{schemeID}.csv"
    )
    # call after the database exists ensures database_name is assigned correctly
    db = create_workspace(workspace, init=with_init)
    lins = np.array(db.get_all_LINs(1))
    old_lins = np.genfromtxt(results_file, delimiter="\t", dtype=str)
    # assert np.array_equal(lins, old_lins)


@pytest.mark.parametrize("schemeID", [1, 4])
def test_matrix(schemeID: int, workspace="test0"):
    test1 = create_workspace(workspace)
    command_string = f"tree {workspace} -s {schemeID} -r 20 --matrix-only"
    run_main(command_string.split())
    new_matrix_file = os.path.join(
        linflow.util.workspace_dir(workspace), f"matrix_scheme{schemeID}.tsv"
    )
    assert os.path.isfile(new_matrix_file)
    old_matrix_file = os.path.join(
        os.path.dirname(__file__), "data", f"matrix_scheme{schemeID}.tsv"
    )
    new_matrix = pandas.read_csv(
        new_matrix_file, header=0, sep="\t", skip_blank_lines=True
    )
    old_matrix = pandas.read_csv(
        old_matrix_file, header=0, sep="\t", skip_blank_lines=True
    )
    assert new_matrix.equals(old_matrix)


def test_build_tree(schemeID=4, workspace="test0"):
    test1 = create_workspace(workspace)
    # from filecmp import cmp
    command_string = f"tree {workspace} -s {schemeID} -r 20"
    run_main(command_string.split())
    new_tree_dir = linflow.util.workspace_dir(workspace, subdir="trees")
    old_tree_dir = os.path.join(os.path.dirname(__file__), "data", "fasta_trees")

    merge_type = ["nj", "upgma"]
    option = ["", "ladderized"]
    out_type = ["nw", "pdf"]

    for mtype in merge_type:
        for opt in option:
            for out in out_type:
                if opt != "":
                    filename = f"{mtype}_{opt}.{out}"
                else:
                    filename = f"{mtype}.{out}"
                assert os.path.isfile(os.path.join(new_tree_dir, filename))

                if out == "nw":
                    assert linflow.compression.hash_file(
                        os.path.join(new_tree_dir, filename)
                    ) == linflow.compression.hash_file(
                        os.path.join(old_tree_dir, filename)
                    )


@pytest.mark.parametrize("workspace", ["test0"])
@pytest.mark.parametrize("scheme_id", [1, 4])
def test_verify(workspace, scheme_id):
    test1 = create_workspace(workspace)
    query = (
        "SELECT genomeID FROM LINTree WHERE schemeSetID = "
        f"{scheme_id} AND genomeID > 1;"
    )
    results = test1.execute(query)
    assert len(results) > 0
    # for record in results:
    # lin_id = record[0]
    # assert test1.verify_lin(lin_id) #TODO: Fix verify


@pytest.mark.parametrize("schemes", [[1, 2, 3, 4], [4, 3, 2, 1], [2, 4, 1, 3]])
def test_add_fasta_multi_scheme(schemes: list, remove_after=True):
    workspace = f"pytest_genome_scheme_multi_{''.join([str(e) for e in schemes])}"
    lf.remove(workspace=workspace)
    lf.init(workspace=workspace)
    for schemeID in schemes:
        data_path = os.path.join(Path(__file__).parents[1], "data")
        input_filepath = os.path.join(data_path, "fasta")
        metadata_filepath = os.path.join(data_path, "metadata.csv")
        command_string = (
            f"genome {workspace} -i {input_filepath} "
            f"-d {metadata_filepath} -s {schemeID}"
        )
        run_main(command_string.split())
        assert os.path.isdir(linflow.util.workspace_dir(workspace))
        db = linflow.lindb.LINdb(workspace)
        result = db.execute(
            "SELECT COUNT(*) FROM LINTree WHERE " f"schemeSetID={schemeID}"
        )
        assert int(result[0][0]) > 0
    # assert np.array_equal(lins, old_lins)
    if remove_after:
        lf.remove(workspace=workspace)


# test_add_fasta_multi_scheme([1, 2, 3, 4])
