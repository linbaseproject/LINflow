#!/usr/bin/env python3
"""
This test module was designed to validate the existing genome signatures
in a workspace based on the current Sourmash version
"""
import os
import shutil
import sys
import traceback
from glob import glob
from multiprocessing.pool import ThreadPool

import pytest

import linflow.extensions
import linflow.sourmash_custom4
import linflow.util

TEMP_SIG_DIR = "tmp_sig"
DEFAULT_WORKSPACE = os.path.realpath("./workspaces/test8")
THREAD_COUNT = 24


@pytest.mark.parametrize("workspace_path", [DEFAULT_WORKSPACE])
def test_sigs(workspace_path: str):
    paths = os.path.join(workspace_path, "fasta/*/*.gz")
    workspace = os.path.basename(workspace_path)
    files = glob(paths)
    sourmash_filter = linflow.sourmash_custom4.SourmashSigFilter()

    if os.path.isdir(TEMP_SIG_DIR):
        shutil.rmtree(TEMP_SIG_DIR)
    file_count = len(files)
    ten_precent = file_count // 100

    os.makedirs(TEMP_SIG_DIR, exist_ok=False)
    POOL: ThreadPool = ThreadPool(THREAD_COUNT)
    outputs = []

    for idx, file in enumerate(files):
        outputs += [
            POOL.apply_async(
                validate_sig,
                kwds={
                    "file": file,
                    "workspace": workspace,
                    "filter_obj": sourmash_filter,
                },
            )
        ]
    POOL.close()
    POOL.join()

    """if idx % batch == 0:
        print("'%s'\t/'%s'" % (idx,file_count))"""


def validate_sig(file: str, workspace: str, filter_obj):
    try:
        gid = int(linflow.extensions.remove_extension(file))
    except ValueError:
        print("file '%s' was skipped" % file)
        return False
    new_sig_path = os.path.join(os.getcwd(), TEMP_SIG_DIR, f"{gid}.sig")
    old_sig_path = filter_obj.filepath(workspace, genomeID=gid, create=False)

    try:
        signatures = filter_obj.create_sketch(file, new_sig_path)
        old_sig = filter_obj.load_signatures([old_sig_path])[0]
        new_sig = filter_obj.load_signatures([new_sig_path])[0]

        # old_sig.md5sum() == new_sig.md5sum()
        if old_sig.md5sum() == new_sig.md5sum():
            os.remove(new_sig_path)
            return True
        else:
            print("signature different '%s'" % old_sig_path)
        return False
    except Exception as e:
        print(traceback.print_exc())
    return False


if __name__ == "__main__":
    workspace_path = os.path.realpath(sys.argv[1])
    test_sigs(workspace_path)

# test_sigs(DEFAULT_WORKSPACE)
