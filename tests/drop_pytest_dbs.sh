#!/bin/bash

# Define the pattern for database names you want to drop
PATTERN="pytest_%"

# Get the list of databases matching the pattern
DATABASES=$(mysql -e "SHOW DATABASES LIKE '$PATTERN';" | grep -v Database)

# Loop through the list and drop each database
for DB in $DATABASES; do
    mysql -e "DROP DATABASE IF EXISTS $DB;"
done