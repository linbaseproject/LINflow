import os.path

import numpy as np
import pytest

import linflow.linflow as lf
import linflow.util
from linflow.genome import Genome
from linflow.lindb import LINdb

SAMPLE_GENOMES = [
    "tests/data/fasta/NC_003197.2.fasta",
    "tests/data/fasta/NC_003197.2.fasta.gz",
    "tests/data/fasta/NC_003197.2.fasta.tar",
    "tests/data/fasta/NC_003197.2.fasta.tar.gz",
]
for idx, genome1 in enumerate(SAMPLE_GENOMES):
    SAMPLE_GENOMES[idx] = os.path.realpath(genome1)
SAMPLE_GENOME = SAMPLE_GENOMES[0]


def test_genome_init():
    workspace = linflow.util.get_pytest_workspace()
    db = LINdb(workspace, create=True)
    db.remove()
    genome = Genome(db, filepath=SAMPLE_GENOME)
    assert genome.filepath == os.path.realpath(SAMPLE_GENOME)
    assert genome.filename == os.path.basename(SAMPLE_GENOME)
    db.remove()


@pytest.mark.parametrize("sample_genome", SAMPLE_GENOMES)
def test_genome_register(sample_genome):
    workspace = linflow.util.get_pytest_workspace(
        extra=[str(SAMPLE_GENOMES.index(sample_genome))]
    )
    lf.remove(workspace=workspace)
    lf.init(workspace=workspace)
    db = LINdb(workspace, create=True)
    genome = Genome(db, filepath=sample_genome)
    linbase_filepath, extracted = genome.process_file()
    genome2 = Genome(db, genome_id=genome.id)
    assert genome.id == 2
    assert genome2.id == 2
    assert genome == genome2
    assert os.path.isfile(linbase_filepath)
    db.remove()


def test_genome_properties():
    workspace = linflow.util.get_pytest_workspace()
    db = LINdb(workspace, create=True)
    db.remove()
    db.initiate()
    genome = Genome(db, filepath=SAMPLE_GENOME)
    assert isinstance(genome.hash, str)
    assert genome.sequence_length > 0
    assert np.all(genome.scaffold_sizes > 500)
    db.remove()


def test_genome_remove():
    workspace = linflow.util.get_pytest_workspace()
    db = LINdb(workspace, create=True)
    db.remove()
    db.initiate()
    genome = Genome(db, filepath=SAMPLE_GENOME)
    genome.process_file()
    genome.remove()
    try:
        genome2 = Genome(db, genome_id=genome.id)
    except FileNotFoundError:
        db.remove()
        assert True
        return
    db.remove()
    assert False
