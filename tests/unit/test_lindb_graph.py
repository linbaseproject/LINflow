import copy
from typing import Optional

import numpy as np
import pytest

import linflow.lindb
import linflow.linflow as lf
import linflow.util

SAMPLE_GRAPH = [
    # (ID,idx,val,parent)
    (2, 0, 1, 1),
    (3, 0, 2, 1),
    (4, 1, 2, 2),
    (5, 1, 2, 3),
    (6, 3, 1, 5),
    (7, 0, 3, 1),
    (8, 2, 3, 7),
    (9, 3, 1, 7),
    (10, 1, 1, 3),
    (11, 3, 1, 4),
    (12, 2, 2, 10),
    (13, 3, 1, 12),
    (14, 2, 3, 10),
    (15, 3, 3, 14),
    (16, 1, 3, 3),
    (17, 2, 1, 16),
    (18, 2, 2, 16),
    (19, 2, 3, 16),
    (20, 3, 1, 18),
    (21, 3, 3, 19),
    (22, 3, 1, 5),
]

SAMPLE_GRAPH_DLIN = {
    # ID:dlin
    2: {0: 1},
    3: {0: 2},
    4: {0: 1, 1: 2},
    5: {0: 2, 1: 2},
    6: {0: 2, 1: 2, 3: 1},
    7: {0: 3},
    8: {0: 3, 2: 3},
    9: {0: 3, 3: 1},
    10: {0: 2, 1: 1},
    11: {0: 1, 1: 2, 3: 1},
    12: {0: 2, 1: 1, 2: 2},
    13: {0: 2, 1: 1, 2: 2, 3: 1},
    14: {0: 2, 1: 1, 2: 3},
    15: {0: 2, 1: 1, 2: 3, 3: 3},
    16: {0: 2, 1: 3},
    17: {0: 2, 1: 3, 2: 1},
    18: {0: 2, 1: 3, 2: 2},
    19: {0: 2, 1: 3, 2: 3},
    20: {0: 2, 1: 3, 2: 2, 3: 1},
    21: {0: 2, 1: 3, 2: 3, 3: 3},
    22: {0: 2, 1: 2, 3: 1},
}
SAMPLE_PREFIX = [
    (23, 5, 0, 8),
    (24, 5, 0, 9),
    (25, 5, 0, 5),
    (26, 2, 0, 10),
    (27, 10, 0, 6),
    (28, 2, 0, 2),
    (29, 1, 0, 2),
]

SAMPLE_PREFIX_DLIN = {
    23: {0: 3, 2: 3, 5: 0},
    24: {0: 3, 3: 1, 5: 0},
    25: {0: 2, 1: 2, 5: 0},
    26: {0: 2, 1: 1, 2: 0},
    27: {0: 2, 1: 2, 3: 1, 10: 0},
    28: {0: 1, 2: 0},
    29: {0: 1, 1: 0},
}


def setup_sample_graph(
    db: linflow.lindb.LINdb, table_name: str, id_column_name: str, prefix=False
):
    result = db.execute(f"SELECT MAX({id_column_name}) FROM {table_name}")
    offset = 0 if result[0][0] is None else int(result[0][0])
    values = []
    if prefix:
        node_graph = SAMPLE_GRAPH + SAMPLE_PREFIX
    else:
        node_graph = copy.deepcopy(SAMPLE_GRAPH)
        for i in range(len(node_graph)):
            db.add_genome(f"{i}.fasta", scaffolds=0, genome_length=0, hash=f"{i}")
    # sets id,idx,lastValue,parent
    for arr_idx, (nodeID, node_idx, value, parent) in enumerate(node_graph):
        # excludes nodes connected to the root from offsetting
        parent2 = parent if parent == 1 else parent + offset
        values += [f"({nodeID+offset},{node_idx},{value},{parent2})"]
    query = f"""
	INSERT INTO {table_name} ({id_column_name}, idx, lastValue, parent) 
		VALUES {",".join(values)};
	"""
    db.execute(query)
    return offset


def get_children(
    nodeID, prefix=False, descend_lvl: Optional[int] = None, relative=True
):
    descend_all = descend_lvl is None
    GRAPH = copy.deepcopy(SAMPLE_GRAPH)
    DLIN_GRAPH = copy.deepcopy(SAMPLE_GRAPH_DLIN)
    if prefix:
        GRAPH += SAMPLE_PREFIX
        DLIN_GRAPH = {**DLIN_GRAPH, **SAMPLE_PREFIX_DLIN}
    nodeIDs = {}
    newNodeIDs = {nodeID}
    if relative and not descend_all:
        descend_lvl += max(DLIN_GRAPH[nodeID].keys())
    # descend_lvl >= level and
    while len(nodeIDs) != len(newNodeIDs):
        nodeIDs = newNodeIDs
        for iid, idx, val, parent in GRAPH:
            if parent in nodeIDs and (descend_all or idx <= descend_lvl):
                newNodeIDs.add(iid)
    return list(newNodeIDs)


def test_graph_init():
    workspace = linflow.util.get_pytest_workspace()
    db = linflow.lindb.LINdb(workspace, create=True)
    db.remove()


@pytest.mark.parametrize("idx", [0])
@pytest.mark.parametrize("lastValue", [1])
@pytest.mark.parametrize("parent", [1, None])
@pytest.mark.parametrize("schemeSetID", [1, 2, 3, 4])
def test_graph_lingroup_add(lastValue, parent, schemeSetID, idx):
    workspace = linflow.util.get_pytest_workspace(
        extra=[idx, lastValue, parent, schemeSetID]
    )
    lf.remove(workspace=workspace)
    lf.init(workspace=workspace)
    db = linflow.lindb.LINdb(workspace)
    prefixID = db.update_prefix(idx, lastValue, parent, schemeSetID)
    prefix_df = db.get_prefix(prefixID, schemeSetID)
    if prefix_df.empty:
        raise BaseException("prefix not found with ID '%s'" % prefixID)
    prefix_dict: dict[str, int] = prefix_df.iloc[-1].to_dict()  # type: ignore
    assert prefix_dict["idx"] == idx
    assert prefix_dict["lastValue"] == lastValue
    assert prefix_dict["parent"] == parent
    lf.remove(workspace=workspace)


@pytest.mark.parametrize(
    "table_name,id_column_name",
    [
        # ("LINTree", "genomeID"),
        ("Prefix", "prefixID")
    ],
)
def test_sample_graph_setup(table_name, id_column_name):
    workspace = linflow.util.get_pytest_workspace(extra=[table_name, id_column_name])
    lf.remove(workspace=workspace)
    lf.init(workspace=workspace)
    db = linflow.lindb.LINdb(workspace)
    offset = setup_sample_graph(db, table_name, id_column_name)
    query = f"SELECT MAX({id_column_name}) FROM {table_name}"
    result = db.execute(query)
    assert int(result[0][0]) == offset + SAMPLE_GRAPH[-1][0]
    lf.remove(workspace=workspace)


@pytest.mark.parametrize(
    "table_name,id_column_name,prefix",
    [
        ("LINTree", "genomeID", False),
        ("Prefix", "prefixID", False),
        ("Prefix", "prefixID", True),
    ],
)
def test_tree_parent(table_name, id_column_name, prefix):
    workspace = linflow.util.get_pytest_workspace(extra=[table_name, id_column_name])
    lf.remove(workspace=workspace)
    lf.init(workspace=workspace)
    db = linflow.lindb.LINdb(workspace)
    offset = setup_sample_graph(db, table_name, id_column_name, prefix)
    for iid, true_dlin in SAMPLE_GRAPH_DLIN.items():
        nodes = db.get_tree_parent(
            table_name=table_name, id_column_name=id_column_name, nodeID=iid + offset
        )
        dlin = linflow.util.nodes2dlin(nodes, prefix=True)
        assert dlin == true_dlin
    lf.remove(workspace=workspace)


@pytest.mark.parametrize(
    "table_name,id_column_name,prefix,descend_idx",
    [
        ("LINTree", "genomeID", False, None),
        ("LINTree", "genomeID", False, 1),
        ("LINTree", "genomeID", False, 2),
        ("LINTree", "genomeID", False, 3),
        ("Prefix", "prefixID", False, None),
        ("Prefix", "prefixID", True, None),
        ("Prefix", "prefixID", False, 1),
        ("Prefix", "prefixID", True, 1),
        ("Prefix", "prefixID", False, 2),
        ("Prefix", "prefixID", True, 2),
        ("Prefix", "prefixID", False, 3),
        ("Prefix", "prefixID", True, 3),
    ],
)
def test_tree_children(table_name, id_column_name, prefix, descend_idx):
    GRAPH = copy.deepcopy(SAMPLE_GRAPH) if not prefix else SAMPLE_GRAPH + SAMPLE_PREFIX
    workspace = linflow.util.get_pytest_workspace(
        extra=[table_name, id_column_name, prefix, descend_idx]
    )
    lf.remove(workspace=workspace)
    lf.init(workspace=workspace)
    db = linflow.lindb.LINdb(workspace)
    offset = setup_sample_graph(db, table_name, id_column_name, prefix)
    for iid, idx, val, parent in GRAPH:
        children = db.get_tree_child(
            table_name=table_name,
            id_column_name=id_column_name,
            start_nodeID=iid + offset,
            descend_idx=descend_idx,
        )
        sorted_graph_children = np.unique(children[id_column_name])
        sorted_real_children = (
            np.unique(get_children(iid, prefix=prefix, descend_lvl=descend_idx))
            + offset
        )
        assert sorted_graph_children.tolist() == sorted_real_children.tolist()
    lf.remove(workspace=workspace)


@pytest.mark.parametrize(
    "table_name,id_column_name,prefix",
    [
        ("LINTree", "genomeID", False),
        ("Prefix", "prefixID", True),
    ],
)
def test_tree_search(table_name, id_column_name, prefix):
    DLINS = (
        copy.deepcopy(SAMPLE_GRAPH_DLIN)
        if not prefix
        else {**SAMPLE_GRAPH_DLIN, **SAMPLE_PREFIX_DLIN}
    )
    workspace = linflow.util.get_pytest_workspace(
        extra=[table_name, id_column_name, prefix]
    )
    lf.remove(workspace=workspace)
    lf.init(workspace=workspace)
    db = linflow.lindb.LINdb(workspace)
    offset = setup_sample_graph(db, table_name, id_column_name, prefix)
    for iid, dlin in DLINS.items():
        parent_graph = db.search_graph(
            dlin, ref_table=linflow.lindb.RecursiveTable.Genome
        )
        assert linflow.util.nodes2dlin(parent_graph, prefix=prefix) == dlin
    lf.remove(workspace=workspace)


@pytest.mark.parametrize("dlin", [{0: 1}, {0: 1, 2: 0}])
@pytest.mark.parametrize("schemeSetID", [8])
def test_prefix_search(dlin: dict[int, int], schemeSetID: int):
    db = linflow.lindb.LINdb("genomerxiv_dev")
    result = db.search_prefix(dlin, schemeSetID, exact=True)
    assert len(result) == len(dlin)


@pytest.mark.parametrize(
    "dlin",
    [
        {0: 76, 1: 6, 2: 2},
        {0: 76, 1: 6, 2: 2, 5: 2, 10: 0},
        {0: 76, 1: 6, 2: 2, 5: 2, 11: 1},
    ],
)
@pytest.mark.parametrize("schemeSetID", [7])
def test_prefix_add(dlin: dict[int, int], schemeSetID: int):
    db = linflow.lindb.LINdb("genomerxiv_main")
    result = db.add_prefix(dlin, schemeSetID)
    test_prefix_search(dlin, schemeSetID)


# test_prefix_search({0: 1, 3:8})
# test_prefix_add({0:76,1:6,2:2,5:2,10:0}, 7)
