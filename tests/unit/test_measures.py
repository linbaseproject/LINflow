import pytest

measure_location = "linflow"
measure_modules = ["sourmash_custom4", "ani_custom"]
measure_classes = ["Sourmash", "PyANI"]


def get_class(measure_file):
    classname = measure_classes[measure_modules.index(measure_file)]
    return getattr(
        __import__(".".join([measure_location, measure_file]), fromlist=[classname]),
        classname,
    )


@pytest.mark.parametrize("measure_file", measure_modules)
def test_measure_filetype(measure_file):
    class_ref = get_class(measure_file)
    assert isinstance(class_ref.filetype(), str)


@pytest.mark.parametrize("measure_file", measure_modules)
def test_measure_create(measure_file):
    class_ref = get_class(measure_file)


@pytest.mark.parametrize("measure_file", measure_modules)
def test_measure_compare(measure_file):
    class_ref = get_class(measure_file)
