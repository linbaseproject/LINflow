bash
#!/bin/bash

# =============================
# Script Name: phylotree_perf.sh
# Description: Measures the execution time of pylotree.py with varying numbers of genome IDs.
# Usage: ./phylotree_perf.sh
# Output: time_results.csv
# =============================

# -----------------------------
# Configuration Parameters
# -----------------------------
# Get the directory of the script
SCRIPT_DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")
# Path to the Python script
PYLOTREE_SCRIPT=$(abspath "$SCRIPT_DIR/../../pylotree.py")

# Output CSV file to store the results
OUTPUT_FILE="time_results.csv"

# Test case definitions: Maximum genome IDs for each test
#TEST_CASES=(101 301 501 701 901 1001 1501 2001)
TEST_CASES=(101 201)
# Number of runs per test case to average execution time
RUNS_PER_TEST=3

# -----------------------------
# Initialize the Output File
# -----------------------------

# Write CSV headers
echo "Max_GenomeID,Run,Execution_Time_sec" > "$OUTPUT_FILE"

# -----------------------------
# Function to Run a Single Test Case
# -----------------------------

run_test_case() {
    local max_id=$1
    local run_number=$2

    # Generate genome IDs from 2 to max_id
    genome_ids=$(seq 2 "$max_id" | tr '\n' ' ' | sed 's/ $//')  # e.g., "2 3 4 ... 101"

    echo "Running Test Case: Genome IDs from 2 to $max_id (Run $run_number/$RUNS_PER_TEST)"

    # Measure execution time using the 'time' command
    # Redirect stdout to /dev/null to suppress output
    # Capture stderr where 'time' outputs the timing information
    { /usr/bin/time -f "%e" python "$PYLOTREE_SCRIPT" -g $genome_ids > /dev/null; } 2>temp_time.txt

    # Extract the real time from the time command output
    real_time=$(cat temp_time.txt)

    # Append the result to the CSV file
    echo "$max_id,$run_number,$real_time" >> "$OUTPUT_FILE"

    # Clean up temporary file
    rm temp_time.txt
}

# -----------------------------
# Main Execution Loop
# -----------------------------

for max_id in "${TEST_CASES[@]}"; do
    for (( run=1; run<=RUNS_PER_TEST; run++ )); do
        run_test_case "$max_id" "$run"
    done
done

echo "All tests completed. Results saved to $OUTPUT_FILE."
